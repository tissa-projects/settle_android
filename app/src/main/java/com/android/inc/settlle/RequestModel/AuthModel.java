package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class AuthModel implements Serializable {

    public String auth_token;

    public AuthModel(String auth_token) {
        this.auth_token = auth_token;
    }
}
