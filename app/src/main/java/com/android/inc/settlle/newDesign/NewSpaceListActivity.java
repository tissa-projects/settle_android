package com.android.inc.settlle.newDesign;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.GuestSpaceActivity;
import com.android.inc.settlle.Activities.HomeActivity;
import com.android.inc.settlle.Activities.HomeGuestActivity;
import com.android.inc.settlle.Activities.LoginActivity;
import com.android.inc.settlle.Activities.SpaceDetailAndBookActivity;
import com.android.inc.settlle.Adapter.GuestSpaceAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AddSpaceToFavModel;
import com.android.inc.settlle.RequestModel.DeleteFavSpaceModel;
import com.android.inc.settlle.RequestModel.GuestSpaceActivityModel;
import com.android.inc.settlle.RequestModel.SpaceSearchModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewSpaceListActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtGuest, txtDate, txtVenue, txtEvent;
    private Button btnSearch;
    private ShimmerFrameLayout shimmerFrameLayout;
    private RecyclerView recyclerView;
    private Context context;
    private ImageView imgNoDataFound;
    private GuestSpaceAdapter guestSpaceAdapter;
    private String strEventId = "", strDate = "", strguestId = "", strVenueId = "";
    Dialog loadingDialog;
    private JSONArray dataArray = null;
    private boolean is_search_btn_click = false;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private final String[] guestLimitArray = {"0-100", "100-200", "200-300", "300-400", "400-500", "500 & UP"};
    private final String[] guestIdsArray = {"1", "2", "3", "4", "5", "6"};
    private String[] venueId, venueName, eventIds, eventNames;
    private String eventId, location, eventName;

    ProgressBar progressBar;
    int page = 1;
    private boolean isPageAvailable;
    //variables for pagination
    private boolean isLoading = true;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private final int view_threshold = 3;


    private static final String TAG = GuestSpaceActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_guest_space);
        setContentView(R.layout.activity_new_space_list);
        context = NewSpaceListActivity.this;

        getIntentData();
        initialize();
        initRecycler();
        if (VU.isConnectingToInternet(context)) {
            page = 1;
            SpaceData();
        }
    }


    private void getIntentData() {
        ArrayList<String> venueIdList, venueNameList, eventNameList, eventIdList;
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            venueIdList = new ArrayList<>();
            venueNameList = new ArrayList<>();
            eventId = bundle.getString("eventId");
            location = bundle.getString("loaction");
            eventName = bundle.getString("eventName");
            eventNameList = bundle.getStringArrayList("eventNames");
            eventIdList = bundle.getStringArrayList("eventIds");

            HashMap<String, String> venueIdNames = (HashMap<String, String>) getIntent().getSerializableExtra("venueIdNamesMap");


            for (String key : venueIdNames.keySet()) {
                String name = venueIdNames.get(key);
                venueIdList.add(key);
                venueNameList.add(name);
            }
            venueId = CommonFunctions.toStringArray(venueIdList);
            venueName = CommonFunctions.toStringArray(venueNameList);
            eventIds = CommonFunctions.toStringArray(eventIdList);
            eventNames = CommonFunctions.toStringArray(eventNameList);

        }
    }

    private void initialize() {

        txtGuest = findViewById(R.id.txt_guest);
        txtEvent = findViewById(R.id.txt_event);
        txtVenue = findViewById(R.id.txt_venue);
        txtDate = findViewById(R.id.txt_date);
        TextView txtClearFilters = findViewById(R.id.txt_clear_all_filter);

        if (eventName != null && eventName.length() > 1)
            txtEvent.setText(eventName);

        btnSearch = findViewById(R.id.guest_space_search);
        imgNoDataFound = findViewById(R.id.img_nodatafound);
        progressBar = findViewById(R.id.progress_bar);

        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);
        shimmerFrameLayout.startShimmerAnimation();
        recyclerView = findViewById(R.id.recycler_guest_space);



        btnSearch.setOnClickListener(this);
        txtClearFilters.setOnClickListener(this);
        txtEvent.setOnClickListener(this);
        txtVenue.setOnClickListener(this);

        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        findViewById(R.id.back_space).setOnClickListener(v -> onBackPressed());
    }


    @SuppressLint({"NonConstantResourceId", "SetTextI18n"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay1:  //select guest
                showBottomSheetDialog(guestLimitArray, "Guest");
                break;
            case R.id.txt_event:  //select EVENT
                showBottomSheetDialog(eventNames, "Event");
                break;
            case R.id.txt_venue:  //select venueId
                showBottomSheetDialog(venueName, "venue");
                break;
            case R.id.lay4:  //select date
                dialogDatePickerLight();
                break;
            case R.id.guest_space_search:  //search button click
                if (VU.isConnectingToInternet(context)) {
                    page = 1;
                    is_search_btn_click = true;
                    SpaceSearch();
                }
                break;
            case R.id.txt_clear_all_filter:
                eventId = "";
                eventName = "";
                strDate = "";
                strEventId = "";
                strguestId = "";
                strVenueId = "";
                txtGuest.setText("Select guest count");
                txtEvent.setText("Select event");
                txtVenue.setText("Select venue");
                txtDate.setText("Select date");
                page = 1;
                is_search_btn_click = true;
                guestSpaceAdapter.cleardata();
                guestSpaceAdapter.setDataArray(new JSONArray());
                SpaceData();
                break;
        }
    }

    private void initRecycler() {
        @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        guestSpaceAdapter = new GuestSpaceAdapter(context);
        recyclerView.setAdapter(guestSpaceAdapter);

        guestSpaceAdapter.setOnItemClickListener((View view, int position, JSONArray jsonArray) -> {
            String uni;
            try {
                uni = jsonArray.getJSONObject(position).getString("uni");
                Log.e(TAG, "onItemClick: uni: " + uni + " position :" + position);
                Intent intent = new Intent(context, SpaceDetailAndBookActivity.class);
                intent.putExtra("eventID", eventId);
                intent.putExtra("eventName", eventName);
                intent.putExtra("uni", uni);
                startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        guestSpaceAdapter.setOnFavBtnClickListener((View view, int position, JSONArray jsonArray) -> {

            if (jsonArray != null) {
                try {
                    if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin)) {
                        if (VU.isConnectingToInternet(context)) {
                            boolean isFav = jsonArray.getJSONObject(position).getBoolean("is_fav");
                            if (!isFav) {
                                addSpaceFav(jsonArray.getJSONObject(position).getString("space_id"), view, jsonArray, position);
                            } else {
                                deleteFavSpace(jsonArray.getJSONObject(position).getString("space_id"), view, jsonArray, position);
                            }
                        }
                    } else {
                        sharedPreference();
                        Intent intent = new Intent(context, LoginActivity.class);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVariableItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > privious_total) {
                            isLoading = false;
                            privious_total = totalItemCount;
                        }
                    }

                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                        if (isPageAvailable) {
                            page++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                SpaceData();
                        }/* else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }*/
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void sharedPreference() {
        Utilities.setSPboolean(context, Utilities.spAtGuestServiceActivity, false);
        Utilities.setSPboolean(context, Utilities.spAtGuestSpaceActivity, true);
        Utilities.setSPboolean(context, Utilities.spAtBookingSpaceActivity, false);
        Utilities.setSPboolean(context, Utilities.spAtBookingServiceActivity, false);
    }

    private void addSpaceFav(String spaceId, final View view, final JSONArray dataArray, final int position) {
        // loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().addFaveSpace(new AddSpaceToFavModel(Utilities.getSPstringValue(context, Utilities.spUserId), spaceId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e("api_response", "api_response: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);

                    if (jsonObject.getInt("status_code") == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (jsonObject.getInt("status_code") == 200) {
                        ImageView imageView = (ImageView) view;
                        imageView.setImageResource(R.drawable.ic_red_heart);
                        dataArray.getJSONObject(position).put("is_fav", true);
                        guestSpaceAdapter.cleardata();
                        guestSpaceAdapter.setDataArray(dataArray);
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
            }
        });
    }

    private void deleteFavSpace(String spaceId, final View view, final JSONArray dataArray, final int position) {
        // loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteFaveSpace(new DeleteFavSpaceModel("", Utilities.getSPstringValue(context, Utilities.spUserId), spaceId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
//                if ((loadingDialog != null) && loadingDialog.isShowing()) {
//                    loadingDialog.dismiss();
//                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e("api_response", "api_response: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);

                    if (jsonObject.getInt("status_code") == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (jsonObject.getInt("status_code") == 200) {
                        ImageView imageView = (ImageView) view;
                        imageView.setImageResource(R.drawable.ic_white_heart);
                        dataArray.getJSONObject(position).put("is_fav", false);
                        guestSpaceAdapter.cleardata();
                        guestSpaceAdapter.setDataArray(dataArray);
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        });
    }

    //Bottom shhet code
    @SuppressLint("SetTextI18n")
    private void showBottomSheetDialog(final String[] filterArray, final String type) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_list, null);

        ListView bottom_listview = view.findViewById(R.id.bottom_listview);
        TextView txtheading = view.findViewById(R.id.bottom_heading);
        txtheading.setText("Select " + type);

        List<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        list.addAll(Arrays.asList(filterArray));

        bottom_listview.setAdapter(adapter);
        bottom_listview.setOnItemClickListener((parent, view1, position, id) -> {
            if (type.equalsIgnoreCase("Guest")) {
                txtGuest.setText(filterArray[position]);
                strguestId = guestIdsArray[position];
            } else if (type.equalsIgnoreCase("Event")) {
                txtEvent.setText(filterArray[position]);
                strEventId = eventIds[position];
                eventId = strEventId;
                eventName = txtEvent.getText().toString().trim();
            } else if (type.equalsIgnoreCase("venue")) {
                txtVenue.setText(filterArray[position]);
                strVenueId = venueId[position];
                Log.e(TAG, "onItemClick: strVenueId : " + strVenueId);
            }
            mBottomSheetDialog.cancel();


        });


        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    public void dialogDatePickerLight() {


        // Get Current Date
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year1, monthOfYear, dayOfMonth) -> {
                    txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year1);
                    // yyyy/MM/dd
                    strDate = year1 + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
                }, year, month, day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        calendar.add(Calendar.MONTH, Utilities.MAX_ALLOWED_MONTH);
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.setTitle("Select date of event");
        datePickerDialog.show();
    }

    // Request Call space
    private void SpaceData() {

        if (page == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        String user_id = "";
        boolean isLoggedin = Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin);
        if (isLoggedin) {
            user_id = Utilities.getSPstringValue(context, Utilities.spUserId);
        }
        Log.e(TAG, "SpaceData: " + location + " " + eventId);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceDataByEventLocation(new GuestSpaceActivityModel(location,
                eventId, user_id, page, Utilities.LIMIT));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                try {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: SpaceData " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            isPageAvailable = jsonObject.getBoolean("count");
                            dataArray = jsonObject.getJSONArray("data");
                            Log.e(TAG, "SpaceData: SpaceData :" + dataArray);
                            if (dataArray != null) {
                                guestSpaceAdapter.setDataArray(dataArray);
                            }
                        }

                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    page--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                    btnSearch.setEnabled(true);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                page--;

            }
        });
    }

    //request call : Space Search
    public void SpaceSearch() {
        if (page == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
            recyclerView.setVisibility(View.GONE);
        }
        imgNoDataFound.setVisibility(View.GONE);
        String struserId = Utilities.getSPstringValue(context, Utilities.spUserId);

        Log.e(TAG, "SpaceSearch: userId" + struserId);

        Log.e(TAG, "SpaceSearch: strDate :" + strDate + "strEventId: " + strEventId + " strguestId: " + strguestId + "strVenueId: " + strVenueId);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceSearchData(new SpaceSearchModel(strDate, strEventId, strguestId,
                strVenueId, struserId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                try {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: SpaceSearch: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String strStatusCode = jsonObject.getString("status_code");
                    String strMag = jsonObject.getString("message");
                    Log.e(TAG, "onResponse: " + strMag + " " + strStatusCode);

                    if (!strStatusCode.equalsIgnoreCase("301")) {
                        if (strStatusCode.equalsIgnoreCase("200")) {
                            recyclerView.setVisibility(View.VISIBLE);
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            dataArray = dataObj.getJSONArray("spacelist");
                            if (is_search_btn_click) {
                                guestSpaceAdapter.cleardata();
                            }
                            guestSpaceAdapter.setDataArray(dataArray);
                        }
                    } else {

                        recyclerView.setVisibility(View.GONE);
                        imgNoDataFound.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    page--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                page--;
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Utilities.getSPbooleanValue(context, Utilities.spIsBookingType)) {
            Intent intent;
            if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin)) {
                intent = new Intent(context, HomeActivity.class);
            } else {
                intent = new Intent(context, HomeGuestActivity.class);
            }
            startActivity(intent);
            finish();
        } else {
            finish();   //going back to scenarioHomeActivity
        }
    }
}