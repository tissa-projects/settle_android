package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.inc.settlle.Activities.Service.EditServiceKYCActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.barteksc.pdfviewer.PDFView;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PreviewPDFImage extends AppCompatActivity {

    String fileType, filePath;
    ImageView imageView;
    PDFView pdfView;
    private static final String TAG = "OpenPDFImage";
    RelativeLayout layoutProgress;
    TextView tv_loading, textError;
    boolean isBitMap = false;
    String bitMapValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_pdfimage);

        imageView = findViewById(R.id.imageView);
        pdfView = findViewById(R.id.pdfView);
        layoutProgress = findViewById(R.id.layoutProgress);
        tv_loading = findViewById(R.id.cp_title);
        textError = findViewById(R.id.textError);

        fileType = getIntent().getStringExtra("fileType");
        filePath = getIntent().getStringExtra("filePath");
        isBitMap = getIntent().getBooleanExtra("isBitmap", false);
        bitMapValue = getIntent().getStringExtra("bitmap");

        if (fileType != null && filePath.length() > 1)
            openFileForRegistration();
        else {
            layoutProgress.setVisibility(View.GONE);
            pdfView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            Glide.with(getApplicationContext()).load(R.drawable.nofile).into(imageView);
        }

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Document Preview");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (urlConnection != null)
            urlConnection.disconnect();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void openFileForRegistration() {
        if (fileType.equalsIgnoreCase(".PDF") || fileType.equalsIgnoreCase("PDF")) {
            imageView.setVisibility(View.GONE);
            if (filePath.toLowerCase().contains("http")) {
                createFile(filePath, filePath.substring(filePath.lastIndexOf("/")).replace("/", ""));
            } else {
                layoutProgress.setVisibility(View.GONE);
                pdfView.setVisibility(View.VISIBLE);
                pdfView.fromFile(new File(filePath))
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)
                        .spacing(10)
                        .load();
            }
        } else {
            layoutProgress.setVisibility(View.GONE);
            pdfView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);

            if (filePath.toLowerCase().contains("http")) {
                Glide.with(getApplicationContext()).load(filePath)
                        .thumbnail(0.5f)
                        .error(R.drawable.ic_pdf)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(imageView);
            } else {
                Log.d(TAG, "openFileForRegistration: From Local " + EditServiceKYCActivity.transferBase64Data);
                imageView.setImageBitmap(CommonFunctions.GetBitmap(Utilities.TRANSFER_BITMAP_VALUE));//EditServiceKYCActivity.transferBase64Data

            }
        }
    }

    public void createFile(String url, String fileName1) {
        File imgFile = CommonFunctions.createFileInLocalDirectory(fileName1, getApplicationContext());
        Thread thread = new Thread(() -> {
            try {
                downloadFile(url, imgFile);
            } catch (Exception e) {
                showError(false);
                e.printStackTrace();
            }
        });
        thread.start();
    }

    private static final int MEGABYTE = 1024 * 1024;
    HttpURLConnection urlConnection;

    @SuppressLint("SetTextI18n")
    public void downloadFile(String fileUrl, File directory) {
        try {
            fileUrl = fileUrl.replaceAll(" ", "%20");
            URL sourceUrl = new URL(fileUrl);
            Log.d(TAG, "downloadFile: " + sourceUrl);
            urlConnection = (HttpURLConnection) sourceUrl.openConnection();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();
            long total = 0;
            byte[] buffer = new byte[MEGABYTE];
            int bufferLength;
            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
                total += bufferLength;
                int progress = (int) ((double) (total * 100) / (double) totalSize);
                setProgressbar(progress);
            }
            onDownloadComplete(directory);
            if (urlConnection != null)
                urlConnection.disconnect();
            fileOutputStream.close();
        } catch (Exception e) {
            showError(false);
            Log.d(TAG, "downloadFile: " + e);
            e.printStackTrace();
        }
    }

    private void onDownloadComplete(File file) {
        try {
            runOnUiThread(() -> {
                tv_loading.setVisibility(View.GONE);
                layoutProgress.setVisibility(View.GONE);
                pdfView.setVisibility(View.VISIBLE);
                pdfView.fromFile(file)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)
                        .onError(t -> showError(true))
                        .load();

            });
        } catch (Exception ignore) {
        }
    }

    @SuppressLint("SetTextI18n")
    public void showError(boolean fileNotFind) {
        try {
            runOnUiThread(() -> {
                if (fileNotFind)
                    textError.setText("corrupted");
                else
                    textError.setText("exception");

                textError.setVisibility(View.VISIBLE);
                layoutProgress.setVisibility(View.GONE);
            });
        } catch (Exception ignore) {
        }
    }

    @SuppressLint("SetTextI18n")
    public void setProgressbar(int progress) {
        try {
            runOnUiThread(() -> tv_loading.setText(getResources().getString(R.string.loading2) + " " + progress + "%"));
        } catch (Exception ignore) {
        }
    }


}
