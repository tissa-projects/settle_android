package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.RulesAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetSpaceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RulesActivity extends AppCompatActivity {

    private RecyclerView recyclerRules;
    private Context context;
    private Dialog loadingDialog;
    private String uni;
    private static final String TAG = RulesActivity.class.getSimpleName();


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);
        context = RulesActivity.this;
        recyclerRules = findViewById(R.id.recycler_rules);
        getIntentData();
        if (VU.isConnectingToInternet(context)) {
            getSpaceRules();
        }
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Space Rules");
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
        }

    }


    private void getSpaceRules() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceRules(new GetSpaceDetailModel(uni));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getBoolean("status")) {
//
                        JSONArray dataArray = new JSONArray();
                        String selectedKeys = jsonObject.getString("selected");

                        String[] selectedKeysArray = selectedKeys.split(",");
                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        JSONArray rulesArray = dataObject.getJSONArray("rules");
                        for (int i = 0; i < rulesArray.length(); i++) {
                            JSONObject rulesObject = rulesArray.getJSONObject(i);
                            String rulesId = rulesObject.getString("rules_id");
                            String rule = rulesObject.getString("rules_name");

                            for (String key : selectedKeysArray) {
                                if (key.equals(rulesId)) {
                                    JSONObject json = new JSONObject();
                                    json.put("rules_id", rulesId);
                                    json.put("rules_name", rule);
                                    dataArray.put(json);
                                }
                            }
                        }

                        spaceRulesAdapterCall(dataArray);

                    } else {
                        //handle failed logic here
                        Toast.makeText(RulesActivity.this, "Error... " + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void spaceRulesAdapterCall(JSONArray dataArray) {
        RulesAdapter rulesAdapter;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerRules.setLayoutManager(layoutManager);
        rulesAdapter = new RulesAdapter(dataArray, this);
        recyclerRules.setAdapter(rulesAdapter);

    }

}
