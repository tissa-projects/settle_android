package com.android.inc.settlle.Retrofit;


import com.android.inc.settlle.RequestModel.AddServiceDescriptionModel;
import com.android.inc.settlle.RequestModel.AddServiceReviewModel;
import com.android.inc.settlle.RequestModel.AddSpaceToFavModel;
import com.android.inc.settlle.RequestModel.AddtocartServiceModel;
import com.android.inc.settlle.RequestModel.AddtocartSpaceModel;
import com.android.inc.settlle.RequestModel.AuthModel;
import com.android.inc.settlle.RequestModel.BookingRequestDetailModel;
import com.android.inc.settlle.RequestModel.CartServiceDetailUpdate;
import com.android.inc.settlle.RequestModel.CartSpaceDetailUpdate;
import com.android.inc.settlle.RequestModel.CartViewDetailsModel;
import com.android.inc.settlle.RequestModel.ChangeBookingRequestStatusModel;
import com.android.inc.settlle.RequestModel.ChangeMobileNoModel;
import com.android.inc.settlle.RequestModel.ChangePasswordModel;
import com.android.inc.settlle.RequestModel.CheckServiceSubcriptionModel;
import com.android.inc.settlle.RequestModel.CheckSpaceSubcriptionModel;
import com.android.inc.settlle.RequestModel.ConfirmServiceBookingModel;
import com.android.inc.settlle.RequestModel.DeleteFavServiceModel;
import com.android.inc.settlle.RequestModel.DeleteFavSpaceModel;
import com.android.inc.settlle.RequestModel.DeleteImageModel;
import com.android.inc.settlle.RequestModel.DeletePackageModel;
import com.android.inc.settlle.RequestModel.DiscountModel;
import com.android.inc.settlle.RequestModel.EditSpaceServiceDataModel;
import com.android.inc.settlle.RequestModel.FacbookGmailLoginModel;
import com.android.inc.settlle.RequestModel.FavServiceModel;
import com.android.inc.settlle.RequestModel.GetBookingDetailsModel;
import com.android.inc.settlle.RequestModel.GetScenarioHomaDataModel;
import com.android.inc.settlle.RequestModel.GetServiceBookFormDetailModel;
import com.android.inc.settlle.RequestModel.GetSpaceDetailModel;
import com.android.inc.settlle.RequestModel.Glocery;
import com.android.inc.settlle.RequestModel.GloceryLogin;
import com.android.inc.settlle.RequestModel.GuestSpaceActivityModel;
import com.android.inc.settlle.RequestModel.LoginModel;
import com.android.inc.settlle.RequestModel.MyListModel;
import com.android.inc.settlle.RequestModel.MyServiceListModel;
import com.android.inc.settlle.RequestModel.MyBookingListModel;
import com.android.inc.settlle.RequestModel.MySpaceListModel;
import com.android.inc.settlle.RequestModel.PackageDetailsModel;
import com.android.inc.settlle.RequestModel.ProfileDataModel;
import com.android.inc.settlle.RequestModel.ProfileImageModel;
import com.android.inc.settlle.RequestModel.RegisterModel;
import com.android.inc.settlle.RequestModel.RemoveItemFromCartModel;
import com.android.inc.settlle.RequestModel.RequestListModel;
import com.android.inc.settlle.RequestModel.SearchServiceModel;
import com.android.inc.settlle.RequestModel.SendBookingRequestModel;
import com.android.inc.settlle.RequestModel.SendOtpModel;
import com.android.inc.settlle.RequestModel.ServiceDetailModel;
import com.android.inc.settlle.RequestModel.ServiceMultipleImageModel;
import com.android.inc.settlle.RequestModel.ServiceSubscriptionModel;
import com.android.inc.settlle.RequestModel.GetDataToRenewModel;
import com.android.inc.settlle.RequestModel.SpaceMultipleImagesModel;
import com.android.inc.settlle.RequestModel.SpaceRegistartionDetailsModel;
import com.android.inc.settlle.RequestModel.RenewalModel;
import com.android.inc.settlle.RequestModel.SpaceRequestListModel;
import com.android.inc.settlle.RequestModel.SpaceReviewModel;
import com.android.inc.settlle.RequestModel.SpaceSearchModel;
import com.android.inc.settlle.RequestModel.SpaceSubscriptionModel;
import com.android.inc.settlle.RequestModel.StateCityModel;
import com.android.inc.settlle.RequestModel.UpdateProfileModel;
import com.android.inc.settlle.RequestModel.UpdateServiceDesModel;
import com.android.inc.settlle.RequestModel.UpdateServiceDescriptionModel;
import com.android.inc.settlle.RequestModel.UpdateSpaceImagesModel;
import com.android.inc.settlle.RequestModel.UpdateVendorRequestModel;
import com.android.inc.settlle.RequestModel.UpdateVendorServiceRequestModel;
import com.android.inc.settlle.RequestModel.UploadServiceKYCDocModel;
import com.android.inc.settlle.RequestModel.UploadSpaceKYCDocModel;
import com.android.inc.settlle.RequestModel.UserIdModel;
import com.android.inc.settlle.RequestModel.VerifyOtpModel;
import com.android.inc.settlle.RequestModel.UserAuthModel;
import com.android.inc.settlle.RequestModel.ViewMyBookingDetailsModel;
import com.android.inc.settlle.RequestModel.getKycDocModel;
import com.android.inc.settlle.RequestModel.getMultipleImagesModel;
import com.android.inc.settlle.RequestModel.getUserDataRequestModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {
    //1
//    @FormUrlEncoded
    @POST("host_register")
    Call<ResponseBody> register(
            @Body RegisterModel rm);

    //2
    //@Headers({"Accept: application/json"})
    @POST("host_login")
    Call<ResponseBody> login(
            @Body LoginModel lm);

    @POST("shop/auth/login/?format=json")
    Call<ResponseBody> Glocerylogin(
            @Body Glocery lm);


    @GET("personal-pages/your-orders/?format=json")
    Call<ResponseBody> orders();

    //3
    //@Headers({"Accept: application/json"})
    @POST("index")
    Call<ResponseBody> getSpaceEventLocationList(@Body UserAuthModel indexRequestModule);//

    //4
    @POST("activity")
    Call<ResponseBody> getSpaceDataByEventLocation(
            @Body GuestSpaceActivityModel guestSpaceActivityModel);

    //5
    @POST("spacelist")
    Call<ResponseBody> getMySpaceListData(
            @Body MySpaceListModel mySpaceListModel);

    //6
    @POST("change_dp")
    Call<ResponseBody> setProfileImage(
            @Body ProfileImageModel profileImageModel);

    //7
    @POST("myaccount")
    Call<ResponseBody> getProfileData(
            @Body ProfileDataModel profileDataModel);

    //8
    @POST("venue")
    Call<ResponseBody> getVenueList(@Body ProfileDataModel profileDataModel);


    //9
    @POST("update_profile")
    Call<ResponseBody> updateProfileData(@Body UpdateProfileModel updateProfileModel);

    //10
    @POST("space_subscription")
    Call<ResponseBody> getSpaceSubcriptionList(@Body MyListModel myListModel);

    //11
    @POST("service_subscription")
    Call<ResponseBody> getServiceSubcriptionList(@Body MyListModel myListModel);

    //12
    @POST("add_space_description")
    Call<ResponseBody> setSpaceRegistrationData(
            @Body SpaceRegistartionDetailsModel spaceRegistartionDetails);

    //13
    @POST("space_subscribe")
    Call<ResponseBody> sendSpaceSubscriptionData(
            @Body SpaceSubscriptionModel spaceSubscriptionModel);

    //14
    @POST("space_form")
    Call<ResponseBody> getEventsAmenitesRules(
            @Body AuthModel authModel);

    //15
    @POST("get_cities")
    Call<ResponseBody> getCityData(
            @Body StateCityModel stateCityModel);

    //16
    @POST("get_states")
    Call<ResponseBody> getStateData(
            @Body StateCityModel stateCityModel);

    //17
    @POST("space_description_details")
    Call<ResponseBody> getSpaceDetail(
            @Body GetSpaceDetailModel getSpaceDetailModel);

    //18
    @POST("space_review_details")
    Call<ResponseBody> getSpaceReview(
            @Body GetSpaceDetailModel getSpaceDetailModel);

    //19
    @POST("space_rules_details")
    Call<ResponseBody> getSpaceRules(
            @Body GetSpaceDetailModel getSpaceDetailModel);


    //20
    @POST("space_amenities_details")
    Call<ResponseBody> getSpaceAmenities(
            @Body GetSpaceDetailModel getSpaceDetailModel);


    //21
    @POST("add_space_document")
    Call<ResponseBody> uploadSpaceKYCDoc(
            @Body UploadSpaceKYCDocModel uploadSpaceKYCDocModel);


    //22
    @POST("space_search")
    Call<ResponseBody> getSpaceSearchData(
            @Body SpaceSearchModel spaceSearchModel);

    //23
    @POST("add_space_image")
    Call<ResponseBody> setSpaceMultipleImages(
            @Body SpaceMultipleImagesModel spaceMultipleImagesModel);

    //24
    @POST("send_otp")
    Call<ResponseBody> sendOTP(
            @Body SendOtpModel sendOtpModel);

    //25
    @POST("otp_verify")
    Call<ResponseBody> VerifyOTP(
            @Body VerifyOtpModel verifyOtpModel);

    //26
    @POST("user_logout")
    Call<ResponseBody> logout(
            @Body AuthModel authModel);

    //27
    @POST("social_login")
    Call<ResponseBody> facebookGmailLogin(
            @Body FacbookGmailLoginModel facbookGmailLoginModel);


    //28
    @POST("forgotpassword")
    Call<ResponseBody> forgotPassword(
            @Body LoginModel loginModel);


    //29
    @POST("space_request")
    Call<ResponseBody> getSpaceRequestList(
            @Body SpaceRequestListModel spaceRequestListModel);

    //30
    @POST("view_booked_space_request")
    Call<ResponseBody> getSpaceRequestDetail(
            @Body BookingRequestDetailModel bookingRequestDetailModel);


    //30
    @POST("update_booked_status")
    Call<ResponseBody> changeBookingRequestStatus(
            @Body ChangeBookingRequestStatusModel changeBookingRequestStatusModel);


    //31
    @POST("update_booked_space_details")
    Call<ResponseBody> updateBookingSpaceRequest(
            @Body UpdateVendorRequestModel updateVendorRequestModel);


    //32
    @POST("change_thatpass")
    Call<ResponseBody> changePassword(
            @Body ChangePasswordModel changePasswordModel);

    //33
    @POST("change_number")
    Call<ResponseBody> changeMobileNo(
            @Body ChangeMobileNoModel changeMobileNoModel);

    //34
    @POST("editspace")
    Call<ResponseBody> getEditSpaceData(
            @Body EditSpaceServiceDataModel editSpaceDataModel);

    //35
    @POST("update_space_description")
    Call<ResponseBody> setEditSpaceData(
            @Body SpaceRegistartionDetailsModel spaceRegistartionDetailsModel);

    //36
    @POST("space_description_details")
    Call<ResponseBody> getMultipleSpaceImages(
            @Body getMultipleImagesModel getMultipleImagesModel);

    //37
    @POST("delete_image")
    Call<ResponseBody> deleteSpaceImage(
            @Body DeleteImageModel deleteImageModel);

    //38
    @POST("update_space_image")
    Call<ResponseBody> updateSpaceImages(
            @Body UpdateSpaceImagesModel updateSpaceImagesModel);

    //39
    @POST("space_booking")
    Call<ResponseBody> getSpaceBookingList(
            @Body MyBookingListModel myBookingListModel);

    //40
    @POST("document_details")
    Call<ResponseBody> getKYCDoc(
            @Body getKycDocModel getKycDocModel);

    //41
    @POST("favourite_space")
    Call<ResponseBody> getMyFavouriteSpace(
            @Body MyListModel myListModel);

    //42
    @POST("delete_fav_space")
    Call<ResponseBody> deleteFaveSpace(
            @Body DeleteFavSpaceModel deleteFavSpaceModel);

    //43
    @POST("favourite_service")
    Call<ResponseBody> getMyFavouriteService(
            @Body MyListModel myListModel);

    //44
    @POST("delete_fav_service")
    Call<ResponseBody> deleteFaveService(
            @Body DeleteFavServiceModel deleteFavServiceModel);

    //45
    @POST("view_booked_space")
    Call<ResponseBody> getSpaceDetails(
            @Body ViewMyBookingDetailsModel viewMyBookingDetailsModel);

    //46
    @POST("check_space_subscription")
    Call<ResponseBody> checkSpaceSubcription(
            @Body CheckSpaceSubcriptionModel checkSpaceSubcriptionModel);

    //47
    @POST("add_fav_space")
    Call<ResponseBody> addFaveSpace(
            @Body AddSpaceToFavModel add);

    //48
    @POST("space_renewal")
    Call<ResponseBody> spaceRenew(
            @Body RenewalModel renewalModel);

    //49
    @POST("renew_space_subscription")
    Call<ResponseBody> getSpaceDataToRenew(
            @Body GetDataToRenewModel getDataToRenewModel);

    //50
    @POST("space_review")
    Call<ResponseBody> addSpaceReview(
            @Body SpaceReviewModel spaceReviewModel);

    //51
    @POST("book_space")
    Call<ResponseBody> sendBookingRequest(
            @Body SendBookingRequestModel sendBookingRequestModel);


    //51
    @POST("book_space_form")
    Call<ResponseBody> getDetailsForBooking(
            @Body GetBookingDetailsModel getBookingDetailsModel);


    //52
    @POST("service")
    Call<ResponseBody> getServiceList(@Body UserIdModel userIdModel);

    //53
    @POST("check_service_subscription")
    Call<ResponseBody> checkServiceSubcription(
            @Body CheckServiceSubcriptionModel checkServiceSubcriptionModel);

    //54
    @POST("service_subscribe")
    Call<ResponseBody> sendServiceSubscriptionData(
            @Body ServiceSubscriptionModel serviceSubscriptionModel);

    //55
    @POST("servicelist")
    Call<ResponseBody> getMyServiceListData(
            @Body MyServiceListModel myServiceListModel);

    //56
    @POST("service_description_details")
    Call<ResponseBody> getServiceDetail(
            @Body ServiceDetailModel serviceDetailModel);

    //57
    @POST("add_service_description")
    Call<ResponseBody> setServiceRegistrationData(
            @Body AddServiceDescriptionModel serviceDetailModel


    );

    //58
    @POST("add_service_image")
    Call<ResponseBody> setServiceMultipleImages(
            @Body ServiceMultipleImageModel serviceMultipleImageModel);


//    //59
//    @POST("service_booking")
//    Call<ResponseBody> getServiceBookingList(
//            @Body RequestListModel requestListModel);

    //59
    @POST("service_request")
    Call<ResponseBody> getServiceRequestList(
            @Body RequestListModel requestListModel);

    //60
    @POST("view_booked_service_request")
    Call<ResponseBody> getServiceRequestDetail(
            @Body BookingRequestDetailModel bookingRequestDetailModel);


    //61
    @POST("update_booked_service_details")
    Call<ResponseBody> updateBookingServiceRequest(
            @Body UpdateVendorServiceRequestModel vendorServiceRequestModel);

    //62
    @POST("add_service_document")
    Call<ResponseBody> uploadServiceKYCDoc(
            @Body UploadServiceKYCDocModel uploadServiceKYCDocModel);

    //63
    @POST("service_description_details")
    Call<ResponseBody> EditServiceData(
            @Body EditSpaceServiceDataModel editSpaceServiceDataModel);

    //64
    @POST("service_packages_details")
    Call<ResponseBody> getpackageListData(
            @Body PackageDetailsModel packageDetailsModel);

    //65
    @POST("update_service_description")
    Call<ResponseBody> updateServiceDescription(
            @Body UpdateServiceDescriptionModel updateServiceDescriptionModel);

    @POST("update_service_description")
    Call<ResponseBody> updateServiceDescription(
            @Body UpdateServiceDesModel updateServiceDescriptionModel);

    //66
    @POST("service_review_details")
    Call<ResponseBody> getServiceReviews(
            @Body ServiceDetailModel serviceDetailModel);

    //67
    @POST("service_packages_details")
    Call<ResponseBody> getServicePackage(
            @Body ServiceDetailModel serviceDetailModel);

    //68
    @POST("service_search")
    Call<ResponseBody> getSearchedService(
            @Body SearchServiceModel searchServiceModel);

    //69
    @POST("add_fav_service")
    Call<ResponseBody> addFavService(
            @Body FavServiceModel favServiceModel);

    //70
    @POST("delete_fav_service")
    Call<ResponseBody> deleteFavService(
            @Body FavServiceModel favServiceModel);

    //71
    @POST("service_description_details")
    Call<ResponseBody> getMultipleServiceImages(
            @Body getMultipleImagesModel getMultipleImagesModel);


    //72
    @POST("delete_image")
    Call<ResponseBody> deleteServiceImage(
            @Body DeleteImageModel deleteImageModel);

    //73
    @POST("add_service_image")
    Call<ResponseBody> updateServiceImages(
            @Body ServiceMultipleImageModel serviceMultipleImageModel);

    //74
    @POST("book_service_form")
    Call<ResponseBody> getServiceBookingFormDetail(
            @Body GetServiceBookFormDetailModel getServiceBookFormDetail);

    //75
    @POST("book_service")
    Call<ResponseBody> confirmserviceBooking(
            @Body ConfirmServiceBookingModel confirmServiceBookingModel);


    //76
    @POST("service_booking")
    Call<ResponseBody> getServiceBookingList(
            @Body MyBookingListModel myBookingListModel);


    //77
    @POST("view_booked_service")
    Call<ResponseBody> getMyServiceBookingDetails(
            @Body ViewMyBookingDetailsModel viewMyBookingDetailsModel);


    //78
    @POST("service_review")
    Call<ResponseBody> addServiceReview(
            @Body AddServiceReviewModel addServiceReviewModel);


    //79
    @POST("renew_service_subscription")
    Call<ResponseBody> getServiceDataToRenew(
            @Body GetDataToRenewModel getDataToRenewModel);

    //80
    @POST("service_renewal")
    Call<ResponseBody> serviceRenew(
            @Body RenewalModel renewalModel);


    //81
    @POST("view_cart")
    Call<ResponseBody> getCartList(
            @Body UserAuthModel viewCartModel);

    //81
    @POST("delete_cart_space")
    Call<ResponseBody> RemoveSpaceFromCart(
            @Body RemoveItemFromCartModel removeItemFromCartModel);

    //81
    @POST("delete_cart_service")
    Call<ResponseBody> RemoveServiceFromCart(
            @Body RemoveItemFromCartModel removeItemFromCartModel);

    //81
    @POST("scenario_search")
    Call<ResponseBody> getScenarioHomeData(
            @Body GetScenarioHomaDataModel getScenarioHomaDataModel);

    //83
    @POST("scenario_next_search")
    Call<ResponseBody> getScenarioCreationData(
            @Body GetScenarioHomaDataModel getScenarioHomaDataModel);

    //84
    @POST("checkout_cart")
    Call<ResponseBody> CheckOutFromCart(
            @Body UserAuthModel userAuthModel);

    //85
    @POST("add_to_cart")
    Call<ResponseBody> AddToCartSpace(
            @Body AddtocartSpaceModel addtocartModel);

    //86
    @POST("service_add_to_cart")
    Call<ResponseBody> AddToCartService(
            @Body AddtocartServiceModel addtocartModel);


    //87
    @POST("delete_package")
    Call<ResponseBody> deletePackage(
            @Body DeletePackageModel deletePackageModel);

    //87
    @POST("check_coupon")
    Call<ResponseBody> getDiscount(
            @Body DiscountModel discountModel);

    //89
    @POST("already_sub_venu")
    Call<ResponseBody> getAlreadySubscribedVenues(
            @Body UserAuthModel userAuthModel);

    //89
    @POST("already_sub_service")
    Call<ResponseBody> getAlreadySubscribedServices(
            @Body UserAuthModel userAuthModel);


    //90
    @POST("display_booked_space")
    Call<ResponseBody> cartViewDetails(
            @Body CartViewDetailsModel cartViewDetails);

    //91
    @POST("edit_space_cart")
    Call<ResponseBody> updateToCartSpaceDetail(
            @Body CartSpaceDetailUpdate cartSpaceDetailUpdate);

    //91
    @POST("edit_service_cart")
    Call<ResponseBody> updateToCartServiceDetail(
            @Body CartServiceDetailUpdate cartServiceDetailUpdate);


    //@Headers({"Accept: application/json"})
    @POST("get_user_data")
    Call<ResponseBody> getUserData(
            @Body getUserDataRequestModel lm);

}
