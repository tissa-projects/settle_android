package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.AddMyReviewActivity;
import com.android.inc.settlle.Activities.Service.ViewMyServiceBookingDetailsActivity;
import com.android.inc.settlle.Activities.ViewMySpaceBookingDetailsActivity;
import com.android.inc.settlle.Adapter.MyServiceBookingListAdapter;
import com.android.inc.settlle.Adapter.MySpaceBookingListAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.MyBookingListModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyBookingFragment extends Fragment implements View.OnClickListener {
    private TextView btnSpaceBooking, btnServiceBooking;
    private RecyclerView recyclerSpace, recyclerService;
    private ShimmerFrameLayout shimmerFrameLayout;
    private MySpaceBookingListAdapter mySpaceBookingAdapter = null;
    private MyServiceBookingListAdapter myServiceBookingListAdapter = null;
    private View view;
    private Context context;
    private RecyclerView.LayoutManager layoutManagerSpace, layoutManagerService;
    private JSONArray spaceDataArray, serviceDataArray;

    private ImageView imgAccount1, imgAccount2, imgAccount3, imgAccount4;
    private TextView txtAccount1, txtAccount2, txtAccount3, txtAccount4;
    private LinearLayout llAccountbtn1, llAccountbtn2, llAccountbtn3, llAccountbtn4;

    ProgressBar progressBar;
    int spacePage = 1, servicePage = 1;
    int requestType = 1; //1 for space and 2 for service
    private boolean isSpacePageAvailable, isServicePageAvailable;
    //variables for pagination
    private boolean isLoading = true;
    private int spacePastVariableItems, spaceVisibleItemCount, spaceTotalItemCount, spacePrivious_total = 0;
    private int servicePastVariableItems, serviceVisibleItemCount, serviceTotalItemCount, servicePrivious_total = 0;
    private final int spaceView_threshold = 3;
    private final int serviceView_threshold = 3;

    private static final String TAG = MyBookingFragment.class.getSimpleName();

    private TextView filter_all, filter_in_review, filter_confirmed, filter_completed, filter_canceled;
    private int filter_code = 4;
    ImageView noDataFoundImg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_booking, container, false);
        ImageView editButton = requireActivity().findViewById(R.id.account_button_edit);
        if (editButton.getVisibility() == View.VISIBLE) {
            editButton.setVisibility(View.GONE);
        }
        context = getActivity();
        initialize();

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (VU.isConnectingToInternet(context)) {
            getSpaceBookingList();
        }
        initializeActivityView();
    }

    private void initialize() {
        btnServiceBooking = view.findViewById(R.id.btn_service_booking);
        btnSpaceBooking = view.findViewById(R.id.btn_space_booking);
        recyclerService = view.findViewById(R.id.recycler_account_service_booking);
        recyclerSpace = view.findViewById(R.id.recycler_account_space_booking);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container_booking);
        progressBar = view.findViewById(R.id.progress_bar);

        layoutManagerSpace = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerSpace.setLayoutManager(layoutManagerSpace);
        layoutManagerService = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerService.setLayoutManager(layoutManagerService);

        mySpaceBookingAdapter = new MySpaceBookingListAdapter(context);
        recyclerSpace.setAdapter(mySpaceBookingAdapter);
        myServiceBookingListAdapter = new MyServiceBookingListAdapter(context);
        recyclerService.setAdapter(myServiceBookingListAdapter);


        filter_all = view.findViewById(R.id.filter_all);
        filter_all.setOnClickListener(this);
        filter_in_review = view.findViewById(R.id.filter_in_review);
        filter_in_review.setOnClickListener(this);
        filter_confirmed = view.findViewById(R.id.filter_confirmed);
        filter_confirmed.setOnClickListener(this);
        filter_completed = view.findViewById(R.id.filter_completed);
        filter_completed.setOnClickListener(this);
        filter_canceled = view.findViewById(R.id.filter_canceled);
        filter_canceled.setOnClickListener(this);
        noDataFoundImg = view.findViewById(R.id.img_nodatafound);
        noDataFoundImg.setVisibility(View.GONE);

        spaceAdapterClicks();
        serviceAdapterClicks();
        spaceBookingVisibility();

        btnServiceBooking.setOnClickListener(this);
        btnSpaceBooking.setOnClickListener(this);

    }

    private void initializeActivityView() {

        imgAccount1 = requireActivity().findViewById(R.id.img_account1);
        imgAccount2 = requireActivity().findViewById(R.id.img_account2);
        imgAccount3 = requireActivity().findViewById(R.id.img_account3);
        imgAccount4 = requireActivity().findViewById(R.id.img_account4);

        txtAccount1 = requireActivity().findViewById(R.id.txt_account1);
        txtAccount2 = requireActivity().findViewById(R.id.txt_account2);
        txtAccount3 = requireActivity().findViewById(R.id.txt_account3);
        txtAccount4 = requireActivity().findViewById(R.id.txt_account4);

        llAccountbtn1 = requireActivity().findViewById(R.id.account_button1);
        llAccountbtn2 = requireActivity().findViewById(R.id.account_button2);
        llAccountbtn3 = requireActivity().findViewById(R.id.account_button3);
        llAccountbtn4 = requireActivity().findViewById(R.id.account_button4);

        accountBtn3Click();
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void accountBtn3Click() {
        imgAccount3.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtAccount3.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgAccount2.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount2.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount1.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount1.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount4.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount4.setTextColor(getResources().getColorStateList(R.color.grey_60));
        llAccountbtn1.setClickable(true);
        llAccountbtn2.setClickable(true);
        llAccountbtn4.setClickable(true);
        llAccountbtn3.setClickable(false);

    }

    private void spaceAdapterClicks() {
        mySpaceBookingAdapter.setOnReviewClickListener((view, position, jsonObject) -> {
            try {
                Intent intent = new Intent(context, AddMyReviewActivity.class);
                intent.putExtra("id", jsonObject.getString("space_id"));
                intent.putExtra("bookingId", jsonObject.getString("space_booking_id"));
                intent.putExtra("type", "space");
                startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "onViewBookingClick: " + e.getMessage());
            }
        });


        mySpaceBookingAdapter.setOnViewBookingClickListener((view, position, jsonObject) -> {
            try {
                Intent intent = new Intent(context, ViewMySpaceBookingDetailsActivity.class);
                intent.putExtra("uniqueId", jsonObject.getString("uni_id"));
                intent.putExtra("spaceBookingId", jsonObject.getString("space_booking_id"));
                startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "onViewBookingClick: " + e.getMessage());
            }
        });

        recyclerSpace.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                spaceVisibleItemCount = layoutManagerSpace.getChildCount();
                spaceTotalItemCount = layoutManagerSpace.getItemCount();
                spacePastVariableItems = ((LinearLayoutManager) layoutManagerSpace).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (spaceTotalItemCount > spacePrivious_total) {
                            isLoading = false;
                            spacePrivious_total = spaceTotalItemCount;
                        }
                    }
                    if (!isLoading && (spaceTotalItemCount - spaceVisibleItemCount) <= (spacePastVariableItems + spaceView_threshold)) {
                        if (isSpacePageAvailable) {
                            spacePage++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getSpaceBookingList();
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }
            }
        });

    }

    private void serviceAdapterClicks() {

        myServiceBookingListAdapter.setOnReviewClickListener((view, position, jsonObject) -> {
            try {
                Log.e(TAG, "onViewBookingClick: " + jsonObject);
                Intent intent = new Intent(context, AddMyReviewActivity.class);
                intent.putExtra("id", jsonObject.getString("service_details_id"));
                intent.putExtra("bookingId", jsonObject.getString("service_booking_id"));
                intent.putExtra("type", "service");
                startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "onViewBookingClick: " + e.getMessage());
            }
        });

        myServiceBookingListAdapter.setOnViewBookingClickListener((view, position, jsonObject) -> {
            try {
                Intent intent = new Intent(context, ViewMyServiceBookingDetailsActivity.class);
                intent.putExtra("uniqueId", jsonObject.getString("uni_id"));
                intent.putExtra("serviceBookingId", jsonObject.getString("service_booking_id"));
                startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "onViewBookingClick: " + e.getMessage());
            }
        });


        recyclerService.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                serviceVisibleItemCount = layoutManagerService.getChildCount();
                serviceTotalItemCount = layoutManagerService.getItemCount();
                servicePastVariableItems = ((LinearLayoutManager) layoutManagerService).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (serviceTotalItemCount > servicePrivious_total) {
                            isLoading = false;
                            servicePrivious_total = serviceTotalItemCount;
                        }
                    }

                    if (!isLoading && (serviceTotalItemCount - serviceVisibleItemCount) <= (servicePastVariableItems + serviceView_threshold)) {
                        if (isServicePageAvailable) {
                            servicePage++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getServiceBookingList("Scroll");
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }
            }
        });

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_space_booking:
                requestType = 1;
                spacePage = 1;
                spaceBookingVisibility();
                // horizontalScrollView.sets
                mySpaceBookingAdapter.clearData();
                spaceBookingVisibility();
                getSpaceBookingList();
                filter_all.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.white));
                filter_in_review.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_code = 0;


                if (VU.isConnectingToInternet(context)) {
                    getFilteredRequestData();
                }
                break;


            case R.id.btn_service_booking:
                requestType = 2;
                servicePage = 1;
                serviceBookingVisibility();
                myServiceBookingListAdapter.clearData();
                serviceBookingVisibility();
                getServiceBookingList("Button Click");
                filter_all.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.white));
                filter_in_review.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_code = 0;


                if (VU.isConnectingToInternet(context)) {
                    getFilteredRequestData();
                }
                break;

            case R.id.filter_all:

                filter_all.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.white));
                filter_in_review.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_code = 0;


                if (VU.isConnectingToInternet(context)) {
                    getFilteredRequestData();
                }
                break;

            case R.id.filter_in_review:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_in_review.setTextColor(getResources().getColor(R.color.white));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));


                filter_code = 4;

                if (VU.isConnectingToInternet(context)) {
                    //  getBookingRequest();
                    getFilteredRequestData();
                }
                break;


            case R.id.filter_confirmed:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_in_review.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.white));
                filter_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_code = 1;

                if (VU.isConnectingToInternet(context)) {
                    // getBookingRequest();
                    getFilteredRequestData();
                }
                break;

            case R.id.filter_completed:

                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_in_review.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_completed.setTextColor(getResources().getColor(R.color.white));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));

                filter_code = 2;
                if (VU.isConnectingToInternet(context)) {
                    getFilteredRequestData();
                }
                break;

            case R.id.filter_canceled:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_in_review.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_completed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_button_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_in_review.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_completed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.white));
                filter_code = 3;
                if (VU.isConnectingToInternet(context)) {
                    // getBookingRequest();
                    getFilteredRequestData();
                }
                break;
        }
    }

    private void getFilteredRequestData() {

        //  getBookingRequest();
        if (requestType == 1) {
            spacePage = 1;
            mySpaceBookingAdapter.clearData();
            spaceBookingVisibility();
            getSpaceBookingList();
        } else if (requestType == 2) {
            servicePage = 1;
            myServiceBookingListAdapter.clearData();
            serviceBookingVisibility();
            getServiceBookingList("get Filter request");

        }
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceBookingVisibility() {
        btnSpaceBooking.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        btnSpaceBooking.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        btnServiceBooking.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        btnServiceBooking.setTextColor(getResources().getColorStateList(R.color.white));
        recyclerSpace.setVisibility(View.VISIBLE);
        recyclerService.setVisibility(View.GONE);

    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void serviceBookingVisibility() {
        btnServiceBooking.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        btnServiceBooking.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        btnSpaceBooking.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        btnSpaceBooking.setTextColor(getResources().getColorStateList(R.color.white));
        recyclerService.setVisibility(View.VISIBLE);
        recyclerSpace.setVisibility(View.GONE);
    }

    //get space booking list
    private void getSpaceBookingList() {
        if (spacePage == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
            mySpaceBookingAdapter.clearData();
        }

        Log.e(TAG, "getSpaceBookingList:  Filter = " + filter_code);
        Log.e(TAG, "getSpaceBookingList:  Type = " + requestType);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceBookingList(new MyBookingListModel(Utilities.getSPstringValue(context, Utilities.spUserId),
                Utilities.getSPstringValue(context, Utilities.spAuthToken), spacePage, Utilities.LIMIT, filter_code));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String apiResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(apiResponse);
                    Log.e(TAG, "onResponse: " + jsonObject);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            noDataFoundImg.setVisibility(View.GONE);
                            spaceDataArray = jsonObject.getJSONArray("data");
                            isSpacePageAvailable = jsonObject.getBoolean("count");
                            if (spacePage == 1)
                                mySpaceBookingAdapter.clearData();

                            mySpaceBookingAdapter.setData(spaceDataArray);
                        } else {
                            // no record found
                            if (spacePage == 1) {
                                noDataFoundImg.setVisibility(View.VISIBLE);
                            } else {
                                noDataFoundImg.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    spacePage--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                spacePage--;

            }
        });

    }

    //get service booking list
    private void getServiceBookingList(String type) {
        if (servicePage == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
            myServiceBookingListAdapter.clearData();
        }
        Log.e(TAG, "getServiceBookingList:  Filter = " + filter_code + "  " + type);
        Log.e(TAG, "getServiceBookingList:  Type = " + requestType);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServiceBookingList(new MyBookingListModel(Utilities.getSPstringValue(context, Utilities.spUserId),
                Utilities.getSPstringValue(context, Utilities.spAuthToken), servicePage, Utilities.LIMIT, filter_code));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String apiResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(apiResponse);
                    Log.e(TAG, "onResponse: " + jsonObject);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            noDataFoundImg.setVisibility(View.GONE);
                            serviceDataArray = jsonObject.getJSONArray("data");
                            isServicePageAvailable = jsonObject.getBoolean("count");
                            if (servicePage == 1)
                                myServiceBookingListAdapter.clearData();
                            myServiceBookingListAdapter.setData(serviceDataArray);
                        } else {
                            if (spacePage == 1) {
                                noDataFoundImg.setVisibility(View.VISIBLE);
                            } else {
                                noDataFoundImg.setVisibility(View.GONE);
                            }
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    servicePage--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                servicePage--;

            }
        });

    }


}
