package com.android.inc.settlle.RequestModel;

public class Glocery {

    private GloceryLogin form_data;

    public Glocery(GloceryLogin form_data) {
        this.form_data = form_data;
    }

    public GloceryLogin getForm_data() {
        return form_data;
    }

    public void setForm_data(GloceryLogin form_data) {
        this.form_data = form_data;
    }
}
