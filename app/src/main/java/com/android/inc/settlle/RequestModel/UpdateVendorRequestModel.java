package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class UpdateVendorRequestModel implements Serializable {





    private String space_booking_id;
    private String startdate;
    private String enddate;
    private String start_time;
    private String end_time;
    private String amount;
    private String auth_token;
    private String guest_id;

    public UpdateVendorRequestModel(String space_booking_id, String startdate, String enddate,
                                    String start_time, String end_time, String amount, String auth_token, String guest_id) {
        this.space_booking_id = space_booking_id;
        this.startdate = startdate;
        this.enddate = enddate;
        this.start_time = start_time;
        this.end_time = end_time;
        this.amount = amount;
        this.auth_token = auth_token;
        this.guest_id = guest_id;
    }
}
