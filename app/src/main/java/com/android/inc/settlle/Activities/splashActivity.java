package com.android.inc.settlle.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.newDesign.FirstScreenActivity;
import com.android.inc.settlle.newDesign.NewRegistrationActivity;


public class splashActivity extends AppCompatActivity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.background)); //status bar or the time bar at the top
        context = splashActivity.this;
        //  getSpaceDetail();
        //startActivity(new Intent(getApplicationContext(), FirstScreenActivity.class));
        // finish();
        splashTime();
    }

    public void splashTime() {
        new Handler().postDelayed(() -> {
            if (!Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin) && !Utilities.getSPbooleanValue(context, Utilities.spFirstTime)) {
                startActivity(new Intent(splashActivity.this, FirstScreenActivity.class));
                finish();
            } else {
                if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin) && Utilities.getSPbooleanValue(context, Utilities.spIsMobileverify)) {
                    startActivity(new Intent(splashActivity.this, HomeActivity.class));
                    finish();
                } else if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin) && !Utilities.getSPbooleanValue(context, Utilities.spIsMobileverify)) {
                    startActivity(new Intent(splashActivity.this, MobileVerificationActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(splashActivity.this, HomeGuestActivity.class));
                    finish();
                }
            }
        }, Utilities.SPLASH_TIME_OUT);
    }
}
