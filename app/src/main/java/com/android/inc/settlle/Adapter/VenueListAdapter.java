package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.VenueModel;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;

import java.util.ArrayList;

public class VenueListAdapter extends RecyclerView.Adapter<VenueListAdapter.MyViewHolder> {

    ArrayList<VenueModel> venueModels;

    private RecyclerViewItemClickListener.ICheckBoxClick iCheckBoxClick;


    public VenueListAdapter(ArrayList<VenueModel> venueModels) {
        this.venueModels = venueModels;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_list_item_layout, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        try {
            String venue = venueModels.get(i).getVenueName();
            // String venue= dataArray.getJSONObject(i).getString("venue_name");
            boolean isSelected = venueModels.get(i).isSelected();

            // boolean isSelected = dataArray.getJSONObject(i).getBoolean("isSelected");
            myViewHolder.checkbox.setText("" + venue);
            if (isSelected) {
                myViewHolder.checkbox.setChecked(true);

            }

            myViewHolder.checkbox.setOnClickListener(view -> iCheckBoxClick.onCheckBoxClickListner(view, i, new JSONArray()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return venueModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkbox;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.checkbox = itemView.findViewById(R.id.checkbox);
        }
    }

    public void setOnCheckBoxClickListener(RecyclerViewItemClickListener.ICheckBoxClick iCheckBoxClick) {
        this.iCheckBoxClick = iCheckBoxClick;
    }

}
