package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class ChangePasswordModel implements Serializable {

    private String user_id;
    private String oldpass;
    private String newpass;
    private String conpass;
    private String auth_token;

    public ChangePasswordModel(String user_id,String oldpass, String newpass, String conpass, String auth_token) {
        this.user_id = user_id;
        this.oldpass = oldpass;
        this.newpass = newpass;
        this.conpass = conpass;
        this.auth_token = auth_token;
    }
}
