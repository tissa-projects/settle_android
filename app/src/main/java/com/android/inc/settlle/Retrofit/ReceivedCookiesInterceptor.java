package com.android.inc.settlle.Retrofit;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.inc.settlle.Utilities.Utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

public class ReceivedCookiesInterceptor implements Interceptor {

    private Context context;
    private static final String TAG = ReceivedCookiesInterceptor.class.getSimpleName();

    public ReceivedCookiesInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());
        Log.e(TAG, "intercept: originalResponse: " + originalResponse.header("Set-Cookie"));
        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
     //       HashSet<String> cookies = (HashSet<String>) PreferenceManager.getDefaultSharedPreferences(context).getStringSet("PREF_COOKIES", new HashSet<String>());
            String cookie = "", jsessionid = "";
            for (String header : originalResponse.headers("Set-Cookie")) {
                Log.e(TAG, "intercept: header: " + header);
                jsessionid = (header.split(";"))[0];
                Log.e(TAG, "intercept: jsessionid :" + jsessionid);
                String jsessionid1 = jsessionid.split("=")[0];
                Log.e(TAG, "intercept:jsessionid1 :" + jsessionid1);
                if (jsessionid1.equals("sessionid") || jsessionid1.equals("django_language")) {
                    if (cookie.length() > 0)
                        cookie = cookie + ";" + header.split(";")[0];
                    else
                        cookie = header.split(";")[0];
                }
            }
        //    list.add(cookie);
            Log.e(TAG, "intercept: listCookies : " + cookie);
            Utilities.setSPstring(context, Utilities.cookie, cookie);
        /*    SharedPreferences.Editor memes = PreferenceManager.getDefaultSharedPreferences(context).edit();
            memes.putStringSet("PREF_COOKIES", cookies).apply();
            memes.commit();*/
        }

        return originalResponse;
    }
}
