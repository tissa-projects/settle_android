package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class RegisterModel implements Serializable {

    public String fname;
    public String lname;
    public String email_id;
    public String password;

    public RegisterModel(String fname, String lname, String email_id, String password) {
        this.fname = fname;
        this.lname = lname;
        this.email_id = email_id;
        this.password = password;
    }
}
