package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class UploadSpaceKYCDocModel implements Serializable {

    private String id_proof;
    private String space_proof;
    private String id_proof_extension;
    private String space_proof_extension;
    private String space_id;
    private String user_id;
    private String auth_token;

    public UploadSpaceKYCDocModel(String id_proof, String space_proof,String id_proof_extension, String space_proof_extension, String space_id,String user_id, String auth_token) {
        this.id_proof = id_proof;
        this.space_proof = space_proof;
        this.id_proof_extension = id_proof_extension;
        this.space_proof_extension = space_proof_extension;
        this.space_id = space_id;
        this.user_id = user_id;
        this.auth_token = auth_token;
    }
}
