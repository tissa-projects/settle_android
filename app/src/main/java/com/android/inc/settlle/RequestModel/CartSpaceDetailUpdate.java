package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class CartSpaceDetailUpdate implements Serializable {


    private String space_cart_id;
    private String unique_id;
    private String space_id;
    private String user_id;
    private String event_id;
    private String guest_id;
    private String startdate;
    private String enddate;
    private String start_time;
    private String end_time;
    private String amount;
    private String auth_token;

    public CartSpaceDetailUpdate(String space_cart_id, String unique_id, String space_id,
                                 String user_id, String event_id, String guest_id, String startdate, String enddate,
                                 String start_time, String end_time, String amount, String auth_token)
    {
        this.space_cart_id = space_cart_id;
        this.unique_id = unique_id;
        this.space_id = space_id;
        this.user_id = user_id;
        this.event_id = event_id;
        this.guest_id = guest_id;
        this.startdate = startdate;
        this.enddate = enddate;
        this.start_time = start_time;
        this.end_time = end_time;
        this.amount = amount;
        this.auth_token = auth_token;
    }
}
