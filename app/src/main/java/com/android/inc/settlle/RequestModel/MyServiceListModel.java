package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class MyServiceListModel implements Serializable {

    public String user_id;
    public String auth_token;
    public int page;
    private int limit;
    private int filter;

    public MyServiceListModel(String user_id, String auth_token, int page, int limit,int filter) {
        this.user_id = user_id;
        this.auth_token = auth_token;
        this.page = page;
        this.limit = limit;
        this.filter = filter;
    }
}
