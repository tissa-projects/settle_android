package com.android.inc.settlle.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AllImagesAdapter extends PagerAdapter {

    private final Context context;
    private final ArrayList<String> img_urls;

    public AllImagesAdapter(Context context, ArrayList<String> img_urls)
    {
        this.context=context;
        this.img_urls=img_urls;
    }

    @Override
    public int getCount() {
        return img_urls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view==o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageview=new ImageView(context);
        Picasso.get()
                .load(img_urls.get(position))
           //     .fit()
//                .centerCrop()
                .into(imageview);
        container.addView(imageview);

        return  imageview;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((View)object);
    }
}
