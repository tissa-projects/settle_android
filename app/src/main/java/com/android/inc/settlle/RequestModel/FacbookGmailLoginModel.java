package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class FacbookGmailLoginModel implements Serializable {

    private String email_id;
    private String fname;
    private String lname;
    private String login_by;

    public FacbookGmailLoginModel(String email_id, String fname, String lname, String login_by) {
        this.email_id = email_id;
        this.fname = fname;
        this.lname = lname;
        this.login_by = login_by;
    }
}
