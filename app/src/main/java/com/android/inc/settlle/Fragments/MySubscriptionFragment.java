package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.Service.ServiceRenewActivity;
import com.android.inc.settlle.Activities.SpaceRenewActivity;
import com.android.inc.settlle.Adapter.ServiceSubscriptionAdapter;
import com.android.inc.settlle.Adapter.SpaceSubscriptionAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.MyListModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySubscriptionFragment extends Fragment implements View.OnClickListener {

    private TextView btnSpaceSubscribe, btnServiceSubscribe;
    private RecyclerView recyclerSpace, recyclerService;
    private ShimmerFrameLayout shimmerFrameLayout;
    View view;
    Context context;
    private RecyclerView.LayoutManager layoutManagerSpace, layoutManagerService;
    SpaceSubscriptionAdapter spaceSubscriptionAdapter = null;
    ServiceSubscriptionAdapter serviceSubscriptionAdapter = null;

    ProgressBar progressBar;
    int spacePage = 1, servicePage = 1;
    private boolean isSpacePageAvailable, isServicePageAvailable;
    //variables for pagination
    private boolean isLoading = true;
    private int spacePastVariableItems, spaceVisibleItemCount, spaceTotalItemCount, spacePrivious_total = 0;
    private int servicePastVariableItems, serviceVisibleItemCount, serviceTotalItemCount, servicePrivious_total = 0;
    private final int spaceView_threshold = 3;
    private final int serviceView_threshold = 3;

    private ImageView imgAccount1, imgAccount2, imgAccount3, imgAccount4;
    private TextView txtAccount1, txtAccount2, txtAccount3, txtAccount4;
    private LinearLayout llAccountbtn1, llAccountbtn2, llAccountbtn3, llAccountbtn4;


    private static final String TAG = MySubscriptionFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_subscription, container, false);
        ImageView editButton = requireActivity().findViewById(R.id.account_button_edit);
        if (editButton.getVisibility() == View.VISIBLE) {
            editButton.setVisibility(View.GONE);
        }
        context = getActivity();
        initialize();

        if (VU.isConnectingToInternet(context)) {
            getSpaceSubscriptionList();
            getServiceSubscriptionList();
        }
        return view;
    }

    private void initialize() {

        ImageView txtEdit = requireActivity().findViewById(R.id.account_button_edit);
        txtEdit.setVisibility(View.GONE);

        btnServiceSubscribe = view.findViewById(R.id.btn_service_subscribe);
        btnSpaceSubscribe = view.findViewById(R.id.btn_space_subscribe);
        recyclerService = view.findViewById(R.id.recycler_account_service_subscription);
        recyclerSpace = view.findViewById(R.id.recycler_account_space_subscription);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container_subscription);
        progressBar = view.findViewById(R.id.progress_bar);

        layoutManagerSpace = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerSpace.setLayoutManager(layoutManagerSpace);
        layoutManagerService = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerService.setLayoutManager(layoutManagerService);

        spaceSubscriptionAdapter = new SpaceSubscriptionAdapter(context);
        recyclerSpace.setAdapter(spaceSubscriptionAdapter);
        serviceSubscriptionAdapter = new ServiceSubscriptionAdapter(context);
        recyclerService.setAdapter(serviceSubscriptionAdapter);

        spaceAdapterClicks();
        serviceAdapterClicks();
        spaceSubscriptionVisibility();

        btnServiceSubscribe.setOnClickListener(this);
        btnSpaceSubscribe.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActivityView();
    }

    private void initializeActivityView() {

        imgAccount1 = requireActivity().findViewById(R.id.img_account1);
        imgAccount2 = requireActivity().findViewById(R.id.img_account2);
        imgAccount3 = requireActivity().findViewById(R.id.img_account3);
        imgAccount4 = requireActivity().findViewById(R.id.img_account4);

        txtAccount1 = requireActivity().findViewById(R.id.txt_account1);
        txtAccount2 = requireActivity().findViewById(R.id.txt_account2);
        txtAccount3 = requireActivity().findViewById(R.id.txt_account3);
        txtAccount4 = requireActivity().findViewById(R.id.txt_account4);

        llAccountbtn1 = requireActivity().findViewById(R.id.account_button1);
        llAccountbtn2 = requireActivity().findViewById(R.id.account_button2);
        llAccountbtn3 = requireActivity().findViewById(R.id.account_button3);
        llAccountbtn4 = requireActivity().findViewById(R.id.account_button4);

        accountBtn2Click();
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void accountBtn2Click() {
        imgAccount2.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtAccount2.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgAccount3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount1.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount1.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount4.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount4.setTextColor(getResources().getColorStateList(R.color.grey_60));
        llAccountbtn1.setClickable(true);
        llAccountbtn2.setClickable(false);
        llAccountbtn3.setClickable(true);
        llAccountbtn4.setClickable(true);

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_space_subscribe:
                spaceSubscriptionVisibility();
                break;
            case R.id.btn_service_subscribe:
                ServiceSubscriptionVisibility();
                break;
        }
    }


    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceSubscriptionVisibility() {
        btnSpaceSubscribe.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        btnSpaceSubscribe.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        btnServiceSubscribe.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        btnServiceSubscribe.setTextColor(getResources().getColorStateList(R.color.white));
        recyclerSpace.setVisibility(View.VISIBLE);
        recyclerService.setVisibility(View.GONE);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void ServiceSubscriptionVisibility() {
        btnServiceSubscribe.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        btnServiceSubscribe.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        btnSpaceSubscribe.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        btnSpaceSubscribe.setTextColor(getResources().getColorStateList(R.color.white));
        recyclerService.setVisibility(View.VISIBLE);
        recyclerSpace.setVisibility(View.GONE);
    }


    //get space subcription list
    private void getSpaceSubscriptionList() {
        if (spacePage == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceSubcriptionList(new MyListModel(Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken),
                spacePage, Utilities.LIMIT));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String apiResponse = response.body().string();
                    Log.e(TAG, "onResponse: " + apiResponse);
                    JSONObject jsonObject = new JSONObject(apiResponse);
                    Log.e(TAG, "getSpaceSubscriptionList : onResponse:  " + jsonObject);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            isSpacePageAvailable = jsonObject.getBoolean("count");
                            JSONArray dataArray = jsonObject.getJSONArray("data");
                            spaceSubscriptionAdapter.setData(dataArray);
                        } else {
                            //handle failed logic here
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    spacePage--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                spacePage--;

            }
        });

    }

    //get service subcription list
    private void getServiceSubscriptionList() {
        if (servicePage == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServiceSubcriptionList(new MyListModel(Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken),
                servicePage, Utilities.LIMIT));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String apiResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(apiResponse);
                    Log.e(TAG, "getServiceSubscriptionList : onResponse:  " + jsonObject);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            JSONArray dataArray = jsonObject.getJSONArray("data");
                            isServicePageAvailable = jsonObject.getBoolean("count");
                            serviceSubscriptionAdapter.setData(dataArray);
                        }
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    servicePage--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                servicePage--;

            }
        });

    }

    private void spaceAdapterClicks() {
        spaceSubscriptionAdapter.setOnRenewClickListener((view, position, jsonObject) -> {
            try {
                String strSubscriptionId = jsonObject.getString("subscription_id");
                String strPlanId = jsonObject.getString("plan_id");
                String strAmt = jsonObject.getString("amount");
                String strStartOn = jsonObject.getString("starts_on");
                String strEndOn = jsonObject.getString("ends_on");
                String strUniqueId = jsonObject.getString("uni_id");

                Intent intent = new Intent(context, SpaceRenewActivity.class);
                intent.putExtra("subscriptionId", strSubscriptionId);
                intent.putExtra("planId", strPlanId);
                intent.putExtra("Amt", strAmt);
                intent.putExtra("startsOn", strStartOn);
                intent.putExtra("endsOn", strEndOn);
                intent.putExtra("uniqueId", strUniqueId);
                startActivity(intent);
            } catch (JSONException e) {
                Log.e(TAG, "spaceAdapterCall: catch: " + e.getMessage());
            }
        });

        recyclerSpace.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                spaceVisibleItemCount = layoutManagerSpace.getChildCount();
                spaceTotalItemCount = layoutManagerSpace.getItemCount();
                spacePastVariableItems = ((LinearLayoutManager) layoutManagerSpace).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (spaceTotalItemCount > spacePrivious_total) {
                            isLoading = false;
                            spacePrivious_total = spaceTotalItemCount;
                        }
                    }

                    if (!isLoading && (spaceTotalItemCount - spaceVisibleItemCount) <= (spacePastVariableItems + spaceView_threshold)) {
                        if (isSpacePageAvailable) {
                            spacePage++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getSpaceSubscriptionList();
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }
            }
        });

    }

    private void serviceAdapterClicks() {
        serviceSubscriptionAdapter.setOnRenewClickListener((v, position, jsonObject) -> {
            try {
                String strSubscriptionId = jsonObject.getString("subscription_id");
                String strPlanId = jsonObject.getString("plan_id");
                String strAmt = jsonObject.getString("amount");
                String strStartOn = jsonObject.getString("starts_on");
                String strEndOn = jsonObject.getString("ends_on");
                String strUniqueId = jsonObject.getString("uni_id");
                Intent intent = new Intent(context, ServiceRenewActivity.class);
                intent.putExtra("subscriptionId", strSubscriptionId);
                intent.putExtra("planId", strPlanId);
                intent.putExtra("Amt", strAmt);
                intent.putExtra("startsOn", strStartOn);
                intent.putExtra("endsOn", strEndOn);
                intent.putExtra("uniqueId", strUniqueId);
                startActivity(intent);
            } catch (JSONException e) {
                Log.e(TAG, "serviceAdapterClicks : catch: " + e.getMessage());
            }
        });

        recyclerService.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                serviceVisibleItemCount = layoutManagerService.getChildCount();
                serviceTotalItemCount = layoutManagerService.getItemCount();
                servicePastVariableItems = ((LinearLayoutManager) layoutManagerService).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (serviceTotalItemCount > servicePrivious_total) {
                            isLoading = false;
                            servicePrivious_total = serviceTotalItemCount;
                        }
                    }

                    if (!isLoading && (serviceTotalItemCount - serviceVisibleItemCount) <= (servicePastVariableItems + serviceView_threshold)) {
                        if (isServicePageAvailable) {
                            servicePage++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getServiceSubscriptionList();
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        shimmerFrameLayout.stopShimmerAnimation();
        shimmerFrameLayout.setVisibility(View.GONE);
    }
}
