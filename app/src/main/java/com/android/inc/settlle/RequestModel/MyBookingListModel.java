package com.android.inc.settlle.RequestModel;

public class MyBookingListModel {
    private  String user_id;
    private String auth_token;
    private int page ;
    private  int limit;
    private int filter;

    public MyBookingListModel(String user_id, String auth_token, int page, int limit,int filter) {
        this.user_id = user_id;
        this.auth_token = auth_token;
        this.page = page;
        this.limit = limit;
        this.filter = filter;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
