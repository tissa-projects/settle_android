package com.android.inc.settlle.RequestModel;

import java.util.List;

public class SpaceMultipleImagesModel {

    private List<String> space_image;
    private  String auth_token;
    private  String user_id;
    private  String insert_id;


    public SpaceMultipleImagesModel(List<String> space_image, String auth_token, String user_id, String insert_id) {
        this.space_image = space_image;
        this.auth_token = auth_token;
        this.user_id = user_id;
        this.insert_id = insert_id;
    }

    public List<String> getSpace_image() {
        return space_image;
    }

    public void setSpace_image(List<String> space_image) {
        this.space_image = space_image;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getInsert_id() {
        return insert_id;
    }

    public void setInsert_id(String insert_id) {
        this.insert_id = insert_id;
    }
}
