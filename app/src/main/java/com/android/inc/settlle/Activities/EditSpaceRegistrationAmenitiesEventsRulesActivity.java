package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.EditSpaceAmenitiesAdapter;
import com.android.inc.settlle.Adapter.EditSpaceEventsAdapter;
import com.android.inc.settlle.Adapter.EditSpaceRulesAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AmenitiesRulesModel;
import com.android.inc.settlle.RequestModel.AuthModel;
import com.android.inc.settlle.RequestModel.EventModel;
import com.android.inc.settlle.RequestModel.SpaceRegistartionDetailsModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSpaceRegistrationAmenitiesEventsRulesActivity extends AppCompatActivity implements View.OnClickListener {

    public String space_title, SpaceId, UniqueId, venue_id, subscription_id, user_id, guest, country, state, city, address,
            latitude, longitude, zip_code, landmark, accommodate, description, discount, price_type, from_time,
            to_time, auth_token, venueName;
    private Context context;
    private ArrayList<Integer> days = null;
    private Dialog SpaceRigisDialog;
    private RecyclerView recyclerViewEvent, recyclerViewAmenities, recyclerViewRules;
    private ArrayList<String> ruleNameList = null, ruleIdlist = null, eventNameList = null, eventIdList = null, eventPriceList = null, amenitiesNamelist = null, amenitiesIdlist = null;
    private EditSpaceAmenitiesAdapter editSpaceAmenitiesAdapter = null;
    private EditSpaceRulesAdapter editSpaceRulesAdapter = null;
    private EditSpaceEventsAdapter editSpaceEventAdapter = null;
    private ArrayList<String> newEventIds = null, newEventPrice = null, newAmenitiesId = null, newRulesId = null, newSpaceEventsId = null;
    private final ArrayList<AmenitiesRulesModel> rulesList = new ArrayList<>();
    private final ArrayList<AmenitiesRulesModel> amenitiesList = new ArrayList<>();
    private ArrayList<String> selectedamenitiesArray = null, selectedRulesArray = null;
    private JSONArray spaceEventArray;
    private View layout;
    private static final String TAG = EditSpaceRegistrationAmenitiesEventsRulesActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_space_registration_amenities_events_rules);
        context = EditSpaceRegistrationAmenitiesEventsRulesActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        try {
            getIntentData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initialize();
        initRecyclerAmenities();
        initRecyclerRules();
        initRecyclerEvent();
        if (VU.isConnectingToInternet(context)) {
            getEventsAmenitesRules();
        }
    }

    private void initialize() {
        recyclerViewEvent = findViewById(R.id.recycler_add_space_event);
        recyclerViewAmenities = findViewById(R.id.recycler_add_space_amenities);
        recyclerViewRules = findViewById(R.id.recycler_add_space_rules);

        findViewById(R.id.btn_next_space_regis_ame_event_rules).setOnClickListener(this);
        //  findViewById(R.id.back_arrow).setOnClickListener(this);

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Edit Space Rules And Amenities");

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_next_space_regis_ame_event_rules) {
            addAdaptersData();
            if (validate()) {
                setEditSpaceRegistrationData();
            }
        }
    }

    public boolean validate() {
        if (newEventIds.size() == 0 || newEventPrice.size() == 0) {
            CustomToast.custom_Toast(context, "Select Atleast One Event And Add Price", layout);
            return false;
        } else if (newEventIds.size() > newEventPrice.size()) {
            CustomToast.custom_Toast(context, "SomeWhere You Forgot To Add Price.Please Add", layout);
            return false;
        } else if (newAmenitiesId.size() == 0) {
            CustomToast.custom_Toast(context, "Select Atleast One Amenities", layout);
            return false;
        } else if (newRulesId.size() == 0) {
            CustomToast.custom_Toast(context, "Select Atleast One Rule", layout);
            return false;
        }
        return true;
    }

    private void addAdaptersData() {
        Map<Integer, EventModel> eventArrayList = editSpaceEventAdapter.getEventList();
        Map<Integer, AmenitiesRulesModel> amenitiesArrayList = editSpaceAmenitiesAdapter.getAmenitiesList();
        Map<Integer, AmenitiesRulesModel> rulesArrayList = editSpaceRulesAdapter.getRuleList();

        newEventIds = new ArrayList<>();
        newEventPrice = new ArrayList<>();
        newAmenitiesId = new ArrayList<>();
        newRulesId = new ArrayList<>();

        for (int key : eventArrayList.keySet()) {
            EventModel eventModel = eventArrayList.get(key);
            assert eventModel != null;
            String id = eventModel.getEventNameId();
            String price = eventModel.getEventPrice();
            Log.e(TAG, "onClick: events: " + id + "  " + price);
            if (id != null || price != null) {
                newEventIds.add(id);
                newEventPrice.add(price);
            }
        }

        for (int key : amenitiesArrayList.keySet()) {
            AmenitiesRulesModel amenitiesModel = amenitiesArrayList.get(key);
            assert amenitiesModel != null;
            String id = amenitiesModel.getAmenitiesRulesNameId();
            Log.e(TAG, "onClick: amenities: " + id + "  ");
            if (id != null) {
                newAmenitiesId.add(id);
            }
        }

        for (int key : rulesArrayList.keySet()) {
            AmenitiesRulesModel rulesModel = rulesArrayList.get(key);
            assert rulesModel != null;
            String id = rulesModel.getAmenitiesRulesNameId();
            Log.e(TAG, "onClick: rules: " + id + "  ");
            if (id != null) {
                newRulesId.add(id);
            }
        }
    }


    private void getIntentData() throws JSONException {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            space_title = extras.getString("serviceProvider");
            venue_id = extras.getString("venue_id");
            guest = extras.getString("guest");
            country = extras.getString("country");
            state = extras.getString("state");
            city = extras.getString("city");
            address = extras.getString("address");
            latitude = extras.getString("latitude");
            longitude = extras.getString("longitude");
            zip_code = extras.getString("zip_code");
            landmark = extras.getString("landmark");
            accommodate = extras.getString("accommodate");
            description = extras.getString("description");
            discount = extras.getString("discount");
            price_type = extras.getString("price_type");
            from_time = extras.getString("from_time");
            to_time = extras.getString("to_time");
            SpaceId = extras.getString("SpaceId");
            UniqueId = extras.getString("UniqueId");
            venueName = extras.getString("venue_name");
            subscription_id = extras.getString("subscription_id");
            days = extras.getIntegerArrayList("daysId");
            selectedamenitiesArray = extras.getStringArrayList("amenitiesId");
            selectedRulesArray = extras.getStringArrayList("rulesId");
            String array = extras.getString("spaceEvent");

            spaceEventArray = new JSONArray(array);
            newSpaceEventsId = new ArrayList<>();
            for (int i = 0; i < spaceEventArray.length(); i++) {
                newSpaceEventsId.add(i, spaceEventArray.getJSONObject(i).getString("space_event_id"));
            }
        }

        Log.e(TAG, "getIntentData: " + space_title + " " + venue_id + " " + guest + " " + country + " " + state + " " + city + "\n " +
                address + latitude + longitude + zip_code + landmark + accommodate + description + discount + price_type + from_time + to_time + days + "\n" +
                selectedRulesArray + " " + selectedamenitiesArray + "\n" + spaceEventArray + "\n" + SpaceId + "  " + UniqueId);
    }

    //get Events amenities and rules
    private void getEventsAmenitesRules() {
        ruleNameList = new ArrayList<>();
        ruleIdlist = new ArrayList<>();
        eventNameList = new ArrayList<>();
        eventIdList = new ArrayList<>();
        eventPriceList = new ArrayList<>();
        amenitiesNamelist = new ArrayList<>();
        amenitiesIdlist = new ArrayList<>();
        SpaceRigisDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "getEventsAmenitesRules: " + authToken);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getEventsAmenitesRules(new AuthModel(authToken));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceRigisDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: getEventsAmenitesRules : " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String strStatusMsg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            JSONArray rulesArray = dataObj.getJSONArray("rules");
                            JSONArray amenitiesArray = dataObj.getJSONArray("amenities");
                            for (int i = 0; i < rulesArray.length(); i++) {
                                ruleNameList.add(rulesArray.getJSONObject(i).getString("rules_name"));
                                ruleIdlist.add(rulesArray.getJSONObject(i).getString("rules_id"));
                            }
                            // set rules Adapter
                            addRulesAdapter();

                            for (int j = 0; j < spaceEventArray.length(); j++) {
                                eventNameList.add(spaceEventArray.getJSONObject(j).getString("event_name"));
                                eventIdList.add(spaceEventArray.getJSONObject(j).getString("event_id"));
                                eventPriceList.add(spaceEventArray.getJSONObject(j).getString("event_price"));
                            }
                            //set event adapter
                            addEventsAdapter();

                            for (int k = 0; k < amenitiesArray.length(); k++) {
                                amenitiesNamelist.add(amenitiesArray.getJSONObject(k).getString("amenities_name"));
                                amenitiesIdlist.add(amenitiesArray.getJSONObject(k).getString("amenities_id"));
                            }
                            //set Amenities adapter
                            addAmenitiesAdapter();

                        } else {
                            Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Log.e(TAG, "onResponse: rules: " + ruleNameList + " " + ruleIdlist + " events : " + eventNameList + " " + eventIdList + " amenities: " + amenitiesNamelist + " " + amenitiesIdlist);
                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                SpaceRigisDialog.dismiss();
            }
        });
    }

    private void initRecyclerAmenities() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerViewAmenities.setLayoutManager(layoutManager);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addAmenitiesAdapter() {

        for (int i = 0; i < amenitiesIdlist.size(); i++) {
            boolean isAmenitySelected = false;
            for (int j = 0; j < selectedamenitiesArray.size(); j++) {
                if (amenitiesIdlist.get(i).equals(selectedamenitiesArray.get(j))) {
                    isAmenitySelected = true;
                }
            }
            amenitiesList.add(new AmenitiesRulesModel(amenitiesNamelist.get(i), amenitiesIdlist.get(i), isAmenitySelected));
        }

        editSpaceAmenitiesAdapter = new EditSpaceAmenitiesAdapter(amenitiesNamelist, amenitiesIdlist, amenitiesList);
        recyclerViewAmenities.setAdapter(editSpaceAmenitiesAdapter);
        editSpaceAmenitiesAdapter.notifyDataSetChanged();

        editSpaceAmenitiesAdapter.setOnItemClickListener();
    }

    private void initRecyclerRules() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerViewRules.setLayoutManager(layoutManager);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addRulesAdapter() {

        for (int i = 0; i < ruleIdlist.size(); i++) {
            boolean isruleSelected = false;
            for (int j = 0; j < selectedRulesArray.size(); j++) {
                if (ruleIdlist.get(i).equals(selectedRulesArray.get(j))) {
                    isruleSelected = true;
                }
            }

            rulesList.add(new AmenitiesRulesModel(ruleNameList.get(i), ruleIdlist.get(i), isruleSelected));
        }
        editSpaceRulesAdapter = new EditSpaceRulesAdapter(ruleNameList, ruleIdlist, rulesList);
        recyclerViewRules.setAdapter(editSpaceRulesAdapter);
        editSpaceRulesAdapter.notifyDataSetChanged();

        editSpaceRulesAdapter.setOnItemClickListener();
    }

    private void initRecyclerEvent() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerViewEvent.setLayoutManager(layoutManager);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addEventsAdapter() {

        editSpaceEventAdapter = new EditSpaceEventsAdapter(eventNameList, eventIdList, eventPriceList);
        recyclerViewEvent.setAdapter(editSpaceEventAdapter);
        editSpaceEventAdapter.notifyDataSetChanged();

        editSpaceEventAdapter.setOnItemClickListener();
    }


    //Get releted Data
    private void setEditSpaceRegistrationData() {
        //  String phone = Utilities.getSPstringValue(context,Utilities.spMobileNo);
        String phone = "9673140477";
        Log.e(TAG, "setSpaceRegistrationData: " + newEventPrice);

        //subscription_id = Utilities.getSPstringValue(context, Utilities.spSubscriptionId);
        user_id = Utilities.getSPstringValue(context, Utilities.spUserId);
        auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "setEditSpaceRegistrationData: country : " + country + " city : " + city + " state : " + state);
        Log.e(TAG, "setEditSpaceRegistrationData: newEventIds : " + newEventIds + "\n" + " newAmenitiesId : " + newAmenitiesId + "\n" + " newRulesId : " + newRulesId + "\n" + " newEventPrice : " + newEventPrice);
        SpaceRigisDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().setEditSpaceData(new SpaceRegistartionDetailsModel(space_title, phone, venue_id, venueName, subscription_id,
                user_id, guest, country, state, city, address, latitude, longitude, zip_code, landmark, accommodate, description, discount,
                price_type, from_time, to_time, auth_token, days, newEventIds, newEventPrice, newAmenitiesId, newRulesId, newSpaceEventsId, UniqueId, SpaceId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceRigisDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        statusAlert("Information", Obj.getString("message"), "Success");
                    } else {
                        statusAlert("Alert", Obj.getString("message"), "Error");
                        // Toast.makeText(EditSpaceRegistrationAmenitiesEventsRulesActivity.this, Obj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    statusAlert("Alert", context.getResources().getString(R.string.network_error), "Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                SpaceRigisDialog.dismiss();
            }
        });
    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        Intent intent = new Intent(context, EditSpaceImagesActivity.class);
                        intent.putExtra("uniqueId", UniqueId);
                        intent.putExtra("spaceId", SpaceId);
                        startActivity(intent);
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(context, EditSpaceRegistrationDetailsActivity.class);
        intent.putExtra("spaceId", SpaceId);
        intent.putExtra("uniqueId", UniqueId);
        startActivity(intent);
        finish();
    }
}
