package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.inc.settlle.Activities.Service.ViewServiceBookingActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyServiceRequestAdapter extends RecyclerView.Adapter<MyServiceRequestAdapter.MyViewHolder> {


    private final Context context;
    private JSONArray dataArray;
    private static final String TAG = MyServiceListAdapter.class.getSimpleName();

    public MyServiceRequestAdapter(@NonNull Context context) {
        this.context = context;
        dataArray = new JSONArray();

    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_service_request_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {


        try {
            String provider = dataArray.getJSONObject(position).getString("title");
            String bookedBy = dataArray.getJSONObject(position).getString("fname") + " " + dataArray.getJSONObject(position).getString("lname");
            String bookingDate = dataArray.getJSONObject(position).getString("startdate");
            String bookingStatusSode = dataArray.getJSONObject(position).getString("chk_status");
            String service_name = dataArray.getJSONObject(position).getString("service_name");

            switch (bookingStatusSode) {
                case "0":
                    myViewHolder.bookingStatus.setText("New request");
                    myViewHolder.bookingStatus.setTextColor(Color.RED);
                    break;
                case "1":
                    myViewHolder.bookingStatus.setText("Confirmed");
                    myViewHolder.bookingStatus.setTextColor(Color.parseColor("#ff9500"));
                    break;
                case "2":
                    myViewHolder.bookingStatus.setText("Completed");
                    myViewHolder.bookingStatus.setTextColor(Color.parseColor("#0b5711"));
                    break;
                default:
                    myViewHolder.bookingStatus.setText("Canceled");
                    myViewHolder.bookingStatus.setTextColor(Color.BLACK);
                    break;
            }
            String imgName = dataArray.getJSONObject(position).getString("image_name");
            String imgUrl = Utilities.IMG_SERVICE_URL + imgName;

            myViewHolder.providerName.setText( provider);
            myViewHolder.venueText.setText( service_name);
            myViewHolder.bookedBy.setText(bookedBy);
            myViewHolder.bookingDate.setText(bookingDate);
            Tools.displayImageOriginalString(myViewHolder.img, imgUrl);


            myViewHolder.btnViewBooking.setOnClickListener(v -> {

                try {
                    String bookingid = dataArray.getJSONObject(position).getString("service_booking_id");
                    Intent intent = new Intent(context, ViewServiceBookingActivity.class);
                    intent.putExtra("booking_id", bookingid);
                    intent.putExtra("unique_id", dataArray.getJSONObject(position).getString("uni_id"));
                    intent.putExtra("type", "service");
                    context.startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray jsonArray) {

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("image_name", jsonArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("title", jsonArray.getJSONObject(i).getString("title"));
                jsonObject.put("fname", jsonArray.getJSONObject(i).getString("fname"));
                jsonObject.put("lname", jsonArray.getJSONObject(i).getString("lname"));
                jsonObject.put("startdate", jsonArray.getJSONObject(i).getString("startdate"));
                jsonObject.put("chk_status", jsonArray.getJSONObject(i).getString("chk_status"));
                jsonObject.put("service_booking_id", jsonArray.getJSONObject(i).getString("service_booking_id"));
                jsonObject.put("uni_id", jsonArray.getJSONObject(i).getString("uni_id"));
                jsonObject.put("service_name", jsonArray.getJSONObject(i).getString("service_name"));
                this.dataArray.put(jsonObject);
            }
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {


        TextView providerName, bookedBy, bookingDate, bookingStatus, venueText;
        CardView cardView;
        ImageView img;
        AppCompatButton btnViewBooking;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            this.img = itemView.findViewById(R.id.service_request_img);
            this.providerName = itemView.findViewById(R.id.service_request_service_provider_name);
            this.bookedBy = itemView.findViewById(R.id.service_request_booked_by);
            this.bookingDate = itemView.findViewById(R.id.service_request_booking_date);
            this.bookingStatus = itemView.findViewById(R.id.service_request_status);
            this.cardView = itemView.findViewById(R.id.cardview_my_space_request);
            this.btnViewBooking = itemView.findViewById(R.id.service_request_btn_view_booking);
            this.venueText = itemView.findViewById(R.id.selected_venue_text);

        }
    }

    public void clearData() {
        this.dataArray = null;
        this.dataArray = new JSONArray();
    }
}
