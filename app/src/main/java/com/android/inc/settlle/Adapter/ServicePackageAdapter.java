package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.inc.settlle.R;

import org.json.JSONArray;

public class ServicePackageAdapter extends RecyclerView.Adapter<ServicePackageAdapter.MyViewHolder> {
    private final JSONArray dataArray;

    public ServicePackageAdapter(JSONArray dataArray) {
        this.dataArray = dataArray;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_service_package_layout, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        try {
            //String rule= ;
            myViewHolder.pkg_name_text.setText("" + dataArray.getJSONObject(i).getString("package_name"));
            myViewHolder.pkg_price_text.setText("" + dataArray.getJSONObject(i).getString("package_price"));
            myViewHolder.pkg_descriptionText.setText("" + dataArray.getJSONObject(i).getString("package_description"));
            myViewHolder.serviceName.setText(dataArray.getJSONObject(i).getString("service_name"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView pkg_name_text, pkg_price_text, pkg_descriptionText, serviceName;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.pkg_name_text = itemView.findViewById(R.id.service_pkg_name);
            this.pkg_price_text = itemView.findViewById(R.id.service_pkg_price);
            this.pkg_descriptionText = itemView.findViewById(R.id.service_pkg_description);
            this.serviceName = itemView.findViewById(R.id.serviceName);

        }
    }
}