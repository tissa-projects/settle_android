package com.android.inc.settlle.RequestModel;

import java.io.Serializable;
import java.util.ArrayList;

public class UpdateServiceDesModel implements Serializable {
    String service_details_id;
    String unique_id;
    String company_name;
    String mobile;
    String country;
    String state;
    String city;
    String guest;
    String user_id;
    String address;
    String latitude;
    String longitude;
    String landmark;
    String zip_code;
    String description;
    String price_type;
    String discount;
    String from_time;
    String to_time;
    ArrayList<Integer> package_id;
    ArrayList<String> service_type;
    ArrayList<String> package_name;
    ArrayList<String> package_description;
    ArrayList<Integer> package_price;
    ArrayList<Integer> days;
    String auth_token;

    public UpdateServiceDesModel(String service_details_id, String unique_id, String company_name, String mobile, String country,
                                 String state, String city, String guest, String user_id, String address, String latitude, String longitude,
                                 String landmark, String zip_code, String description, String price_type, String discount, String from_time,
                                 String to_time, ArrayList<Integer> package_id, ArrayList<String> service_type,
                                 ArrayList<String> package_name, ArrayList<String> package_description, ArrayList<Integer> package_price,
                                 ArrayList<Integer> days, String auth_token) {
        this.service_details_id = service_details_id;
        this.unique_id = unique_id;
        this.company_name = company_name;
        this.mobile = mobile;
        this.country = country;
        this.state = state;
        this.city = city;
        this.guest = guest;
        this.user_id = user_id;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.landmark = landmark;
        this.zip_code = zip_code;
        this.description = description;
        this.price_type = price_type;
        this.discount = discount;
        this.from_time = from_time;
        this.to_time = to_time;
        this.package_id = package_id;
        this.service_type = service_type;
        this.package_name = package_name;
        this.package_description = package_description;
        this.package_price = package_price;
        this.days = days;
        this.auth_token = auth_token;


    }
}
