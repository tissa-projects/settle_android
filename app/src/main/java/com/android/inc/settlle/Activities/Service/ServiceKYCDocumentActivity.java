package com.android.inc.settlle.Activities.Service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.Settings;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.HomeActivity;
import com.android.inc.settlle.Activities.PreviewPDFImage;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.UploadServiceKYCDocModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceKYCDocumentActivity extends AppCompatActivity implements View.OnClickListener {

    TextView idProoffFileNameTV, serviceProofFileNameTV;

    AppCompatButton idProof, serviceProof;
    String docType, imgselectionType;
    ImageView idProofImageIV, serviceProofImageIV;

    String idProofDocExtension, serviceProofDocExtension;
    String proofType;
    Button sendKYCDoc;
    private String insertId;
    String idProofBase64 = "", serviceProofBase64 = "";
    Context context;
    private Dialog lodingDialog;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String[] appPermission = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };
    private static final String TAG = ServiceKYCDocumentActivity.class.getSimpleName();
    ActivityResultLauncher<Intent> activityResultLauncher;
    int selectionType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_kycdocument);
        context = ServiceKYCDocumentActivity.this;
        getIntentData();
        initOnStartActivity();
        initialize();
        checkNRequestPermission();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            insertId = bundle.getString("insertId");
            Log.e(TAG, "getIntentData : Service KYC " + insertId);
        }
    }


    private void initialize() {
        idProof = findViewById(R.id.btn_choose_identity_proof);
        serviceProof = findViewById(R.id.btn_choose_service_proof);
        idProofImageIV = findViewById(R.id.KYC_img_1);
        serviceProofImageIV = findViewById(R.id.KYC_img_2);
        sendKYCDoc = findViewById(R.id.btn_send_kyc_documents);


        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Add Service KYC Doc.");


        idProoffFileNameTV = findViewById(R.id.id_proof_file_name_text);
        serviceProofFileNameTV = findViewById(R.id.service_proof_file_name_text);


        idProof.setOnClickListener(this);
        serviceProof.setOnClickListener(this);
        sendKYCDoc.setOnClickListener(this);

        idProofImageIV.setOnClickListener(this);
        serviceProofImageIV.setOnClickListener(this);
        serviceProofFileNameTV.setOnClickListener(this);
        idProoffFileNameTV.setOnClickListener(this);

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.btn_choose_identity_proof:
                proofType = "ID_PROOF";
                documentTypeDialog();
                break;

            case R.id.btn_choose_service_proof:
                proofType = "SERVICE_PROOF";
                documentTypeDialog();
                break;


            case R.id.btn_send_kyc_documents:

                if (idProofBase64.isEmpty()) {
                    Toast.makeText(context, "Choose ID proof ", Toast.LENGTH_SHORT).show();


                } else if (serviceProofBase64.isEmpty()) {
                    Toast.makeText(context, "Choose service proof ", Toast.LENGTH_SHORT).show();
                } else {
                    if (VU.isConnectingToInternet(context)) {
                        uploadKYCDOC();
                    }
                }
                break;
            case R.id.id_proof_file_name_text:
            case R.id.KYC_img_1:

                if (idProofBitMap) {
                    callPreviewActivity(".Png", "idProofBase64", true, idProofBase64);
                } else {
                    if (idProofPath != null && idProofPath.length() > 5 && CommonFunctions.getMimeType(idProofPath) != null)
                        callPreviewActivity(CommonFunctions.getMimeType(idProofPath), idProofPath, false, "");
                    else
                        Toast.makeText(getApplicationContext(), "Please select file first or Please select PDF/Image file", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.service_proof_file_name_text:
            case R.id.KYC_img_2:
                if (idSpaceBitmap) {
                    Log.d(TAG, "openFileForRegistration: From Local 111  " + serviceProofBase64);
                    callPreviewActivity(".Png", "serviceProofBase64", true, serviceProofBase64);
                } else {
                    if (idServiceProofPath != null && idServiceProofPath.length() > 5 && CommonFunctions.getMimeType(idServiceProofPath) != null)
                        callPreviewActivity(CommonFunctions.getMimeType(idServiceProofPath), idServiceProofPath, false, "");
                    else
                        Toast.makeText(getApplicationContext(), "Please select file first or Please select PDF/Image file", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void documentTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"IMAGE", "PDF"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(this);
            docTypeAlert.setTitle("Select Document Type");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                docType = doc_type_list[which];

                //handle logic here
                if (docType.equals("IMAGE")) {
                    imageTypeDialog();
                } else if (docType.equals("PDF")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("application/pdf");
                    selectionType = Utilities.RESULT_FM;
                    activityResultLauncher.launch(intent);
                    // startActivityForResult(Intent.createChooser(intent, "Select PDF File"), RESULT_FM);
                }
            });

            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

    private void imageTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(this);
            docTypeAlert.setTitle("Add Photo!");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];

                //handle logic here

                if (imgselectionType.equals("Take Photo")) {

                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    selectionType = Utilities.RESULT_CAMERA;
                    activityResultLauncher.launch(cameraIntent);
                    // startActivityForResult(cameraIntent, RESULT_CAMERA);
                } else if (imgselectionType.equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_PICK);
                    // Sets the type as image/*. This ensures only components of type image are selected
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    selectionType = Utilities.RESULT_GALLERY;
                    activityResultLauncher.launch(intent);

                    //startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_GALLERY);
                }
            });

            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

    String idProofPath = "", idServiceProofPath = "";
    boolean idProofBitMap = false, idSpaceBitmap = false;

  /*  @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImageURI = data.getData();
                if (proofType.equalsIgnoreCase("SERVICE_PROOF")) {
                    getDataForServiceProof(selectedImageURI, "image", "SERVICE_PROOF.png");
                } else {
                    getDataForID(selectedImageURI, "image", "ID_PROOF.png");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
            if (proofType.equals("ID_PROOF")) {
                idProofDocExtension = "png";
                idProoffFileNameTV.setText("ID-PROOF.png");
                idProofBitMap = true;
                idProofBase64 = CommonFunctions.ResizedBitmapEncodeToBase64(CommonFunctions.GetResizedBitmap(bitmap, 5000));
                idProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(idProofBase64));
            } else if (proofType.equals("SERVICE_PROOF")) {
                serviceProofDocExtension = "png";
                serviceProofFileNameTV.setText("SERVICE_PROOF.png");
                idSpaceBitmap = true;
                serviceProofBase64 = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                serviceProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(serviceProofBase64));
            }
            //}
        } else if (requestCode == RESULT_FM) {
            try {
                Uri selectedImageURI = data.getData();
                if (proofType.equals("ID_PROOF")) {
                    getDataForID(selectedImageURI, "PDF", "ID_PROOF.pdf");
                }
                if (proofType.equals("SERVICE_PROOF")) {
                    getDataForServiceProof(selectedImageURI, "PDF", "SERVICE_PROOF.pdf");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
*/

    @SuppressLint("SetTextI18n")
    public void initOnStartActivity() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                if (proofType.equalsIgnoreCase("SERVICE_PROOF")) {
                                    getDataForServiceProof(selectedImageURI, "image", "SERVICE_PROOF.png");
                                } else {
                                    getDataForID(selectedImageURI, "image", "ID_PROOF.png");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Utilities.RESULT_CAMERA == selectionType) {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            if (proofType.equals("ID_PROOF")) {
                                idProofDocExtension = "png";
                                idProoffFileNameTV.setText("ID-PROOF.png");
                                idProofBitMap = true;
                                idProofBase64 = CommonFunctions.ResizedBitmapEncodeToBase64(CommonFunctions.GetResizedBitmap(bitmap, 5000));
                                idProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(idProofBase64));
                            } else if (proofType.equals("SERVICE_PROOF")) {
                                serviceProofDocExtension = "png";
                                serviceProofFileNameTV.setText("SERVICE_PROOF.png");
                                idSpaceBitmap = true;
                                serviceProofBase64 = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                                serviceProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(serviceProofBase64));
                            }
                        } else if (Utilities.RESULT_FM == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                if (proofType.equals("ID_PROOF")) {
                                    getDataForID(selectedImageURI, "PDF", "ID_PROOF.pdf");
                                }
                                if (proofType.equals("SERVICE_PROOF")) {
                                    getDataForServiceProof(selectedImageURI, "PDF", "SERVICE_PROOF.pdf");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    private void getDataForID(Uri selectedImageURI, String type, String tempFileName) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory(tempFileName, context);//CommonFunctions.createFile("ID_PROOF.pdf");
            if (fileName != null) {//&& (fileName.length() / 1024) > 200
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    idProofBase64 = CommonFunctions.GetBase64FromFile(fileName);
                    idProofPath = fileName.getPath();
                    Log.d(TAG, "onActivityResult: " + idProofPath);
                    if (type.equalsIgnoreCase("image")) {
                        idProofDocExtension = "png";
                        idProoffFileNameTV.setText(idProofPath.substring(idProofPath.lastIndexOf("/") + 1));
                        idProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(idProofBase64));
                        idProofBitMap = true;
                    } else {
                        idProofDocExtension = "pdf";
                        idProoffFileNameTV.setText(idProofPath.substring(idProofPath.lastIndexOf("/") + 1));
                        idProofImageIV.setImageResource(R.drawable.ic_pdf);
                        idProofBitMap = false;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void getDataForServiceProof(Uri selectedImageURI, String type, String tempFileName) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory(tempFileName, context);
            if (fileName != null) {
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    idServiceProofPath = fileName.getPath();
                    serviceProofBase64 = CommonFunctions.GetBase64FromFile(fileName);
                    if (type.equalsIgnoreCase("image")) {
                        serviceProofDocExtension = "png";
                        serviceProofFileNameTV.setText(idServiceProofPath.substring(idServiceProofPath.lastIndexOf("/") + 1));
                        idSpaceBitmap = true;
                        idProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(serviceProofBase64));
                    } else {
                        serviceProofDocExtension = "pdf";
                        serviceProofFileNameTV.setText(idServiceProofPath.substring(idServiceProofPath.lastIndexOf("/") + 1));
                        serviceProofImageIV.setImageResource(R.drawable.ic_pdf);
                        idSpaceBitmap = false;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void uploadKYCDOC() {

        lodingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        CommonFunctions.showLog(TAG, "uploadKYCDOC: idProofBase64 :" + idProofBase64);
        CommonFunctions.showLog(TAG, "uploadKYCDOC: serviceProofBase64 :" + serviceProofBase64);
        CommonFunctions.showLog(TAG, "uploadKYCDOC: idProofDocExtension :" + idProofDocExtension);
        CommonFunctions.showLog(TAG, "uploadKYCDOC: serviceProofDocExtension :" + serviceProofDocExtension);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().uploadServiceKYCDoc(new UploadServiceKYCDocModel(idProofBase64, serviceProofBase64, idProofDocExtension, serviceProofDocExtension,
                insertId, Utilities.getSPstringValue(context, Utilities.spUserId),
                Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                lodingDialog.dismiss();

                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getBoolean("status") && jsonObject.getString("status_code").equalsIgnoreCase("200")) {
                        statusAlert("Information", "Document uploaded successfully", "Success");
                    } else {
                        statusAlert("Alert", jsonObject.getString("message"), "Error");
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                lodingDialog.dismiss();

            }
        });
    }

    private void checkNRequestPermission() {
        //check which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String perm : appPermission) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }

        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);

        }

        //app has all permissions go ahead
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            HashMap<String, Integer> permissionResult = new HashMap<>();
            int deniedCount = 0;

            //gather permission grant result
            for (int i = 0; i < grantResults.length; i++) {

                //add only permission which denied
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResult.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            //check if all permissions are granted
            if (deniedCount == 0) {
                CommonFunctions.showLog(TAG, "onRequestPermissionsResult: ");
            }
            // atlest one or all permissions are denied
            else {
                for (Map.Entry<String, Integer> entry : permissionResult.entrySet()) {
                    String permName = entry.getKey();
                    //permission is denied (this is the first time, when "never asked  again" is not clicked)
                    //so aske again explaining the usage of permission
                    // shouldShowRequestPermission will return true
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {

                        showDialog("This app needs Camera and Storage permissions to work without any problem",
                                "Yes, Grant Permissions",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    checkNRequestPermission();
                                },
                                "No, Exit app",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    finish();
                                }, false);
                    }
                    //permission is denied (and never asked again is checked)
                    else {
                        // asked user to go to setting and manually allow  permission
                        showDialog("You have denied some permissions.Allow all permissions at [Setting] > [Permissions]",
                                "Go to Settings",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                            Uri.fromParts("package", getPackageName(), null));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                },
                                "No, Exit app",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    finish();
                                }, false);
                    }

                }
            }
        }
    }

    public void showDialog(String msg, String positiveLable, DialogInterface.OnClickListener positiveOnCLick,
                           String negativeLable, DialogInterface.OnClickListener negativeOnCLick, boolean isCancelable) {


        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("title");
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLable, positiveOnCLick);
        builder.setNegativeButton(negativeLable, negativeOnCLick);

        android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        Intent i = new Intent(context, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void callPreviewActivity(String fileType, String filePath, boolean isBitmap, String bitMap) {
        Intent intent = new Intent(getApplicationContext(), PreviewPDFImage.class);
        intent.putExtra("fileType", fileType);
        intent.putExtra("filePath", filePath);
        intent.putExtra("isBitmap", isBitmap);
        intent.putExtra("bitmap", " ");
        Utilities.TRANSFER_BITMAP_VALUE = bitMap;
        Log.d(TAG, "openFileForRegistration: From Local 222  " + bitMap);
        startActivity(intent);
    }

}
