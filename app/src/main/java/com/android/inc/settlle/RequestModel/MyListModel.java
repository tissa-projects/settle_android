package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class MyListModel implements Serializable {
    private String user_id;
    private String auth_token;
    private int page;
    private int limit;

    public MyListModel(String user_id, String auth_token, int page, int limit) {
        this.user_id = user_id;
        this.auth_token = auth_token;
        this.page = page;
        this.limit = limit;
    }
}
