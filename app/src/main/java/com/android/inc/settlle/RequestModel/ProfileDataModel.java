package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class ProfileDataModel implements Serializable {

    public String user_id;
    public String auth_token;

    public ProfileDataModel(String user_id, String auth_token) {
        this.user_id = user_id;
        this.auth_token = auth_token;
    }
}
