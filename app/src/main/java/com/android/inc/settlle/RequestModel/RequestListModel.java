package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class RequestListModel implements Serializable {

    private String user_id;
    private int filter;
    private String auth_token;
    private int page;
    private int limit;

    public RequestListModel(String user_id, int filter, String auth_token, int page, int limit) {
        this.user_id = user_id;
        this.filter = filter;
        this.auth_token = auth_token;
        this.page = page;
        this.limit = limit;

    }
}
