package com.android.inc.settlle.RequestModel;

import java.io.Serializable;
import java.util.ArrayList;

public class ServiceSubscriptionModel implements Serializable {
    public ArrayList<String> service_id;
    public String service_name;
    public String user_id;
    public String plan_id;
    public String plan_type;
    public String amount;
    public String auth_token;
    public String payment_id;

    public ServiceSubscriptionModel(ArrayList<String> service_id, String service_name, String user_id, String plan_id, String plan_type, String amount, String auth_token, String payment_id) {
        this.service_id = service_id;
        this.service_name = service_name;
        this.user_id = user_id;
        this.plan_id = plan_id;
        this.plan_type = plan_type;
        this.amount = amount;
        this.auth_token = auth_token;
        this.payment_id = payment_id;
    }

    public ArrayList<String> getService_id() {
        return service_id;
    }

    public void setService_id(ArrayList<String> service_id) {
        this.service_id = service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(String plan_type) {
        this.plan_type = plan_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }
}
