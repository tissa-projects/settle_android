package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ChangePasswordModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {


    EditText oldPswEt, newPswEt, confPswEt;
    String oldPassword, newPassword, confPassword;
    Context context = ChangePasswordActivity.this;
    private Dialog loadingDialog;

    @SuppressLint({"MissingInflatedId", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initialize();

        findViewById(R.id.change_psw_submit_button).setOnClickListener(v -> validate());
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Change Your Password");
    }

    private void initialize() {
        oldPswEt = findViewById(R.id.change_psw_old_password);
        newPswEt = findViewById(R.id.change_psw_new_password);
        confPswEt = findViewById(R.id.change_psw_conf_password);
    }


    private void validate() {
        oldPassword = oldPswEt.getText().toString();//.trim();
        newPassword = newPswEt.getText().toString().trim();
        confPassword = confPswEt.getText().toString().trim();

        if (oldPassword.isEmpty()) {
            oldPswEt.setError("Enter old password");
            oldPswEt.requestFocus();
        } else if (newPassword.isEmpty()) {
            newPswEt.setError("Enter new password");
            newPswEt.requestFocus();
        } else if (confPassword.isEmpty()) {
            confPswEt.setError("Re-enter new password");
        } else if (!newPassword.equals(confPassword)) {
            confPswEt.setError("Password mismatch");
        } else if (newPassword.length() < 8) {
            confPswEt.setError("Password length should be minimum 8 characters");
        } else {
            if (VU.isConnectingToInternet(context)) {
                changePassword();
            }
        }
    }

    private void changePassword() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        // Toast.makeText(context, ""+oldPassword+"  "+newPassword, Toast.LENGTH_SHORT).show();

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().changePassword(new ChangePasswordModel(Utilities.getSPstringValue(context, Utilities.spUserId), oldPassword, newPassword, confPassword, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    CommonFunctions.showLog("Change Password", jsonObject.toString());
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(ChangePasswordActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (statusCode == 300) {
                        Toast.makeText(ChangePasswordActivity.this, "Enter correct old password", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });


    }


}
