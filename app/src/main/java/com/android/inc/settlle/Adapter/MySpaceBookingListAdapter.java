package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class MySpaceBookingListAdapter extends RecyclerView.Adapter<MySpaceBookingListAdapter.MyViewHolder> {


    private RecyclerViewItemClickListener.IViewBookingBtn viewBookingBtn;
    private RecyclerViewItemClickListener.IReviewBtn reviewBtn;
    private final Context context;
    private JSONArray jsonArray;
    private static final String TAG = MySpaceBookingListAdapter.class.getSimpleName();

    public MySpaceBookingListAdapter(@NonNull Context context) {
        this.context = context;
        jsonArray = new JSONArray();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_my_booking_space_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint({"Range", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, @SuppressLint("RecyclerView") final int i) {

        try {
            final JSONObject jsonObject = jsonArray.getJSONObject(i);

            Log.d(TAG, "onBindViewHolder: " + jsonObject);

            String statusType;
            float rating;
            String status = jsonObject.getString("chk_status");
            String imgName = jsonObject.getString("image_name");
            String date = jsonObject.getString("startdate");
            String ratingStar = jsonObject.getString("stars");
            String isReviewed = jsonObject.getString("is_reviewed");
            String imgUrl = Utilities.IMG_SPACE_URL + imgName;

            if (status.equalsIgnoreCase("0")) {
                statusType = "In Review";  //
                myViewHolder.btnReview.setVisibility(View.GONE);
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_inreviewed));
            } else if (status.equalsIgnoreCase("1")) {
                statusType = "Confirmed";
                myViewHolder.btnReview.setVisibility(View.GONE);
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_confirmed));
            } else if (status.equalsIgnoreCase("2")) {
                statusType = "Completed";  // review visible
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_completed));
                if (isReviewed.equalsIgnoreCase("1")) {
                    myViewHolder.btnReview.setText("Reviewed");
                    myViewHolder.btnReview.setVisibility(View.GONE);
                } else {
                    myViewHolder.btnReview.setVisibility(View.VISIBLE);
                }
            } else {
                statusType = "Canceled";
                myViewHolder.btnReview.setVisibility(View.GONE);
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_cancled));
            }
            if (ratingStar.equalsIgnoreCase("N/A")) {
                rating = Float.parseFloat("0");
            } else {
                rating = Float.parseFloat(ratingStar);
            }

            myViewHolder.txtSelectedEvent.setText(jsonObject.getString("event"));
            myViewHolder.txtBookedOn.setText(date);
            myViewHolder.txtStatus.setText(statusType);
            myViewHolder.txtSpaceName.setText(jsonObject.getString("title"));
            myViewHolder.spaceRating.setRating(rating);

            Tools.displayImageOriginalString(myViewHolder.imgSpace, imgUrl);


            myViewHolder.btnReview.setOnClickListener(v -> reviewBtn.onReviewClick(v, i, jsonObject));

            myViewHolder.btnViewBooking.setOnClickListener(v -> viewBookingBtn.onViewBookingClick(v, i, jsonObject));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("chk_status", dataArray.getJSONObject(i).getString("chk_status"));
                jsonObject.put("startdate", dataArray.getJSONObject(i).getString("startdate"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("is_reviewed", dataArray.getJSONObject(i).getString("is_reviewed"));
                jsonObject.put("event", dataArray.getJSONObject(i).getString("event"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                jsonObject.put("uni_id", dataArray.getJSONObject(i).getString("uni_id"));
                jsonObject.put("space_booking_id", dataArray.getJSONObject(i).getString("space_booking_id"));
                jsonObject.put("space_id", dataArray.getJSONObject(i).getString("space_id"));
                jsonArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView txtSelectedEvent, txtBookedOn, txtStatus, txtSpaceName;
        ImageView imgSpace;
        //  Button btnViewBooking, btnReview;
        RatingBar spaceRating;
        AppCompatButton btnReview, btnViewBooking;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.cardview);
            this.txtSelectedEvent = itemView.findViewById(R.id.txt_space_event);
            this.txtBookedOn = itemView.findViewById(R.id.txt_booked_on);
            this.txtStatus = itemView.findViewById(R.id.txt_status);
            this.txtSpaceName = itemView.findViewById(R.id.txt_space_name);
            this.imgSpace = itemView.findViewById(R.id.img_my_booking);
            this.spaceRating = itemView.findViewById(R.id.space_rating);
            btnReview = itemView.findViewById(R.id.btnReview);
            btnViewBooking = itemView.findViewById(R.id.btnViewBooking);

        }
    }

    //Set method of OnItemClickListener object
    public void setOnViewBookingClickListener(RecyclerViewItemClickListener.IViewBookingBtn viewBookingBtn) {
        this.viewBookingBtn = viewBookingBtn;
    }

    //Set method of OnItemClickListener object
    public void setOnReviewClickListener(RecyclerViewItemClickListener.IReviewBtn reviewBtn) {
        this.reviewBtn = reviewBtn;
    }

    private void reviewBtnDisable(Button btn) {
        btn.setEnabled(false);
        btn.setFocusable(false);
        btn.setAlpha((float) 0.4);
    }

    public void clearData() {
        jsonArray = null;
        jsonArray = new JSONArray();
    }


}
