package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class UpdateProfileModel implements Serializable {





    private String fname;
    private String lname;
    private String mobileno;
    private String about_me;
    private String address;
    private String occupation;
    private String auth_token;
    private  String user_id;

    public UpdateProfileModel(String fname, String lname, String mobileno, String about_me, String address, String occupation, String auth_token, String user_id) {
        this.fname = fname;
        this.lname = lname;
        this.mobileno = mobileno;
        this.about_me = about_me;
        this.address = address;
        this.occupation = occupation;
        this.auth_token = auth_token;
        this.user_id = user_id;

    }
}
