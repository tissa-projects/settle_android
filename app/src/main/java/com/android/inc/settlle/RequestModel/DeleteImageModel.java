package com.android.inc.settlle.RequestModel;

public class DeleteImageModel {
    private String table;
    private String image_id;
    private String auth_token;

    public DeleteImageModel(String table, String image_id, String auth_token) {
        this.table = table;
        this.image_id = image_id;
        this.auth_token = auth_token;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
