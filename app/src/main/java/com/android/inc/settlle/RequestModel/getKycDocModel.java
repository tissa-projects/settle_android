package com.android.inc.settlle.RequestModel;

public class getKycDocModel {

    private String unique_id;
    private String table;

    public getKycDocModel(String unique_id, String table) {
        this.unique_id = unique_id;
        this.table = table;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
