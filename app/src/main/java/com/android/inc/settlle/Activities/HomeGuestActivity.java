package com.android.inc.settlle.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.Scenario.ScenarioHomeActivity;
import com.android.inc.settlle.Fragments.HomeFragment;
import com.android.inc.settlle.Menu.DrawerAdapter;
import com.android.inc.settlle.Menu.DrawerItem;
import com.android.inc.settlle.Menu.SimpleItem;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Utilities;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;

public class HomeGuestActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener {

    Context context;
    ImageView imageView;
    TextView txtHeading;
    View toolBarView;

    boolean doubleBackToExitPressedOnce = false;

    private SlidingRootNav slidingRootNav;
    private static final int POS_HOME = 0;
    private static final int POS_LOGIN = 1;
    private static final int POS_SIGN_UP = 2;
    private static final int POS_SCENARIO = 3;
    private static final int POS_ABOUT_APP_BEFORE_LOGIN = 4;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_home);
        context = HomeGuestActivity.this;
        initialize();
        //  initToolbar();
        initSideNavigation();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    private void initialize() {
        imageView = findViewById(R.id.imageView);
        txtHeading = findViewById(R.id.txt_heading);
        toolBarView = findViewById(R.id.toolbar_view);
        findViewById(R.id.ll_imageView).setOnClickListener(view -> slidingRootNav.openMenu());
    }

    private void initSideNavigation() {
        //NAVIGATION
        slidingRootNav = new SlidingRootNavBuilder(this)
                //  .withToolbarMenuToggle((toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(true)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter drawadapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_HOME).setChecked(true),
                createItemFor(POS_LOGIN),
                createItemFor(POS_SIGN_UP),
                createItemFor(POS_SCENARIO),
                createItemFor(POS_ABOUT_APP_BEFORE_LOGIN)));
        drawadapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(drawadapter);

        drawadapter.setSelected(POS_HOME);
    }

    @Override
    public void onItemSelected(int position) {
        switch (position) {

            case POS_HOME:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                /*Utilities.fragmentTransaction = getSupportFragmentManager().beginTransaction();
                Utilities.fragmentTransaction.replace(R.id.container, new HomeFragment());
                Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                Utilities.fragmentTransaction.commit();*/
                changeFragment(new HomeFragment());
                break;
            case POS_LOGIN:
                txtHeading.setVisibility(View.GONE);
                toolBarView.setVisibility(View.GONE);
                startActivity(new Intent(context, LoginActivity.class));
                break;
            case POS_SIGN_UP:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, SignUpActivity.class));
                break;

            case POS_SCENARIO:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, ScenarioHomeActivity.class));
                finish();
                break;
            case POS_ABOUT_APP_BEFORE_LOGIN:
//                txtHeading.setVisibility(View.VISIBLE);
//                toolBarView.setVisibility(View.VISIBLE);
//                changeFragment(new AboutUsFragment());

                Toast.makeText(context, "Under development...", Toast.LENGTH_SHORT).show();
                break;
        }
        slidingRootNav.closeMenu();
    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.white))
                .withTextTint(color(R.color.white))
                .withSelectedIconTint(color(R.color.white))
                .withSelectedTextTint(color(R.color.white));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitlesBeforeLogin);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIconsBeforeLogin);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    private void changeFragment(Fragment targetFragment) {
        Utilities.fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Utilities.fragmentTransaction.replace(R.id.container, targetFragment);
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Utilities.fragmentTransaction.commit();
    }


    // for gettting on activity recsult on home fragment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, intent);
        }
    }

}
