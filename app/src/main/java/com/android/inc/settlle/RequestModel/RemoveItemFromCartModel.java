package com.android.inc.settlle.RequestModel;

public class RemoveItemFromCartModel {

    private String cart_id;
    private String auth_token;

    public RemoveItemFromCartModel(String cart_id, String auth_token) {
        this.cart_id = cart_id;
        this.auth_token = auth_token;
    }
}
