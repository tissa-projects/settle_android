package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetSpaceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceDetailAndBookActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private SliderLayout sliderLayout;
    private AppCompatButton btnBookSpace;
    private String uni, uniqueId, spaceId, star;
    private Dialog loadingDialog;
    private TextView spaceNameText, desciptionText, priceTypeText, accomodatesText, noOfGuestTxt, eventTypeTxt, hostNameText, hostMobileText, hostEmail, memberSinceTxt;
    private RatingBar ratingBar;
    ArrayList<String> imagesAddress;
    private float rating;
    private String spaceLatitude, spaceLongitude, spaceName;
    private static final String TAG = SpaceDetailAndBookActivity.class.getName();
    private String selectedEventID = "", selectedEventName = "";


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_detail_and_book);
        context = SpaceDetailAndBookActivity.this;


        findViewById(R.id.back_booking_space).setOnClickListener(v -> finish());

        initialize();
        getIntentData();
        Log.e(TAG, "onCreate: " + Utilities.getSPbooleanValue(context, Utilities.spIsBookingType));
        if (Utilities.getSPbooleanValue(context, Utilities.spIsBookingType)) {  // coming from space/ service  true and  scenario false
            btnBookSpace.setText("Send Booking Request");
        } else {
            btnBookSpace.setText("Add to Cart");  // when coming from scenario
        }
        if (VU.isConnectingToInternet(context)) {
            getSpaceDetail();
        }
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
            selectedEventID = extras.getString("eventID");
            selectedEventName = extras.getString("eventName");
            Log.e(TAG, "getIntentData: " + uni);
        }

    }

    private void initialize() {

        imagesAddress = new ArrayList<>();
        btnBookSpace = findViewById(R.id.book_space_button);

        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.SWAP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        sliderLayout.setScrollTimeInSec(5); //set scroll delay in seconds :

        findViewById(R.id.ll_map).setOnClickListener(this);
        findViewById(R.id.ll_amenities).setOnClickListener(this);
        findViewById(R.id.ll_reviews).setOnClickListener(this);
        findViewById(R.id.ll_rules).setOnClickListener(this);
        findViewById(R.id.img_show_all_images).setOnClickListener(this);
        btnBookSpace.setOnClickListener(this);
        findViewById(R.id.back_booking_space).setOnClickListener(this);


        spaceNameText = findViewById(R.id.booking_space_space_name);
        desciptionText = findViewById(R.id.booking_space_space_about);
        priceTypeText = findViewById(R.id.booking_space_price_type);
        accomodatesText = findViewById(R.id.booking_space_accomodates);
        ratingBar = findViewById(R.id.booking_space_space_rating);
        noOfGuestTxt = findViewById(R.id.booking_space_no_of_guest);
        eventTypeTxt = findViewById(R.id.booking_space_event_type);
        hostNameText = findViewById(R.id.space_show_host_name);
        hostMobileText = findViewById(R.id.space_show_host_mobile);
        hostEmail = findViewById(R.id.space_show_host_email);
        memberSinceTxt = findViewById(R.id.space_show_host_member_since);


    }

    private void setSliderViews() {
        try {

            Log.d("IMAGES", imagesAddress.toString());
            for (int i = 0; i < 3; i++) {

                DefaultSliderView sliderView = new DefaultSliderView(context);

                switch (i) {
                    case 0:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 1:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 2:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                }

                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
//            sliderView.setDescription("The quick brown fox jumps over the lazy dog.\n" +
//                    "Jackdaws love my big sphinx of quartz. " + (i + 1));
                final int finalI = i;

                //  sliderView.setOnSliderClickListener(sliderView1 -> Toast.makeText(context, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show());

                //at last add this view in your layout :
                sliderLayout.addSliderView(sliderView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_map:
                Intent mapIntent = new Intent(context, MapsActivity.class);
                mapIntent.putExtra("latitude", spaceLatitude);
                mapIntent.putExtra("longitude", spaceLongitude);
                mapIntent.putExtra("spaceName", spaceName);
                startActivity(mapIntent);
                break;

            case R.id.ll_amenities:
                Intent amenitiesIntent = new Intent(context, AmenitiesActivity.class);
                amenitiesIntent.putExtra("uni", uniqueId);
                startActivity(amenitiesIntent);
                break;

            case R.id.ll_reviews:
                Intent reviewIntent = new Intent(context, SpaceReviewsActivity.class);
                reviewIntent.putExtra("uni", uniqueId);
                reviewIntent.putExtra("star", star);
                startActivity(reviewIntent);
                break;

            case R.id.ll_rules:
                Intent rulesIntent = new Intent(context, RulesActivity.class);
                rulesIntent.putExtra("uni", uniqueId);
                startActivity(rulesIntent);
                break;

            case R.id.img_show_all_images:
                Intent imgIntent = new Intent(context, ShowAllImagesActivity.class);
                imgIntent.putExtra("imgUrls", imagesAddress);
                startActivity(imgIntent);
                break;
            case R.id.book_space_button:
                boolean spIsLoggedin = Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin);
                Intent intent;
                if (spIsLoggedin) {
                    intent = new Intent(context, SpaceBookingFormActivity.class);
                } else {
                    Utilities.setSPboolean(context, Utilities.spAtBookingSpaceActivity, true);
                    Utilities.setSPboolean(context, Utilities.spAtGuestSpaceActivity, false);
                    Utilities.setSPboolean(context, Utilities.spAtBookingServiceActivity, false);
                    intent = new Intent(context, LoginActivity.class);
                }
                intent.putExtra("unique_id", uniqueId);
                intent.putExtra("id", spaceId);
                intent.putExtra("type", "space");
                intent.putExtra("eventID", selectedEventID);
                intent.putExtra("eventName", selectedEventName);
                startActivity(intent);
                break;

            case R.id.back_booking_space:
                finish();

           /* case R.id.add_cart_space_button:
                Toast.makeText(context, "work in progress", Toast.LENGTH_SHORT).show();
                break;
*/

        }
    }

    //get space detail
    private void getSpaceDetail() {
        final String[] guestList = getResources().getStringArray(R.array.GuestList);

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Log.e(TAG, "getSpaceDetail: uni: " + uni);

        System.out.println(Utilities.getSPstringValue(context, Utilities.spAuthToken));
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceDetail(new GetSpaceDetailModel(uni));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"SetTextI18n", "InflateParams"})
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);

                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getBoolean("status")) {

                        //handle success logic here
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject spaceDetailObject = dataObject.getJSONObject("space_details");

                        spaceName = spaceDetailObject.getString("title");
                        eventTypeTxt.setText(spaceDetailObject.getString("venue_name"));
                        hostNameText.setText(spaceDetailObject.getString("fname") + " " + spaceDetailObject.getString("lname"));
                        hostEmail.setText(spaceDetailObject.getString("email_id"));
                        noOfGuestTxt.setText(guestList[Integer.parseInt(spaceDetailObject.getString("guest")) - 1]);
                        hostMobileText.setText(spaceDetailObject.getString("mobileno"));
                        memberSinceTxt.setText(spaceDetailObject.getString("created_on"));
                        uniqueId = spaceDetailObject.getString("unique_id");
                        spaceId = spaceDetailObject.getString("space_id");
                        star = spaceDetailObject.getString("stars");
                        spaceNameText.setText(spaceName);
                        desciptionText.setText(spaceDetailObject.getString("description"));
                        priceTypeText.setText(spaceDetailObject.getString("price_type"));
                        accomodatesText.setText(spaceDetailObject.getString("accomodates"));
                        String ratingStar = spaceDetailObject.getString("stars");
                        if (ratingStar.equalsIgnoreCase("N/A")) {
                            rating = Float.parseFloat("0");
                        } else {
                            rating = Float.parseFloat(ratingStar);
                        }
                        ratingBar.setRating(rating);
                        spaceLatitude = spaceDetailObject.getString("lat");
                        spaceLongitude = spaceDetailObject.getString("lon");


                        String startTime = spaceDetailObject.getString("from_time");
                        String endTime = spaceDetailObject.getString("to_time");
                        String time = startTime + " - " + endTime;

                        JSONArray dayArray = dataObject.getJSONArray("space_days");
                        for (int i = 0; i < dayArray.length(); i++) {
                            JSONObject dayObject = dayArray.getJSONObject(i);
                            // String day = dayObject.getString("day");
                            View view;
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            view = inflater.inflate(R.layout.timing_layout, null);
                            TextView dayText = view.findViewById(R.id.day);
                            TextView daytime = view.findViewById(R.id.timing);

                            dayText.setText(dayObject.getString("day"));
                            daytime.setText(time);

                            LinearLayout linearLayout = findViewById(R.id.space_timing_layout);
                            linearLayout.addView(view);

                        }

                        JSONArray imgAddressArray = dataObject.getJSONArray("space_image");
                        for (int i = 0; i < imgAddressArray.length(); i++) {
                            JSONObject imgObject = imgAddressArray.getJSONObject(i);
                            imagesAddress.add(Utilities.IMG_SPACE_URL + imgObject.getString("space_image"));
                        }
                        setSliderViews();
                    } else {
                        //handle failed logic here
                        Toast.makeText(SpaceDetailAndBookActivity.this, "" + jsonObject.getString("message"),
                                Toast.LENGTH_SHORT).show();
                    }

                } catch (NullPointerException | JSONException | IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

}
