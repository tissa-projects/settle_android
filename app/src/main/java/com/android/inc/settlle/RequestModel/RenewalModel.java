package com.android.inc.settlle.RequestModel;

public class RenewalModel {

    private String starts_on;
    private String ends_on;
    private String amount;
    private String renew_amount;
    private String subscription_id;
    private String plan_id ;
    private String  auth_token;

    public RenewalModel(String starts_on, String ends_on, String amount, String renew_amount, String subscription_id, String plan_id, String auth_token) {
        this.starts_on = starts_on;
        this.ends_on = ends_on;
        this.amount = amount;
        this.renew_amount = renew_amount;
        this.subscription_id = subscription_id;
        this.plan_id = plan_id;
        this.auth_token = auth_token;
    }

    public String getStarts_on() {
        return starts_on;
    }

    public void setStarts_on(String starts_on) {
        this.starts_on = starts_on;
    }

    public String getEnds_on() {
        return ends_on;
    }

    public void setEnds_on(String ends_on) {
        this.ends_on = ends_on;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRenew_amount() {
        return renew_amount;
    }

    public void setRenew_amount(String renew_amount) {
        this.renew_amount = renew_amount;
    }

    public String getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
