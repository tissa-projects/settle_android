package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class AddServiceReviewModel implements Serializable {
    private String  user_id;
    private String  booking_id;
    private String  service_id;
    private String  stars;
    private String  review;
    private String  auth_token;

    public AddServiceReviewModel(String user_id, String booking_id, String service_id, String stars, String review, String auth_token) {
        this.user_id = user_id;
        this.booking_id = booking_id;
        this.service_id = service_id;
        this.stars = stars;
        this.review = review;
        this.auth_token = auth_token;
    }

}
