package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.EditSpaceServiceDataModel;
import com.android.inc.settlle.RequestModel.StateCityModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSpaceRegistrationDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtAddSpaceTitle, edtZipCOde, edtLandMark, edtAccomoddated, edtDescription, edtDiscount;
    private TextView txtCountry, txtState, txtCity, txtAddress, txtFromTime, txtToTime, txtAddVenueType, txtNoOfGuest;
    private RadioButton radiobtnFixed, radiobtnNegotiable;
    private Context context;
    private CheckBox checkboxSelectAll, checkboxMon, checkboxTue, checkboxWed, checkboxThur, checkboxFri, checkboxSat, checkboxSun;
    private Dialog editSpaceDialog;
    private String strVenueName, strVenueId, strlat, strLng, strPriceType = null, strGuestId, strCountryId,
            strStateId, strCityId, strUniqueId, strSpaceId, strSubscriptionId;
    private ArrayList<String> cityNameList = null, cityIdList = null, stateNameList = null, stateIdList = null;
    private final HashMap<Integer, Integer> daysIds = new HashMap<>();
    private static final String TAG = EditSpaceRegistrationDetailsActivity.class.getSimpleName();
    private static final int AUTOCOMPLETE_REQUEST_CODE = 200;
    private View layout;
    private JSONArray spaceEventsArray = null;
    private ArrayList<String> selectedamenitiesArray = null, selectedRulesArray = null;
    // ImageView back_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_space_registration_details);

        context = EditSpaceRegistrationDetailsActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        strCountryId = "1";   // india country id fixed
        getIntentData();
        initialize();
        if (VU.isConnectingToInternet(context)) {
            getEditSpaceData();  //api call
        }
        //  apiKey = getResources().getString(R.string.Api_Key);
        Utilities.guestLimitList = new ArrayList<>(Arrays.asList(Utilities.guestLimitArray));
        Utilities.guestLimitIdList = new ArrayList<>(Arrays.asList(Utilities.guestLimitIdArray));

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), Utilities.API_KEY);
        }
        // Create a new Places client instance.
        Places.createClient(this);
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            strUniqueId = extras.getString("uniqueId");
            strSpaceId = extras.getString("spaceId");
            Log.e(TAG, "getIntentData: strUniqueId : " + strUniqueId + " strSpaceId : " + strSpaceId);

        }

    }

    private void initialize() {
        //   back_button = findViewById(R.id.back_edit_space);
        edtAddSpaceTitle = findViewById(R.id.edt_space_title);
        txtNoOfGuest = findViewById(R.id.txt_space_no_of_guest);
        edtZipCOde = findViewById(R.id.edt_space_zip_code);
        edtLandMark = findViewById(R.id.edt_space_land_mark);
        edtAccomoddated = findViewById(R.id.edt_space_accomodation);
        edtDescription = findViewById(R.id.edt_space_description);
        edtDiscount = findViewById(R.id.edt_space_discount);
        radiobtnFixed = findViewById(R.id.radiobtn_space_fixed);
        radiobtnNegotiable = findViewById(R.id.radiobtn_space_negotiable);

        txtAddVenueType = findViewById(R.id.txt_space_venue_type);
        txtCountry = findViewById(R.id.txt_space_country);
        txtState = findViewById(R.id.txt_space_state);
        txtCity = findViewById(R.id.txt_space_city);
        txtAddress = findViewById(R.id.txt_space_address);
        txtFromTime = findViewById(R.id.txt_space_from_time);
        txtToTime = findViewById(R.id.txt_space_to_time);

        radiobtnFixed = findViewById(R.id.radiobtn_space_fixed);
        radiobtnNegotiable = findViewById(R.id.radiobtn_space_negotiable);

        checkboxSelectAll = findViewById(R.id.check_box_select_all);
        checkboxMon = findViewById(R.id.check_box_monday);
        checkboxTue = findViewById(R.id.check_box_tuesday);
        checkboxWed = findViewById(R.id.check_box_wednesday);
        checkboxThur = findViewById(R.id.check_box_thrusday);
        checkboxFri = findViewById(R.id.check_box_friday);
        checkboxSat = findViewById(R.id.check_box_saturday);
        checkboxSun = findViewById(R.id.check_box_sunday);

        findViewById(R.id.btn_next_space_registration).setOnClickListener(this);
        txtNoOfGuest.setOnClickListener(this);
        findViewById(R.id.ll_state).setOnClickListener(this);
        findViewById(R.id.ll_city).setOnClickListener(this);
        findViewById(R.id.ll_from_time).setOnClickListener(this);
        findViewById(R.id.ll_to_time).setOnClickListener(this);
        radiobtnNegotiable.setOnClickListener(this);
        radiobtnFixed.setOnClickListener(this);
        checkboxSelectAll.setOnClickListener(this);
        checkboxMon.setOnClickListener(this);
        checkboxTue.setOnClickListener(this);
        checkboxThur.setOnClickListener(this);
        checkboxWed.setOnClickListener(this);
        checkboxFri.setOnClickListener(this);
        checkboxSat.setOnClickListener(this);
        checkboxSun.setOnClickListener(this);
        txtAddress.setOnClickListener(this);
        // back_button.setOnClickListener(this);

        findViewById(R.id.imgBack).setOnClickListener(v -> onBackPressed());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Edit Space Details");
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        ArrayList<Integer> days = new ArrayList<>();
        switch (v.getId()) {
            case R.id.btn_next_space_registration:
                if (validation()) {
                    Set<Integer> keys = daysIds.keySet();
                    days.addAll(keys);

                    Log.e(TAG, "onClick: strlat: " + strlat + " strLng: " + strLng);
                    Intent i = new Intent(context, EditSpaceRegistrationAmenitiesEventsRulesActivity.class);
                    i.putExtra("serviceProvider", edtAddSpaceTitle.getText().toString());
                    i.putExtra("venue_id", strVenueId);
                    i.putExtra("guest", strGuestId);
                    i.putExtra("country", strCountryId);
                    i.putExtra("state", strStateId);
                    i.putExtra("city", strCityId);
                    i.putExtra("address", txtAddress.getText().toString());
                    i.putExtra("latitude", strlat);
                    i.putExtra("longitude", strLng);
                    i.putExtra("zip_code", edtZipCOde.getText().toString());
                    i.putExtra("landmark", edtLandMark.getText().toString());
                    i.putExtra("accommodate", edtAccomoddated.getText().toString());
                    i.putExtra("description", edtDescription.getText().toString());
                    i.putExtra("discount", edtDiscount.getText().toString());
                    i.putExtra("price_type", strPriceType);
                    i.putExtra("from_time", txtFromTime.getText().toString());
                    i.putExtra("to_time", txtToTime.getText().toString());
                    i.putExtra("spaceEvent", spaceEventsArray.toString());
                    i.putExtra("SpaceId", strSpaceId);
                    i.putExtra("UniqueId", strUniqueId);
                    i.putExtra("venue_name", strVenueName);
                    i.putExtra("subscription_id", strSubscriptionId);
                    i.putIntegerArrayListExtra("daysId", days);
                    i.putStringArrayListExtra("amenitiesId", selectedamenitiesArray);
                    i.putStringArrayListExtra("rulesId", selectedRulesArray);
                    startActivity(i);
                    finish();
                    Log.e(TAG, "getIntentData: " + days + "\n" +
                            selectedRulesArray + " " + selectedamenitiesArray + "\n" + spaceEventsArray + "\n");
                }

                break;

            case R.id.txt_space_no_of_guest:
                setNoOfGuest();
                break;

            case R.id.txt_space_address:
                if (txtCity != null && txtCity.getText().toString().trim().length() > 1)
                    onSearchCalled();
                else
                    CustomToast.custom_Toast(context, "Please First Select City", layout);
                break;

            case R.id.ll_city:
                if (txtState.getText().toString().equalsIgnoreCase("") || txtState.getText().toString().equalsIgnoreCase("null")) {
                    CustomToast.custom_Toast(context, "Please First Select State", layout);
                } else {
                    getCityData();
                }
                break;

            case R.id.ll_state:
                getStateData();
                break;


            case R.id.ll_from_time:
                setTime("fromTime");
                break;

            case R.id.ll_to_time:
                setTime("toTime");
                break;

            case R.id.radiobtn_space_fixed:
                strPriceType = "fixed";
                break;

            case R.id.radiobtn_space_negotiable:
                strPriceType = "negotiable";
                break;

            case R.id.check_box_select_all:
                if (checkboxSelectAll.isChecked()) {
                    checkboxSelectAll.setChecked(true);
                    checkboxMon.setChecked(true);
                    checkboxTue.setChecked(true);
                    checkboxWed.setChecked(true);
                    checkboxThur.setChecked(true);
                    checkboxFri.setChecked(true);
                    checkboxSat.setChecked(true);
                    checkboxSun.setChecked(true);
                    daysIds.put(1, 1);
                    daysIds.put(2, 2);
                    daysIds.put(3, 3);
                    daysIds.put(4, 4);
                    daysIds.put(5, 5);
                    daysIds.put(6, 6);
                    daysIds.put(7, 7);

                } else {
                    checkboxSelectAll.setChecked(false);
                    checkboxMon.setChecked(false);
                    checkboxTue.setChecked(false);
                    checkboxWed.setChecked(false);
                    checkboxThur.setChecked(false);
                    checkboxFri.setChecked(false);
                    checkboxSat.setChecked(false);
                    checkboxSun.setChecked(false);
                    daysIds.remove(1);
                    daysIds.remove(2);
                    daysIds.remove(3);
                    daysIds.remove(4);
                    daysIds.remove(5);
                    daysIds.remove(6);
                    daysIds.remove(7);
                }
                break;

            case R.id.check_box_monday:
                if (checkboxMon.isChecked()) {
                    checkboxMon.setChecked(true);
                    if (!daysIds.containsKey(1)) {
                        daysIds.put(1, 1);
                    }
                } else {
                    checkboxMon.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(1);
                }
                break;
            case R.id.check_box_tuesday:
                if (checkboxTue.isChecked()) {
                    checkboxTue.setChecked(true);
                    if (!daysIds.containsKey(2)) {
                        daysIds.put(2, 2);
                    }
                } else {
                    checkboxTue.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(2);
                }
                break;
            case R.id.check_box_wednesday:
                if (checkboxWed.isChecked()) {
                    checkboxWed.setChecked(true);
                    if (!daysIds.containsKey(3)) {
                        daysIds.put(3, 3);
                    }
                } else {
                    checkboxWed.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(3);
                }
                break;
            case R.id.check_box_thrusday:
                if (checkboxThur.isChecked()) {
                    checkboxThur.setChecked(true);
                    if (!daysIds.containsKey(4)) {
                        daysIds.put(4, 4);
                    }
                } else {
                    checkboxThur.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(4);
                }
                break;
            case R.id.check_box_friday:
                if (checkboxFri.isChecked()) {
                    checkboxFri.setChecked(true);
                    if (!daysIds.containsKey(5)) {
                        daysIds.put(5, 5);
                    }
                } else {
                    checkboxFri.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(5);
                }
                break;
            case R.id.check_box_saturday:
                if (checkboxSat.isChecked()) {
                    checkboxSat.setChecked(true);
                    if (!daysIds.containsKey(6)) {
                        daysIds.put(6, 6);
                    }
                } else {
                    checkboxSat.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(6);
                }
                break;
            case R.id.check_box_sunday:
                if (checkboxSun.isChecked()) {
                    checkboxSun.setChecked(true);
                    if (!daysIds.containsKey(7)) {
                        daysIds.put(7, 7);
                    }
                } else {
                    checkboxSun.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(7);
                }
                break;
        }
    }

    //get City Data
    private void getCityData() {
        cityNameList = new ArrayList<>();
        cityIdList = new ArrayList<>();
        editSpaceDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Log.e(TAG, "getCityData: " + strStateId);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getCityData(new StateCityModel(strStateId));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                editSpaceDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String strStatusMsg = jsonObject.getString("message");
                    if (statusCode == 200) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            cityNameList.add(dataArray.getJSONObject(i).getString("location_name"));
                            cityIdList.add(dataArray.getJSONObject(i).getString("location_id"));
                        }
                        //city dialog called
                        setCity();
                    } else {
                        Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                editSpaceDialog.dismiss();
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //get state response
    private void getStateData() {
        stateNameList = new ArrayList<>();
        stateIdList = new ArrayList<>();
        editSpaceDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getStateData(new StateCityModel(strCountryId));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                editSpaceDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String strStatusMsg = jsonObject.getString("message");
                    if (statusCode == 200) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            stateNameList.add(dataArray.getJSONObject(i).getString("location_name"));
                            stateIdList.add(dataArray.getJSONObject(i).getString("location_id"));
                        }
                        // state dialog call
                        setState();
                    } else {
                        Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    editSpaceDialog.dismiss();
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                editSpaceDialog.dismiss();
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //state dialog
    private void setState() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select State");
        // add a list
        builder.setItems(Utilities.GetStringArray(stateNameList), (dialog, position) -> {
            txtState.setText(stateNameList.get(position));
            strStateId = stateIdList.get(position);
            dialog.dismiss();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    //city dialog
    private void setCity() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select City");
        // add a list
        builder.setItems(Utilities.GetStringArray(cityNameList), (dialog, position) -> {
            txtCity.setText(cityNameList.get(position));
            strCityId = cityIdList.get(position);
            dialog.dismiss();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private boolean validation() {
        if (VU.isEmpty(edtAddSpaceTitle)) {
            CustomToast.custom_Toast(context, "Add Space Title", layout);
            return false;
        } else if (VU.isTxtEmpty(txtAddVenueType)) {
            CustomToast.custom_Toast(context, "Add Venue Type", layout);
            return false;
        } else if (VU.isTxtEmpty(txtNoOfGuest)) {
            CustomToast.custom_Toast(context, "Select Number Of Guest", layout);
            return false;
        } else if (VU.isTxtEmpty(txtCountry)) {
            CustomToast.custom_Toast(context, "Select Country", layout);
            return false;
        } else if (VU.isTxtEmpty(txtState)) {
            CustomToast.custom_Toast(context, "Select State", layout);
            return false;
        } else if (VU.isTxtEmpty(txtCity)) {
            CustomToast.custom_Toast(context, "Select City", layout);
            return false;
        } else if (VU.isTxtEmpty(txtAddress)) {
            CustomToast.custom_Toast(context, "Add Address", layout);
            return false;
        } else if (VU.isEmpty(edtZipCOde)) {
            CustomToast.custom_Toast(context, "Add ZipCode", layout);
            return false;
        } else if (VU.isEmpty(edtLandMark)) {
            CustomToast.custom_Toast(context, "Add LandMark", layout);
            return false;
        } else if (VU.isEmpty(edtAccomoddated)) {
            CustomToast.custom_Toast(context, "Add Accomodation", layout);
            return false;
        } else if (VU.isEmpty(edtDescription)) {
            CustomToast.custom_Toast(context, "Add Description", layout);
            return false;
        } else if (VU.isEmpty(edtDiscount)) {
            CustomToast.custom_Toast(context, "Add Discount", layout);
            return false;
        } else if (strPriceType == null) {
            CustomToast.custom_Toast(context, "Select Price Type", layout);
            return false;
        } else if ((!checkboxMon.isChecked()) && (!checkboxTue.isChecked()) &&
                (!checkboxWed.isChecked()) && (!checkboxThur.isChecked()) &&
                (!checkboxFri.isChecked()) && (!checkboxSat.isChecked()) && (!checkboxSun.isChecked())) {
            CustomToast.custom_Toast(context, "Select Days", layout);
            return false;

        } else if (VU.isTxtEmpty(txtFromTime)) {
            CustomToast.custom_Toast(context, "Add Start Time", layout);
            return false;
        } else if (VU.isTxtEmpty(txtToTime)) {
            CustomToast.custom_Toast(context, "Add End Time", layout);
            return false;
        }
        return true;
    }


    //search google address
    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN") //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = Autocomplete.getPlaceFromIntent(data);

                // Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                //  List<Address> addresses;
               /* String city;
                try {
                    addresses = geocoder.getFromLocation(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    city = addresses.get(0).getLocality();
                } catch (IOException e) {
                    city = txtCity.getText().toString().trim();
                    e.printStackTrace();
                }*/

                String address = place.getAddress();
                LatLng latlng = place.getLatLng();
                assert latlng != null;
                double lat = latlng.latitude;
                double lng = latlng.longitude;

                strlat = String.valueOf(lat);
                strLng = String.valueOf(lng);
                Log.e(TAG, "onActivityResult: " + address + " " + lat + " " + lng);
                txtAddress.setText(address);
              /*  if (txtCity.getText().toString().trim().equalsIgnoreCase(city)) {
                    txtAddress.setText(address);
                } else {
                    CustomToast.custom_Toast(context, "Please select correct city name", layout);
                }*/
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                assert data != null;
                Status status = Autocomplete.getStatusFromIntent(data);
                Toast.makeText(context, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG).show();
                Log.i(TAG, status.getStatusMessage());
            }
        }
    }

    private void setTime(final String type) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, (timePicker, selectedHour, selectedMinute) -> {
            //    String time = selectedHour + ":" + selectedMinute;
            String time = String.valueOf(selectedHour);
            convertTO12Hrs(type, time);
        }, hour, minute, false);//no 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void convertTO12Hrs(final String type, String time) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H", Locale.ENGLISH);
            final Date dateObj = sdf.parse(time);
            assert dateObj != null;
            String newTime = new SimpleDateFormat("K aa ", Locale.ENGLISH).format(dateObj);
            if (type.equalsIgnoreCase("fromTime")) {
                txtFromTime.setText(newTime);
            } else if (type.equalsIgnoreCase("toTime")) {
                txtToTime.setText(newTime);
            }
        } catch (final ParseException e) {
            e.printStackTrace();
        }
    }


    // No Of Guest dialog
    private void setNoOfGuest() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Guest");
        // add a list
        builder.setItems(Utilities.GetStringArray(Utilities.guestLimitList), (dialog, position) -> {
            txtNoOfGuest.setText(Utilities.guestLimitList.get(position));
            strGuestId = Utilities.guestLimitIdList.get(position);
            dialog.dismiss();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    // request getSpaceData
    private void getEditSpaceData() {
        editSpaceDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "getEditSpaceData: authToken : " + authToken);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getEditSpaceData(new EditSpaceServiceDataModel(strUniqueId,
                authToken));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                editSpaceDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String strStatusMsg = jsonObject.getString("message");

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        setResponseData(dataObj);
                    } else {
                        Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    editSpaceDialog.dismiss();
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                editSpaceDialog.dismiss();
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setResponseData(JSONObject dataObj) {

        try {
            Log.e(TAG, "setResponseData: dataObj : " + dataObj);
            Utilities.guestLimitList = new ArrayList<>(Arrays.asList(Utilities.guestLimitArray));
            selectedamenitiesArray = new ArrayList<>();
            selectedRulesArray = new ArrayList<>();
            try {
                JSONObject spaceDetailsObj = dataObj.getJSONObject("space_detail");
                JSONArray spaceDaysArray = dataObj.getJSONArray("space_days");
                spaceEventsArray = dataObj.getJSONArray("space_event");
                int GuestId = spaceDetailsObj.getInt("guest");
                String strGuestNo = Utilities.guestLimitList.get(GuestId - 1);
                strVenueId = spaceDetailsObj.getString("venue_id");
                strVenueName = spaceDetailsObj.getString("venue_name");
                strSubscriptionId = spaceDetailsObj.getString("subscription_id");
                strlat = spaceDetailsObj.getString("lat");
                strLng = spaceDetailsObj.getString("lon");
                strCountryId = spaceDetailsObj.getString("country");
                strCityId = spaceDetailsObj.getString("city");
                strStateId = spaceDetailsObj.getString("state");
                strGuestId = String.valueOf(GuestId);


                String amenities = spaceDetailsObj.getString("amenities");
                String rules = spaceDetailsObj.getString("rules");
                String[] selectedamenities = amenities.split(",");
                String[] selectedRules = rules.split(",");
                selectedamenitiesArray.addAll(Arrays.asList(selectedamenities));

                selectedRulesArray.addAll(Arrays.asList(selectedRules));
                edtAddSpaceTitle.setText(spaceDetailsObj.getString("title"));
                txtAddVenueType.setText(spaceDetailsObj.getString("venue_name"));
                txtCountry.setText(spaceDetailsObj.getString("country_name"));
                txtState.setText(spaceDetailsObj.getString("state_name"));
                txtCity.setText(spaceDetailsObj.getString("city_name"));
                txtAddress.setText(spaceDetailsObj.getString("my_add"));
                edtZipCOde.setText(spaceDetailsObj.getString("zip_code"));
                edtLandMark.setText(spaceDetailsObj.getString("landmark"));
                edtAccomoddated.setText(spaceDetailsObj.getString("accomodates"));
                edtDescription.setText(spaceDetailsObj.getString("description"));
                edtDiscount.setText(spaceDetailsObj.getString("discount"));
                txtFromTime.setText(spaceDetailsObj.getString("from_time"));
                txtToTime.setText(spaceDetailsObj.getString("to_time"));
                txtNoOfGuest.setText(strGuestNo);
                String PriceType = spaceDetailsObj.getString("price_type");

                if (PriceType.equalsIgnoreCase("fixed")) {
                    radiobtnFixed.setChecked(true);
                    radiobtnNegotiable.setChecked(false);
                    strPriceType = "fixed";
                } else {
                    radiobtnFixed.setChecked(false);
                    radiobtnNegotiable.setChecked(true);
                    strPriceType = "negotiable";
                }

                if (spaceDaysArray.length() == 7) {
                    setAllCheckedDays();
                } else {
                    for (int i = 0; i < spaceDaysArray.length(); i++) {
                        JSONObject dayObj = spaceDaysArray.getJSONObject(i);
                        String day = dayObj.getString("day");
                        if (day.equalsIgnoreCase("Mon")) {
                            checkboxMon.setChecked(true);
                            daysIds.put(1, 1);
                        } else if (day.equalsIgnoreCase("Tue")) {
                            checkboxTue.setChecked(true);
                            daysIds.put(2, 2);
                        } else if (day.equalsIgnoreCase("Wed")) {
                            checkboxWed.setChecked(true);
                            daysIds.put(3, 3);
                        } else if (day.equalsIgnoreCase("Thur")) {
                            checkboxThur.setChecked(true);
                            daysIds.put(4, 4);
                        } else if (day.equalsIgnoreCase("Fri")) {
                            checkboxFri.setChecked(true);
                            daysIds.put(5, 5);
                        } else if (day.equalsIgnoreCase("Sat")) {
                            checkboxSat.setChecked(true);
                            daysIds.put(6, 6);
                        } else if (day.equalsIgnoreCase("Sun")) {
                            checkboxSun.setChecked(true);
                            daysIds.put(7, 7);
                        }

                    }
                }

            } catch (Exception e) {
                Log.e(TAG, "setResponseData: catch :" + e.getMessage());
            }

        } catch (Exception e) {
            Log.e(TAG, "setResponseData: catch : " + e.getMessage());
        }
    }

    private void setAllCheckedDays() {
        checkboxSelectAll.setChecked(true);
        checkboxMon.setChecked(true);
        checkboxTue.setChecked(true);
        checkboxWed.setChecked(true);
        checkboxThur.setChecked(true);
        checkboxFri.setChecked(true);
        checkboxSat.setChecked(true);
        checkboxSun.setChecked(true);
        daysIds.put(1, 1);
        daysIds.put(2, 2);
        daysIds.put(3, 3);
        daysIds.put(4, 4);
        daysIds.put(5, 5);
        daysIds.put(6, 6);
        daysIds.put(7, 7);
    }

    /* private void addSpaceEvents(JSONArray spaceEventsArray) {
         try {
             for (int i = 0; i < spaceEventsArray.length(); i++) {
                 String eventId = spaceEventsArray.getJSONObject(i).getString("event_id");
                 String eventName = spaceEventsArray.getJSONObject(i).getString("event_name");
                 String eventPrice = spaceEventsArray.getJSONObject(i).getString("event_price");
                 String spaceEventId = spaceEventsArray.getJSONObject(i).getString("space_event_id");

                 spaceEvents.add(new SpaceEventModel(eventId, eventName, eventPrice, spaceEventId));
             }
         } catch (Exception e) {
             Log.e(TAG, "addSpaceEvents: catch : " + e.getMessage());
         }
     }
 */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
