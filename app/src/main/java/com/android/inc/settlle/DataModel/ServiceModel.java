package com.android.inc.settlle.DataModel;

public class ServiceModel {

    private String serviceId;
    private String serviceName;
    private String serviceYearly;
    private String serviceHalfYearly;
    private boolean isSelected;

    public ServiceModel(String serviceId, String serviceName, String serviceYearly, String serviceHalfYearly, boolean isSelected) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.serviceYearly = serviceYearly;
        this.serviceHalfYearly = serviceHalfYearly;
        this.isSelected = isSelected;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceYearly() {
        return serviceYearly;
    }

    public void setServiceYearly(String serviceYearly) {
        this.serviceYearly = serviceYearly;
    }

    public String getServiceHalfYearly() {
        return serviceHalfYearly;
    }

    public void setServiceHalfYearly(String serviceHalfYearly) {
        this.serviceHalfYearly = serviceHalfYearly;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
