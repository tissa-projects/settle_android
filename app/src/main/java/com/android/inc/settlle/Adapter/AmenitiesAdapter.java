package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;

import org.json.JSONArray;

public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.MyViewHolder> {

    private final JSONArray dataArray;
    private final Context context;

    public AmenitiesAdapter(JSONArray dataArray, Context context) {
        this.dataArray = dataArray;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_amenities, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        try {
            String amenity = dataArray.getJSONObject(i).getString("amenities_name");
            ((MyViewHolder) myViewHolder).amenityText.setText("" + amenity);

            myViewHolder.imgAmenity.setImageDrawable(CommonFunctions.getRulesIcon(context, dataArray.getJSONObject(i).getString("amenities_name")));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        TextView amenityText;
        ImageView imgAmenity;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.cardview_amenity);
            this.amenityText = itemView.findViewById(R.id.space_amenity_text);
            this.imgAmenity= itemView.findViewById(R.id.imgAmenity);
        }
    }
}
