package com.android.inc.settlle.RequestModel;

public class SendBookingRequestModel {

    private String space_id;
    private String event_id;
    private String user_id;
    private String guest_id;
    private String unique_id;
    private String startdate;
    private String enddate;
    private String start_time;
    private String end_time;
    private String amount;
    private String auth_token;

    public SendBookingRequestModel(String space_id, String event_id, String user_id, String guest_id, String unique_id, String startdate, String enddate, String start_time, String end_time, String amount, String auth_token) {
        this.space_id = space_id;
        this.event_id = event_id;
        this.user_id = user_id;
        this.guest_id = guest_id;
        this.unique_id = unique_id;
        this.startdate = startdate;
        this.enddate = enddate;
        this.start_time = start_time;
        this.end_time = end_time;
        this.amount = amount;
        this.auth_token = auth_token;
    }

    public String getSpace_id() {
        return space_id;
    }

    public void setSpace_id(String space_id) {
        this.space_id = space_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGuest_id() {
        return guest_id;
    }

    public void setGuest_id(String guest_id) {
        this.guest_id = guest_id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
