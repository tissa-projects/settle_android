package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.EditServicePackagesAdapter;
import com.android.inc.settlle.DataModel.PackageModel;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.DeletePackageModel;
import com.android.inc.settlle.RequestModel.PackageDetailsModel;
import com.android.inc.settlle.RequestModel.UpdateServiceDescriptionModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditServicePackagsActivity extends AppCompatActivity implements View.OnClickListener {

    public String serviceProvider, UniqueId, service_id, user_id, guest, country, state, city, address, latitude,
            longitude, zip_code, landmark, description, discount, price_type, from_time, to_time, auth_token, strServiceDetailsId, strSubscriptionId;
    private Context context;
    private EditText edtPackageName, edtPackagePrice, edtPackageDesc;
    private AutoCompleteTextView txtServiceType;
    private ArrayList<Integer> days = null;
    private Dialog Dialog, dialogAddpackage;
    private View layout;
    private RecyclerView recyclerEditService;
    private FloatingActionButton btnAddPackage;
    private String dialogPackageName, dialogPackagePrice, dialogPackageDesc, dialogPackageServiceName, service_type = "";
    private EditServicePackagesAdapter editServicePackagesAdapter = null;
    private JSONArray dataArray;
    private ArrayList<String> packageDescriptionList = null, packageServiceTypeList = null, packageServiceIdList = null, packageNameList = null,
            newpackageDescriptionList = null, newpackageNameList = null, newpackageServiceIdList = null, oldpackageServiceIdList = null;
    private ArrayList<Integer> packageIdList = null, packagePriceList = null, newpackagePriceList = null;
    private int addedPackagePosition;
    private ArrayList<PackageModel> packageList;
    private JSONArray servicePackageList = null;
    private static final String TAG = EditServicePackagsActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_service_packags);
        context = EditServicePackagsActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        try {
            getIntentData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initialize();
        initRecycler();

        if (VU.isConnectingToInternet(context)) {
            getPackageData();
        }
    }


    private void initialize() {

        findViewById(R.id.fab).setOnClickListener(this);
        recyclerEditService = findViewById(R.id.recycler_edit_service_package);
        btnAddPackage = findViewById(R.id.fab);

        btnAddPackage.setVisibility(View.GONE);
        packageList = new ArrayList<>();
        packageDescriptionList = new ArrayList<>();
        packageNameList = new ArrayList<>();
        packageIdList = new ArrayList<>();
        packagePriceList = new ArrayList<>();
        newpackageNameList = new ArrayList<>();
        newpackageDescriptionList = new ArrayList<>();
        newpackagePriceList = new ArrayList<>();
        packageServiceTypeList = new ArrayList<>();
        packageServiceIdList = new ArrayList<>();
        newpackageServiceIdList = new ArrayList<>();
        oldpackageServiceIdList = new ArrayList<>();


        newServiceType = new ArrayList<>();
        oldServiceType = new ArrayList<>();

        dataArray = new JSONArray();


        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Edit Service Packages");


        btnAddPackage.setOnClickListener(this);
        findViewById(R.id.btn_update_package).setOnClickListener(this);

        for (int i = 0; i < servicePackageList.length(); i++) {
            try {
                packageServiceIdList.add(servicePackageList.getJSONObject(i).getString("service_id"));
                packageServiceTypeList.add(servicePackageList.getJSONObject(i).getString("service_name"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void initRecycler() {
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerEditService.setLayoutManager(layoutManager);

        editServicePackagesAdapter = new EditServicePackagesAdapter();
        recyclerEditService.setAdapter(editServicePackagesAdapter);
        btnAddPackage.setVisibility(View.VISIBLE);

        editServicePackagesAdapter.viewDetailsClickListener((v, position) -> {
            addedPackagePosition = position;
            dialogAddPackages("view_and_edit");  // view and edit package
        });

        editServicePackagesAdapter.deletePackageClickListener((v, position) -> {
            ArrayList<PackageModel> list = editServicePackagesAdapter.getPackageList();
            statusChangeAlert(list, position);
        });

    }

    private void getIntentData() throws JSONException {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            serviceProvider = extras.getString("serviceProvider");
            service_id = extras.getString("service_id");
            guest = extras.getString("guest");
            country = extras.getString("country");
            state = extras.getString("state");
            city = extras.getString("city");
            address = extras.getString("address");
            latitude = extras.getString("latitude");
            longitude = extras.getString("longitude");
            zip_code = extras.getString("zip_code");
            landmark = extras.getString("landmark");
            discount = extras.getString("discount");
            price_type = extras.getString("price_type");
            from_time = extras.getString("from_time");
            to_time = extras.getString("to_time");
            UniqueId = extras.getString("UniqueId");
            strServiceDetailsId = extras.getString("ServiceDetailsId");
            strSubscriptionId = extras.getString("SubscriptionId");
            days = extras.getIntegerArrayList("daysId");
            String servicePackage = extras.getString("servicePackageList");
            servicePackageList = new JSONArray(servicePackage);
            service_type = extras.getString("service_type");
        }

        CommonFunctions.showLog(TAG, "getIntentData: " + serviceProvider + " " + service_id + " " + guest + " " + country + " " + state + " " + city + "\n " +
                address + latitude + longitude + zip_code + landmark + discount + price_type + from_time + to_time + days + "\n" +
                UniqueId + " " + strServiceDetailsId + " " + strSubscriptionId + "\n" + servicePackageList);
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                dialogAddPackages("add");
                break;
            case R.id.btn_update_package:
                if (VU.isConnectingToInternet(context)) {
                    updateServiceDescription();
                }
                break;
        }
    }

    ArrayList<String> newServiceType, oldServiceType;

    public void dialogAddPackages(String type) {
        try {
            dialogAddpackage = new Dialog(context);
            dialogAddpackage.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialogAddpackage.setContentView(R.layout.dialog_add_service_package);

            dialogAddpackage.setCancelable(false);
            Log.e(TAG, "dialogAddPackages: packageServiceTypeList: " + packageServiceTypeList);
            ArrayAdapter<String> serviceTypeAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice, CommonFunctions.GetStringArray(packageServiceTypeList));

            edtPackageName = dialogAddpackage.findViewById(R.id.edt_package_name);
            edtPackagePrice = dialogAddpackage.findViewById(R.id.edt_package_price);
            edtPackageDesc = dialogAddpackage.findViewById(R.id.edt_package_description);
            txtServiceType = dialogAddpackage.findViewById(R.id.txt_service_type);
            txtServiceType.setVisibility(View.VISIBLE);

            LinearLayout lySpinner = dialogAddpackage.findViewById(R.id.lySpinner);
            lySpinner.setVisibility(View.GONE);

            Spinner spnServiceType = dialogAddpackage.findViewById(R.id.spinner_service_type);
            spnServiceType.setVisibility(View.GONE);

            Button dialogBtnUpdatePackage = dialogAddpackage.findViewById(R.id.btn_update_package);
            Button dialogBtnAddPackage = dialogAddpackage.findViewById(R.id.btn_add_package);
            Button dialogBtnCancel = dialogAddpackage.findViewById(R.id.btn_cancel);

            txtServiceType.setAdapter(serviceTypeAdapter);
            txtServiceType.setCursorVisible(false);

            txtServiceType.setOnClickListener(arg0 -> txtServiceType.showDropDown());
            txtServiceType.setOnItemClickListener((parent, view, position, id) -> {
                txtServiceType.showDropDown();
                String selectedServiceType = (String) parent.getItemAtPosition(position);
                txtServiceType.setText(selectedServiceType);

                for (int i = 0; i < packageServiceTypeList.size(); i++) {
                    if (selectedServiceType.equalsIgnoreCase(packageServiceTypeList.get(i))) {
                        service_type = packageServiceIdList.get(i);
                        newServiceType.add(service_type);
                        oldServiceType.add(service_type);
                    }
                }
            });


            if (type.equalsIgnoreCase("view_and_edit")) {
                dialogBtnUpdatePackage.setVisibility(View.VISIBLE);
                txtServiceType.setEnabled(false);
                dialogBtnAddPackage.setVisibility(View.GONE);
                edtPackageName.setText(packageList.get(addedPackagePosition).getPackagename());
                edtPackagePrice.setText(packageList.get(addedPackagePosition).getPackagePrice());
                edtPackageDesc.setText(packageList.get(addedPackagePosition).getPackageDesc());
                txtServiceType.setText(packageList.get(addedPackagePosition).getService_type());

            } else {  // add package
                txtServiceType.setEnabled(true);
                dialogBtnUpdatePackage.setVisibility(View.GONE);
                dialogBtnAddPackage.setVisibility(View.VISIBLE);
            }

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialogAddpackage.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialogBtnAddPackage.setOnClickListener(v -> {
                try {
                    if (Validate()) {
                        dialogAddpackage.cancel();
                        dialogPackageName = edtPackageName.getText().toString();
                        dialogPackagePrice = edtPackagePrice.getText().toString();
                        dialogPackageDesc = edtPackageDesc.getText().toString();
                        dialogPackageServiceName = txtServiceType.getText().toString();
                        // dialogPackageServiceId = edtPackageDesc.getText().toString();
                        packageList.add(new PackageModel(dialogPackageName,
                                dialogPackagePrice, dialogPackageDesc, dialogPackageServiceName, packageServiceIdList.get(addedPackagePosition)));
                        CommonFunctions.showLog(TAG, "onClick: " + packageList);
                        editServicePackagesAdapter.setPackageList(packageList);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e.getMessage());
                    e.printStackTrace();
                }
            });

            dialogBtnUpdatePackage.setOnClickListener(v -> {
                if (Validate()) {
                    dialogAddpackage.cancel();
                    dialogPackageName = edtPackageName.getText().toString();
                    dialogPackagePrice = edtPackagePrice.getText().toString();
                    dialogPackageDesc = edtPackageDesc.getText().toString();
                    dialogPackageServiceName = txtServiceType.getText().toString();
                    packageList.remove(addedPackagePosition);
                    packageList.add(new PackageModel(dialogPackageName,
                            dialogPackagePrice, dialogPackageDesc, dialogPackageServiceName, packageServiceIdList.get(addedPackagePosition)));
                    editServicePackagesAdapter.setPackageList(packageList);

                }
            });

            dialogBtnCancel.setOnClickListener(v -> dialogAddpackage.cancel());
            dialogAddpackage.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogAddpackage.show();
            dialogAddpackage.getWindow().setAttributes(lp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean Validate() {
        if (VU.isEmpty(edtPackageName)) {
            CustomToast.custom_Toast(context, "Enter package name", layout);
            return false;
        } else if (VU.isEmpty(edtPackagePrice)) {
            CustomToast.custom_Toast(context, "Enter package price", layout);
            return false;
        } else if (VU.isEmpty(edtPackageDesc)) {
            CustomToast.custom_Toast(context, "Enter package Description", layout);
            return false;
        }
        return true;
    }


    private void getPackageData() {
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);

        Dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getpackageListData(new PackageDetailsModel(UniqueId, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    Dialog.dismiss();
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        try {
                            dataArray = jsonObject.getJSONArray("data");

                            setAdapter();
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: " + e.getMessage());
                        }

                    } else {
                        //handle failed logic here
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Dialog.dismiss();
            }
        });
    }

    private void setAdapter() {

        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject packageObject = dataArray.getJSONObject(i);
                int packageId = packageObject.getInt("package_id");
                String name = packageObject.getString("package_name");
                String price = packageObject.getString("package_price");
                String desc = packageObject.getString("package_description");
                String servicetype = packageObject.getString("service_name");
                String serviceId = packageObject.getString("service");   // key name is just service but its a service id
                packageList.add(new PackageModel(name, price, desc, packageId, servicetype, serviceId));
            }
            editServicePackagesAdapter.setPackageList(packageList);
        } catch (Exception e) {
            Log.e(TAG, "setAdapter: " + e.getMessage());
        }

    }

    private void updateServiceDescription() {

        oldServiceType = new ArrayList<>();

        ArrayList<PackageModel> packageList = editServicePackagesAdapter.getPackageList();
        for (int i = 0; i < dataArray.length(); i++) {   //updated package
            packageIdList.add(i, packageList.get(i).getPackageId());
            packageNameList.add(i, packageList.get(i).getPackagename());
            packagePriceList.add(i, Integer.valueOf(packageList.get(i).getPackagePrice()));
            packageDescriptionList.add(i, packageList.get(i).getPackageDesc());
            oldpackageServiceIdList.add(i, packageList.get(i).getService_id());
            oldServiceType.add(i, packageList.get(i).getService_id());
        }
        for (int i = dataArray.length(); i < packageList.size(); i++) {
            newpackageNameList.add(packageList.get(i).getPackagename());
            newpackageDescriptionList.add(packageList.get(i).getPackageDesc());
            newpackagePriceList.add(Integer.valueOf(packageList.get(i).getPackagePrice()));
            newpackageServiceIdList.add(packageList.get(i).getService_id());
        }


        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String mobileNo = Utilities.getSPstringValue(context, Utilities.spMobileNo);
        user_id = Utilities.getSPstringValue(context, Utilities.spUserId);
        description = ""; //no use of this parameter

        Dialog = ProgressDialog.show(context, "Please wait", "Loading...");
      /*  Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateServiceDescription(new UpdateServiceDescriptionModel(UniqueId, strServiceDetailsId, service_id,
                strSubscriptionId, serviceProvider, mobileNo, country, state, city, guest, user_id, address, latitude, longitude, landmark, zip_code, description,
                price_type, from_time, to_time, discount, days, packageIdList, packageNameList, packageDescriptionList, packagePriceList,
                authToken, newpackageNameList, newpackagePriceList, newpackageDescriptionList, service_type, service_type));*/


        UpdateServiceDescriptionModel udpData = new UpdateServiceDescriptionModel(UniqueId, strServiceDetailsId, service_id,
                strSubscriptionId, serviceProvider, mobileNo, country, state, city, guest, user_id, address, latitude, longitude, landmark, zip_code, description,
                price_type, from_time, to_time, discount, days, packageIdList, packageNameList, packageDescriptionList, packagePriceList,
                authToken, newpackageNameList, newpackagePriceList, newpackageDescriptionList, oldServiceType, newServiceType);

        CommonFunctions.showLog(TAG, "updateServiceDescription: newpackageDescriptionList " + udpData);
        CommonFunctions.showLog(TAG, "updateServiceDescription: oldServiceType " + oldServiceType.toString());

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateServiceDescription(udpData);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Dialog.dismiss();
                try {

                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: api_response " + api_response);

                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    // Toast.makeText(EditServicePackagsActivity.this, msg, Toast.LENGTH_SHORT).show();
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        statusAlert("Information", "Service details successfully updated", "Success");
                    } else {
                        //handle failed logic here
                        statusAlert("Alert", msg, "Error");
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Dialog.dismiss();
            }
        });
    }

    private void deletePackageById(final ArrayList<PackageModel> list, final int position) {

        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        int packageId = list.get(position).getPackageId();

        Dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deletePackage(new DeletePackageModel(packageId, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                Dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        try {
                            Toast.makeText(context, "Package deleted successfully", Toast.LENGTH_SHORT).show();
                            list.remove(position);   // after deleting we have to remove from list
                            editServicePackagesAdapter.setPackageList(list);
                        } catch (Exception e) {
                            Log.e(TAG, "deletePackageById() : onResponse: " + e.getMessage());
                            e.printStackTrace();
                        }

                    } else {
                        //handle failed logic here
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(context, EditServiceRegistrationDetailsActivity.class);
        intent.putExtra("serviceId", service_id);
        intent.putExtra("uniqueId", UniqueId);
        startActivity(intent);
        finish();
    }

    private void statusChangeAlert(final ArrayList<PackageModel> list, final int positon) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        // builder1.setMessage(title);
        builder1.setTitle("Are you sure want to delete package?");
        builder1.setCancelable(true);
        Log.e(TAG, "statusChangeAlert: positon: " + positon);
        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> {
                    if (list.get(positon).getPackageId() == 0) {
                        list.remove(positon);   // new package is on locally

                        editServicePackagesAdapter.setPackageList(list);
                    } else {
                        if (VU.isConnectingToInternet(context))
                            deletePackageById(list, positon);
                    }
                });

        builder1.setNegativeButton(
                "No",
                (dialog, id) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        Intent intent = new Intent(context, EditServiceImagesActivity.class);
                        intent.putExtra("serviceId", strServiceDetailsId);
                        intent.putExtra("uniqueId", UniqueId);
                        startActivity(intent);
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
