package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.ShowAllImagesActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ViewMyBookingDetailsModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMyServiceBookingDetailsActivity extends AppCompatActivity {

    private String strUniqueId, strBookingId;
    private Context context;
    private SliderLayout sliderLayout;
    private TextView txtServiceType, txtHostedBy, txtAddress, txtContact, txtContactEmail,
            txtStartDate, txtEndDate, txtStartTime, txtEndTime, txtGuest, txtAmt, txtTitle, txtStatus;
    private Dialog loadingDialog;
    private static final String TAG = ViewMyServiceBookingDetailsActivity.class.getSimpleName();
    private ArrayList<String> imagesAddress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_my_service_booking_details);

        context = ViewMyServiceBookingDetailsActivity.this;
        getIntentData();
        initialize();
        //getServiceDetail();
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            strUniqueId = extras.getString("uniqueId");
            strBookingId = extras.getString("serviceBookingId");
            Log.e(TAG, "getIntentData: " + strBookingId + "  " + strUniqueId);
            //Toast.makeText(context, ""+strBookingId, Toast.LENGTH_SHORT).show();
            if (VU.isConnectingToInternet(context)) {
                getServiceDetail();
            }
        }

    }

    private void initialize() {
        imagesAddress = new ArrayList<>();
        txtServiceType = findViewById(R.id.txt_service_type);
        txtHostedBy = findViewById(R.id.txt_hosted_by);
        txtAddress = findViewById(R.id.txt_address);
        txtContact = findViewById(R.id.txt_contact);
        txtContactEmail = findViewById(R.id.txt_contact_email);
        txtStartDate = findViewById(R.id.txt_start_date);
        txtEndDate = findViewById(R.id.txt_end_date);
        txtStartTime = findViewById(R.id.txt_start_time);
        txtEndTime = findViewById(R.id.txt_end_time);
        txtGuest = findViewById(R.id.txt_guest);
        txtAmt = findViewById(R.id.txt_amt);
        txtTitle = findViewById(R.id.booking_service_service_provider);
        txtStatus = findViewById(R.id.txt_status);
        findViewById(R.id.img_show_all_images).setOnClickListener(v -> {
            Intent imgIntent = new Intent(context, ShowAllImagesActivity.class);
            imgIntent.putExtra("imgUrls", imagesAddress);
            startActivity(imgIntent);
        });

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Booking Details");

        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.SWAP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        sliderLayout.setScrollTimeInSec(3); //set scroll delay in seconds :

    }


    //get service detail
    private void getServiceDetail() {

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        System.out.println(Utilities.getSPstringValue(context, Utilities.spAuthToken));
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMyServiceBookingDetails(new ViewMyBookingDetailsModel(strUniqueId, strBookingId,
                Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.d(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject serviceObj = jsonObject.getJSONObject("booked_service");
                        JSONArray imageArray = jsonObject.getJSONArray("service_image");
                        setData(serviceObj);
                        setSliderViews(imageArray);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, getResources().getString(R.string.onResponseFail), Toast.LENGTH_SHORT).show();
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("SetTextI18n")
    private void setData(JSONObject jsonObject) {
        try {
            Log.e(TAG, "setData: " + jsonObject);
            String statusType;
            String[] guestList = getResources().getStringArray(R.array.GuestList);
            String status = jsonObject.getString("status");
            txtServiceType.setText(jsonObject.getString("service_name"));
            txtHostedBy.setText(jsonObject.getString("fname") + " " + jsonObject.getString("lname"));
            txtAddress.setText(jsonObject.getString("address"));
            txtContact.setText(jsonObject.getString("mobileno"));
            txtContactEmail.setText(jsonObject.getString("email_id"));
            txtStartDate.setText(jsonObject.getString("startdate"));
            txtEndDate.setText(jsonObject.getString("enddate"));
            txtStartTime.setText(jsonObject.getString("start_time"));
            txtEndTime.setText(jsonObject.getString("end_time"));
            txtGuest.setText(guestList[Integer.parseInt(jsonObject.getString("guest")) - 1]);
            txtAmt.setText(jsonObject.getString("amount"));

            txtAmt.setText(CommonFunctions.formatNumber(Long.parseLong(jsonObject.getString("amount").split("\\.")[0])));

            txtTitle.setText(jsonObject.getString("company_name"));

            if (status.equalsIgnoreCase("0")) {
                statusType = "In Review";  //
                txtStatus.setTextColor(getResources().getColor(R.color.status_inreviewed));
            } else if (status.equalsIgnoreCase("1")) {
                statusType = "Confirmed";
                txtStatus.setTextColor(getResources().getColor(R.color.status_confirmed));
            } else if (status.equalsIgnoreCase("2")) {
                statusType = "Completed";  // review visible
                txtStatus.setTextColor(getResources().getColor(R.color.status_completed));
            } else {
                statusType = "Canceled";
                txtStatus.setTextColor(getResources().getColor(R.color.status_cancled));
            }
            txtStatus.setText(statusType);
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }

    }

    private void setSliderViews(JSONArray imageArray) {
        Log.e(TAG, "setSliderViews: " + imageArray);
        try {
            if (imageArray != null) {
                for (int i = 0; i < imageArray.length(); i++) {
                    JSONObject imgObject = imageArray.getJSONObject(i);
                    imagesAddress.add(Utilities.IMG_SERVICE_URL + imgObject.getString("service_image"));
                }
                for (int i = 0; i <= 2; i++) {
                    DefaultSliderView sliderView = new DefaultSliderView(context);
                    switch (i) {
                        case 0:
                            sliderView.setImageUrl(imagesAddress.get(i));
                            break;
                        case 1:
                            sliderView.setImageUrl(imagesAddress.get(i));
                            break;
                        case 2:
                            sliderView.setImageUrl(imagesAddress.get(i));
                            break;
                    }
                    sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
//            sliderView.setDescription("The quick brown fox jumps over the lazy dog.\n" +
//                    "Jackdaws love my big sphinx of quartz. " + (i + 1));
                    final int finalI = i;
                    sliderView.setOnSliderClickListener(sliderView1 -> Toast.makeText(context, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show());
                    //at last add this view in your layout :
                    sliderLayout.addSliderView(sliderView);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "setSliderViews: " + e.getMessage());
        }
    }

}
