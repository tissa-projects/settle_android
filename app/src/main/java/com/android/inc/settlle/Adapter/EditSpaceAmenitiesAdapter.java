package com.android.inc.settlle.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AmenitiesRulesModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditSpaceAmenitiesAdapter extends RecyclerView.Adapter<EditSpaceAmenitiesAdapter.MyViewHolder> {

    private final ArrayList<String> amenitiesIdlist;
    private final ArrayList<String> amenitiesNamelist;
    private final ArrayList<AmenitiesRulesModel> models = new ArrayList<>();
    private final Map<Integer, AmenitiesRulesModel> amenitiesModels = new HashMap<>();
    private static final String TAG = EditSpaceAmenitiesAdapter.class.getSimpleName();

    public EditSpaceAmenitiesAdapter(ArrayList<String> amenitiesNamelist, ArrayList<String> amenitiesIdlist, ArrayList<AmenitiesRulesModel> amenitiesList) {

        this.amenitiesNamelist = amenitiesNamelist;
        this.amenitiesIdlist = amenitiesIdlist;

        for (int i = 0; i < amenitiesIdlist.size(); i++) {
            models.add(new AmenitiesRulesModel(amenitiesList.get(i).getAmenitiesRulesName(), amenitiesList.get(i).getAmenitiesRulesNameId(),
                    amenitiesList.get(i).getSelected()));
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_add_space_amenities_rules, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myHolder, final int pos) {
        final MyViewHolder myViewHolder = (MyViewHolder) myHolder;
        myViewHolder.checkBoxAmenities.setText(amenitiesNamelist.get(pos));

        if (amenitiesNamelist.size() != 0) {
            myViewHolder.checkBoxAmenities.setChecked(models.get(pos).isSelected());
            myViewHolder.checkBoxAmenities.setTag(models.get(pos));
            if (models.get(pos).isSelected()) {
                amenitiesModels.put(pos, new AmenitiesRulesModel(amenitiesIdlist.get(pos)));
            }
        }

        myViewHolder.checkBoxAmenities.setOnClickListener(v -> {
            CheckBox cb = (CheckBox) v;
            AmenitiesRulesModel contact = (AmenitiesRulesModel) cb.getTag();
            models.get(pos).setPosition(pos);
            //   Log.e("Adapter", "onClick: "+contact.toString() );
            if (!contact.getSelected()) {
                Log.e(TAG, "onClick: " + "true");
                myViewHolder.checkBoxAmenities.setChecked(true);
                contact.setSelected(cb.isChecked());
                models.get(pos).setSelected(cb.isChecked());
                amenitiesModels.put(pos, new AmenitiesRulesModel(amenitiesIdlist.get(pos)));
            } else {
                Log.e(TAG, "onClick: " + "false");
                myViewHolder.checkBoxAmenities.setChecked(false);
                contact.setSelected(cb.isChecked());
                models.get(pos).setSelected(cb.isChecked());
                amenitiesModels.remove(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return amenitiesNamelist.size();
    }

    public Map<Integer, AmenitiesRulesModel> getAmenitiesList() {
        return amenitiesModels;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final CheckBox checkBoxAmenities;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            checkBoxAmenities = itemView.findViewById(R.id.checkbox_rules_amenities);
        }
    }

    //Set method of OnItemClickListener object
    public void setOnItemClickListener() {
    }
}
