package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.android.inc.settlle.Activities.Service.ServiceBookingFormActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.FacbookGmailLoginModel;
import com.android.inc.settlle.RequestModel.LoginModel;
import com.android.inc.settlle.RequestModel.getUserDataRequestModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.OAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Context context;
    private String email;
    private Dialog loadingDialog;
    private EditText edtEmail, edtPassword;
    LinearLayout google_login, apple_sign_in;
    private String strUniqueId, strId;
    private LoginButton btnFacebook;
    CallbackManager callbackManager;
    private AlertDialog forgotAlertDialog;
    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "XYZ";
    private View layout;
    GoogleSignInClient mGoogleSignInClient;

    private String fName = "", lName = "";


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_new_login);
        context = LoginActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        facebookLoginInit();
        initialize();
        getIntentData();



        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);



    }

    private void initialize() {
        edtEmail = findViewById(R.id.edt_email_login_activity);
        edtPassword = findViewById(R.id.edt_password_login_activity);
        google_login = findViewById(R.id.google_login);
        apple_sign_in = findViewById(R.id.apple_sign_in);

        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.sign_up).setOnClickListener(this);
        google_login.setOnClickListener(this);
        findViewById(R.id.imgBack).setOnClickListener(this);
        findViewById(R.id.text_forgot_password).setOnClickListener(this);
        apple_sign_in.setOnClickListener(this);

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strUniqueId = bundle.getString("unique_id");
            strId = bundle.getString("id");
        }
    }

    //validations
    private void LoginValidations() {
        email = edtEmail.getText().toString().trim().toLowerCase();
        String password = edtPassword.getText().toString().toLowerCase();

        if (email.isEmpty()) {
            edtEmail.requestFocus();
            edtEmail.setError("Enter Email");
        } else if (password.isEmpty()) {
            edtPassword.setError("Enter Password");
            edtPassword.requestFocus();
        } else {
            login();
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if (VU.isConnectingToInternet(context)) {
                    LoginValidations();
                }
                break;
            case R.id.google_login:
                google_login();
                break;

            case R.id.sign_up:
                startActivity(new Intent(context, SignUpActivity.class));
                break;

            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.text_forgot_password:
                forgotAlert();
                break;
            case R.id.apple_sign_in:
                loginWithAppleID();
                break;
        }
    }

    // Request Call login
    private void login() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().login(new LoginModel(edtEmail.getText().toString(), edtPassword.getText().toString()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (jsonObject.getBoolean("status")) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        String isMbConform = dataObject.getString("is_noconfirm");
                        String isEmailConform = dataObject.getString("is_idconfirm");
                        Log.e(TAG, "onResponse: " + isMbConform + " isEmailConform " + isEmailConform);
                        Utilities.setSPstring(context, Utilities.spUserId, dataObject.getString("user_id"));
                        Utilities.setSPstring(context, Utilities.spFirstName, dataObject.getString("fname"));
                        Utilities.setSPstring(context, Utilities.spLastName, dataObject.getString("lname"));
                        Utilities.setSPstring(context, Utilities.spEmail, dataObject.getString("email_id"));
                        Utilities.setSPstring(context, Utilities.spMobileNo, dataObject.getString("mobileno"));

                        Utilities.setSPstring(context, Utilities.spAuthToken, dataObject.getString("auth_token"));
                        Intent i;

                        if (statusCode == 200) {
                            //  Utilities.setSPboolean(context, Utilities.spIsLoggedin, true);
                            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                            if (Utilities.getSPbooleanValue(context, Utilities.spAtBookingSpaceActivity) && isMbConform.equals("1")) {
                                i = new Intent(context, SpaceBookingFormActivity.class);
                                saveparameter(Utilities.spAtBookingSpaceActivity, dataObject);
                                startMyActivity(i);
                            } else if (Utilities.getSPbooleanValue(context, Utilities.spAtGuestSpaceActivity) && isMbConform.equals("1")) {
                                finish();
                                saveparameter(Utilities.spAtGuestSpaceActivity, dataObject);
                            } else if (Utilities.getSPbooleanValue(context, Utilities.spAtGuestServiceActivity) && isMbConform.equals("1")) {
                                finish();
                                saveparameter(Utilities.spAtGuestServiceActivity, dataObject);
                            } else if (Utilities.getSPbooleanValue(context, Utilities.spAtBookingServiceActivity) && isMbConform.equals("1")) {
                                i = new Intent(context, ServiceBookingFormActivity.class);
                                saveparameter(Utilities.spAtBookingSpaceActivity, dataObject);
                                startMyActivity(i);
                            } else if (isMbConform.equals("1")) {
                                saveparameter("", dataObject);
                                Utilities.setSPstring(context, Utilities.spLoginType, "settleLogin");
                                i = new Intent(context, HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startMyActivity(i);
                            } else if (isMbConform.equals("0")) {
                                i = new Intent(context, MobileVerificationActivity.class);
                                Utilities.setSPstring(context, Utilities.spLoginType, "settleLogin");
                                startMyActivity(i);
                            }
                        } else if (isEmailConform.equals("0")) {
                            Toast.makeText(LoginActivity.this, "Please Verify Your Email Id", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                    }
                } catch (NullPointerException ignore) {
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void saveparameter(String activityName, JSONObject dataObject) throws JSONException {
        Utilities.setSPboolean(context, Utilities.spIsLoggedin, true);
        Utilities.setSPboolean(context, activityName, false);
        Utilities.setSPboolean(context, Utilities.spIsMobileverify, true);
        Utilities.setSPstring(context, Utilities.spMobileNo, dataObject.getString("mobileno"));
        Utilities.setSPstring(context, Utilities.spLoginType, "settleLogin");
    }

    private void startMyActivity(Intent i) {
        Utilities.setSPstring(context, Utilities.spLoginType, "settleLogin");
        i.putExtra("unique_id", strUniqueId);
        i.putExtra("id", strId);
        startActivity(i);
        finish();
    }

    private void facebookLoginInit() {
        btnFacebook = findViewById(R.id.login_button);
        callbackManager = CallbackManager.Factory.create();
        btnFacebook.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest data_request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), (json_object, response) -> {
                            try {
                                LoginManager.getInstance().logOut();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                //facebook_id = json_object.getString("id");
                                assert json_object != null;
                                fName = json_object.getString("first_name");
                                lName = json_object.getString("last_name");
                                try {
                                    email = json_object.getString("email");
                                } catch (Exception ignore) {
                                    email = "NA";
                                }
                                // socialMediaLogin(email, fname, lname, "Facebook");

                                getUserDataFromServer(email, "Facebook");

                            } catch (Exception e) {
                                CommonFunctions.statusAlert(LoginActivity.this, "Alert", "Unable to login with Facebook, please try other method", "Okey");
                                Log.d(TAG, "onCompleted: facebook " + e);
                                e.printStackTrace();
                            }
                            System.out.println("Output" + json_object);
                        });
                Bundle permission_param = new Bundle();
                permission_param.putString("fields", "id,name,first_name,last_name,email,picture.width(600).height(600)");
                data_request.setParameters(permission_param);
                data_request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(@NonNull FacebookException error) {
                CommonFunctions.statusAlert(LoginActivity.this, "Alert", "Unable to login with Facebook, please try other method", "Okay");
            }
        });

        findViewById(R.id.fbLoginButton).setOnClickListener(v -> btnFacebook.performClick());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != RESULT_CANCELED && data != null) {

            if (FacebookSdk.isFacebookRequestCode(requestCode)) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
        Log.d(TAG, "onActivityResult: Google signIn " + requestCode);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    //handle google login
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.w(TAG, "signInResult:=" + account.toString());
            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct != null) {
                fName = acct.getGivenName();
                lName = acct.getFamilyName();
                String personEmail = acct.getEmail();
                getUserDataFromServer(personEmail, "Gmail");

               /* if (checkUserData(personGivenName, personFamilyName, personEmail)) {//all data available
                    socialMediaLogin(personEmail, personGivenName, personFamilyName, "Gmail", null);
                } else {
                    updateUserProfile("Gmail", personEmail, personGivenName, personFamilyName);
                }*/

            }
            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            CommonFunctions.statusAlert(LoginActivity.this, "Alert", "Unable to login with google, please try other method", "Okay");
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    //google login
    private void google_login() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void socialMediaLogin(String email, String fname, String lname, final String loginType, AlertDialog ad) {

        CommonFunctions.showLog("socialMediaLogin", " email " + email + " fname " + fname + " lname " + lname + " loginType " + loginType);

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().facebookGmailLogin(new FacbookGmailLoginModel(email, fname, lname, loginType));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    if (ad != null)
                        ad.dismiss();
                } catch (Exception ignore) {
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (jsonObject.getBoolean("status")) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        if (loginType.equalsIgnoreCase("Gmail")) {
                            Utilities.setSPstring(context, Utilities.spLoginType, "googleLogin");
                        } else if (loginType.equalsIgnoreCase("Facbook")) {
                            Utilities.setSPstring(context, Utilities.spLoginType, "FaceBookLogin");
                        }
                        String isMbConform = dataObject.getString("is_noconfirm");
                        String isEmailConform = dataObject.getString("is_idconfirm");
                        Log.e(TAG, "onResponse: " + isMbConform + " isEmailConform " + isEmailConform);
                        Utilities.setSPstring(context, Utilities.spUserId, dataObject.getString("user_id"));
                        Utilities.setSPstring(context, Utilities.spFirstName, dataObject.getString("fname"));
                        Utilities.setSPstring(context, Utilities.spLastName, dataObject.getString("lname"));
                        Utilities.setSPstring(context, Utilities.spEmail, dataObject.getString("email_id"));
                        Utilities.setSPstring(context, Utilities.spAuthToken, dataObject.getString("auth_token"));
                        Intent i = null;
                        if (statusCode == 200) {
                            //  Utilities.setSPboolean(context, Utilities.spIsLoggedin, true);
                            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                            if (Utilities.getSPbooleanValue(context, Utilities.spAtBookingSpaceActivity) && isMbConform.equals("1")) {
                                i = new Intent(context, SpaceBookingFormActivity.class);
                                Utilities.setSPboolean(context, Utilities.spIsLoggedin, true);
                                Utilities.setSPboolean(context, Utilities.spAtBookingSpaceActivity, false);
                                Utilities.setSPboolean(context, Utilities.spIsMobileverify, true);
                                Utilities.setSPstring(context, Utilities.spMobileNo, dataObject.getString("mobileno"));
                            } else if (isMbConform.equals("1")) {
                                Utilities.setSPboolean(context, Utilities.spIsMobileverify, true);
                                Utilities.setSPboolean(context, Utilities.spIsLoggedin, true);
                                Utilities.setSPstring(context, Utilities.spMobileNo, dataObject.getString("mobileno"));
                                i = new Intent(context, HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            } else if (isMbConform.equals("0")) {
                                i = new Intent(context, MobileVerificationActivity.class);
                                Utilities.setSPstring(context, Utilities.spLoginType, "settleLogin");
                            }
                            if (i != null) {
                                i.putExtra("unique_id", strUniqueId);
                                i.putExtra("space_id", strId);
                                startActivity(i);
                                finish();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException ignore) {
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(LoginActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();
            }
        });
    }

    private void forgotAlert() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.forget_password, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        alertDialogBuilder.setCancelable(true);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText emailInput = promptsView
                .findViewById(R.id.edt_forgot_email);

        Button submit = promptsView.findViewById(R.id.btn_forgot_submit);
        Button cancel = promptsView.findViewById(R.id.btn_forgot_cancel);
        submit.setOnClickListener(v -> {

            if (emailInput.getText().toString().isEmpty()) {
                emailInput.setError("Enter registered email");
                emailInput.requestFocus();
            } else if (!Utilities.validateEmail(emailInput.getText().toString())) {
                emailInput.setError("Enter valid email");
                emailInput.requestFocus();
            } else {
                forgot_password(emailInput.getText().toString().trim().toLowerCase());
            }

        });

        cancel.setOnClickListener(v -> forgotAlertDialog.dismiss());

        // create alert dialog
        forgotAlertDialog = alertDialogBuilder.create();

        // show it
        forgotAlertDialog.show();
    }

    //forgot api call
    private void forgot_password(String email) {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().forgotPassword(new LoginModel(email, ""));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                String api_response;
                try {
                    assert response.body() != null;
                    api_response = response.body().string();
                    loadingDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(api_response);
                    Log.d(TAG, "onResponse: " + api_response);
                    if (jsonObject.getBoolean("status")) {
                        Toast.makeText(LoginActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        forgotAlertDialog.dismiss();
                    } else if (jsonObject.getInt("status_code") == 300) {
                        statusAlert("This email id does not exist,Please try with another email");
                    } else {
                        statusAlert(jsonObject.getString("message"));
                        //Toast.makeText(LoginActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void loginWithAppleID() {
        OAuthProvider.Builder provider = OAuthProvider.newBuilder("apple.com");
        List<String> scopes = new ArrayList<String>() {{
            add("email");
            add("name");
        }};
        provider.setScopes(scopes);
        provider.addCustomParameter("locale", "en");

        FirebaseAuth auth = FirebaseAuth.getInstance();
        Task<AuthResult> pending = auth.getPendingAuthResult();

        if (pending != null) {
            pending.addOnSuccessListener(authResult -> Log.d(TAG, "checkPending:onSuccess:" + authResult)).addOnFailureListener(e -> {
                CommonFunctions.statusAlert(LoginActivity.this, "Alert 11 ", "Unable to login with apple id, please try other method", "Okay");
                Log.w(TAG, "checkPending:onFailure", e);
            });
        } /*else {
            CommonFunctions.statusAlert(LoginActivity.this, "Alert 222 ", "Unable to login with apple id, please try other method", "Okay");
        }*/

        auth.startActivityForSignInWithProvider(this, provider.build())
                .addOnSuccessListener(
                        authResult -> {
                            // Sign-in successful!
                            Log.d(TAG, "activitySignIn:onSuccess:" + authResult.getUser());
                            FirebaseUser user = authResult.getUser();
                            assert user != null;
                            getUserDataFromServer(user.getEmail(), "Apple");
                        })
                .addOnFailureListener(
                        e -> CommonFunctions.statusAlert(LoginActivity.this, "Alert", "Unable to login with apple id, please try other method", "Okay"));

    }

    private void updateUserProfile(String loginType, String mail) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.update_user_details_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCancelable(true);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        // create alert dialog
        AlertDialog ad = alertDialogBuilder.create();

        //Retrieve Details and ID
        TextInputEditText etUserEmail = promptsView.findViewById(R.id.etUserEmail);
        //  TextInputEditText etUserMobile = promptsView.findViewById(R.id.etUserMobile);
        TextInputEditText etUserLastName = promptsView.findViewById(R.id.etUserLastName);
        TextInputEditText etUserFirstName = promptsView.findViewById(R.id.etUserFirstName);
        AppCompatButton btn_submit = promptsView.findViewById(R.id.btn_submit);
        AppCompatButton btn_cancel = promptsView.findViewById(R.id.btn_cancel);
        etUserEmail.setText(mail);
        etUserFirstName.setText(this.fName);
        etUserLastName.setText(this.lName);

        btn_cancel.setOnClickListener(v -> {
            ad.dismiss();
            finish();
        });

        btn_submit.setOnClickListener(v -> {
            if (Objects.requireNonNull(etUserFirstName.getText()).toString().trim().isEmpty()) {
                CustomToast.custom_Toast(getApplicationContext(), "Please enter first name", layout);
            } else if (Objects.requireNonNull(etUserLastName.getText()).toString().trim().isEmpty()) {
                CustomToast.custom_Toast(getApplicationContext(), "Please enter last name", layout);
            } /*else if (Objects.requireNonNull(etUserMobile.getText()).toString().trim().isEmpty()) {
                CustomToast.custom_Toast(getApplicationContext(), "Please enter mobile no.", layout);
            }*/ else if (Objects.requireNonNull(etUserEmail.getText()).toString().trim().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(etUserEmail.getText().toString().trim()).matches()) {
                CustomToast.custom_Toast(getApplicationContext(), "Please enter valid email", layout);
            } else {
                socialMediaLogin(etUserEmail.getText().toString().trim(), etUserFirstName.getText().toString().trim(), etUserLastName.getText().toString().trim(), loginType, ad);
            }
        });
        // show it
        ad.show();
    }

    private void getUserDataFromServer(String mail, String type) {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getUserData(new getUserDataRequestModel(mail, ""));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, " getUserDataFromServer onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int status_code = jsonObject.getInt("status_code");
                    if (status_code == 200) {
                        JSONObject data = jsonObject.getJSONObject("data");
                        if (data.length() > 0) {
                            String mail = data.getString("email_id");
                            String fname = data.getString("fname");
                            String lname = data.getString("lname");
                            if (checkUserData(fname, lname, mail)) {
                                socialMediaLogin(mail, fname, lname, type, null);
                            } else {
                                CommonFunctions.showLog("getUserDataFromServer", " checkUserData false ");
                                updateUserProfile(type, mail);
                            }
                        } else {
                            CommonFunctions.showLog("getUserDataFromServer", " data length 0 ");
                            updateUserProfile(type, mail);
                        }
                    } else if (status_code == 300) {
                        CommonFunctions.showLog("getUserDataFromServer", " status code 300 ");
                        updateUserProfile(type, mail);
                    } else {
                        CommonFunctions.statusAlert(LoginActivity.this, "Alert", "Unable to fetch user details, please try after some time", "Okay");
                    }
                } catch (Exception e) {
                    CommonFunctions.statusAlert(LoginActivity.this, "Alert", "Unable to fetch user details, please try after some time", "Okay");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });


    }

    public boolean checkUserData(String fName, String lName, String mail) {
        Log.d(TAG, "checkUserData: " + fName + " " + lName + " " + mail);
        return fName != null && !fName.trim().isEmpty() && fName.trim().length() > 1 &&
                lName != null && !lName.trim().isEmpty() && lName.trim().length() > 1 &&
                mail != null && !mail.trim().isEmpty() && mail.trim().length() >= 4;
    }

    private void statusAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle("Alert");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}