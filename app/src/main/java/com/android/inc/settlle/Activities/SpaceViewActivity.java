package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetSpaceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceViewActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private SliderLayout sliderLayout;
    private LinearLayout spaceViewLayout;
    private String uni, uniqueId, star;
    private Dialog loadingDialog;
    private TextView spaceNameText, desciptionText, priceTypeText, memberSinceTxt, accomodatesText, noOfGuestTxt, eventTypeTxt, hostNameText, hostMobileText, hostEmail;
    private RatingBar ratingBar;
    ArrayList<String> imagesAddress;
    private float rating;
    private String spaceLatitude, spaceLongitude, spaceName;
    private static final String TAG = SpaceViewActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_view);
        context = SpaceViewActivity.this;
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h=findViewById(R.id.txtHeading);
        h.setText("Space Details");
        initialize();
        getIntentData();
        if (VU.isConnectingToInternet(context)) {
            getSpaceDetail();
        }
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
            Log.e(TAG, "getIntentData: uni  : " + uni);
        }

    }

    private void initialize() {

        imagesAddress = new ArrayList<>();

        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.SWAP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        sliderLayout.setScrollTimeInSec(5); //set scroll delay in seconds :

        findViewById(R.id.ll_map).setOnClickListener(this);
        findViewById(R.id.ll_amenities).setOnClickListener(this);
        findViewById(R.id.ll_reviews).setOnClickListener(this);
        findViewById(R.id.ll_rules).setOnClickListener(this);
        findViewById(R.id.img_show_all_images).setOnClickListener(this);


        spaceNameText = findViewById(R.id.booking_space_space_name);
        desciptionText = findViewById(R.id.booking_space_space_about);
        priceTypeText = findViewById(R.id.booking_space_price_type);
        accomodatesText = findViewById(R.id.booking_space_accomodates);
        ratingBar = findViewById(R.id.booking_space_space_rating);
        noOfGuestTxt = findViewById(R.id.booking_space_no_of_guest);
        eventTypeTxt = findViewById(R.id.booking_space_space_type);
        hostNameText = findViewById(R.id.space_show_host_name);
        hostMobileText = findViewById(R.id.space_show_host_mobile);
        hostEmail = findViewById(R.id.space_show_host_email);
        memberSinceTxt = findViewById(R.id.space_show_host_member_since);

        spaceViewLayout = findViewById(R.id.spaceViewLayout);
        spaceViewLayout.setVisibility(View.GONE);

    }

    private void setSliderViews() {

        try {
            Log.d("IMAGES", imagesAddress.toString());
            for (int i = 0; i <= 2; i++) {

                DefaultSliderView sliderView = new DefaultSliderView(context);

                switch (i) {
                    case 0:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 1:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 2:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                }

                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
//            sliderView.setDescription("The quick brown fox jumps over the lazy dog.\n" +
//                    "Jackdaws love my big sphinx of quartz. " + (i + 1));
                final int finalI = i;

                sliderView.setOnSliderClickListener(sliderView1 -> Toast.makeText(context, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show());

                //at last add this view in your layout :
                sliderLayout.addSliderView(sliderView);
            }
        } catch (Exception e) {
            Log.e(TAG, "setSliderViews: catch : " + e.getMessage());
        }

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_map:
                Intent mapIntent = new Intent(context, MapsActivity.class);
                mapIntent.putExtra("latitude", spaceLatitude);
                mapIntent.putExtra("longitude", spaceLongitude);
                mapIntent.putExtra("spaceName", spaceName);
                startActivity(mapIntent);
                break;

            case R.id.ll_amenities:
                Intent amenitiesIntent = new Intent(context, AmenitiesActivity.class);
                amenitiesIntent.putExtra("uni", uniqueId);
                startActivity(amenitiesIntent);
                break;

            case R.id.ll_reviews:
                Intent reviewIntent = new Intent(context, SpaceReviewsActivity.class);
                reviewIntent.putExtra("uni", uniqueId);
                reviewIntent.putExtra("star", star);
                startActivity(reviewIntent);

                break;

            case R.id.ll_rules:
                Intent rulesIntent = new Intent(context, RulesActivity.class);
                rulesIntent.putExtra("uni", uniqueId);
                startActivity(rulesIntent);
                break;


            case R.id.img_show_all_images:
                Intent imgIntent = new Intent(context, ShowAllImagesActivity.class);
                imgIntent.putExtra("imgUrls", imagesAddress);
                startActivity(imgIntent);
                break;

        }
    }

    //get space detail
    private void getSpaceDetail() {
        final String[] guestList = getResources().getStringArray(R.array.GuestList);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceDetail(new GetSpaceDetailModel(uni));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"SetTextI18n", "InflateParams"})
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();

                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);

                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getBoolean("status")) {
                        spaceViewLayout.setVisibility(View.VISIBLE);

                        //handle success logic here
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject spaceDetailObject = dataObject.getJSONObject("space_details");

                        spaceName = spaceDetailObject.getString("title");
                        uniqueId = spaceDetailObject.getString("unique_id");
                        star = spaceDetailObject.getString("stars");
                        spaceNameText.setText(spaceName);
                        eventTypeTxt.setText(spaceDetailObject.getString("venue_name"));
                        hostNameText.setText(spaceDetailObject.getString("fname") + " " + spaceDetailObject.getString("lname"));
                        hostEmail.setText(spaceDetailObject.getString("email_id"));
                        noOfGuestTxt.setText(guestList[Integer.parseInt(spaceDetailObject.getString("guest")) - 1]);
                        hostMobileText.setText(spaceDetailObject.getString("mobileno"));
                        desciptionText.setText(spaceDetailObject.getString("description"));
                        priceTypeText.setText(spaceDetailObject.getString("price_type"));
                        accomodatesText.setText(spaceDetailObject.getString("accomodates"));
                        memberSinceTxt.setText("  " + spaceDetailObject.getString("created_on").trim());
                        String ratingStar = spaceDetailObject.getString("stars");
                        if (ratingStar.equalsIgnoreCase("N/A")) {
                            rating = Float.parseFloat("0");
                        } else {
                            rating = Float.parseFloat(ratingStar);
                        }
                        ratingBar.setRating(rating);
                        spaceLatitude = spaceDetailObject.getString("lat");
                        spaceLongitude = spaceDetailObject.getString("lon");

                        String startTime = spaceDetailObject.getString("from_time");
                        String endTime = spaceDetailObject.getString("to_time");
                        String time = startTime + " - " + endTime;

                        JSONArray dayArray = dataObject.getJSONArray("space_days");
                        for (int i = 0; i < dayArray.length(); i++) {
                            JSONObject dayObject = dayArray.getJSONObject(i);
                            // String day = dayObject.getString("day");
                            View view;
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            view = inflater.inflate(R.layout.timing_layout, null);
                            TextView dayText = view.findViewById(R.id.day);
                            TextView daytime = view.findViewById(R.id.timing);

                            dayText.setText(dayObject.getString("day"));
                            daytime.setText(time);

                            LinearLayout linearLayout = findViewById(R.id.space_timing_layout);
                            linearLayout.addView(view);

                        }

                        JSONArray imgAddressArray = dataObject.getJSONArray("space_image");
                        for (int i = 0; i < imgAddressArray.length(); i++) {
                            JSONObject imgObject = imgAddressArray.getJSONObject(i);
                            imagesAddress.add(Utilities.IMG_SPACE_URL + imgObject.getString("space_image"));

                            //   Toast.makeText(SpaceDetailAndBookActivity.this, imgObject.getString("space_image"), Toast.LENGTH_SHORT).show();
                        }

                        setSliderViews();
                        // ratingBar;
                    } else {
                        //handle failed logic here
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, getResources().getString(R.string.onResponseFail), Toast.LENGTH_SHORT).show();
                loadingDialog.dismiss();

            }
        });
    }

}
