package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class CartServiceDetailUpdate implements Serializable {

    private String unique_id;
    private String service_id;
    private String user_id;
    private String guest_id;
    private String startdate;
    private String enddate;
    private String start_time;
    private String end_time;
    private String amount;
    private String auth_token;
    private String service_cart_id;
    private String package_id;

    public CartServiceDetailUpdate(String service_cart_id, String unique_id, String service_id, String user_id, String package_id,
                                   String guest_id, String startdate, String enddate, String start_time,
                                   String end_time, String amount, String auth_token) {
        this.service_cart_id = service_cart_id;
        this.unique_id = unique_id;
        this.service_id = service_id;
        this.user_id = user_id;
        this.package_id = package_id;
        this.guest_id = guest_id;
        this.startdate = startdate;
        this.enddate = enddate;
        this.start_time = start_time;
        this.end_time = end_time;
        this.amount = amount;
        this.auth_token = auth_token;
    }
}


