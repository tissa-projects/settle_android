package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.inc.settlle.Fragments.MySpaceListFragment;
import com.android.inc.settlle.Fragments.MySpaceRequestFragment;
import com.android.inc.settlle.Fragments.MySpaceSpaceFragment;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Utilities;

public class MySpaceActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MySpaceActivity.class.getSimpleName();
    private int count = 0;
    private static FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_space);
        initialize();

        fm = getSupportFragmentManager();
        Utilities.fragmentTransaction = fm.beginTransaction();
        Utilities.fragmentTransaction.add(R.id.fragment_my_space_container, new MySpaceSpaceFragment());
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        //  Utilities.fragmentTransaction.addToBackStack("ProfileFragment");
        Utilities.fragmentTransaction.commit();

    }

    private void initialize() {
        findViewById(R.id.space_button1).setOnClickListener(this);
        findViewById(R.id.space_button2).setOnClickListener(this);
        findViewById(R.id.space_button3).setOnClickListener(this);

        findViewById(R.id.imgBack).setOnClickListener(v -> onBackPressed());

        TextView h = findViewById(R.id.txtHeading);
        h.setText("My Spaces");

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.space_button1:
                changeFragment(new MySpaceSpaceFragment(), "MySpaceSpaceFragment");
                break;
            case R.id.space_button2:
                changeFragment(new MySpaceListFragment(), "MySpaceListFragment");
                break;
            case R.id.space_button3:
                changeFragment(new MySpaceRequestFragment(), "MySpaceRequestFragment");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        count++;
        Log.e(TAG, "onBackPressed: " + count);
        if (count == 3) {
            // FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            finish();
        }
    }

    private void changeFragment(Fragment targetFragment, String fragmentName) {
        Utilities.fragmentTransaction = fm.beginTransaction();
        Utilities.fragmentTransaction.replace(R.id.fragment_my_space_container, targetFragment);
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Utilities.fragmentTransaction.addToBackStack(fragmentName);
        Utilities.fragmentTransaction.commit();
    }


}
