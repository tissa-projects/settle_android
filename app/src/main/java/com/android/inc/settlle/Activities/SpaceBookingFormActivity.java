package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.android.inc.settlle.Utilities.CommonFunctions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AddtocartSpaceModel;
import com.android.inc.settlle.RequestModel.CartSpaceDetailUpdate;
import com.android.inc.settlle.RequestModel.CartViewDetailsModel;
import com.android.inc.settlle.RequestModel.GetBookingDetailsModel;
import com.android.inc.settlle.RequestModel.SpaceEventModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.DTUtil;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceBookingFormActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnSendBookingRqst, btnAddToCart;
    private EditText edtEvent, edtGuest, edtStartDate, edtEndDate, edtStartTime, edtEndTime;
    private TextView txtSpacePrice;
    private TextView txtDiscountOffered, txtTotalPrice;
    private TextView txtSpaceTotalPrice;
    private TextView txtEstimatePrice;
    private TextView txtFinalPrice;
    private Context context;
    private Dialog loadingDialog;
    private String strUniqueId, Id, strGuestId, spaceEventId, eventPrice, NoOfGuest, fromTime, toTime, discount, strType,
            eventname, endtime, starttime, spaceId;
    private ArrayList<SpaceEventModel> spaceEventList;
    private ArrayList<String> eventList;
    private String startDate, endDate;
    @SuppressLint("SimpleDateFormat")
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private int timeType = 0;
    private int selectedPkg = -1;
    private static final String TAG = SpaceBookingFormActivity.class.getSimpleName();

    @SuppressLint({"SetTextI18n", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_form_space);
        context = SpaceBookingFormActivity.this;
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());

        TextView h = findViewById(R.id.txtHeading);
        h.setText("Booking Request");


        initialize();
        getIntentData();
        Log.e(TAG, "onCreate: strType: " + strType + " strUniqueId: " + strUniqueId + " Id :" + Id);
        if (VU.isConnectingToInternet(context)) {
            if (strType.equals("cart")) {
                getBookingDetails(strUniqueId, Id);  // if coming from viewCartActivity   : update booking details API
                btnSendBookingRqst.setText("Update Booking Request");
            } else {
                btnSendBookingRqst.setText("Send Booking Request");   //  if coming from SpaceDetailAndBookActivity
            }
        }

        if (Utilities.getSPbooleanValue(context, Utilities.spIsBookingType)) {  // coming from space/ service  true and  scenario false
            btnSendBookingRqst.setVisibility(View.VISIBLE);
            btnAddToCart.setVisibility(View.GONE);
        } else {
            btnSendBookingRqst.setVisibility(View.GONE);
            btnAddToCart.setVisibility(View.VISIBLE);
        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            strUniqueId = bundle.getString("unique_id");
            Id = bundle.getString("id");   //spaceID for booking abd card id for  update when coming from cart
            strType = bundle.getString("type");

            if (bundle.getString("eventName").length() > 3) {
                spaceEventId = bundle.getString("eventID");
                edtEvent.setText(bundle.getString("eventName"));

            }
            CommonFunctions.showLog("SpaceBooking", "Event ID " + spaceEventId + " event name " + edtEvent.getText().toString());
        }

        if (VU.isConnectingToInternet(context)) {
            getDetailsForBooking();
        }
    }

    private void initialize() {
        spaceEventList = new ArrayList<>();
        eventList = new ArrayList<>();
        btnSendBookingRqst = findViewById(R.id.book_space_button);
        btnAddToCart = findViewById(R.id.add_to_cart_button);
        edtEvent = findViewById(R.id.edt_booking_events);
        edtGuest = findViewById(R.id.edt_booking_guest);
        edtStartDate = findViewById(R.id.edt_booking_start_date);
        edtEndDate = findViewById(R.id.edt_booking_end_date);
        edtStartTime = findViewById(R.id.edt_booking_start_time);
        edtEndTime = findViewById(R.id.edt_booking_end_time);

        txtSpaceTotalPrice = findViewById(R.id.txt_total_space_price);
        txtSpacePrice = findViewById(R.id.txt_space_price);
        txtDiscountOffered = findViewById(R.id.txt_discount);
        txtEstimatePrice = findViewById(R.id.txt_estimate_price);
        txtFinalPrice = findViewById(R.id.txt_fnal_price);
        txtTotalPrice = findViewById(R.id.txtTotalPrice);

        View bottom_sheet = findViewById(R.id.bottom_sheet2);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        btnSendBookingRqst.setOnClickListener(this);
        edtStartDate.setOnClickListener(this);
        edtEndDate.setOnClickListener(this);
        edtEvent.setOnClickListener(this);
        edtStartTime.setOnClickListener(this);
        edtEndTime.setOnClickListener(this);
        btnAddToCart.setOnClickListener(this);

    }

    @SuppressLint("NonConstantResourceId")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.book_space_button:
                if (VU.isConnectingToInternet(context)) {
                    if (validate()) {
                        if (strType.equals("cart")) {
                            cartAlert("Are you sure want to update cart space detail");
                        } else {
                            gotoConformBookingActivity();
                        }
                    }
                }
                break;
            case R.id.add_to_cart_button:
                if (VU.isConnectingToInternet(context)) {
                    if (validate())
                        cartAlert("Are you sure want to add into the cart");
                }
                break;

            case R.id.edt_booking_start_date:
                CommonFunctions.showLog("SpaceBookin", edtEvent.getText().toString());
                if (!edtEvent.getText().toString().isEmpty() && !edtEvent.getText().toString().trim().equalsIgnoreCase("Select Event")) {
                    selectDate("startDate");
                } else {
                    Toast.makeText(context, "Select event first", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.edt_booking_end_date:
                if (!edtStartDate.getText().toString().isEmpty()) {
                    selectDate("endDate");
                } else {
                    Toast.makeText(context, "Select start date first", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.edt_booking_events:
                eventList.clear();
                setEvent();
                break;

            case R.id.edt_booking_start_time:
                if (!edtStartDate.getText().toString().isEmpty()) {
                    timeType = 1;
                    DTUtil.newTimeArray.clear();
                    ArrayList<String> startTimeArrays = DTUtil.getTiming(edtStartDate.getText().toString(), fromTime, toTime, context, timeType);
                    if (startTimeArrays.size() > 1)
                        showBottomSheetDialog(CommonFunctions.GetStringArray(startTimeArrays));
                    else
                        Toast.makeText(context, "Booking is closed for the day (" + edtStartDate.getText().toString() + ")", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Select start date first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.edt_booking_end_time:
                if (!edtEndDate.getText().toString().isEmpty()) {
                    if (!edtStartTime.getText().toString().isEmpty()) {
                        timeType = 2;
                        DTUtil.newTimeArray.clear();
                        String startTimeTemp = fromTime;
                        if (DTUtil.compareTwoDate(edtStartDate.getText().toString().trim(), edtEndDate.getText().toString().trim(), "=="))
                            startTimeTemp = edtStartTime.getText().toString().trim();
                        ArrayList<String> endTimeArrays = DTUtil.getTiming(edtEndDate.getText().toString(), startTimeTemp, toTime, context, timeType);
                        showBottomSheetDialog(CommonFunctions.GetStringArray(endTimeArrays));
                    } else {
                        Toast.makeText(context, "Select start time first", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Select end date first", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void gotoConformBookingActivity() {
        Intent intent = new Intent(context, ConformBookingActivity.class);
        intent.putExtra("spaceId", Id);
        intent.putExtra("eventId", spaceEventId);
        intent.putExtra("guestId", strGuestId);
        intent.putExtra("uniqueId", strUniqueId);
        intent.putExtra("startDate", edtStartDate.getText().toString());
        intent.putExtra("endDate", edtEndDate.getText().toString());
        intent.putExtra("startTime", edtStartTime.getText().toString());
        intent.putExtra("endTime", edtEndTime.getText().toString());
        intent.putExtra("finalPrice", txtFinalPrice.getText().toString().replace(",", ""));
        intent.putExtra("discount", discount);
        intent.putExtra("spacePrice", txtSpaceTotalPrice.getText().toString());
        intent.putExtra("estimatePrice", txtEstimatePrice.getText().toString().replace(",", ""));
        intent.putExtra("eventName", edtEvent.getText().toString());
        startActivity(intent);
    }

    //get space detail
    private void getDetailsForBooking() {
        final String[] guestList = getResources().getStringArray(R.array.GuestList);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getDetailsForBooking(new GetBookingDetailsModel(strUniqueId));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        //handle success logic here
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject spaceDetailObj = dataObject.getJSONObject("space_details");
                        JSONArray spaceEventArray = dataObject.getJSONArray("space_event");
                        Log.e(TAG, "onResponse: spaceEventArray : " + spaceEventArray);
                        for (int i = 0; i < spaceEventArray.length(); i++) {
                            spaceEventList.add(new SpaceEventModel(spaceEventArray.getJSONObject(i).getString("event_id"),
                                    spaceEventArray.getJSONObject(i).getString("event_name"),
                                    spaceEventArray.getJSONObject(i).getString("event_price"),
                                    spaceEventArray.getJSONObject(i).getString("space_event_id")));
                        }
                        strGuestId = spaceDetailObj.getString("guest");
                        NoOfGuest = guestList[Integer.parseInt(strGuestId) - 1];
                        fromTime = spaceDetailObj.getString("from_time");
                        toTime = spaceDetailObj.getString("to_time");
                        discount = spaceDetailObj.getString("discount");
                        txtDiscountOffered.setText(discount + " %");


                        NumberFormat format = NumberFormat.getCurrencyInstance();
                        format.setMaximumFractionDigits(0);
                        format.setCurrency(Currency.getInstance("INR"));

                        format.format(1000000);


                        edtGuest.setText(NoOfGuest);
                        for (int j = 0; j < spaceEventList.size(); j++) {
                            if (spaceEventList.get(j).getEvent_name().trim().equalsIgnoreCase(edtEvent.getText().toString().trim())) {
                                eventPrice = spaceEventList.get(j).getEvent_price();
                                break;
                            }
                        }
                        CommonFunctions.showLog("TAG", " " + eventPrice);
                    } else {
                        //handle failed logic here
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //update the cart space detail
    private void update_cart() {
        String struserId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String strAuthToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateToCartSpaceDetail(new CartSpaceDetailUpdate(Id, strUniqueId, spaceId, struserId, spaceEventId, strGuestId,
                startDate, endDate, edtStartTime.getText().toString(), edtEndTime.getText().toString(), txtFinalPrice.getText().toString().replace(",", ""), strAuthToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = Objects.requireNonNull(response.body()).string();
                    Log.e(TAG, "onResponse: update_cart " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        //handle success logic here
                        statusAlert("Information", "Cart item updated successfully", "Success");
                        //Toast.makeText(context, "Cart updated successfully", Toast.LENGTH_SHORT).show();
                        //finish();
                    } else {
                        //handle failed logic here
                        statusAlert("Alert", msg, "Error");
                        //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    // Log.e(TAG, "onResponse: catch " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    // eventlist dialog
    private void setEvent() {
        Log.e(TAG, "setEvent: spaceEventList : " + spaceEventList.size());

        for (int i = 0; i < spaceEventList.size(); i++) {
            Log.e(TAG, "setEvent: eventList : Price " + spaceEventList.get(i).getEvent_price() + " " + spaceEventList.get(i).getEvent_name());
            //if (!eventList.contains(spaceEventList.get(i).getEvent_name()))
            eventList.add(spaceEventList.get(i).getEvent_name());
        }

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Event");
        // add a list
        builder.setSingleChoiceItems(Utilities.GetStringArray(eventList), selectedPkg, (dialog, position) -> {
            selectedPkg = position;
            edtEvent.setText(eventList.get(position));
            spaceEventId = spaceEventList.get(position).getEvent_id();
            eventPrice = spaceEventList.get(position).getEvent_price();

            edtStartDate.setText("");
            edtEndDate.setText("");
            edtStartTime.setText("");
            edtEndTime.setText("");


            dialog.dismiss();
        });
        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void selectDate(final String dateType) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        try {
            if (dateType.equals("endDate")) {
                mYear = Integer.parseInt(startDate.split("/")[2].trim());
                mMonth = Integer.parseInt(startDate.split("/")[1].trim());
                mDay = Integer.parseInt(startDate.split("/")[0].trim());
            }
        } catch (Exception ignore) {
        }

        Log.d(TAG, "selectDate: " + mYear + " " + mMonth + " " + mDay);

        //   startdate = sdf.parse(startDate);  //=====================================
        // enddate = sdf.parse(endDate+" "+); ///===================================================
        //showPricingDetails();
        @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {

                    try {
                        if (dateType.equalsIgnoreCase("startDate")) {
                            edtStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            startDate = dayOfMonth + "/" + monthOfYear + "/" + year;
                            edtEndDate.setText("");
                            edtStartTime.setText("");
                            edtEndTime.setText("");
                            //   startdate = sdf.parse(startDate);  //=====================================
                        } else {
                            edtEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            endDate = dayOfMonth + "/" + monthOfYear + "/" + year;
                            edtEndTime.setText("");
                            // enddate = sdf.parse(endDate+" "+); ///===================================================
                            //showPricingDetails();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onDateSet: catch : " + e.getMessage());
                    }

                }, mYear, mMonth, mDay);

        if (dateType.equalsIgnoreCase("startDate")) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            calendar.add(Calendar.DATE, Utilities.MAX_ALLOWED_BOOKING_DAY);
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        }
        datePickerDialog.show();

    }

    @SuppressLint("SetTextI18n")
    private void showPricingDetails() {
        try {
            if (eventPrice == null || eventPrice.equalsIgnoreCase("null"))
                eventPrice = "0.0";

            Date enddate = sdf.parse(endDate + " " + (edtEndTime.getText().toString()).split(" ")[0]);
            Date startdate = sdf.parse(startDate + " " + (edtStartTime.getText().toString()).split(" ")[0]);
            assert enddate != null;
            assert startdate != null;
            long diff = enddate.getTime() - startdate.getTime();
            long daysBetweenDates = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            daysBetweenDates++;
            Log.e(TAG, "onDateSet: days: " + daysBetweenDates + " eventPrice :" + eventPrice);
            txtSpacePrice.setText(" (" + daysBetweenDates + " * " + eventPrice + ") ");

            txtTotalPrice.setText(CommonFunctions.formatNumber((daysBetweenDates) * Long.parseLong(eventPrice)));

            txtSpaceTotalPrice.setText(String.valueOf((daysBetweenDates) * Long.parseLong(eventPrice)));

            //    txtEstimatePrice.setText(txtSpaceTotalPrice.getText().toString());
            txtEstimatePrice.setText(CommonFunctions.formatNumber((daysBetweenDates) * Long.parseLong(eventPrice)));

            strikeThroughText(txtEstimatePrice);

            calculateDiscount(txtSpaceTotalPrice);
        } catch (ParseException e) {
            Log.d(TAG, "showPricingDetails: " + e);
            e.printStackTrace();
        }

    }

    private void strikeThroughText(TextView price) {
        price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @SuppressLint("SetTextI18n")
    private void calculateDiscount(TextView amt) {
        // getting value from editPrice and parsing to double
        double price = Integer.parseInt(amt.getText().toString());
        // same like that getting value from ePercent and parsing to double
        double ePer = Integer.parseInt(discount);
        // percent
        double per = (price / 100f) * ePer;
        double finalAmt = price - per;
        txtFinalPrice.setText(CommonFunctions.formatNumber((long) finalAmt));
       // txtFinalPrice.setText(finalAmt + "0");
    }

    //Bottom shhet code
    @SuppressLint("SetTextI18n")
    private void showBottomSheetDialog(final String[] timeArray) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_list, null);

        ListView bottom_listview = view.findViewById(R.id.bottom_listview);
        TextView txtheading = view.findViewById(R.id.bottom_heading);
        if (timeType == 1) {
            txtheading.setText("Select start Time");

        } else if (timeType == 2) {
            txtheading.setText("Select end Time");

        }

        List<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        list.addAll(Arrays.asList(timeArray));

        bottom_listview.setAdapter(adapter);
        bottom_listview.setOnItemClickListener((parent, view1, position, id) -> {

            if (timeType == 1) {
                edtStartTime.setText("" + timeArray[position]);
                edtEndTime.setText("");
            } else if (timeType == 2) {
                edtEndTime.setText("" + timeArray[position]);
                showPricingDetails();//==========================================================
            }
            mBottomSheetDialog.cancel();
            DTUtil.newTimeArray.clear();
        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    private void addToCart() {
        String struserId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String strAuthToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().AddToCartSpace(new AddtocartSpaceModel(strUniqueId, Id, struserId, spaceEventId, strGuestId,
                startDate, endDate, edtStartTime.getText().toString(), edtEndTime.getText().toString(), txtFinalPrice.getText().toString().replace(",",""), strAuthToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: addToCart " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        //handle success logic here
                        statusAlert("Information", "Successfully added item in your cart", "Success");
                        // Toast.makeText(context, "Added to cart successfully", Toast.LENGTH_SHORT).show();

                    } else {
                        //handle failed logic here
                        statusAlert("Alert", msg, "Error");
                        // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    Log.e(TAG, "onResponse: catch " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        /*Intent i = new Intent(context, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);*/
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private boolean validate() {
        if (VU.isEmpty(edtEvent)) {
            Toast.makeText(context, "Choose your event", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtStartDate)) {
            Toast.makeText(context, "Select start date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtEndDate)) {
            Toast.makeText(context, "Select end date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtStartTime)) {
            Toast.makeText(context, "Select start time", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtEndTime)) {
            Toast.makeText(context, "Select end time", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void cartAlert(String title) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        // builder1.setMessage(title);
        builder1.setTitle(title);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> {

                    if (strType.equals("cart")) {
                        update_cart();
                    } else {
                        addToCart();
                    }
                });

        builder1.setNegativeButton(
                "No",
                (dialog, id) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void getBookingDetails(String unique_id, String space_cart_id) {
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().cartViewDetails(new CartViewDetailsModel(unique_id, space_cart_id, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: getBookingDetails " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("spacelist");
                        JSONObject spaceDataObject = dataArray.getJSONObject(0);
                        spaceEventId = spaceDataObject.getString("event_id");
                        strGuestId = spaceDataObject.getString("guest_id");
                        startDate = spaceDataObject.getString("startdate");
                        endDate = spaceDataObject.getString("enddate");
                        starttime = spaceDataObject.getString("start_time");
                        endtime = spaceDataObject.getString("end_time");
                        discount = spaceDataObject.getString("discount");
                        eventname = spaceDataObject.getString("event_name");
                        spaceId = spaceDataObject.getString("space_id");
                        eventPrice = spaceDataObject.getString("event_price");
                        edtEvent.setText(eventname);

                        edtStartTime.setText(starttime);
                        edtEndTime.setText(endtime);
                        String[] startDateTemp = startDate.split("/");
                        String[] emdDateTemp = endDate.split("/");
                        try {
                            edtStartDate.setText(startDateTemp[0] + "/" + (Integer.parseInt(startDateTemp[1].trim()) + 1) + "/" + startDateTemp[2]);
                            edtEndDate.setText(emdDateTemp[0] + "/" + (Integer.parseInt(emdDateTemp[1].trim()) + 1) + "/" + emdDateTemp[2]);
                        } catch (Exception ignore) {
                        }

                        showPricingDetails();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();

            }
        });
    }


}
