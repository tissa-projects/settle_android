package com.android.inc.settlle.RequestModel;

import java.util.ArrayList;

public class VenueModel {

    String venueId;
    String venueName;
    String yearlyAmt;
    String halfyearlyAmt;
    boolean isSelected;

    public VenueModel(String venueId, String venueName, String yearlyAmt, String halfyearlyAmt, boolean isSelected) {
        this.venueId = venueId;
        this.venueName = venueName;
        this.yearlyAmt = yearlyAmt;
        this.halfyearlyAmt = halfyearlyAmt;
        this.isSelected = isSelected;
    }

    public String getVenueId() {
        return venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public String getYearlyAmt() {
        return yearlyAmt;
    }

    public String getHalfyearlyAmt() {
        return halfyearlyAmt;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
