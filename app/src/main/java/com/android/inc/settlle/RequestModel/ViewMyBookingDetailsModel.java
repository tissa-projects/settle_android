package com.android.inc.settlle.RequestModel;

public class ViewMyBookingDetailsModel {

    private String unique_id;
    private String booking_id;
    private String auth_token;

    public ViewMyBookingDetailsModel(String unique_id, String booking_id, String auth_token) {
        this.unique_id = unique_id;
        this.booking_id = booking_id;
        this.auth_token = auth_token;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
