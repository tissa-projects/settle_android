package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ChangeMobileNoModel;
import com.android.inc.settlle.RequestModel.VerifyOtpModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeMobileNoActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout verifiedLayout, newNoLayout;
    final static String TAG = ChangeMobileNoActivity.class.getName();
    TextView change, mobilenoText;
    Button verify;
    String strMb;
    EditText edtMobileNo;
    Context context;


    Dialog loadingDialog;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mobile_no);
        context = ChangeMobileNoActivity.this;
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h=findViewById(R.id.txtHeading);
        h.setText("Change mobile no.");

        initialize();
    }

    private void initialize() {
        verifiedLayout = findViewById(R.id.verified_layout);
        newNoLayout = findViewById(R.id.new_no_layout);
        newNoLayout.setVisibility(View.GONE);
        change = findViewById(R.id.new_no_change_text);
        change.setOnClickListener(this);
        verify = findViewById(R.id.btn_change_verify_otp);
        verify.setOnClickListener(this);
        edtMobileNo = findViewById(R.id.change_edtMobile);

        mobilenoText = findViewById(R.id.change_mbl_mblno);
        mobilenoText.setText(Utilities.getSPstringValue(context, Utilities.spMobileNo));
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.new_no_change_text:
                verifiedLayout.setVisibility(View.GONE);
                newNoLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_change_verify_otp:
                validate();
                break;
        }

    }

    private void validate() {
        strMb = edtMobileNo.getText().toString();
        if (strMb.isEmpty()) {
            edtMobileNo.setError("Enter mobile number");
            edtMobileNo.requestFocus();
        } else if (strMb.length() != 10) {
            edtMobileNo.setError("Enter valid mobile number");
            edtMobileNo.requestFocus();
        } else if (strMb.equalsIgnoreCase(mobilenoText.getText().toString().trim())) {
            edtMobileNo.setError("Please enter different mobile no.");
            edtMobileNo.requestFocus();
        } else {
            if (VU.isConnectingToInternet(context)) {
                sendOtp();
            }
        }
    }


    private void sendOtp() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().changeMobileNo(new ChangeMobileNoModel(strMb, Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        dailogMobileVerify();
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void dailogMobileVerify() {
        final Dialog mBottomSheetDialog = new Dialog(context);
        mBottomSheetDialog.setContentView(R.layout.dialog_otp_verify); // your custom view.
        final EditText edtOtp = mBottomSheetDialog.findViewById(R.id.edt_otp);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();

        Button btnSubmit = mBottomSheetDialog.findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(view -> {
            String strOtp;
            strOtp = edtOtp.getText().toString();
            Log.e(TAG, "onClick: submit click");
            if (strOtp.trim().equals("")) {
                //CustomToast.custom_Toast(context, "Enter OTP", layout);
                Toast.makeText(context, "Enter OTP", Toast.LENGTH_LONG).show();
            } else {
                if (VU.isConnectingToInternet(context)) {
                    VerifyOtpRequest(strOtp);
                }
            }
        });
        mBottomSheetDialog.findViewById(R.id.resend_otp).setOnClickListener(view -> {
            Log.e(TAG, "onClick: " + strMb);
            if (VU.isConnectingToInternet(context)) {
                validate();
            }
        });
    }


    //call  request
    private void VerifyOtpRequest(String strOTP) {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Log.e(TAG, "VerifyOtpRequest: " + strOTP);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().VerifyOTP(new VerifyOtpModel(strMb, strOTP, Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: api_response :" + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Utilities.setSPstring(context, Utilities.spMobileNo, strMb);
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }
}
