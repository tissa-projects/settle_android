package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.FavourateServiceAdapter;
import com.android.inc.settlle.Adapter.FavourateSpaceAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.DeleteFavServiceModel;
import com.android.inc.settlle.RequestModel.DeleteFavSpaceModel;
import com.android.inc.settlle.RequestModel.MyListModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavourateFragment extends Fragment implements View.OnClickListener {
    private TextView btnSpaceFavourate, btnServiceFavourate;
    private RecyclerView recyclerSpace, recyclerService;
    private ShimmerFrameLayout shimmerFrameLayout;
    View view;
    Context context;

    Dialog loadingDialog;
    FavourateSpaceAdapter favourateSpaceAdapter = null;
    FavourateServiceAdapter favourateServiceAdapter = null;
    private RecyclerView.LayoutManager layoutManagerSpace, layoutManagerService;

    ProgressBar progressBar;
    int spacePage = 1, servicePage = 1;
    private boolean isSpacePageAvailable, isServicePageAvailable;
    //variables for pagination
    private boolean isLoading = true;
    private int spacePastVariableItems, spaceVisibleItemCount, spaceTotalItemCount, spacePrivious_total = 0;
    private int servicePastVariableItems, serviceVisibleItemCount, serviceTotalItemCount, servicePrivious_total = 0;
    private final int spaceView_threshold = 3;
    private final int serviceView_threshold = 3;

    private ImageView imgAccount1, imgAccount2, imgAccount3, imgAccount4;
    private TextView txtAccount1, txtAccount2, txtAccount3, txtAccount4;
    private LinearLayout llAccountbtn1, llAccountbtn2, llAccountbtn3, llAccountbtn4;

    private static final String TAG = FavourateFragment.class.getSimpleName();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_favourate, container, false);
        ImageView editButton = requireActivity().findViewById(R.id.account_button_edit);
        if (editButton.getVisibility() == View.VISIBLE) {
            editButton.setVisibility(View.GONE);
        }
        context = getActivity();
        initialize();
        if (VU.isConnectingToInternet(context)) {
            getFavSpaceList();
            getFavServiceList();
        }
        return view;
    }


    private void initialize() {
        btnServiceFavourate = view.findViewById(R.id.btn_service_favourate);
        btnSpaceFavourate = view.findViewById(R.id.btn_space_favourate);
        recyclerService = view.findViewById(R.id.recycler_account_service_favourate);
        recyclerSpace = view.findViewById(R.id.recycler_account_space_favourate);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container_favourate);
        progressBar = view.findViewById(R.id.progress_bar);

        layoutManagerSpace = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerSpace.setLayoutManager(layoutManagerSpace);
        layoutManagerService = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerService.setLayoutManager(layoutManagerService);

        favourateSpaceAdapter = new FavourateSpaceAdapter(context);
        recyclerSpace.setAdapter(favourateSpaceAdapter);

        favourateServiceAdapter = new FavourateServiceAdapter(context);
        recyclerService.setAdapter(favourateServiceAdapter);

        spaceFavourateVisibility();
        setSpaceAdapterClick();
        setServiceAdapterClick();

        btnServiceFavourate.setOnClickListener(this);
        btnSpaceFavourate.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActivityView();
    }

    private void initializeActivityView() {

        imgAccount1 = requireActivity().findViewById(R.id.img_account1);
        imgAccount2 = requireActivity().findViewById(R.id.img_account2);
        imgAccount3 = requireActivity().findViewById(R.id.img_account3);
        imgAccount4 = requireActivity().findViewById(R.id.img_account4);

        txtAccount1 = requireActivity().findViewById(R.id.txt_account1);
        txtAccount2 = requireActivity().findViewById(R.id.txt_account2);
        txtAccount3 = requireActivity().findViewById(R.id.txt_account3);
        txtAccount4 = requireActivity().findViewById(R.id.txt_account4);

        llAccountbtn1 = requireActivity().findViewById(R.id.account_button1);
        llAccountbtn2 = requireActivity().findViewById(R.id.account_button2);
        llAccountbtn3 = requireActivity().findViewById(R.id.account_button3);
        llAccountbtn4 = requireActivity().findViewById(R.id.account_button4);

        accountBtn4Click();
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void accountBtn4Click() {
        imgAccount4.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtAccount4.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgAccount3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount2.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount2.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount1.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount1.setTextColor(getResources().getColorStateList(R.color.grey_60));

        llAccountbtn1.setClickable(true);
        llAccountbtn2.setClickable(true);
        llAccountbtn3.setClickable(true);
        llAccountbtn4.setClickable(false);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_space_favourate:
                spaceFavourateVisibility();
                break;

            case R.id.btn_service_favourate:
                ServiceFavourateVisibility();
                break;
        }

    }


    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceFavourateVisibility() {
        btnSpaceFavourate.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        btnSpaceFavourate.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        btnServiceFavourate.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        btnServiceFavourate.setTextColor(getResources().getColorStateList(R.color.white));
        recyclerSpace.setVisibility(View.VISIBLE);
        recyclerService.setVisibility(View.GONE);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void ServiceFavourateVisibility() {
        btnServiceFavourate.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        btnServiceFavourate.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        btnSpaceFavourate.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimary));
        btnSpaceFavourate.setTextColor(getResources().getColorStateList(R.color.white));
        recyclerService.setVisibility(View.VISIBLE);
        recyclerSpace.setVisibility(View.GONE);
    }

    private void setSpaceAdapterClick() {
        favourateSpaceAdapter.setOnDeleteClickListener((view, position, dataArray) -> {
            if (dataArray != null) {
                try {
                    if (VU.isConnectingToInternet(context)) {
                        statusChangeAlert("Are you sure want to delete favourite space?", "space", dataArray, position);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onDeleteClickListner: catch : " + e.getMessage());
                }

            }
        });


        recyclerSpace.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                spaceVisibleItemCount = layoutManagerSpace.getChildCount();
                spaceTotalItemCount = layoutManagerSpace.getItemCount();
                spacePastVariableItems = ((LinearLayoutManager) layoutManagerSpace).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (spaceTotalItemCount > spacePrivious_total) {
                            isLoading = false;
                            spacePrivious_total = spaceTotalItemCount;
                        }
                    }

                    if (!isLoading && (spaceTotalItemCount - spaceVisibleItemCount) <= (spacePastVariableItems + spaceView_threshold)) {
                        if (isSpacePageAvailable) {
                            spacePage++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getFavSpaceList();
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void setServiceAdapterClick() {
        favourateServiceAdapter.setOnDeleteClickListener((view, position, jsonArray) -> {
            if (jsonArray != null) {
                try {
                    if (VU.isConnectingToInternet(context)) {
                        statusChangeAlert("Are you sure want to delete favourite service?", "service", jsonArray, position);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onDeleteClickListner: catch : " + e.getMessage());
                }
            }
        });

        recyclerService.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                serviceVisibleItemCount = layoutManagerService.getChildCount();
                serviceTotalItemCount = layoutManagerService.getItemCount();
                servicePastVariableItems = ((LinearLayoutManager) layoutManagerService).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (serviceTotalItemCount > servicePrivious_total) {
                            isLoading = false;
                            servicePrivious_total = serviceTotalItemCount;
                        }
                    }

                    if (!isLoading && (serviceTotalItemCount - serviceVisibleItemCount) <= (servicePastVariableItems + serviceView_threshold)) {
                        if (isServicePageAvailable) {
                            servicePage++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getFavServiceList();
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void getFavSpaceList() {
        Log.e(TAG, "getFavSpaceList: ");
        if (spacePage == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        recyclerSpace.setVisibility(View.GONE);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMyFavouriteSpace(new MyListModel(Utilities.getSPstringValue(context,
                Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken), spacePage, Utilities.LIMIT));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                try {
                    recyclerSpace.setVisibility(View.VISIBLE);
                    assert response.body() != null;
                    String api_response = response.body().string();

                    JSONObject jsonObject = new JSONObject(api_response);
                    Log.e(TAG, "getFavSpaceList: onResponse:" + jsonObject);
                    if (jsonObject.getBoolean("status")) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        if (dataArray.length() > 0) {
                            isSpacePageAvailable = jsonObject.getBoolean("count");
                            favourateSpaceAdapter.setData(dataArray);
                        } else {
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    spacePage--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                spacePage--;

            }
        });
    }

    private void getFavServiceList() {
        Log.d(TAG, "getFavServiceList: wewewer");
        if (servicePage == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMyFavouriteService(new MyListModel(Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken),
                servicePage, Utilities.LIMIT));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "getFavServiceList : onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);

                    if (jsonObject.getBoolean("status")) {
                        Log.e(TAG, "getFavServiceList : onResponse: 1111");
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        Log.e(TAG, "getFavServiceList : onResponse: 22222");
                        if (dataArray.length() > 0) {
                            Log.e(TAG, "getFavServiceList : onResponse: 3333");
                            isServicePageAvailable = jsonObject.getBoolean("count");
                            Log.e(TAG, "onResponse: isServicePageAvailable: " + isServicePageAvailable);
                            favourateServiceAdapter.setData(dataArray);
                        } else {
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getFavServiceList: catch " + e.getMessage());
                    servicePage--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                servicePage--;

            }
        });
    }

    private void deleteFavSpace(String fav_id, String spaceId, final int position) {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteFaveSpace(new DeleteFavSpaceModel(fav_id, Utilities.getSPstringValue(context, Utilities.spUserId), spaceId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e("api_response", "api_response: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String msg = jsonObject.getString("message");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        if (statusCode == 200) {
                            favourateSpaceAdapter.clearData(position);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void deleteFavService(String fav_id, String serviceId, final int position) {
        Log.e(TAG, "deleteFavService: position: " + position + " serviceId: " + serviceId + " fav_id: " + fav_id);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteFaveService(new DeleteFavServiceModel(fav_id,
                Utilities.getSPstringValue(context, Utilities.spUserId), serviceId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e("api_response", "api_response: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String msg = jsonObject.getString("message");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        if (statusCode == 200) {
                            favourateServiceAdapter.clearData(position);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {

                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void statusChangeAlert(String title, final String favType, final JSONArray dataArray, final int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(title);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> {
                    try {
                        if (favType.equalsIgnoreCase("service")) {
                            deleteFavService(dataArray.getJSONObject(position).getString("fav_id"),
                                    dataArray.getJSONObject(position).getString("service_details_id"), position);
                        } else if (favType.equalsIgnoreCase("space")) {
                            deleteFavSpace(dataArray.getJSONObject(position).getString("fav_id"),
                                    dataArray.getJSONObject(position).getString("space_id"), position);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onClick: catch : " + e.getMessage());
                    }
                });

        builder1.setNegativeButton(
                "No",
                (dialog, id) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

}