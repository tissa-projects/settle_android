package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.inc.settlle.Activities.SearchItemListActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.DataModel.NameIdPairModel;

import java.util.ArrayList;
import java.util.Locale;

public class SerachItemListAdapter extends BaseAdapter {


    private final ArrayList<NameIdPairModel> listItemList;
    Context context;
    SearchItemListActivity myActivity;
    ArrayList<NameIdPairModel> searchItemList;


    public SerachItemListAdapter(ArrayList<NameIdPairModel> listItemList, Context context) {
        this.listItemList = listItemList;
        this.context = context;
        myActivity = (SearchItemListActivity) context;
        this.searchItemList = new ArrayList<>();
        searchItemList.addAll(listItemList);

    }

    @Override
    public int getCount() {
        return listItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return listItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.search_item_list_layout, null);
            holder.txt_item_name = view.findViewById(R.id.txt_item_name);
            holder.rl = view.findViewById(R.id.rl);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.txt_item_name.setText(listItemList.get(position).getName());


        view.setOnClickListener(v -> {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("searchName", listItemList.get(position).getName());
            resultIntent.putExtra("searchId", listItemList.get(position).getId());
            myActivity.setResult(Activity.RESULT_OK, resultIntent);
            myActivity.finish();
        });


        return view;
    }

    public static class ViewHolder {
        TextView txt_item_name;
        RelativeLayout rl;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        listItemList.clear();
        Log.e("filter", charText);

        if (charText.length() == 0) {
            listItemList.addAll(searchItemList);
        } else {
            Log.e("char", charText);
            for (NameIdPairModel listItem : searchItemList) {
                if (listItem.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    listItemList.add(listItem);
                    Log.e("list", listItem.getName() + "  " + listItem.getId());
                }
            }
        }
        notifyDataSetChanged();
    }

}