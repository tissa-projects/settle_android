package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class PackageDetailsModel implements Serializable {

        private String unique_id;
        private String auth_token;

    public PackageDetailsModel(String unique_id, String auth_token) {
        this.unique_id = unique_id;
        this.auth_token = auth_token;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
