package com.android.inc.settlle.DataModel;

public class PackageModel {

    private String packagename;
    private String packagePrice;
    private String packageDesc;
    private int packageId;
    private String service_type;
    private String service_id;

    public PackageModel(String packagename, String packagePrice, String packageDesc, String service_type, String service_id) {
        this.packagename = packagename;
        this.packagePrice = packagePrice;
        this.packageDesc = packageDesc;
        this.service_type = service_type;
        this.service_id = service_id;
    }

    public PackageModel(String packagename, String packagePrice, String packageDesc,String service_id) {
        this.packagename = packagename;
        this.packagePrice = packagePrice;
        this.packageDesc = packageDesc;
        this.service_id = service_id;
    }

    public PackageModel(String packagename, String packagePrice, String packageDesc, int packageId, String service_type, String service_id) {
        this.packagename = packagename;
        this.packagePrice = packagePrice;
        this.packageDesc = packageDesc;
        this.packageId = packageId;
        this.service_type = service_type;
        this.service_id = service_id;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }

    public String getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(String packagePrice) {
        this.packagePrice = packagePrice;
    }

    public String getPackageDesc() {
        return packageDesc;
    }

    public void setPackageDesc(String packageDesc) {
        this.packageDesc = packageDesc;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }


    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    @Override
    public String toString() {
        return "PackageModel{" +
                "packagename='" + packagename + '\'' +
                ", packagePrice='" + packagePrice + '\'' +
                ", packageDesc='" + packageDesc + '\'' +
                ", packageId=" + packageId +
                ", service_type='" + service_type + '\'' +
                ", service_id='" + service_id + '\'' +
                '}';
    }
}
