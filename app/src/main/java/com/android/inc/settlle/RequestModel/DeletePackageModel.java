package com.android.inc.settlle.RequestModel;

public class DeletePackageModel {
    private int package_id;
    private String auth_token;

    public DeletePackageModel(int package_id, String auth_token) {
        this.package_id = package_id;
        this.auth_token = auth_token;
    }

    public int getPackage_id() {
        return package_id;
    }

    public void setPackage_id(int package_id) {
        this.package_id = package_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
