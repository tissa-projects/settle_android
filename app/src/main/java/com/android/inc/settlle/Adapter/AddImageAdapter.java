package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.interfaces.IOnCLickListener;

import java.util.ArrayList;
import java.util.List;

public class AddImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> myList;
    private IOnCLickListener onCLickListener;

    public AddImageAdapter() {
        myList = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_add_images, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, final int i) {
        if (myViewHolder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) myViewHolder;
            view.imageView.setImageBitmap(CommonFunctions.GetBitmap(myList.get(i)));
            view.imgCross.setOnClickListener(v -> onCLickListener.OnClick(v, i));
        }
    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView, imgCross;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_image1);
            imgCross = itemView.findViewById(R.id.cross1);
        }
    }

    public void setOnCLickListener(IOnCLickListener onCLickListener) {
        this.onCLickListener = onCLickListener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListData(List<String> list) {
        myList = list;
        notifyDataSetChanged();
    }
}
