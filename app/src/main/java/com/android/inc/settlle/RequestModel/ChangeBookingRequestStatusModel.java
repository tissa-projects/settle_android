package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class ChangeBookingRequestStatusModel implements Serializable {

    private String booking_id;
    private String status;
    private String unique_id;
    private String table;
    private String auth_token;

    public ChangeBookingRequestStatusModel(String booking_id, String status, String unique_id, String table, String auth_token) {
        this.booking_id = booking_id;
        this.status = status;
        this.unique_id = unique_id;
        this.table = table;
        this.auth_token = auth_token;
    }
}
