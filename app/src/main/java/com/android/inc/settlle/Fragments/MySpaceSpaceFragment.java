package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.PaymentMediatorActivity;
import com.android.inc.settlle.Activities.SpaceRegistrationDetailsActivity;
import com.android.inc.settlle.Adapter.VenueListAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.DiscountModel;
import com.android.inc.settlle.RequestModel.ProfileDataModel;
import com.android.inc.settlle.RequestModel.VenueModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomDialog;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.razorpay.Checkout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySpaceSpaceFragment extends Fragment implements View.OnClickListener {

    Button btnPlanYearly, btnplanhalfYearly, btnAlreadySubscribed;
    Context context;
    View view;
    private ArrayList<VenueModel> venueList;
    private TextView txtSelectVenue, txtYearlyAmt, txtApplyCoupon;
    private Dialog dialog;
    private String strVenueId;
    private int totalAmt = 0, subscriptioncount = 0;
    private boolean ispaymentAlreadyDone;

    private LinearLayout llSpacebtn1, llSpacebtn2, llSpacebtn3;
    private ImageView imgSpace1, imgSpace2, imgSpace3;
    private TextView txtSpace1, txtSpace2, txtSpace3;
    private ArrayList<String> venueNameList;
    private ArrayList<String> selectVeneueIdList;
    private ArrayList<String> selectedVenueNameList;
    private ArrayList<String> subscribedVenueIdList;
    private final static String TAG = MySpaceSpaceFragment.class.getName();


    public MySpaceSpaceFragment() {
    }

    //  private JSONArray venueListArray;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_space_space, container, false);
        context = getActivity();
        initialize();
        if (VU.isConnectingToInternet(context)) {
            getVenueList();

        }
        Checkout.preload(requireActivity());
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();

        //txtSelectVenue.setText("Select venue");
        //txtYearlyAmt.setText("₹ " + "0");

        /*if (venueList.size() > 0) {
            for (int i = 0; i < venueList.size(); i++) {
                venueList.get(i).setSelected(false);
            }
        }*/

        initializeActivityView();
        spaceBtn1Click();
    }

    public void initialize() {
        btnPlanYearly = view.findViewById(R.id.btn_select_plan);
        btnplanhalfYearly = view.findViewById(R.id.btn_plan_half_yearly);
        txtSelectVenue = view.findViewById(R.id.txt_select_venue);
        txtYearlyAmt = view.findViewById(R.id.txt_yearly_amt);
        btnAlreadySubscribed = view.findViewById(R.id.btn_already_subscribed);
        txtApplyCoupon = view.findViewById(R.id.txt_apply_coupon);

        btnPlanYearly.setOnClickListener(this);
        btnplanhalfYearly.setOnClickListener(this);
        view.findViewById(R.id.lay1).setOnClickListener(this);
        txtApplyCoupon.setOnClickListener(this);
        btnAlreadySubscribed.setOnClickListener(this);
        // btnAlreadySubscribed.setVisibility(View.GONE);


        venueList = new ArrayList<>();
        venueNameList = new ArrayList<>();
        selectedVenueNameList = new ArrayList<>();
        selectVeneueIdList = new ArrayList<>();

        subscribedVenueIdList = new ArrayList<>();
    }

    private void initializeActivityView() {
        llSpacebtn1 = requireActivity().findViewById(R.id.space_button1);
        llSpacebtn2 = requireActivity().findViewById(R.id.space_button2);
        llSpacebtn3 = requireActivity().findViewById(R.id.space_button3);


        imgSpace1 = requireActivity().findViewById(R.id.img_space1);
        imgSpace2 = requireActivity().findViewById(R.id.img_space2);
        imgSpace3 = requireActivity().findViewById(R.id.img_space3);
        txtSpace1 = requireActivity().findViewById(R.id.txt_space1);
        txtSpace2 = requireActivity().findViewById(R.id.txt_space2);
        txtSpace3 = requireActivity().findViewById(R.id.txt_space3);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceBtn1Click() {
        imgSpace1.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtSpace1.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgSpace3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtSpace3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgSpace2.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtSpace2.setTextColor(getResources().getColorStateList(R.color.grey_60));

        llSpacebtn2.setClickable(true);
        llSpacebtn1.setClickable(false);
        llSpacebtn3.setClickable(true);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_plan_half_yearly:
                String strPlanId;   // fixed halfyearly id
                String strPlanType;
                String strAmt;
                //    sendSpaceSubscriptionData();   // dummy ======================================
                goToSpaceRegistration();  // next Activity with intent data
                break;

            case R.id.btn_select_plan:
                if (validation()) {
                    if (VU.isConnectingToInternet(context)) {
                        strPlanId = "1";   // fixed yearly id
                        strPlanType = "yearly";
                        //strAmt = txtYearlyAmt.getText().toString().split("₹ ")[1];
                        strAmt = txtYearlyAmt.getText().toString().replace(",", "");
                        Utilities.setSPstring(context, Utilities.spPaymentFromActivity, "MySpaceSpaceFragment");
                        Intent paymentIntent = new Intent(context, PaymentMediatorActivity.class);
                        paymentIntent.putExtra("strPlanId", strPlanId);
                        paymentIntent.putExtra("strPlanType", strPlanType);
                        paymentIntent.putExtra("strAmt", strAmt);
                        paymentIntent.putStringArrayListExtra("strIds", selectVeneueIdList);
                        paymentIntent.putStringArrayListExtra("selectedName", selectedVenueNameList);
                        startActivity(paymentIntent);
                        //     checkPaymentDone();
                    }

                }
                break;

            case R.id.lay1:
                checkBoxListDialog();
                txtApplyCoupon.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_already_subscribed:
                Intent i = new Intent(context, SpaceRegistrationDetailsActivity.class);
                startActivity(i);
                break;
            case R.id.txt_apply_coupon:
                if (Integer.parseInt(txtYearlyAmt.getText().toString().replace(",", "")) > 0)
                    showCouponCodeDialog();
                else
                    Toast.makeText(context, "First select venues", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    private boolean validation() {
        if (txtSelectVenue.getText().toString().equalsIgnoreCase("Select Venue")) {
            Toast.makeText(context, "Select Venue", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void goToSpaceRegistration() {
        if (validation()) {
            Intent i = new Intent(context, SpaceRegistrationDetailsActivity.class);
            i.putExtra("venueType", txtSelectVenue.getText().toString());
            i.putExtra("venueId", strVenueId);
            startActivity(i);

        }
    }

    //Get Venue releted Data
    private void getVenueList() {
        dialog = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getVenueList(new ProfileDataModel(Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
                // dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: getVenueList " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String statusCode = Obj.getString("status_code");

                    if (statusCode.equalsIgnoreCase("200")) {
                        JSONObject dataObj = Obj.getJSONObject("data");
                        subscriptioncount = dataObj.getInt("subscount");
                        JSONArray venueArray = dataObj.getJSONArray("venue");
                        Log.e(TAG, "onResponse: venueArray " + venueArray);
                        for (int i = 0; i < venueArray.length(); i++) {
                            String venueId = venueArray.getJSONObject(i).getString("venue_id");
                            String venueName = venueArray.getJSONObject(i).getString("venue_name");
                            String yearlyAmt = venueArray.getJSONObject(i).getString("amount");  // yearly amt
                            String halfyearlyAmt = venueArray.getJSONObject(i).getString("halfly");
                            venueList.add(new VenueModel(venueId, venueName, yearlyAmt, halfyearlyAmt, false));
                        }
                        for (int i = 0; i < venueList.size(); i++) {
                            venueNameList.add(venueList.get(i).getVenueName() + "     ₹. " + venueList.get(i).getYearlyAmt());
                        }
                        if (subscriptioncount > 0) {
                            btnAlreadySubscribed.setVisibility(View.VISIBLE);
                            JSONArray sArray = dataObj.getJSONArray("subscription_id");
                            for (int i = 0; i < sArray.length(); i++) {
                                JSONObject sObject = sArray.getJSONObject(i);
                                subscribedVenueIdList.add(sObject.getString("venue_id"));
                            }
                            Log.e(TAG, "onResponse: sID = " + subscribedVenueIdList.toString());
                        } else {
                            btnAlreadySubscribed.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {
                    Log.d(TAG, "onResponse: getVenueList " + e);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }


    private boolean checkValuePresentInArray(ArrayList<String> array, int toCheckValue) {
        String[] arr = array.toArray(new String[0]);
        boolean isAvailable = false;
        for (String element : arr) {
            if (element.equals("" + toCheckValue)) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        dialog.dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        dialog.dismiss();
    }


    @SuppressLint("SetTextI18n")
    protected void onChangeSelectedReceivers() {
        StringBuilder stringBuilder = new StringBuilder();
        //  txtYearlyAmt.setText("₹ " + totalAmt);

        txtYearlyAmt.setText(CommonFunctions.formatNumber(totalAmt));

        if (selectedVenueNameList.size() > 0) {
            txtApplyCoupon.setVisibility(View.VISIBLE);
            if (selectedVenueNameList.size() < 3) {
                for (String service : selectedVenueNameList) {

                    if (selectedVenueNameList.size() > 1)
                        stringBuilder.append(service).append(",");
                    else
                        stringBuilder.append(service);
                    txtSelectVenue.setText(stringBuilder.toString());
                }
            } else {
                txtSelectVenue.setText(selectedVenueNameList.size() + " venue selected");

            }
        } else {
            txtSelectVenue.setText("Select Venue");
            txtApplyCoupon.setVisibility(View.GONE);
        }
    }

    public void showCouponCodeDialog() {
        final Dialog dialog = new Dialog(context, R.style.Theme_Design_BottomSheetDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_coupon_code);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        EditText edtDiscount = dialog.findViewById(R.id.edt_coupon_code);
        dialog.findViewById(R.id.btn_apply_coupon).setOnClickListener(v -> {
            if (!edtDiscount.getText().toString().isEmpty())
                getDiscount(edtDiscount);
            else
                Toast.makeText(context, "Enter coupon code", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @SuppressLint("SetTextI18n")
    private void calculateDiscount(String discount) {
        // getting value from editPrice and parsing to double
        double price = Integer.parseInt(txtYearlyAmt.getText().toString().replace(",", ""));
        // same like that getting value from ePercent and parsing to double
        Log.e(TAG, "calculateDiscount: discount :" + discount);
        double ePer = Integer.parseInt(discount);
        // percent
        double per = (price / 100f) * ePer;
        double finalAmt = price - per;
        //txtYearlyAmt.setText("₹ " + finalAmt + "0");

        txtYearlyAmt.setText(CommonFunctions.formatNumber((long) finalAmt));
        txtApplyCoupon.setVisibility(View.GONE);
    }

    //Get Venue releted Data
    private void getDiscount(EditText editDiscount) {
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        dialog = ProgressDialog.show(getActivity(), "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getDiscount(new DiscountModel(editDiscount.getText().toString(), authToken, "Space", Utilities.getSPstringValue(context, Utilities.spUserId)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
                // dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String msg = Obj.getString("message");
                    int statusCode = Obj.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        String discount = Obj.getString("data");
                        Log.e(TAG, "onResponse: discount :" + discount);
                        calculateDiscount(discount);
                    } else {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void checkBoxListDialog() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.checkbox_list_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final RecyclerView recyclerView = promptsView
                .findViewById(R.id.checkbox_list_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        VenueListAdapter venueListAdapter = new VenueListAdapter(venueList);
        recyclerView.setAdapter(venueListAdapter);

        venueListAdapter.setOnCheckBoxClickListener((view, position, dataArray) -> {
            CheckBox checkBox = (CheckBox) view;
            boolean isChecked = checkBox.isChecked();


            if (isChecked) {

                if (!checkValuePresentInArray(subscribedVenueIdList, Integer.parseInt(venueList.get(position).getVenueId()))) {
                    selectVeneueIdList.add(venueList.get(position).getVenueId());
                    selectedVenueNameList.add(venueList.get(position).getVenueName());
                    totalAmt = totalAmt + Integer.parseInt(venueList.get(position).getYearlyAmt());
                    venueList.get(position).setSelected(true);
                } else {
                    checkBox.setChecked(false);
                    // Toast.makeText(context, "Already subscribed", Toast.LENGTH_SHORT).show();
                    CustomDialog.alertDialog(context, "Already subscribed!");
                    // btnAlreadySubscribed.setVisibility(View.VISIBLE);
                }
            } else {

                selectVeneueIdList.remove(venueList.get(position).getVenueId());
                selectedVenueNameList.remove(venueList.get(position).getVenueName());
                totalAmt = totalAmt - Integer.parseInt(venueList.get(position).getYearlyAmt());
                venueList.get(position).setSelected(false);

            }

            if (selectVeneueIdList.size() > 0) {
                btnAlreadySubscribed.setVisibility(View.GONE);
            }

        });


        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        (dialog, id) -> {
                            // get user input and set it to result
                            // edit text

                            onChangeSelectedReceivers();

                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle("Select venue");
        alertDialog.setCancelable(false);

        // show it
        alertDialog.show();

    }

}