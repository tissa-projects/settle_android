package com.android.inc.settlle.Adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;

import org.json.JSONArray;

public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.MyViewHolder> {
    private final JSONArray dataArray;
    private final Context context;

    public RulesAdapter(JSONArray dataArray, Context context) {
        this.dataArray = dataArray;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_rules, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        try {
            String rule = dataArray.getJSONObject(i).getString("rules_name");
            myViewHolder.ruleText.setText("" + rule);


            myViewHolder.imgRules.setImageDrawable(CommonFunctions.getRulesIcon(context, dataArray.getJSONObject(i).getString("rules_name")));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;

        TextView ruleText;
        ImageView imgRules;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.cardview_rules);
            this.ruleText = itemView.findViewById(R.id.space_rule_text);
            this.imgRules = itemView.findViewById(R.id.imgRules);
        }
    }
}
