package com.android.inc.settlle.DataModel;

import java.util.List;

public class ScenarioHomeParentModel {
    private String title= "";
    private List<ScenarioHomeChildModel> children;

    public ScenarioHomeParentModel(String title, List<ScenarioHomeChildModel> children) {
        this.title = title;
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ScenarioHomeChildModel> getChildren() {
        return children;
    }

    public void setChildren(List<ScenarioHomeChildModel> children) {
        this.children = children;
    }
}
