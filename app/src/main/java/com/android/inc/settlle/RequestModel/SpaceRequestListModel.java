package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class SpaceRequestListModel implements Serializable {

    private String user_id;
    private int filter;
    private String auth_token;
    private int page;

    public SpaceRequestListModel(String user_id, int filter, String auth_token,int page) {
        this.user_id = user_id;
        this.filter = filter;
        this.auth_token = auth_token;
        this.page = page;

    }
}
