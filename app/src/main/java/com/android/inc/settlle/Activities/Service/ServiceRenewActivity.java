package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.PaymentMediatorActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetDataToRenewModel;
import com.android.inc.settlle.RequestModel.RenewalModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceRenewActivity extends AppCompatActivity {
    private Context context;
    private TextView txtServiceProvider, txtServiceType, txtNoOfGuest, txtPlanType, txtSubscribeOn, txtExpireOn, txtAmt;
    private Dialog dialog;
    private String starts_on;
    private String ends_on;
    private String amount;
    private String renew_amount;
    private String subscription_id;
    private String plan_id;
    private String uniqueId;

    private static final String TAG = ServiceRenewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_renew);

        context = ServiceRenewActivity.this;
        getIntentData();
        initialize();
        if (VU.isConnectingToInternet(context)) {
            getServiceDataToRenew();
        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            starts_on = bundle.getString("startsOn");
            ends_on = bundle.getString("endsOn");
            amount = bundle.getString("Amt");
            renew_amount = bundle.getString("Amt");
            subscription_id = bundle.getString("subscriptionId");
            plan_id = bundle.getString("planId");
            uniqueId = bundle.getString("uniqueId");

            Log.e(TAG, "getIntentData: " + starts_on + "  " + ends_on + "  " + amount + "  " + renew_amount + "  " +
                    subscription_id + "  " + plan_id + " " + uniqueId);
        }
    }

    private void initialize() {
        txtServiceProvider = findViewById(R.id.txt_service_provider);
        txtServiceType = findViewById(R.id.txt_service_type);
        txtNoOfGuest = findViewById(R.id.txt_guest);
        txtPlanType = findViewById(R.id.txt_plan_type);
        txtSubscribeOn = findViewById(R.id.txt_subscribe_on);
        txtExpireOn = findViewById(R.id.txt_expire_on);
        txtAmt = findViewById(R.id.txt_amount);

        findViewById(R.id.imgBack).setOnClickListener(v -> onBackPressed());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Renew Service");

        findViewById(R.id.btn_renew).setOnClickListener(v -> {
            Toast.makeText(context, "work in progress", Toast.LENGTH_SHORT).show();
            if (VU.isConnectingToInternet(context)) {
                Utilities.setSPstring(context, Utilities.spPaymentFromActivity, "RenewActivity");
                Intent paymentIntent = new Intent(context, PaymentMediatorActivity.class);
                paymentIntent.putExtra("strAmt", amount);
                startActivityForResult(paymentIntent, 200);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                String paymentId = data.getStringExtra("result");
                if (paymentId != null) {
                    renewService();
                }
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(context, "You Cancelled The Payment", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //get data Service renew
    private void getServiceDataToRenew() {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServiceDataToRenew(new GetDataToRenewModel(Utilities.getSPstringValue(context, Utilities.spUserId),
                uniqueId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String[] guestList = getResources().getStringArray(R.array.GuestList);

                    int statusCode = jsonObject.getInt("status_code");
                    JSONObject dataObj = jsonObject.getJSONObject("data");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject spaceDetails = dataObj.optJSONObject("subscribed_service");
                        assert spaceDetails != null;
                        String planId = spaceDetails.getString("plan_id");
                        txtServiceProvider.setText(" : " + spaceDetails.getString("company_name"));
                        txtServiceType.setText(" : " + spaceDetails.getString("service_name"));
                        txtSubscribeOn.setText(" : " + spaceDetails.getString("starts_on"));
                        txtExpireOn.setText(" : " + spaceDetails.getString("ends_on"));
                        //txtAmt.setText(" : " + spaceDetails.getString("amount") + "/- Rs");

                        txtAmt.setText(CommonFunctions.formatNumber(Long.parseLong(spaceDetails.getString("amount"))));

                        txtNoOfGuest.setText(" : " + guestList[Integer.parseInt(spaceDetails.getString("guest")) - 1]);
                        if (planId.equalsIgnoreCase("1")) {
                            txtPlanType.setText(" : Yearly");
                        } else {
                            txtPlanType.setText(" : Half Yearly");
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    // Service renew
    private void renewService() {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().serviceRenew(new RenewalModel(starts_on, ends_on, amount, renew_amount,
                subscription_id, plan_id, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(context, "Service Renew Successfully", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "onResponse: catch : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });
    }

}
