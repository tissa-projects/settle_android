package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.EventModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditSpaceEventsAdapter extends RecyclerView.Adapter<EditSpaceEventsAdapter.MyViewHolder> {
    private final ArrayList<String> eventsNamelist;
    private final ArrayList<String> eventsIdlist;
    private final ArrayList<String> eventPriceList;
    Map<Integer, EventModel> eventModels = new HashMap<>();


    public EditSpaceEventsAdapter(ArrayList<String> eventsNamelist, ArrayList<String> eventsIdlist, ArrayList<String> eventPriceList) {
        this.eventsNamelist = eventsNamelist;
        this.eventsIdlist = eventsIdlist;
        this.eventPriceList = eventPriceList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_edit_space_event_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myHolder, @SuppressLint("RecyclerView") final int pos) {

        final MyViewHolder myViewHolder = (MyViewHolder) myHolder;
        final EventModel eventModel = new EventModel();
        eventModel.setPosition(pos);
        myViewHolder.txteventName.setText(eventsNamelist.get(pos));
        myViewHolder.edtPrice.setText(eventPriceList.get(pos));

        eventModel.setEventNameId(eventsIdlist.get(pos));
        eventModel.setEventPrice(myViewHolder.edtPrice.getText().toString());

        myViewHolder.edtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (myViewHolder.edtPrice.length() > 2) {
                    eventModel.setEventPrice(myViewHolder.edtPrice.getText().toString());
                    eventPriceList.set(pos, myViewHolder.edtPrice.getText().toString());
                }
            }
        });
        eventModels.put(pos, eventModel);
        //  eventModels.put(pos, new EventModel(eventsIdlist.get(pos), eventPriceList.get(pos), true));
    }

    @Override
    public int getItemCount() {
        return eventsNamelist.size();
    }


    public Map<Integer, EventModel> getEventList() {
        return eventModels;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        EditText edtPrice;
        TextView txteventName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            edtPrice = itemView.findViewById(R.id.edt_event_price);
            txteventName = itemView.findViewById(R.id.event_name);
        }
    }


    //Set method of OnItemClickListener object
    public void setOnItemClickListener() {
    }
}
