package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class SpaceSearchModel implements Serializable {

    public String startdate;
    public String events;
    public String guest;
    public String venue_id;
    public String user_id;



    public SpaceSearchModel(String startdate, String events, String guest, String venue_id,String user_id) {
        this.startdate = startdate;
        this.events = events;
        this.guest = guest;
        this.venue_id = venue_id;
        this.user_id = user_id;
    }
}
