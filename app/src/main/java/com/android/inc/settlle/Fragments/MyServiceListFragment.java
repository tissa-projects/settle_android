package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.Service.EditServiceKYCActivity;
import com.android.inc.settlle.Activities.Service.EditServiceRegistrationDetailsActivity;
import com.android.inc.settlle.Activities.Service.ServiceShowActivity;
import com.android.inc.settlle.Adapter.MyServiceListAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.MyServiceListModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyServiceListFragment extends Fragment implements View.OnClickListener {


    private RecyclerView recyclerView;
    private View view;
    private ShimmerFrameLayout shimmerFrameLayout;
    private Context context;
    private MyServiceListAdapter myServiceListAdapter;
    private JSONArray dataArray;

    ProgressBar progressBar;
    int page = 1;
    private boolean isPageAvailable = false;
    //variables for pagination
    private boolean isLoading = true;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private final int view_threshold = 3;

    private LinearLayout llServicebtn1, llServicebtn2, llServicebtn3;
    private ImageView imgService1;
    private ImageView imgService2;
    private ImageView imgService3;
    private TextView txtService1, txtService2, txtService3;

    private static final String TAG = MyServiceListFragment.class.getName();

    private TextView filter_all, filter_approved, filter_pending;
    ImageView noDataFoundImg;
    private int filterCode = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_service_list, container, false);
        context = getActivity();
        initialize();
        initRecycler();


        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActivityView();
        spaceBtn2Click();
        if (VU.isConnectingToInternet(context)) {
            // service list response
            myServiceListAdapter.clearData();
            page = 1;
            getServiceList();//by default 1st page data
        }
    }

    private void initialize() {
        recyclerView = view.findViewById(R.id.recycler_my_service_list_fragment);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container_my_service_list);
        progressBar = view.findViewById(R.id.progress_bar);
        noDataFoundImg = view.findViewById(R.id.img_nodatafound);

        filter_all = view.findViewById(R.id.filter_all);
        filter_all.setOnClickListener(this);
        filter_approved = view.findViewById(R.id.filter_approved);
        filter_approved.setOnClickListener(this);
        filter_pending = view.findViewById(R.id.filter_pending);
        filter_pending.setOnClickListener(this);
    }

    private void initializeActivityView() {
        llServicebtn1 = requireActivity().findViewById(R.id.service_button1);
        llServicebtn2 = requireActivity().findViewById(R.id.service_button2);
        llServicebtn3 = requireActivity().findViewById(R.id.service_button3);

        imgService1 = requireActivity().findViewById(R.id.img_service1);
        imgService2 = requireActivity().findViewById(R.id.img_service2);
        imgService3 = requireActivity().findViewById(R.id.img_service3);
        txtService1 = requireActivity().findViewById(R.id.txt_service1);
        txtService2 = requireActivity().findViewById(R.id.txt_service2);
        txtService3 = requireActivity().findViewById(R.id.txt_service3);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceBtn2Click() {
        imgService2.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtService2.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgService3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtService3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgService1.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtService1.setTextColor(getResources().getColorStateList(R.color.grey_60));

        llServicebtn1.setClickable(true);
        llServicebtn2.setClickable(false);
        llServicebtn3.setClickable(true);
    }

    private void initRecycler() {
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        myServiceListAdapter = new MyServiceListAdapter(context);
        recyclerView.setAdapter(myServiceListAdapter);
        adapterCall();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVariableItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > privious_total) {
                            isLoading = false;
                            privious_total = totalItemCount;
                        }
                    }
                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                        if (isPageAvailable) {
                            page++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getServiceList();
                        }/* else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }*/
                        isLoading = true;
                    }
                }
            }
        });
    }


    // Request Call space
    private void getServiceList() {
        noDataFoundImg.setVisibility(View.GONE);
        if (page == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMyServiceListData(new MyServiceListModel(userId, authToken, page, Utilities.LIMIT, filterCode));  //by default 1st page
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            dataArray = jsonObject.getJSONArray("data");
                            isPageAvailable = jsonObject.getBoolean("count");
                            CommonFunctions.showLog("CheckDataSize Exception 111 ", " " + dataArray);
                            myServiceListAdapter.setData(dataArray);
                        } else {
                            if (page == 1) {
                                noDataFoundImg.setVisibility(View.VISIBLE);
                            } else {
                                noDataFoundImg.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch: " + e.getMessage());
                    e.printStackTrace();
                    page--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                page--;

            }
        });
    }

    private void adapterCall() {

        //Edit btn
        myServiceListAdapter.setOnEditBtnClickListener((uniqueId, serviceId) -> {
            CommonFunctions.showLog("CheckDataSize ", " Edit Called" + uniqueId + " " + serviceId);
            Intent intent = new Intent(context, EditServiceRegistrationDetailsActivity.class);
            intent.putExtra("uniqueId", uniqueId);
            intent.putExtra("serviceId", serviceId);
            startActivity(intent);
        });
        //View btn
        myServiceListAdapter.setOnViewBtnClickListener((uniqueId, serviceID) -> {
            // Toast.makeText(context, "View clicked", Toast.LENGTH_SHORT).show();
            CommonFunctions.showLog("CheckDataSize ", " View Called " + uniqueId);
            Intent intent = new Intent(context, ServiceShowActivity.class);
            intent.putExtra("uni", uniqueId);
            startActivity(intent);

        });
        //documents btn
        myServiceListAdapter.setOnDocumentsBtnClickListener((uniqueId, serviceId) -> {
            CommonFunctions.showLog("CheckDataSize ", " Doc Called");
            Intent intent = new Intent(context, EditServiceKYCActivity.class);
            Log.e(TAG, "CheckDataSize : uniqueId : " + uniqueId + " serviceId: " + serviceId);
            intent.putExtra("uniqueId", uniqueId);
            intent.putExtra("serviceId", serviceId);
            startActivity(intent);
        });

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        page = 1;
        myServiceListAdapter.clearData();

        switch (v.getId()) {

            case R.id.filter_all:
                filter_all.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_approved.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_pending.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.white));
                filter_approved.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_pending.setTextColor(getResources().getColor(R.color.colorPrimary));


                filterCode = 0;
                if (VU.isConnectingToInternet(context)) {
                    getServiceList();
                }
                break;

            case R.id.filter_approved:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_approved.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_pending.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_approved.setTextColor(getResources().getColor(R.color.white));
                filter_pending.setTextColor(getResources().getColor(R.color.colorPrimary));

                filterCode = 1;
                if (VU.isConnectingToInternet(context)) {
                    getServiceList();
                }
                break;


            case R.id.filter_pending:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_approved.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_pending.setBackgroundResource(R.drawable.curve_button_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_approved.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_pending.setTextColor(getResources().getColor(R.color.white));
                filterCode = 4;
                if (VU.isConnectingToInternet(context)) {
                    getServiceList();
                }
                break;


        }
    }
}
