package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.inc.settlle.Fragments.MyServiceListFragment;
import com.android.inc.settlle.Fragments.MyServiceRequestFragment;
import com.android.inc.settlle.Fragments.MyServiceServiceFragment;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Utilities;

public class MyServiceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MyServiceActivity.class.getSimpleName();
    private int count = 0;
    private static FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_service);
        initialize();
        fm = getSupportFragmentManager();
        Utilities.fragmentTransaction = fm.beginTransaction();
        Utilities.fragmentTransaction.add(R.id.fragment_my_service_container, new MyServiceServiceFragment());
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Utilities.fragmentTransaction.commit();
    }

    private void initialize() {
        findViewById(R.id.service_button1).setOnClickListener(this);
        findViewById(R.id.service_button2).setOnClickListener(this);
        findViewById(R.id.service_button3).setOnClickListener(this);
        findViewById(R.id.imgBack).setOnClickListener(this);

        TextView h = findViewById(R.id.txtHeading);
        h.setText("My Service");
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.service_button1:
                changeFragment(new MyServiceServiceFragment(), "MyServiceServiceFragment");
                break;

            case R.id.service_button2:
                changeFragment(new MyServiceListFragment(), "MyServiceListFragment");
                break;

            case R.id.service_button3:
                changeFragment(new MyServiceRequestFragment(), "MyServiceRequestFragment");
                break;

            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        count++;
        Log.e(TAG, "onBackPressed: " + count);
        if (count == 3) {
            // FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            finish();
        }
    }

    private void changeFragment(Fragment targetFragment, String fragmentName) {
        Utilities.fragmentTransaction = fm.beginTransaction();
        Utilities.fragmentTransaction.replace(R.id.fragment_my_service_container, targetFragment);
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Utilities.fragmentTransaction.addToBackStack(fragmentName);
        Utilities.fragmentTransaction.commit();
    }


}
