package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.inc.settlle.Adapter.AddServicePackageAdapter;
import com.android.inc.settlle.DataModel.PackageModel;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AddServiceDescriptionModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddServicePackageActivity extends AppCompatActivity implements View.OnClickListener {
    private Context context;
    private String serviceProvider;
    private String guest;
    private String country;
    private String state;
    private String city;
    private String address;
    private String latitude;
    private String longitude;
    private String zip_code;
    private String landmark;
    private String discount;
    private String price_type;
    private String from_time;
    private String to_time;
    private ArrayList<Integer> days = null;
    private ArrayList<String> service_id, serviceNameList, subscription_ids;
    private Dialog dialog;
    private View layout;
    private ArrayList<String> packageNameList = null, packageDescList = null, serviceType = null;
    private ArrayList<Integer> packagePriceList = null;
    private AddServicePackageAdapter addServicePackageAdapter;
    private Dialog dialogAddpackage;
    private EditText edtPackageName, edtPackagePrice, edtPackageDesc;
    private String dialogPackageName, dialogPackagePrice, dialogPackageDesc;
    //  private JSONArray jsonAddNewPackage;
    private int addedPackagePosition;
    private ArrayList<PackageModel> packageList;
    String selected_service_id_for_pkg;
    Spinner pkgSpinner;

    private static final String TAG = AddServicePackageActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service_package);

        context = AddServicePackageActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        Init();
        getIntentData();
    }

    @SuppressLint("NotifyDataSetChanged")
    private void Init() {
        RecyclerView recycler = findViewById(R.id.recycler_edit_service_package);


//        jsonAddNewPackage = new JSONArray();
        packageNameList = new ArrayList<>();
        packagePriceList = new ArrayList<>();
        packageDescList = new ArrayList<>();
        packageList = new ArrayList<>();
        serviceType = new ArrayList<>();


        @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recycler.setLayoutManager(layoutManager);
        addServicePackageAdapter = new AddServicePackageAdapter();
        recycler.setAdapter(addServicePackageAdapter);

        addServicePackageAdapter.viewDetailsClickListener((v, position) -> {
            addedPackagePosition = position;
            dialogAddPackages("view_and_edit");  // view and edit package
        });

        addServicePackageAdapter.deletePackageClickListener((v, position) -> {
            packageList.remove(position);
            addServicePackageAdapter.notifyDataSetChanged();
        });

        findViewById(R.id.btn_send_for_review).setOnClickListener(this);
        findViewById(R.id.fab).setOnClickListener(this);
        //  findViewById(R.id.back_searched_service).setOnClickListener(this);
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Add Service Packages");
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            serviceProvider = extras.getString("serviceProvider");
            service_id = extras.getStringArrayList("service_id");
            subscription_ids = extras.getStringArrayList("subscription_ids");
            serviceNameList = extras.getStringArrayList("service_name_list");

            guest = extras.getString("guest");
            country = extras.getString("country");
            state = extras.getString("state");
            city = extras.getString("city");
            address = extras.getString("address");
            latitude = extras.getString("latitude");
            longitude = extras.getString("longitude");
            zip_code = extras.getString("zip_code");
            landmark = extras.getString("landmark");
            discount = extras.getString("discount");
            price_type = extras.getString("price_type");
            from_time = extras.getString("from_time");
            to_time = extras.getString("to_time");
            days = extras.getIntegerArrayList("daysId");

        }

        CommonFunctions.showLog(TAG, "getIntentData: " + serviceProvider + service_id + guest + country + state + city + " " +
                address + latitude + longitude + zip_code + landmark + discount + price_type + from_time + to_time + days);

    }

    //Get releted Data
    private void setServiceRegistrationData(ArrayList<PackageModel> packageArray) {
        String phone = Utilities.getSPstringValue(context, Utilities.spMobileNo);
        for (int i = 0; i < packageArray.size(); i++) {
            try {
                packageNameList.add(packageArray.get(i).getPackagename());
                packagePriceList.add(Integer.valueOf(packageArray.get(i).getPackagePrice()));
                packageDescList.add(packageArray.get(i).getPackageDesc());
            } catch (Exception e) {
                e.printStackTrace();
                CommonFunctions.showLog(TAG, "setServiceRegistrationData: " + e.getMessage());
            }
        }
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: " + packageNameList + "/n" + packageDescList + "/n" + packagePriceList);

        //  subscription_id = Utilities.getSPstringValue(context, Utilities.spSubscriptionId);
        String user_id = Utilities.getSPstringValue(context, Utilities.spUserId);
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: country : " + country + " city : " + city + " state : " + state);
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        AddServiceDescriptionModel modelObj = new AddServiceDescriptionModel(service_id, subscription_ids, serviceNameList, serviceProvider, phone,  // no use of description in service but send blank bcoz api is like that
                country, state, city, guest, user_id, address, latitude, longitude, landmark, zip_code, "",
                price_type, from_time, to_time, discount, auth_token, days, packageNameList, packageDescList, packagePriceList, serviceType);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().setServiceRegistrationData
                (modelObj);
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: " + modelObj);


        CommonFunctions.showLog(TAG, "setServiceRegistrationData: service_id " + service_id.toString());
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: subscription_ids " + subscription_ids.toString());
        CommonFunctions.showLog(TAG, "setServiceRegistrationData:  serviceNameList " + serviceNameList.toString());
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: serviceProvider " + serviceProvider);
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: packageNameList " + packageNameList.toString());
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: packageDescList " + packageDescList.toString());
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: packagePriceList " + packagePriceList.toString());
        CommonFunctions.showLog(TAG, "setServiceRegistrationData: serviceType " + serviceType.toString());


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    CommonFunctions.showLog(TAG, "onResponse: add detail" + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");
                    String msg = Obj.getString("message");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        String insertId = Obj.getString("insert_id");
                        statusAlert("Information", msg, "Success", insertId);
                    } else {
                        statusAlert("Alert", msg, "Error", "");
                    }
                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onResponse: " + e.getMessage());
                    statusAlert("Alert", context.getResources().getString(R.string.network_error), "Error", "");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                CommonFunctions.showLog(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
    }

    private void statusAlert(String title, String msg, String type, String insertId) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        Intent intent = new Intent(context, ServiceAddImagesActivity.class);
                        intent.putExtra("insertId", insertId);
                        startActivity(intent);
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                dialogAddPackages("add");  // add package
                break;

            case R.id.btn_send_for_review:
                ArrayList<PackageModel> packageArray = addServicePackageAdapter.getPackageList();


                if (!checkServicePkgAddedOrNot(service_id, serviceType)) {
                    CustomToast.custom_Toast(context, "Add atleast one package for each service", layout);

                } else {
                    if (VU.isConnectingToInternet(context)) {
                        setServiceRegistrationData(packageArray);
                    }
                }
//                if (packageArray.size() > 0) {
//                    if (VU.isConnectingToInternet(context)) {
//                        setServiceRegistrationData(packageArray);
//                    }
//                } else {
//                    CustomToast.custom_Toast(context, "Add atleast one package", layout);
//                }
                break;
        }
    }


    private boolean checkServicePkgAddedOrNot(ArrayList<String> array1, ArrayList<String> array2) {
        int count = 0;
        for (int i = 0; i < array1.size(); i++) {
            for (int j = 0; j < array2.size(); j++) {

                if (array1.get(i).equalsIgnoreCase(array1.get(j))) {
                    count++;
                    break;
                }
            }
        }
        return count == array1.size();
    }

    public void dialogAddPackages(String type) {
        try {
            dialogAddpackage = new Dialog(context);
            dialogAddpackage.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            dialogAddpackage.setContentView(R.layout.dialog_add_service_package);

            dialogAddpackage.setCancelable(false);

            edtPackageName = dialogAddpackage.findViewById(R.id.edt_package_name);
            edtPackagePrice = dialogAddpackage.findViewById(R.id.edt_package_price);
            edtPackageDesc = dialogAddpackage.findViewById(R.id.edt_package_description);
            Button dialogBtnUpdatePackage = dialogAddpackage.findViewById(R.id.btn_update_package);
            Button dialogBtnAddPackage = dialogAddpackage.findViewById(R.id.btn_add_package);

            String[] serviceList = CommonFunctions.toStringArray(serviceNameList);

            pkgSpinner = dialogAddpackage.findViewById(R.id.spinner_service_type);

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, serviceList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            pkgSpinner.setAdapter(adapter);
            //spinner.setPrompt("Select service");
            //if you want to set any action you can do in this listener
            pkgSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1,
                                           int position, long id) {

                    selected_service_id_for_pkg = service_id.get(position);
                }


                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });
            ///////


            if (type.equalsIgnoreCase("view_and_edit")) {
                dialogBtnUpdatePackage.setVisibility(View.VISIBLE);
                dialogBtnAddPackage.setVisibility(View.GONE);

                edtPackageName.setText(packageList.get(addedPackagePosition).getPackagename());
                edtPackagePrice.setText(packageList.get(addedPackagePosition).getPackagePrice());
                edtPackageDesc.setText(packageList.get(addedPackagePosition).getPackageDesc());

                Log.d(TAG, "dialogAddPackages: " + packageList.get(addedPackagePosition).getService_type());
                Log.d(TAG, "dialogAddPackages: " + packageList.get(addedPackagePosition).getService_id());
                for (int i = 0; i < service_id.size(); i++) {
                    if (service_id.get(i).equalsIgnoreCase(packageList.get(addedPackagePosition).getService_id())) {
                        pkgSpinner.setSelection(i);
                        break;
                    }
                }
            } else {
                dialogBtnUpdatePackage.setVisibility(View.GONE);
                dialogBtnAddPackage.setVisibility(View.VISIBLE);
            }

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialogAddpackage.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

            dialogBtnAddPackage.setOnClickListener(v -> {
                try {
                    if (Validate()) {
                        dialogAddpackage.cancel();
                        dialogPackageName = edtPackageName.getText().toString();
                        dialogPackagePrice = edtPackagePrice.getText().toString();
                        dialogPackageDesc = edtPackageDesc.getText().toString();

                        CommonFunctions.showLog(TAG, "onClick: selected_service_id_for_pkg===" + selected_service_id_for_pkg);
                        packageList.add(new PackageModel(dialogPackageName, dialogPackagePrice, dialogPackageDesc, selected_service_id_for_pkg));
                        CommonFunctions.showLog(TAG, "onClick: " + packageList);
                        addServicePackageAdapter.setPackageList(packageList);
                        serviceType.add(selected_service_id_for_pkg);
                    }
                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onClick: " + e.getMessage());
                    e.printStackTrace();
                }
            });

            dialogBtnUpdatePackage.setOnClickListener(v -> {
                if (Validate()) {
                    dialogAddpackage.cancel();
                    dialogPackageName = edtPackageName.getText().toString();
                    dialogPackagePrice = edtPackagePrice.getText().toString();
                    dialogPackageDesc = edtPackageDesc.getText().toString();
                    packageList.remove(addedPackagePosition);
                    packageList.add(addedPackagePosition, new PackageModel(dialogPackageName, dialogPackagePrice, dialogPackageDesc, selected_service_id_for_pkg));
                    addServicePackageAdapter.setPackageList(packageList);

                }
            });

            dialogAddpackage.findViewById(R.id.btn_cancel).setOnClickListener(v -> dialogAddpackage.cancel());
            dialogAddpackage.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogAddpackage.show();
            dialogAddpackage.getWindow().setAttributes(lp);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean Validate() {
        if (VU.isEmpty(edtPackageName)) {
            CustomToast.custom_Toast(context, "Enter package name", layout);
            return false;
        } else if (VU.isEmpty(edtPackagePrice)) {
            CustomToast.custom_Toast(context, "Enter package price", layout);
            return false;
        } else if (VU.isEmpty(edtPackageDesc)) {
            CustomToast.custom_Toast(context, "Enter package Description", layout);
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
