package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.inc.settlle.Fragments.FavourateFragment;
import com.android.inc.settlle.Fragments.MyBookingFragment;
import com.android.inc.settlle.Fragments.MySubscriptionFragment;
import com.android.inc.settlle.Fragments.ProfileFragment;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Utilities;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = AccountActivity.class.getSimpleName();
    private int count = 0;
    public static FragmentManager fm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        initialize();
        fm = getSupportFragmentManager();
        Utilities.fragmentTransaction = fm.beginTransaction();
        Utilities.fragmentTransaction.add(R.id.fragment_account_container, new ProfileFragment());
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Utilities.fragmentTransaction.commit();

        @SuppressLint({"MissingInflatedId", "LocalSuppress"}) TextView h = findViewById(R.id.txtHeading);
        h.setText("Account");


    }

    private void initialize() {

        findViewById(R.id.account_button1).setOnClickListener(this);
        findViewById(R.id.account_button2).setOnClickListener(this);
        findViewById(R.id.account_button3).setOnClickListener(this);
        findViewById(R.id.account_button4).setOnClickListener(this);
        findViewById(R.id.back_arrow).setOnClickListener(this);

        findViewById(R.id.account_button1).setClickable(false);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.account_button1:
                changeFragment(new ProfileFragment(), "ProfileFragment");
                break;

            case R.id.account_button2:
                changeFragment(new MySubscriptionFragment(), "MySubscriptionFragment");
                break;

            case R.id.account_button3:
                changeFragment(new MyBookingFragment(), "BookingFragment");
                break;

            case R.id.account_button4:
                changeFragment(new FavourateFragment(), "FavourateFragment");
                break;

            case R.id.back_arrow:
                onBackPressed();
                break;

            case R.id.account_button_edit:
                //showing popup menu
                break;

        }
    }

    private void changeFragment(Fragment targetFragment, String fragmentName) {
        Utilities.fragmentTransaction = fm.beginTransaction();
        Utilities.fragmentTransaction.replace(R.id.fragment_account_container, targetFragment);
        Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        Utilities.fragmentTransaction.addToBackStack(fragmentName);
        Utilities.fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        count++;
        Log.e(TAG, "onBackPressed: " + count);

        if (count == 4) {
            /*// FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }*/
            finish();
        }
    }
}
