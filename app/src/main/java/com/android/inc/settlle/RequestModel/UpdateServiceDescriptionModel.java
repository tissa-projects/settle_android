package com.android.inc.settlle.RequestModel;

import java.io.Serializable;
import java.util.ArrayList;

public class UpdateServiceDescriptionModel implements Serializable {

    private String unique_id;
    private String service_details_id;
    private String service_id;
    private String subscription_id;
    private String company_name;
    private String mobile;
    private String country;
    private String state;
    private String city;
    private String guest;
    private String user_id;
    private String address;
    private String latitude;
    private String longitude;
    private String landmark;
    private String zip_code;
    private String description;
    private String price_type;
    private String from_time;
    private String to_time;
    private String discount;
    private ArrayList<Integer> days;
    private ArrayList<Integer> package_id;
    private ArrayList<String> package_name;
    private ArrayList<String> package_description;
    private ArrayList<Integer> package_price;
    private String auth_token;
    private ArrayList<String> new_package_name;
    private ArrayList<Integer> new_package_price;
    private ArrayList<String> new_package_description;
    private ArrayList<String> service_type;
    private ArrayList<String> new_service_type;


    public UpdateServiceDescriptionModel(String unique_id, String service_details_id, String service_id, String subscription_id, String company_name,
                                         String mobile, String country, String state, String city, String guest, String user_id, String address,
                                         String latitude, String longitude, String landmark, String zip_code, String description, String price_type,
                                         String from_time, String to_time, String discount, ArrayList<Integer> days, ArrayList<Integer> package_id,
                                         ArrayList<String> package_name, ArrayList<String> package_description, ArrayList<Integer> package_price,
                                         String auth_token, ArrayList<String> new_package_name, ArrayList<Integer> new_package_price,
                                         ArrayList<String> new_package_description, ArrayList<String> service_type, ArrayList<String> new_service_type) {
        this.unique_id = unique_id;
        this.service_details_id = service_details_id;
        this.service_id = service_id;
        this.subscription_id = subscription_id;
        this.company_name = company_name;
        this.mobile = mobile;
        this.country = country;
        this.state = state;
        this.city = city;
        this.guest = guest;
        this.user_id = user_id;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.landmark = landmark;
        this.zip_code = zip_code;
        this.description = description;
        this.price_type = price_type;
        this.from_time = from_time;
        this.to_time = to_time;
        this.discount = discount;
        this.days = days;
        this.package_id = package_id;
        this.package_name = package_name;
        this.package_description = package_description;
        this.package_price = package_price;
        this.auth_token = auth_token;
        this.new_package_name = new_package_name;
        this.new_package_price = new_package_price;
        this.new_package_description = new_package_description;
        this.service_type = service_type;
        this.new_service_type = new_service_type;
    }
}