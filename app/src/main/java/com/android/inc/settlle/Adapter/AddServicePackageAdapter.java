package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.inc.settlle.DataModel.PackageModel;
import com.android.inc.settlle.R;
import com.android.inc.settlle.interfaces.IOnCLickListener;

import java.util.ArrayList;

public class AddServicePackageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<PackageModel> packageList;
    private IOnCLickListener viewOnCLickListener;
    private IOnCLickListener deleteOnCLickListener;
    private static final String TAG = AddServicePackageAdapter.class.getSimpleName();

    public AddServicePackageAdapter() {
        packageList = new ArrayList<>();
        Log.e(TAG, "AddServicePackageAdapter: "+packageList.size() );
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_edit_service_package, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,final int i) {
        if (viewHolder instanceof MyViewHolder) {
            final MyViewHolder myViewHolder = (MyViewHolder) viewHolder;

            try {
                myViewHolder.txtPackageName.setText(packageList.get(i).getPackagename());
                myViewHolder.txtViewDetails.setOnClickListener(v -> viewOnCLickListener.OnClick(v,i));

                myViewHolder.imgDelete.setOnClickListener(v -> deleteOnCLickListener.OnClick(v,i));
            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder: " + e.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPackageList(ArrayList<PackageModel> packageList) {
        this.packageList = packageList;
        notifyDataSetChanged();
    }

    public void viewDetailsClickListener(IOnCLickListener iOnCLickListener){
        this.viewOnCLickListener = iOnCLickListener;
    }

    public void deletePackageClickListener(IOnCLickListener iOnCLickListener){
        this.deleteOnCLickListener = iOnCLickListener;
    }

    public ArrayList<PackageModel> getPackageList() {
       return this.packageList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtPackageName, txtViewDetails;
        ImageView imgDelete;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtPackageName = itemView.findViewById(R.id.txt_package_name);
            txtViewDetails = itemView.findViewById(R.id.txt_view_details);
            imgDelete = itemView.findViewById(R.id.img_delete);
        }
    }
}
