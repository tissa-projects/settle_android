package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class EditSpaceImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private List<String> myList, IdList;
    private RecyclerViewItemClickListener.ICrossBtnClickListeners onCLickListener;

    public EditSpaceImageAdapter(Context context) {
        this.myList = new ArrayList<>();
        this.IdList = new ArrayList<>();
        //     this.imageIdList = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_add_images, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, final int pos) {
        if (myViewHolder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) myViewHolder;
            if (myList.get(pos).length() < 100) {
                String imgUrl = Utilities.IMG_SPACE_URL + myList.get(pos);
                Tools.displayImageOriginalString(view.imageView, imgUrl);
            } else {
                view.imageView.setImageBitmap(CommonFunctions.GetBitmap(myList.get(pos)));
            }
            view.imgCross.setOnClickListener(v -> onCLickListener.onItemClick(pos, myList, IdList));
        }
    }

    @Override
    public int getItemCount() {
        return myList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView, imgCross;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_image1);
            imgCross = itemView.findViewById(R.id.cross1);
        }
    }


    public void setOnCLickListener(RecyclerViewItemClickListener.ICrossBtnClickListeners onCLickListener) {
        this.onCLickListener = onCLickListener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListData(List<String> list, List<String> IdList) {
        for (int i = 0; i < list.size(); i++) {
            myList.add(list.get(i));
            this.IdList.add(IdList.get(i));
        }

        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setUpdatedListData(List<String> list, List<String> IdList) {
        this.myList = list;
        this.IdList = IdList;
        notifyDataSetChanged();
    }

    public List<String> getImageIdList() {
        return IdList;
    }

    public List<String> getImageList() {
        return myList;
    }
}