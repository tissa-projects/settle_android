package com.android.inc.settlle.RequestModel;

public class GetDataToRenewModel {

    private String user_id;
    private String unique_id;
    private String auth_token;

    public GetDataToRenewModel(String user_id, String unique_id, String auth_token) {
        this.user_id = user_id;
        this.unique_id = unique_id;
        this.auth_token = auth_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
