package com.android.inc.settlle.RequestModel;

public class CartViewDetailsModel {
    private String unique_id;
    private String space_cart_id;
    private String auth_token;

    public CartViewDetailsModel(String unique_id, String space_cart_id, String auth_token) {
        this.unique_id = unique_id;
        this.space_cart_id = space_cart_id;
        this.auth_token = auth_token;
    }
}
