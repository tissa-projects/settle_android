package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class AddSpaceToFavModel implements Serializable {


    private String user_id;
    private String space_id;
    private String auth_token;

    public AddSpaceToFavModel(String user_id, String space_id, String auth_token) {
        this.user_id = user_id;
        this.space_id = space_id;
        this.auth_token = auth_token;
    }
}
