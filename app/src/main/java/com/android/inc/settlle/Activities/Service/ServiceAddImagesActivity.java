package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.inc.settlle.Adapter.AddImageAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ServiceMultipleImageModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceAddImagesActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private List<String> myList;
    private String insertId;
    private Dialog SpaceUploadImagesDialog;
    private static final String TAG = ServiceAddImagesActivity.class.getName();
    AddImageAdapter myAdapter;
    ActivityResultLauncher<Intent> activityResultLauncher;
    int selectionType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service_images);
        initOnStartActivity();
        context = ServiceAddImagesActivity.this;
        initialise();
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Add Service Image");
        getIntentData();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        /* insertId = "3";*/
        if (bundle != null) {
            insertId = bundle.getString("insertId");
            CommonFunctions.showLog(TAG, "getIntentData Service ID : " + insertId);
        }
    }

    private void initialise() {

        RecyclerView addServiceImageRecycler = findViewById(R.id.recycler);
        myList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        addServiceImageRecycler.setLayoutManager(mLayoutManager);

        myAdapter = new AddImageAdapter();
        addServiceImageRecycler.setAdapter(myAdapter);
        myAdapter.setOnCLickListener((v, position) -> {
            myList.remove(position);
            myAdapter.setListData(myList);
        });

        findViewById(R.id.btn_add_images).setOnClickListener(this);
        findViewById(R.id.btn).setOnClickListener(this);
        findViewById(R.id.fab).setOnClickListener(this);
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_images:
            case R.id.fab:
                try {
                    if (myList.size() < 5)
                        if (checkNRequestPermission())
                            imageTypeDialog();
                        else
                            Toast.makeText(context, "Please allow permission", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, "Services does not contain more than 5 images", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn:
                if (myList.size() < 3) {
                    Toast.makeText(context, "Services must have at least 3 images", Toast.LENGTH_SHORT).show();
                } else if (myList
                        .size() > 5) {
                    Toast.makeText(context, "Services does not contain more than 5 images", Toast.LENGTH_SHORT).show();
                } else {
                    if (VU.isConnectingToInternet(context)) {
                        UploadImageArrayData();
                    }
                }
                break;
        }
    }


    //upload images
    private void UploadImageArrayData() {

        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);

        SpaceUploadImagesDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().setServiceMultipleImages(new ServiceMultipleImageModel(myList, auth_token, userId, insertId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceUploadImagesDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");
                    String msg = Obj.getString("message");
                    Log.e(TAG, "onResponse: " + statusCode + " " + msg);
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        statusAlert("Congratulations", "Service images added successfully", "Success", Obj);
                    } else {
                        statusAlert("Alert", msg, "error", Obj);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    statusAlert("Alert", getResources().getString(R.string.network_error), "error", new JSONObject());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                SpaceUploadImagesDialog.dismiss();
            }
        });
    }

    public void LoadImage() {
        myAdapter.setListData(myList);
    }

    private static final int PERMISSION_REQUEST_CODE = 200;

    String imgselectionType, serviceProofDocExtension;
    boolean accessPermission = false;

    private boolean checkNRequestPermission() {
        //check which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String perm : Utilities.appPermission) {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);
            accessPermission = false;
            return false;
        }
        accessPermission = true;
        return true;
    }

    private void imageTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(context);
            docTypeAlert.setTitle("Add Photo!");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];
                //handle logic here
                if (imgselectionType.equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    selectionType = Utilities.RESULT_CAMERA;
                    activityResultLauncher.launch(cameraIntent);
                    // startActivityForResult(cameraIntent, Utilities.RESULT_CAMERA);
                } else if (imgselectionType.equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    selectionType = Utilities.RESULT_GALLERY;
                    activityResultLauncher.launch(intent);
                    // startActivityForResult(Intent.createChooser(intent, "Select Picture"), Utilities.RESULT_GALLERY);
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImageURI = data.getData();
                getDataForImage(selectedImageURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Utilities.RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
            serviceProofDocExtension = "png";
            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
            myList.add(strProfileImg);
            LoadImage();
        }
    }*/

    @SuppressLint("SetTextI18n")
    public void initOnStartActivity() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                getDataForImage(selectedImageURI);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Utilities.RESULT_CAMERA == selectionType) {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            serviceProofDocExtension = "png";
                            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                            myList.add(strProfileImg);
                            LoadImage();
                        }
                    }
                });
    }

    private void getDataForImage(Uri selectedImageURI) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory("SPACE_IMAGE.png", context);
            if (fileName != null) {
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    myList.add(CommonFunctions.GetBase64FromFile(fileName));
                    serviceProofDocExtension = "png";
                    LoadImage();
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void statusAlert(String title, String msg, String type, JSONObject Obj) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        try {
                            String insertId = Obj.getString("insert_id");
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, ServiceKYCDocumentActivity.class);
                            intent.putExtra("insertId", insertId);
                            startActivity(intent);
                        } catch (Exception ignore) {
                        }
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
