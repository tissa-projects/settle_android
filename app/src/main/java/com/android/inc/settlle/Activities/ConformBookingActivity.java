package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.SendBookingRequestModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConformBookingActivity extends AppCompatActivity implements View.OnClickListener {

    private String strUniqueId, strSpaceId, strGuestId, spaceEventId, startTime, endTime,
            discount, startDate, endDate, finalPrice, eventName, estimatePrice, spacePrice;
    private Context context;
    private Dialog loadingDialog;
    private static final String TAG = ConformBookingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conform_booking);
        context = ConformBookingActivity.this;
        getIntentData();
        initialize();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strUniqueId = bundle.getString("uniqueId");
            strSpaceId = bundle.getString("spaceId");
            strGuestId = bundle.getString("guestId");
            spaceEventId = bundle.getString("eventId");
            startTime = bundle.getString("startTime");
            endTime = bundle.getString("endTime");
            startDate = bundle.getString("startDate");
            endDate = bundle.getString("endDate");
            finalPrice = bundle.getString("finalPrice");
            discount = bundle.getString("discount");
            eventName = bundle.getString("eventName");
            estimatePrice = bundle.getString("estimatePrice");
            spacePrice = bundle.getString("spacePrice");

        }
    }

    @SuppressLint("SetTextI18n")
    private void initialize() {
        String[] guestList = getResources().getStringArray(R.array.GuestList);
        TextView txtEvent = findViewById(R.id.txtEventName);
        TextView txtGuest = findViewById(R.id.txt_number_guest);
        TextView txtStartDate = findViewById(R.id.txt_select_strt_date);
        TextView txtEndDate = findViewById(R.id.txt_select_end_date);
        TextView txtStartTime = findViewById(R.id.txt_select_strt_time);
        TextView txtEndTime = findViewById(R.id.txt_select_end_time);
        TextView txtSpacePrice = findViewById(R.id.txt_space_price);
        TextView txtDiscount = findViewById(R.id.txt_discount);
        TextView txtestimatePrice = findViewById(R.id.txt_estimate_price);
        TextView txtFinalPrice = findViewById(R.id.txt_final_price);


        txtEvent.setText(eventName);
        txtGuest.setText(guestList[Integer.parseInt(strGuestId) - 1]);
        txtStartDate.setText(startDate);
        txtEndDate.setText(endDate);
        txtStartTime.setText(startTime);
        txtEndTime.setText(endTime);
        //txtSpacePrice.setText("₹ " + spacePrice);
        txtDiscount.setText(discount + " %");
        //  txtestimatePrice.setText("₹ " + estimatePrice);
        //  txtFinalPrice.setText("₹ " + finalPrice);


        txtSpacePrice.setText(CommonFunctions.formatNumber(Long.parseLong(spacePrice)));
        txtestimatePrice.setText(CommonFunctions.formatNumber(Long.parseLong(estimatePrice)));
        txtFinalPrice.setText(CommonFunctions.formatNumber(Long.parseLong(finalPrice.split("\\.")[0])));

        strikeThroughText(txtestimatePrice);
        findViewById(R.id.btn_confirm).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Confirm Booking");
    }

    private void strikeThroughText(TextView price) {
        price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                if (VU.isConnectingToInternet(context)) {
                    sendBookingReqst();
                }
                break;
            case R.id.btn_cancel:
                finish();
                break;
        }
    }

    //get space detail
    private void sendBookingReqst() {
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String auth = Utilities.getSPstringValue(context, Utilities.spAuthToken);

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Log.e(TAG, "sendBookingReqst: " + strSpaceId + " " + spaceEventId + " " + userId + " " + strGuestId + " " + strUniqueId);
        System.out.println(Utilities.getSPstringValue(context, Utilities.spAuthToken));
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendBookingRequest(new SendBookingRequestModel(strSpaceId, spaceEventId, userId, strGuestId,
                strUniqueId, startDate, endDate, startTime, endTime, finalPrice, auth));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        statusAlert("Congratulations", msg, "Success");
                    } else {
                        statusAlert("Alert", msg, "Error");
                    }
                } catch (Exception e) {
                    statusAlert("Information", getResources().getString(R.string.network_error), "Error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    dialog.dismiss();
                    if (type.equalsIgnoreCase("Success")) {
                        Intent i = new Intent(context, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }

                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
