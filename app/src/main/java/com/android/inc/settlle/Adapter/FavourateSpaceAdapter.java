package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.Activities.SpaceDetailAndBookActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FavourateSpaceAdapter extends RecyclerView.Adapter<FavourateSpaceAdapter.MyViewHolder> {

    private final Context context;
    JSONArray dataArray;
    private static final String TAG = FavourateSpaceAdapter.class.getSimpleName();
    public RecyclerViewItemClickListener.DelFavListner deleteClickListener;

    public FavourateSpaceAdapter(Context context) {
        this.context = context;
        dataArray = new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_favourate_space_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        try {

            myViewHolder.spaceNameTextView.setText(dataArray.getJSONObject(position).getString("title"));
            myViewHolder.spaceAddressTextView.setText(dataArray.getJSONObject(position).getString("address"));
            myViewHolder.venueTypeTextView.setText(dataArray.getJSONObject(position).getString("venue_name"));
            String imageName = dataArray.getJSONObject(position).getString("image_name");
            String imageURl = Utilities.IMG_SPACE_URL + imageName;
            String rating = dataArray.getJSONObject(position).getString("stars");

            if (rating.equalsIgnoreCase("N/A")) {
                rating = "0";
            }
            myViewHolder.ratingBar.setRating(Float.parseFloat(rating));

            Log.e(TAG, "onBindViewHolder: " + imageURl);
            if (VU.isConnectingToInternet(context)) {
                Tools.displayImageOriginalString(myViewHolder.spaceImage, imageURl);
            }
            myViewHolder.viewButton.setOnClickListener(v -> {
                try {
                    Intent intent = new Intent(context, SpaceDetailAndBookActivity.class);
                    intent.putExtra("uni", dataArray.getJSONObject(position).getString("unique_id"));
                    intent.putExtra("eventID", "");
                    intent.putExtra("eventName", "");
                    context.startActivity(intent);

                } catch (JSONException e) {
                    Log.e(TAG, "onClick: catch: " + e.getMessage());
                    e.printStackTrace();
                }
            });

            myViewHolder.deleteButton.setOnClickListener(v -> {
                if (dataArray != null) {
                    deleteClickListener.onDeleteClickListner(v, position, dataArray);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("venue_name", dataArray.getJSONObject(i).getString("venue_name"));
                jsonObject.put("address", dataArray.getJSONObject(i).getString("address"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("unique_id", dataArray.getJSONObject(i).getString("unique_id"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                jsonObject.put("fav_id", dataArray.getJSONObject(i).getString("fav_id"));
                jsonObject.put("space_id", dataArray.getJSONObject(i).getString("space_id"));
                this.dataArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearData(int position) {
        dataArray.remove(position);
        notifyDataSetChanged();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView spaceImage;
        TextView spaceNameTextView;
        TextView spaceAddressTextView;
        TextView venueTypeTextView;
        AppCompatButton viewButton;
        AppCompatButton deleteButton;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.cardview);
            this.spaceImage = itemView.findViewById(R.id.fav_space_image);
            this.spaceNameTextView = itemView.findViewById(R.id.fav_space_space_name);
            this.spaceAddressTextView = itemView.findViewById(R.id.fav_space_space_address);
            this.venueTypeTextView = itemView.findViewById(R.id.fav_space_space_venue_type);
            this.viewButton = itemView.findViewById(R.id.fav_space_view_button);
            this.deleteButton = itemView.findViewById(R.id.fav_space_delete_button);
            this.ratingBar = itemView.findViewById(R.id.fav_space_ratingBar);

        }
    }

    public void setOnDeleteClickListener(RecyclerViewItemClickListener.DelFavListner deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }
}
