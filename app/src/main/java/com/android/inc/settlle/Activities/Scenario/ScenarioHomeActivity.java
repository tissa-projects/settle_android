package com.android.inc.settlle.Activities.Scenario;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.android.inc.settlle.Utilities.CommonFunctions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.inc.settlle.Activities.GuestSpaceActivity;
import com.android.inc.settlle.Activities.HomeActivity;
import com.android.inc.settlle.Activities.HomeGuestActivity;
import com.android.inc.settlle.Activities.Service.GuestServiceActivity;
import com.android.inc.settlle.Activities.Service.ServiceDetailsAndBookActivity;
import com.android.inc.settlle.Activities.SpaceDetailAndBookActivity;
import com.android.inc.settlle.Adapter.ScenarioServiceAdapter;
import com.android.inc.settlle.Adapter.ScenarioSpaceAdapter;
import com.android.inc.settlle.Fragments.HomeFragment;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetScenarioHomaDataModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.SliderLayout;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScenarioHomeActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView spaceRecyclerView, serviceRecyclerView, spaceSearchRecyclerView, serviceSearchRecyclerView;
    private Context context;
    private ShimmerFrameLayout shimmerFrameLayout;
    // private ParentAdapter parentAdapter;
    private ScenarioSpaceAdapter scenarioSpaceAdapter, scenarioSearchSpaceAdapter;
    private ScenarioServiceAdapter scenarioServiceAdapter, scenarioSearchServiceAdapter;

    private final String TAG = ScenarioHomeActivity.class.getName();

    private String[] placeNameArray, eventIdsArray, eventNamesArray, serviceIdArray, serviceNameArray;
    Dialog loadingDialog;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private TextView txtPlace, txtEvent, txtGuest, txtService;
    private String[] guestLimitArray;
    private String[] guestLimitIdArray;
    LinearLayout spaceLayout, serviceLayout;
    String location = "", eventId = "", guestId = "";
    TextView spaceAllViewbutton, serviceAllViewbutton;
    private ArrayList<String> cityList, cityIdList;
    ArrayList<String> imagesAddress;
    private SliderLayout sliderLayout;


    //protected Button selectReceiversBtn;
//    protected CharSequence[] receivers = {
//            "Receiver1", "Receiver2", "Receivers3"};
    protected ArrayList<String> selectedServices = new ArrayList<>();
    protected ArrayList<String> selectedServicesName = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scenario_home);
        context = ScenarioHomeActivity.this;
        initialize();
        setAdapters();
        if (VU.isConnectingToInternet(context)) {
            getHomeData();
            getCreatedScenarioData();
        }
    }

    private void initialize() {
        cityList = HomeFragment.cityList;
        cityIdList = HomeFragment.cityIdList;
        Button btnCreate = findViewById(R.id.scenario_create_button);
        txtPlace = findViewById(R.id.scenario_home_txtPlace);
        txtEvent = findViewById(R.id.scenario_home_txtEvent);
        txtGuest = findViewById(R.id.scenario_home_txtGuest);
        txtService = findViewById(R.id.scenario_home_txtService);
        //txtService = findViewById(R.id.scenario_create_button);
        spaceLayout = findViewById(R.id.scenario_space_layout);
        serviceLayout = findViewById(R.id.scenario_service_layout);
        spaceLayout.setVisibility(View.GONE);
        serviceLayout.setVisibility(View.GONE);

        sliderLayout = findViewById(R.id.imageSlider);
        imagesAddress = new ArrayList<>();
        String imgURL = Utilities.SCENARIO_SLIDER;
        imagesAddress.add(imgURL + "vendor-1.jpg");
        imagesAddress.add(imgURL + "vendor-7.jpg");
        imagesAddress.add(imgURL + "location-pic.jpg");
        imagesAddress.add(imgURL + "location-pic-2.jpg");
        imagesAddress.add(imgURL + "location-pic-3.jpg");
        imagesAddress.add(imgURL + "catering.jpg");
        setSliderViews();


        spaceAllViewbutton = findViewById(R.id.seeAllSpace);
        serviceAllViewbutton = findViewById(R.id.seeAllServices);

        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        findViewById(R.id.lay3).setOnClickListener(this);
        findViewById(R.id.lay4).setOnClickListener(this);
        findViewById(R.id.lay2).setOnClickListener(this);
        findViewById(R.id.lay1).setOnClickListener(this);
        btnCreate.setOnClickListener(this);
        spaceAllViewbutton.setOnClickListener(this);
        serviceAllViewbutton.setOnClickListener(this);
        guestLimitArray = getResources().getStringArray(R.array.GuestList);
        guestLimitIdArray = getResources().getStringArray(R.array.GuestIdList);


        spaceRecyclerView = findViewById(R.id.rv_space);
        serviceRecyclerView = findViewById(R.id.rv_service);
        spaceSearchRecyclerView = findViewById(R.id.rv_scenario_space);
        serviceSearchRecyclerView = findViewById(R.id.rv_scenario_service);
        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);

        findViewById(R.id.imgBack).setOnClickListener((View v) -> onBackPressed());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Scenario Home");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent;
        if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin)) {
            intent = new Intent(context, HomeActivity.class);
        } else {
            intent = new Intent(context, HomeGuestActivity.class);
        }
        startActivity(intent);
        finish();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public Context getContext() {
        return context;
    }

    private void getCreatedScenarioData() {
        Log.d(TAG, "getCreatedScenarioData: Called ");
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getScenarioCreationData(new GetScenarioHomaDataModel(location, eventId, guestId, selectedServices));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {

                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    Log.d(TAG, "getCreatedScenarioData: " + jsonObject);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {

                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONArray spaceArray = dataObject.getJSONArray("spacelist");
                        JSONArray serviceArray = dataObject.getJSONArray("servicelist");

                        Log.d(TAG, "getCreatedScenarioData: " + spaceArray);
                        Log.d(TAG, "getCreatedScenarioData: " + serviceArray);
                        if (spaceArray.length() > 0) {
                            scenarioSearchSpaceAdapter.setData(spaceArray);
                            spaceLayout.setVisibility(View.VISIBLE);
                        }
                        if (serviceArray.length() > 0) {
                            scenarioSearchServiceAdapter.setData(serviceArray);
                            serviceLayout.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CommonFunctions.showLog(TAG, "onResponse: Catch:" + e);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void getHomeData() {
        //  loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getScenarioHomeData(new GetScenarioHomaDataModel("", "", "", new ArrayList<>()));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        getPlace(dataObject.getJSONArray("space_address"));
                        getEvent(dataObject.getJSONArray("events"));
                        getService(dataObject.getJSONArray("services"));
                        scenarioSpaceAdapter.setData(dataObject.getJSONArray("spacelist"));
                        scenarioServiceAdapter.setData(dataObject.getJSONArray("servicelist"));
                    } else {
                        statusAlert(response.message());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        });
    }

    private void statusAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle("Alert");
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Retry",
                (dialog, id) -> {
                    getHomeData();
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    //for scenario space search
    private void setAdapters() {
        // space search Adapter
        RecyclerView.LayoutManager layoutManagerSearchSpace = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        spaceSearchRecyclerView.setLayoutManager(layoutManagerSearchSpace);
        scenarioSearchSpaceAdapter = new ScenarioSpaceAdapter(context);
        spaceSearchRecyclerView.setAdapter(scenarioSearchSpaceAdapter);
        // Stopping Shimmer Effect's animation after data is loaded to ListView
        scenarioSearchSpaceAdapter.setOnItemClickListener((view, position, spaceArray) -> {
            try {
                //  Toast.makeText(context, "OnSpaceItemClick", Toast.LENGTH_SHORT).show();
                Utilities.setSPboolean(context, Utilities.spIsBookingType, false); // type: add to cart
                Intent intent = new Intent(context, SpaceDetailAndBookActivity.class);
                intent.putExtra("eventID", eventId);
                intent.putExtra("eventName", txtEvent.getText().toString());
                intent.putExtra("uni", spaceArray.getJSONObject(position).getString("uni"));
                startActivity(intent);

            } catch (Exception e) {
                Log.e(TAG, "onItemClickListner: " + e);
            }
        });

        //Top Space Adapter
        RecyclerView.LayoutManager layoutManagerSpace = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        spaceRecyclerView.setLayoutManager(layoutManagerSpace);
        scenarioSpaceAdapter = new ScenarioSpaceAdapter(context);
        spaceRecyclerView.setAdapter(scenarioSpaceAdapter);

        scenarioSpaceAdapter.setOnItemClickListener((view, position, dataArray) -> {
            try {

                //  Toast.makeText(context, "OnSpaceItemClick", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, SpaceDetailAndBookActivity.class);
                Utilities.setSPboolean(context, Utilities.spIsBookingType, false);
                intent.putExtra("eventID", eventId);
                intent.putExtra("eventName", txtEvent.getText().toString());// type: add to cart
                intent.putExtra("uni", dataArray.getJSONObject(position).getString("uni"));
                startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "onItemClickListner: " + e);
            }
        });

        // service search adapter
        RecyclerView.LayoutManager layoutManagerSearchService = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        serviceSearchRecyclerView.setLayoutManager(layoutManagerSearchService);
        scenarioSearchServiceAdapter = new ScenarioServiceAdapter(context);
        serviceSearchRecyclerView.setAdapter(scenarioSearchServiceAdapter);

        scenarioSearchServiceAdapter.setOnItemClickListener((view, position, dataArray) -> {
            try {

                //  Toast.makeText(context, "OnSpaceItemClick", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ServiceDetailsAndBookActivity.class);
                Utilities.setSPboolean(context, Utilities.spIsBookingType, false); // type: add to cart
                intent.putExtra("uni", dataArray.getJSONObject(position).getString("uni"));
                startActivity(intent);
            } catch (Exception e) {
                Log.e(TAG, "onItemClickListner: " + e);
            }
        });


        //Top service adapter
        RecyclerView.LayoutManager layoutManagerService = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        serviceRecyclerView.setLayoutManager(layoutManagerService);

        scenarioServiceAdapter = new ScenarioServiceAdapter(context);
        serviceRecyclerView.setAdapter(scenarioServiceAdapter);
        // Stopping Shimmer Effect's animation after data is loaded to ListView
        shimmerFrameLayout.stopShimmerAnimation();
        shimmerFrameLayout.setVisibility(View.GONE);


        scenarioServiceAdapter.setOnItemClickListener((view, position, dataArray) -> {
            try {

                //  Toast.makeText(context, "OnSpaceItemClick", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ServiceDetailsAndBookActivity.class);
                Utilities.setSPboolean(context, Utilities.spIsBookingType, false); // type: add to cart
                intent.putExtra("uni", dataArray.getJSONObject(position).getString("uni"));
                startActivity(intent);
            } catch (Exception e) {
                CommonFunctions.showLog(TAG, "onItemClickListner: " + e);
            }
        });

    }

    private void getPlace(JSONArray placeArray) {
        placeNameArray = new String[placeArray.length()];
        CommonFunctions.showLog(TAG, "getPlace: " + placeArray);
        try {
            for (int i = 0; i < placeArray.length(); i++) {
                JSONObject placeObject = placeArray.getJSONObject(i);
                placeNameArray[i] = placeObject.getString("address");
            }
        } catch (Exception e) {
            CommonFunctions.showLog(TAG, "getAddress: " + e);
        }
    }

    private void getEvent(JSONArray eventArray) {
        eventIdsArray = new String[eventArray.length()];
        eventNamesArray = new String[eventArray.length()];
        try {
            for (int i = 0; i < eventArray.length(); i++) {
                JSONObject eventObject = eventArray.getJSONObject(i);
                eventIdsArray[i] = eventObject.getString("event_id");
                eventNamesArray[i] = eventObject.getString("event_name");
            }

        } catch (Exception e) {
            CommonFunctions.showLog(TAG, "event_list: " + e);
        }
    }

    private void getService(JSONArray serviceArray) {

        serviceIdArray = new String[serviceArray.length()];
        serviceNameArray = new String[serviceArray.length()];

        try {
            for (int i = 0; i < serviceArray.length(); i++) {
                JSONObject eventObject = serviceArray.getJSONObject(i);
                serviceIdArray[i] = eventObject.getString("service_id");
                serviceNameArray[i] = eventObject.getString("service_name");
            }
        } catch (Exception e) {
            CommonFunctions.showLog(TAG, "service_list: " + e);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay1:
                //place
                showBottomSheetDialog(placeNameArray, "place");

                break;

            case R.id.lay2:
                //service
                //showBottomSheetDialog(serviceNameArray,"service");

                showSelectReceiversDialog();

                break;

            case R.id.lay3:
                //guest
                showBottomSheetDialog(guestLimitArray, "guest");

                break;

            case R.id.lay4:
                //event
                showBottomSheetDialog(eventNamesArray, "event");
                break;


            case R.id.scenario_create_button:
                if (VU.isConnectingToInternet(context)) {
                    getCreatedScenarioData();
                }
                break;
            case R.id.seeAllServices:
//                Toast.makeText(context, "View all services", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, GuestServiceActivity.class);
                intent.putExtra("citiId", "");
                intent.putExtra("serviceID", "");
                intent.putExtra("guestID", "");
                intent.putExtra("cities", cityList);
                intent.putExtra("cityIdList", cityIdList);

                intent.putExtra("citiName", "");
                intent.putExtra("serviceName", "");
                intent.putExtra("guestCount", "");


                ArrayList<String> serviceArrayList = new ArrayList<>(Arrays.asList(serviceNameArray));
                ArrayList<String> serviceIdArrayList = new ArrayList<>(Arrays.asList(serviceIdArray));

                intent.putExtra("services", serviceArrayList);
                intent.putExtra("serviceIdList", serviceIdArrayList);
                Utilities.setSPboolean(context, Utilities.spIsBookingType, false);  // type: add to cart
                startActivity(intent);
                break;

            case R.id.seeAllSpace:
                ArrayList<String> eventList = new ArrayList<>(Arrays.asList(eventNamesArray));
                ArrayList<String> eventIDList = new ArrayList<>(Arrays.asList(eventIdsArray));

                Intent intentAllSpace = new Intent(context, GuestSpaceActivity.class);
                intentAllSpace.putExtra("loaction", location);
                intentAllSpace.putExtra("eventId", "");
                intentAllSpace.putExtra("eventNames", eventList);
                intentAllSpace.putExtra("eventIds", eventIDList);
                intentAllSpace.putExtra("eventName", "");
                intentAllSpace.putExtra("venueIdNamesMap", HomeFragment.venueIdNameList);
                Utilities.setSPboolean(context, Utilities.spIsBookingType, false); // type: add to cart
                startActivity(intentAllSpace);
                break;
        }

    }

    //Bottom shhet code
    @SuppressLint("SetTextI18n")
    private void showBottomSheetDialog(final String[] filterArray, final String type) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_list, null);

        ListView bottom_listview = view.findViewById(R.id.bottom_listview);
        TextView txtheading = view.findViewById(R.id.bottom_heading);
        txtheading.setText("Select " + type);

        List<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        list.addAll(Arrays.asList(filterArray));

        bottom_listview.setAdapter(adapter);
        bottom_listview.setOnItemClickListener((parent, view1, position, id) -> {
            if (type.equalsIgnoreCase("Guest")) {
                txtGuest.setText(filterArray[position]);
                guestId = guestLimitIdArray[position];
            } else if (type.equalsIgnoreCase("Event")) {
                txtEvent.setText(filterArray[position]);
                eventId = eventIdsArray[position];
            } else if (type.equalsIgnoreCase("Place")) {
                txtPlace.setText(filterArray[position]);
                location = filterArray[position];
//                    strVenueId = venueId[position];
//                    Log.e(TAG, "onItemClick: strVenueId : " + strVenueId);
            }
            mBottomSheetDialog.cancel();


        });


        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    private void setSliderViews() {
        try {
            Log.d("IMAGES", imagesAddress.toString());
            for (int i = 0; i <= 5; i++) {

                DefaultSliderView sliderView = new DefaultSliderView(context);

                switch (i) {
                    case 0:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 1:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 2:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;

                    case 3:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;

                    case 4:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;

                    case 5:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                }


                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
//            sliderView.setDescription("The quick brown fox jumps over the lazy dog.\n" +
//                    "Jackdaws love my big sphinx of quartz. " + (i + 1));

                sliderView.setOnSliderClickListener(sliderView1 -> {
                    // Toast.makeText(context, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();

                });

                //at last add this view in your layout :
                sliderLayout.addSliderView(sliderView);
            }
        } catch (Exception e) {
            Log.e(TAG, "setSliderViews: catch : " + e.getMessage());
        }

    }

    @SuppressLint("SetTextI18n")
    protected void onChangeSelectedReceivers() {
        StringBuilder stringBuilder = new StringBuilder();

        if (selectedServicesName.size() > 2) {
            txtService.setText(selectedServicesName.size() + " + Services ");
        } else {
            if (selectedServicesName.size() > 0) {
                for (String service : selectedServicesName) {
                    stringBuilder.append(service).append(",");
                }
                stringBuilder.setLength(stringBuilder.length() - 1);
                txtService.setText(stringBuilder.toString());
            }
        }

        /*if (selectedServicesName.size() < 3) {
            for (String service : selectedServicesName) {
                stringBuilder.append(service + ",");
            }
            try {
                stringBuilder.setLength(stringBuilder.length() - 1);
            } catch (Exception ignore) {
            }

            txtService.setText(stringBuilder.toString());
        } else {
            txtService.setText("" + selectedServicesName.size());
        }*/

        CommonFunctions.showLog(TAG, "onChangeSelectedReceivers: " + stringBuilder);
    }

    protected void showSelectReceiversDialog() {
        boolean[] checkedServices = new boolean[serviceIdArray.length];
        int count = serviceIdArray.length;

        for (int i = 0; i < count; i++)
            checkedServices[i] = selectedServices.contains(serviceIdArray[i]);

        DialogInterface.OnMultiChoiceClickListener servicesDialogListener = (dialog, which, isChecked) -> {
            if (isChecked) {
                selectedServices.add(serviceIdArray[which]);
                selectedServicesName.add(serviceNameArray[which]);

            } else {
                selectedServices.remove(serviceIdArray[which]);
                selectedServicesName.remove(serviceNameArray[which]);
            }
            onChangeSelectedReceivers();
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Select service")
                .setMultiChoiceItems(serviceNameArray, checkedServices, servicesDialogListener)
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
