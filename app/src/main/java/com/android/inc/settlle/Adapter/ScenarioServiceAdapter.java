package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;

public class ScenarioServiceAdapter extends RecyclerView.Adapter<ScenarioServiceAdapter.MyViewHolder> {

    private final Context context;
    private JSONArray dataArray;
    private RecyclerViewItemClickListener.OnItemClick recyclerViewItemClickListener;


    public ScenarioServiceAdapter(Context context) {
        this.context = context;
        this.dataArray = new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_recycler_scenario_home, parent, false);
        return new MyViewHolder(view);

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {

        try {
            myViewHolder.serviceProviderNameText.setText("" + dataArray.getJSONObject(position).getString("company_name"));
            myViewHolder.serviceTypeText.setText("" + dataArray.getJSONObject(position).getString("service_name"));
            myViewHolder.addressText.setText("" + dataArray.getJSONObject(position).getString("address"));
            String imgName = dataArray.getJSONObject(position).getString("name");
            String imgUrl = Utilities.IMG_SERVICE_URL + imgName;
            Tools.displayImageOriginalString(myViewHolder.serviceImageView, imgUrl);
            String ratingStar = dataArray.getJSONObject(position).getString("stars");
            float rating;
            if (ratingStar.equalsIgnoreCase("N/A")) {
                rating = Float.parseFloat("0");
            } else {
                rating = Float.parseFloat(ratingStar);
            }
            myViewHolder.ratingBar.setRating(rating);

            myViewHolder.cardView.setOnClickListener(v -> {
                //When item view is clicked, trigger the itemclicklistener
                //Because that itemclicklistener is indicated in GuestSpaceActivity
                recyclerViewItemClickListener.onItemClickListner(v, position, dataArray);
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView serviceProviderNameText;
        TextView serviceTypeText;
        ImageView serviceImageView;
        TextView addressText;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.card_scenario_home);
            this.serviceProviderNameText = itemView.findViewById(R.id.company_name);
            this.serviceTypeText = itemView.findViewById(R.id.space_service_type);
            this.serviceImageView = itemView.findViewById(R.id.scenario_home_recycler_imageView);
            this.addressText = itemView.findViewById(R.id.address);
            this.ratingBar = itemView.findViewById(R.id.ratingBar);

        }

    }

    //Set method of OnItemClickListener object
    public void setOnItemClickListener(RecyclerViewItemClickListener.OnItemClick recyclerViewItemClickListener) {
        this.recyclerViewItemClickListener = recyclerViewItemClickListener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        this.dataArray = dataArray;
        notifyDataSetChanged();

    }
}