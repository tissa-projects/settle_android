package com.android.inc.settlle.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.widget.Toast;

import com.android.inc.settlle.Activities.Service.ServiceRegistrationDetailsActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ServiceSubscriptionModel;
import com.android.inc.settlle.RequestModel.SpaceSubscriptionModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMediatorActivity extends AppCompatActivity implements PaymentResultListener {
    public static final String TAG = PaymentMediatorActivity.class.getName();

    Context context = PaymentMediatorActivity.this;
    private Dialog dialog;
    private String strPlanId;
    private String strPlanType;
    private String strAmt;
    private final String selectedName = "";
    private ArrayList<String> strId, selectedNameList;
    ArrayList<String> subscriptionIdList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_mediator);

        getIntentData();
        startPayment();

    }


    private void getIntentData() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            strId = bundle.getStringArrayList("strIds");
            strPlanId = bundle.getString("strPlanId");
            strPlanType = bundle.getString("strPlanType");
            strAmt = bundle.getString("strAmt");
            selectedNameList = bundle.getStringArrayList("selectedName");

        }
    }


    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        // final Activity activity = getActivity();
        final Activity activity = this;

        final Checkout co = new Checkout();
        co.setKeyID("rzp_test_r2dAxBrb56JLsv");// For Testing Mode
        //co.setKeyID("rzp_live_XTK1F5JiP55Dww"); // For Live Mode

        try {
            JSONObject options = new JSONObject();

            options.put("currency", "INR");
            options.put("amount", Double.parseDouble(strAmt) * 100);
            options.put("name", "Settle Solution LLP");

            JSONObject preFill = new JSONObject();
            preFill.put("name", Utilities.getSPstringValue(context, Utilities.spFirstName) + " " + Utilities.getSPstringValue(context, Utilities.spLastName));
            preFill.put("contact", Utilities.getSPstringValue(context, Utilities.spMobileNo));
            preFill.put("email", Utilities.getSPstringValue(context, Utilities.spEmail));

            options.put("prefill", preFill);
            co.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, "Payment failed", Toast.LENGTH_SHORT)
                    .show();

            Log.e(TAG, "onPaymentFailed2:  " + e.getMessage());

            e.printStackTrace();
            finish();
        }
    }


    @SuppressWarnings("unused")
    @Override
    public void onPaymentSuccess(String paymentId) {

        try {
            Log.e(TAG, "onPaymentSuccess: sendSpaceSubscriptionData " + paymentId);
            String fragmentType = Utilities.getSPstringValue(context, Utilities.spPaymentFromActivity);
            Log.e(TAG, "onPaymentSuccess: " + fragmentType);
            if (fragmentType.equalsIgnoreCase("MyServiceServiceFragment")) {
                sendServiceSubscriptionData(paymentId);
            } else if (fragmentType.equalsIgnoreCase("MySpaceSpaceFragment")) {
                sendSpaceSubscriptionData(paymentId);
            } else if (fragmentType.equalsIgnoreCase("RenewActivity")) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("result", paymentId);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }


        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);

        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onPaymentError(int code, String response) {
        try {
            Log.d(TAG, "onPaymentFailed: sendSpaceSubscriptionData " + response);
            Toast.makeText(context, "Payment failed", Toast.LENGTH_SHORT).show();
            finish();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }


    //send space subscribttion data  to get subscription_id
    public void sendSpaceSubscriptionData(String paymentId) {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Log.e(TAG, "sendSpaceSubscriptionData: authtoken " + Utilities.getSPstringValue(context, Utilities.spAuthToken));
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendSpaceSubscriptionData(new SpaceSubscriptionModel(strId, selectedName,
                Utilities.getSPstringValue(context, Utilities.spUserId), strPlanId, strPlanType, strAmt, Utilities.getSPstringValue(context, Utilities.spAuthToken), paymentId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: sendSpaceSubscriptionData " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            String strMessage = jsonObject.getString("message");
                            JSONArray strSubscriptionIds = jsonObject.getJSONArray("subscription_id");
                            for (int i = 0; i < strSubscriptionIds.length(); i++) {
                                subscriptionIdList.add(strSubscriptionIds.getString(i));
                            }
                            //Utilities.setArrayList(context, Utilities.spSubscriptionId, subscriptionIdList);
                            // Utilities.setSPstring(context, Utilities.spSubscriptionId, strSubscriptionId);
                            Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            goToSpaceRegistration();
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    //send service subscribttion data  to get subscription_id
    public void sendServiceSubscriptionData(String paymentId) {
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendServiceSubscriptionData(new ServiceSubscriptionModel(strId, selectedName,
                Utilities.getSPstringValue(context, Utilities.spUserId), strPlanId, strPlanType, strAmt, Utilities.getSPstringValue(context, Utilities.spAuthToken), paymentId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            String strMessage = Obj.getString("message");
                            String strSubscriptionId = Obj.getString("subscription_id");
                            Utilities.setSPstring(context, Utilities.spSubscriptionId, strSubscriptionId);
                            Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                            goToServiceRegistration();
                        }
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void goToSpaceRegistration() {

        Intent i = new Intent(context, SpaceRegistrationDetailsActivity.class);
        i.putStringArrayListExtra("venueType", selectedNameList);
        i.putStringArrayListExtra("venueId", strId);
        i.putStringArrayListExtra("subscriptionId", subscriptionIdList);
        startActivity(i);
        finish();

    }

    private void goToServiceRegistration() {

        Intent i = new Intent(context, ServiceRegistrationDetailsActivity.class);
        i.putExtra("serviceType", selectedName);
        i.putExtra("serviceId", strId);
        startActivity(i);
        finish();

    }
}
