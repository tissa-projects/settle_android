package com.android.inc.settlle.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.SendOtpModel;
import com.android.inc.settlle.RequestModel.VerifyOtpModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import org.json.JSONObject;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileVerificationActivity extends AppCompatActivity {

    private EditText edtMobileNo;
    private Context context;
    private Dialog loadingDialog;
    private String strMb = "";
    private static final String TAG = MobileVerificationActivity.class.getName();
    boolean doubleBackToExitPressedOnce = false;
    private String strUniqueId, strSpaceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        context = MobileVerificationActivity.this;
        initialize();
        getIntentData();
    }

    private void initialize() {
        edtMobileNo = findViewById(R.id.edtMobile);
        findViewById(R.id.btn_verify_otp).setOnClickListener(v -> {
            strMb = edtMobileNo.getText().toString();
            if (strMb.trim().equals("")) {
                Toast.makeText(context, "Enter mobile number", Toast.LENGTH_LONG).show();
            } else if (strMb.length() != 10) {
                Toast.makeText(context, "Enter valid mobile number", Toast.LENGTH_LONG).show();
            } else {
                if (VU.isConnectingToInternet(context)) {
                    SendOtpRequest(strMb);
                }
            }
        });
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            strUniqueId = bundle.getString("unique_id");
            strSpaceId = bundle.getString("id");
        }
    }

    private void dailogMobileVerify() {
        final Dialog mBottomSheetDialog = new Dialog(this);
        mBottomSheetDialog.setContentView(R.layout.dialog_otp_verify); // your custom view.
        final EditText edtOtp = mBottomSheetDialog.findViewById(R.id.edt_otp);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();

        Button btnSubmit = mBottomSheetDialog.findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(view -> {
            String strOtp;
            strOtp = edtOtp.getText().toString();
            Log.e(TAG, "onClick: submit click");
            if (strOtp.trim().equals("")) {
                //CustomToast.custom_Toast(context, "Enter OTP", layout);
                Toast.makeText(context, "Enter OTP", Toast.LENGTH_LONG).show();
            } else {
                if (VU.isConnectingToInternet(context)) {
                    VerifyOtpRequest(strOtp);
                }
            }
        });

        mBottomSheetDialog.findViewById(R.id.resend_otp).setOnClickListener(view -> {
            Log.e(TAG, "onClick: " + strMb);
            if (VU.isConnectingToInternet(context)) {
                SendOtpRequest(strMb);
            }
        });
    }

    //call  request
    private void SendOtpRequest(String strMbNo) {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().sendOTP(new SendOtpModel(strMbNo, Utilities.getSPstringValue(context, Utilities.spUserId),
                Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: api_response :" + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    Log.e(TAG, "onResponse: " + msg);
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(MobileVerificationActivity.this, msg, Toast.LENGTH_SHORT).show();
                        dailogMobileVerify();
                    } else {
                        Toast.makeText(MobileVerificationActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });


    }

    //call  request
    private void VerifyOtpRequest(String strOTP) {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Log.e(TAG, "VerifyOtpRequest: " + strOTP);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().VerifyOTP(new VerifyOtpModel("", strOTP, Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: api_response :" + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    Toast.makeText(MobileVerificationActivity.this, msg, Toast.LENGTH_SHORT).show();

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            Utilities.setSPboolean(context, Utilities.spIsLoggedin, true);
                            Utilities.setSPboolean(context, Utilities.spIsMobileverify, true);
                            Utilities.setSPstring(context, Utilities.spMobileNo, strMb);
                            if (Utilities.getSPbooleanValue(context, Utilities.spAtBookingSpaceActivity)) {
                                Intent i = new Intent(MobileVerificationActivity.this, SpaceBookingFormActivity.class);
                                i.putExtra("unique_id", strUniqueId);
                                i.putExtra("space_id", strSpaceId);
                                i.putExtra("eventID", "");
                                i.putExtra("eventName", "");
                                startActivity(i);
                                Utilities.setSPboolean(context, Utilities.spAtBookingSpaceActivity, false);
                            } else {
                                Intent i = new Intent(MobileVerificationActivity.this, HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                            }
                            finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
            }
        });


    }

    @Override
    public void onBackPressed() {

        if (Utilities.getSPbooleanValue(context, Utilities.spAtBookingSpaceActivity)) {
            logout();
            finish();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> {
                doubleBackToExitPressedOnce = false;
                logout();
            }, 2000);
        }
    }

    private void logout() {
        if (VU.isConnectingToInternet(context)) {
            String logoutType;
            logoutType = Utilities.getSPstringValue(context, Utilities.spLoginType);
            if (logoutType.equalsIgnoreCase("FaceBookLogin")) {
                // Toast.makeText(context, "Facebook logout  ", Toast.LENGTH_SHORT).show();
                LoginManager.getInstance().logOut();
            } else if (logoutType.equalsIgnoreCase("googleLogin")) {
                GooglesignOut();
            }
        }
    }

    private void GooglesignOut() {
        if (VU.isConnectingToInternet(context)) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, task -> {
                    });
        }
    }
}
