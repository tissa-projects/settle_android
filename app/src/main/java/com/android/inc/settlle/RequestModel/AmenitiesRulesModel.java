package com.android.inc.settlle.RequestModel;

public class AmenitiesRulesModel {
    private String AmenitiesRulesName;
    private String AmenitiesRulesNameId;
    private boolean isSelected = false;
    private int position;

    public AmenitiesRulesModel(String amenitiesRulesName, String amenitiesRulesNameId, boolean isSelected) {
        AmenitiesRulesName = amenitiesRulesName;
        AmenitiesRulesNameId = amenitiesRulesNameId;
        this.isSelected = isSelected;
    }

    public AmenitiesRulesModel(String amenitiesRulesNameId) {
        AmenitiesRulesNameId = amenitiesRulesNameId;
    }

    public String getAmenitiesRulesNameId() {
        return AmenitiesRulesNameId;
    }

    public void setAmenitiesRulesNameId(String amenitiesRulesNameId) {
        AmenitiesRulesNameId = amenitiesRulesNameId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean getSelected() {
        return isSelected;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getAmenitiesRulesName() {
        return AmenitiesRulesName;
    }

    public void setAmenitiesRulesName(String amenitiesRulesName) {
        AmenitiesRulesName = amenitiesRulesName;
    }
}
