package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class SearchServiceModel implements Serializable {


    private String places;
    private String service;
    private String startdate;
    private String guest;
    private String user_id;
    private int page;
    private int limit;

    public SearchServiceModel(String places, String service, String startdate, String guest, String user_id, int page, int limit) {
        this.places = places;
        this.service = service;
        this.startdate = startdate;
        this.guest = guest;
        this.user_id = user_id;
        this.page = page;
        this.limit = limit;
    }
}
