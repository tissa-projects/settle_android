package com.android.inc.settlle.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.inc.settlle.Adapter.SerachItemListAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.DataModel.NameIdPairModel;
import com.android.inc.settlle.Utilities.ListItem;

import java.util.ArrayList;
import java.util.List;

public class SearchItemListActivity extends AppCompatActivity {


    private Context context;
    Activity activity;
    ImageView ll_backArrow;
    private static final String TAG = SearchItemListActivity.class.getSimpleName();

    List<String> searchItemList;
    ListView listView;

    SearchView searchView;

    SerachItemListAdapter adapter;
    ArrayList<String> searchList = new ArrayList<>();
    ArrayList<String> IdList = new ArrayList<>();
    ArrayList<NameIdPairModel> nameIdList = new ArrayList<>();
    //  String serarchKey;
    private String searchType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_item_list);
        context = SearchItemListActivity.this;
        activity = SearchItemListActivity.this;
        getIntentData();
        searchItemList = searchList;
        initialize();

     /*   if (serarchKey.equals(Utilities.mEventList)) {
            Utilities.setSPstring(context, Utilities.mKey, serarchKey);
        } else if (serarchKey.equals(Utilities.mSpaceLocationList)) {
            Utilities.setSPstring(context, Utilities.mKey, serarchKey);
        } else if (serarchKey.equals(Utilities.mServiceList)) {
            Utilities.setSPstring(context, Utilities.mKey, serarchKey);
        } else if (serarchKey.equals(Utilities.mServiceLocationList)) {
            Utilities.setSPstring(context, Utilities.mKey, serarchKey);
        } else if (serarchKey.equals(Utilities.mGuestLimitList)) {
            Utilities.setSPstring(context, Utilities.mKey, serarchKey);
        }
*/

    }

    private void getIntentData() {
        Log.d(TAG, "getIntentData: called");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            searchList = bundle.getStringArrayList("searchList");
            IdList = bundle.getStringArrayList("IdList");
            searchType = bundle.getString("searchType");
            for (int i = 0; i < IdList.size(); i++) {
                nameIdList.add(new NameIdPairModel(IdList.get(i), searchList.get(i)));
            }
        }
        Log.e(TAG, "getIntentData: searchList : " + searchList + "\n IdList : " + IdList + "\n searchType : " + searchType);
    }


    private void initialize() {
        ll_backArrow = findViewById(R.id.back_searchedList);
        listView = findViewById(R.id.searchItem_list_view);
        // listView.setFooterDividersEnabled(false);
        searchView = findViewById(R.id.searchItemList_search);
        List<ListItem> itemList = new ArrayList<>();
        if (searchItemList != null) {
            for (int i = 0; i < searchItemList.size(); i++) {
                ListItem listItem = new ListItem();
                listItem.setSearchItem(searchItemList.get(i));
                itemList.add(listItem);
            }
        }
        adapter = new SerachItemListAdapter(nameIdList, context);
        listView.setAdapter(adapter);
        if (searchType.equals("location")) {
            adapter.filter("***");
            listView.clearTextFilter();
        } else {
            adapter.filter("");
            listView.clearTextFilter();
        }
        searchView.setActivated(true);
        searchView.setQueryHint("Enter Your Search");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (TextUtils.isEmpty(newText)) {

                    if (searchType.equals("location")) {
                        adapter.filter("***");
                        listView.clearTextFilter();
                    } else {
                        adapter.filter("");
                        listView.clearTextFilter();
                    }

                } else {
                    adapter.filter(newText);
                }

                return true;
            }
        });

        ll_backArrow.setOnClickListener(v -> finish());

    }


}