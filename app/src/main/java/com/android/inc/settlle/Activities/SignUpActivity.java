package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.RegisterModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_fname, et_lname, et_email, et_password, et_cnfm_password;
    String fname, lname, email, password, cnfm_password;
    Context context;
    Button signup;
    TextView tx_login, checkBoxTermsPolicyError;
    private Dialog loadingDialog;
    CheckBox termPolicyCheckBox;
    TextView termPolicyText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_registration);
        context = SignUpActivity.this;
        initialization();

    }

    //view initialization
    private void initialization() {
        et_fname = findViewById(R.id.register_et_fname);
        et_lname = findViewById(R.id.register_et_lname);
        et_email = findViewById(R.id.register_et_email);
        et_password = findViewById(R.id.register_et_password);
        et_cnfm_password = findViewById(R.id.register_et_confirm_password);
        termPolicyCheckBox = findViewById(R.id.checkBoxTermsPolicy);
        termPolicyText = findViewById(R.id.register_text_termpolicy);
        checkBoxTermsPolicyError = findViewById(R.id.checkBoxTermsPolicyError);


        tx_login = findViewById(R.id.register_text_login);
        tx_login.setOnClickListener(this);
        findViewById(R.id.imgBack).setOnClickListener(this);

        signup = findViewById(R.id.register_btn_signup);
        signup.setOnClickListener(this);
        termPolicyCheckBox.setOnClickListener(this);
        termPolicyText.setOnClickListener(this);

        termPolicyCheckBox.setOnClickListener(v -> {
            if (termPolicyCheckBox.isChecked()) {
                termPolicyCheckBox.setError(null);
                checkBoxTermsPolicyError.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.register_btn_signup:
                if (VU.isConnectingToInternet(context)) {
                    signUpValidations();
                }
                break;
            case R.id.register_text_login:
                startActivity(new Intent(context, LoginActivity.class));
                finish();
                break;
            case R.id.register_text_termpolicy:
                Intent intent = new Intent(getApplicationContext(), PreviewPDFImage.class);
                String filePath = Utilities.TERMS_AND_POLICY;
                String fileType = CommonFunctions.getMimeType(filePath);
                intent.putExtra("fileType", fileType);
                intent.putExtra("filePath", filePath);
                intent.putExtra("isBitmap", false);
                intent.putExtra("bitmap", " ");
                startActivity(intent);
                break;

            case R.id.imageView:
                onBackPressed();
                break;

            case R.id.checkBoxTermsPolicy:
                if (termPolicyCheckBox.isChecked()) {
                    termPolicyCheckBox.setError(null);
                }
                break;
        }


    }

    //validations
    private void signUpValidations() {

        fname = et_fname.getText().toString().trim().toLowerCase();
        lname = et_lname.getText().toString().trim().toLowerCase();
        email = et_email.getText().toString().trim().toLowerCase();
        password = et_password.getText().toString().trim().toLowerCase();
        cnfm_password = et_cnfm_password.getText().toString().trim().toLowerCase();

        if (fname.isEmpty()) {
            et_fname.requestFocus();
            et_fname.setError(Utilities.fnameEmptyError);
        } else if (lname.isEmpty()) {
            et_lname.setError(Utilities.lnameEmptyError);
            et_lname.requestFocus();
        } else if (email.isEmpty()) {
            et_email.setError(Utilities.emailEmptyError);
            et_email.requestFocus();
        } else if (!Utilities.validateEmail(email)) {
            et_email.setError(Utilities.emailInvalidError);
            et_email.requestFocus();
        } else if (password.isEmpty()) {
            et_password.requestFocus();
            et_password.setError(Utilities.passwordEmptyError);
        } else if (password.length() <= 7) {
            et_password.requestFocus();
            et_password.setError(Utilities.passwordLengthError);
        } else if (cnfm_password.length() <= 7) {
            et_cnfm_password.requestFocus();
            et_cnfm_password.setError(Utilities.passwordLengthError);
        } else if (!password.equals(cnfm_password)) {
            et_password.requestFocus();
            et_password.setError(Utilities.passwordMismatchError);
        } else if (!termPolicyCheckBox.isChecked()) {
            checkBoxTermsPolicyError.setVisibility(View.VISIBLE);
            checkBoxTermsPolicyError.setText("Please accept the terms and policy");
            termPolicyCheckBox.requestFocus();
        } else {
            signup();
        }
    }

    //call signup request
    private void signup() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

//        RegisterModel  rm = new RegisterModel(fname,lname,email,password);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().register(new RegisterModel(fname, lname, email, password));
        //new JsonParser().parse().getAsJsonObject();
        Log.d("TAG", "signup: " + call.request());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getBoolean("status")) {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        //Handle success logic here
                        startActivity(new Intent(context, LoginActivity.class));
                        finish();
                    } else {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

}
