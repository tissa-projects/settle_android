package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class FavServiceModel implements Serializable {

    private String user_id;
    private String service_id;
    private String auth_token;

    public FavServiceModel(String user_id, String service_id, String auth_token) {
        this.user_id = user_id;
        this.service_id = service_id;
        this.auth_token = auth_token;
    }
}
