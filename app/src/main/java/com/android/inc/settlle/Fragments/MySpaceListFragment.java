package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.EditSpaceKYCActivity;
import com.android.inc.settlle.Activities.EditSpaceRegistrationDetailsActivity;
import com.android.inc.settlle.Activities.SpaceViewActivity;
import com.android.inc.settlle.Adapter.MySpaceListAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.MySpaceListModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySpaceListFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView;
    private View view;
    private ShimmerFrameLayout shimmerFrameLayout;
    private Context context;
    private MySpaceListAdapter mySpaceListAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private JSONArray dataArray;


    ProgressBar progressBar;
    int page = 1;
    private boolean isPageAvailable;
    //variables for pagination
    private boolean isLoading = true;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private final int view_threshold = 3;

    private LinearLayout llSpacebtn1, llSpacebtn2, llSpacebtn3;
    private ImageView imgSpace1, imgSpace2, imgSpace3;
    private TextView txtSpace1, txtSpace2, txtSpace3;

    private static final String TAG = MySpaceListFragment.class.getName();
    //ImageView img_nodatafound;


    private TextView filter_all, filter_approved, filter_pending;
    ImageView noDataFoundImg;
    private int filterCode = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_space_list, container, false);
       /* TextView editButton = getActivity().findViewById(R.id.account_button_edit);
        if (editButton.getVisibility() == View.VISIBLE) {
            editButton.setVisibility(View.GONE);
        }*/
        context = getActivity();
        initialize();
        initRecycler();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        initializeActivityView();
        spaceBtn2Click();
        if (VU.isConnectingToInternet(context)) {
            mySpaceListAdapter.clearData();
            dataArray = new JSONArray();
            SpaceData();//by default 1st page data
        }
    }


    private void initialize() {
        noDataFoundImg = view.findViewById(R.id.img_nodatafound);
        recyclerView = view.findViewById(R.id.recycler_my_space_list_fragmen);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container_my_space_list);
        progressBar = view.findViewById(R.id.progress_bar);
        dataArray = new JSONArray();

        filter_all = view.findViewById(R.id.filter_all);
        filter_all.setOnClickListener(this);
        filter_approved = view.findViewById(R.id.filter_approved);
        filter_approved.setOnClickListener(this);
        filter_pending = view.findViewById(R.id.filter_pending);
        filter_pending.setOnClickListener(this);
    }

    private void initializeActivityView() {
        llSpacebtn1 = requireActivity().findViewById(R.id.space_button1);
        llSpacebtn2 = requireActivity().findViewById(R.id.space_button2);
        llSpacebtn3 = requireActivity().findViewById(R.id.space_button3);

        imgSpace1 = requireActivity().findViewById(R.id.img_space1);
        imgSpace2 = requireActivity().findViewById(R.id.img_space2);
        imgSpace3 = requireActivity().findViewById(R.id.img_space3);
        txtSpace1 = requireActivity().findViewById(R.id.txt_space1);
        txtSpace2 = requireActivity().findViewById(R.id.txt_space2);
        txtSpace3 = requireActivity().findViewById(R.id.txt_space3);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceBtn2Click() {
        imgSpace2.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtSpace2.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgSpace3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtSpace3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgSpace1.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtSpace1.setTextColor(getResources().getColorStateList(R.color.grey_60));

        llSpacebtn1.setClickable(true);
        llSpacebtn2.setClickable(false);
        llSpacebtn3.setClickable(true);
    }

    private void initRecycler() {
        layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        shimmerFrameLayout.stopShimmerAnimation();
        recyclerView.setLayoutManager(layoutManager);
        mySpaceListAdapter = new MySpaceListAdapter(context);
        recyclerView.setAdapter(mySpaceListAdapter);
        adapterCall();
    }

    private void adapterCall() {
        //Edit btn
        mySpaceListAdapter.setOnEditBtnClickListener((uniqueId, spaceId) -> {
            Intent intent = new Intent(context, EditSpaceRegistrationDetailsActivity.class);
            intent.putExtra("uniqueId", uniqueId);
            intent.putExtra("spaceId", spaceId);
            startActivity(intent);
        });


        //View btn
        mySpaceListAdapter.setOnViewBtnClickListener((uniqueID, spaceID) -> {
            Intent intent = new Intent(context, SpaceViewActivity.class);
            intent.putExtra("uni", uniqueID);
            startActivity(intent);
        });
        //documents btn
        mySpaceListAdapter.setOnDocumentsBtnClickListener((uniqueId, spaceId) -> {
            Intent intent = new Intent(context, EditSpaceKYCActivity.class);
            intent.putExtra("uniqueId", uniqueId);
            intent.putExtra("spaceId", spaceId);
            startActivity(intent);
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVariableItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > privious_total) {
                            isLoading = false;
                            privious_total = totalItemCount;
                        }
                    }

                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                        if (isPageAvailable) {
                            page++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                SpaceData();
                        } /*else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }*/
                        isLoading = true;
                    }
                }
            }
        });
    }


    // Request Call space
    private void SpaceData() {

        if (page == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMySpaceListData(new MySpaceListModel(userId, authToken, page, Utilities.LIMIT, filterCode));  //by default 1st page
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            noDataFoundImg.setVisibility(View.GONE);
                            dataArray = jsonObject.getJSONArray("data");
                            isPageAvailable = jsonObject.getBoolean("count");
                            CommonFunctions.showLog("CheckDataSize Exception 111 ", " " + dataArray.length());
                            mySpaceListAdapter.setData(dataArray);
                        } else {
                            if (page == 1) {
                                noDataFoundImg.setVisibility(View.VISIBLE);
                            } else {
                                noDataFoundImg.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                    page--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                page--;

            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        page = 1;
        mySpaceListAdapter.clearData();

        switch (v.getId()) {

            case R.id.filter_all:
                filter_all.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_approved.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_pending.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.white));
                filter_approved.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_pending.setTextColor(getResources().getColor(R.color.colorPrimary));


                filterCode = 0;
                if (VU.isConnectingToInternet(context)) {
                    page = 1;
                    SpaceData();

                }
                break;

            case R.id.filter_approved:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_approved.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_pending.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_approved.setTextColor(getResources().getColor(R.color.white));
                filter_pending.setTextColor(getResources().getColor(R.color.colorPrimary));

                filterCode = 1;
                if (VU.isConnectingToInternet(context)) {
                    page = 1;
                    SpaceData();
                }
                break;


            case R.id.filter_pending:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_approved.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_pending.setBackgroundResource(R.drawable.curve_button_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_approved.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_pending.setTextColor(getResources().getColor(R.color.white));
                filterCode = 4;
                if (VU.isConnectingToInternet(context)) {
                    page = 1;
                    SpaceData();
                }
                break;


        }
    }
}
