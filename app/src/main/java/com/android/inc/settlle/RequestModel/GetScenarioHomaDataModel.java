package com.android.inc.settlle.RequestModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetScenarioHomaDataModel implements Serializable {

    private String location;
    private String event;
    private String guest_id;
    private ArrayList<String> service;


    public GetScenarioHomaDataModel(String location, String event,String guest_id,ArrayList<String> service) {
        this.location = location;
        this.event = event;
        this.guest_id = guest_id;
        this.service = service;
    }
}