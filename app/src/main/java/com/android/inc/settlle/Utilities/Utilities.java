package com.android.inc.settlle.Utilities;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.fragment.app.FragmentTransaction;

import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Utilities {


    public static FragmentTransaction fragmentTransaction;
    // Fragment object
    //Old API
   /* public static final String IMG_SPACE_URL = "http://eventus.tissatech.in/uploads/space_image/";
    public static final String IMG_SERVICE_URL = "http://eventus.tissatech.in/uploads/service_image/";
    public static final String IMG_PROFILE_URL = "http://eventus.tissatech.in/uploads/profile_pic/";
    public static final String SPACE_DOCUMENT = "http://eventus.tissatech.in/uploads/space_documents/";
    public static final String SERVICE_DOCUMENT = "http://eventus.tissatech.in/uploads/service_documents/";
    public static final String SCENARIO_SLIDER = "http://eventus.tissatech.in/themes/frontend/images/";
    public static final String TERMS_AND_POLICY = "http://eventus.tissatech.in/asset/Settle-TermsOfUse&PrivacyPolicy.pdf";*/


    public static final String IMG_SPACE_URL = RetrofitClient.Base_URL_FOR_DOC + "uploads/space_image/";
    public static final String IMG_SERVICE_URL = RetrofitClient.Base_URL_FOR_DOC + "uploads/service_image/";
    public static final String IMG_PROFILE_URL = RetrofitClient.Base_URL_FOR_DOC + "uploads/profile_pic/";
    public static final String SPACE_DOCUMENT = RetrofitClient.Base_URL_FOR_DOC + "uploads/space_documents/";
    public static final String SERVICE_DOCUMENT = RetrofitClient.Base_URL_FOR_DOC + "uploads/service_documents/";
    public static final String SCENARIO_SLIDER = RetrofitClient.Base_URL_FOR_DOC + "themes/frontend/images/";
    public static final String TERMS_AND_POLICY = RetrofitClient.Base_URL_FOR_DOC + "asset/Settle-TermsOfUse&PrivacyPolicy.pdf";


    public static final String ABOUT_US = "https://settle.ind.in/home/aboutus";

    public static final int MAX_ALLOWED_MONTH = 18;
    public static final int MAX_ALLOWED_BOOKING_DAY = 60;
    public static String TRANSFER_BITMAP_VALUE = "";

    public static final int MEGABYTE = 1024 * 1024;

    public static final String[] appPermission = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };
    public static final int RESULT_GALLERY = 1;
    public static final int RESULT_CAMERA = 2;
    public static final int RESULT_FM = 3;
    public static final int QUALITY = 60;
    public static final int DENSITY = 80;
    public static int SPLASH_TIME_OUT = 1000;

    public static final String API_KEY = "AIzaSyCmVGINf7pdZm1WrWKpL3z8Fgm4y2_Y97k"; // Google Place API Key


    //Guest Limit array
    public final static String[] guestLimitArray = {"0-100 Guest", "100-200 Guest", "200-300 Guest", "300-400 Guest", "400-500 Guest", "500 & UP Guest"};
    public final static String[] guestLimitIdArray = {"1", "2", "3", "4", "5", "6"};
    public final static int LIMIT = 4;


    //***************************************************************************//
    //Error message String
    public static String emailEmptyError = "Enter email address";
    public static String emailInvalidError = "Enter valid email address";
    public static String passwordEmptyError = "Enter password";
    public static String fnameEmptyError = "Enter first name";
    public static String lnameEmptyError = "Enter last name";
    public static String passwordLengthError = "Password length should be min 8 character";
    public static String passwordMismatchError = "Password mismatch";
    public static String cookie = "cookie";

    public static SimpleDateFormat SDF_WITH_HOUR = new SimpleDateFormat("dd/MM/yyyy HH", Locale.ENGLISH);
    public static SimpleDateFormat SDF_WITHOUT_HOUR = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    public static SimpleDateFormat SDF_HOUR = new SimpleDateFormat("HH", Locale.ENGLISH);


    //***************************************************************************//
    public static ArrayList<String> guestLimitList = new ArrayList<>();
    public static ArrayList<String> guestLimitIdList = new ArrayList<>();


    //Validation functions

    //Email check
    public static boolean validateEmail(CharSequence email) {
        return email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    // Function to convert ArrayList<String> to String[]
    public static String[] GetStringArray(ArrayList<String> arr) {
        // declaration and initialise String Array
        String[] str = new String[arr.size()];
        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {
            // Assign each value to String array
            str[j] = arr.get(j);
        }
        return str;
    }


    //*************************************************************************** Shared Preference
    //SharedPreference Key
    public static final String spIsLoggedin = "isLoggedin";

    public static final String spFirstTime = "isFirstTime";
    public static final String spAtBookingSpaceActivity = "atBookingSpaceActivity";
    public static final String spAtBookingServiceActivity = "atBookingServiceActivity";
    public static final String spAtGuestSpaceActivity = "atguestSpaceActivity";
    public static final String spAtGuestServiceActivity = "atguestServiceActivity";
    public static final String spIsMobileverify = "isMobileVerify";
    public static final String spAuthToken = "authToken";
    public static final String spFirstName = "fName";
    public static final String spLastName = "lName";
    public static final String spEmail = "email";
    public static final String spUserId = "userId";
    public static final String spMobileNo = "mobileNo";
    public static final String spLoginType = "loginType";
    public static final String spPaymentFromActivity = "paymentFromActivity";
    public static final String spIsBookingType = "isBookingType";
    public static final String CHECK_OUT_COUNT = "check_out_count";

    //================================================

    public static final String spSubscriptionId = "subscriptionId";


    //Store string using Shared Preference
    public static void setSPstring(Context mContext, String key, String value) {
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    //Store boolean using Shared Preference
    public static void setSPboolean(Context mContext, String key, boolean value) {
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    //Store integer using Shared Preference
    public static void setSPineger(Context mContext, String key, int value) {
        SharedPreferences pref = mContext.getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.apply();
    }


    //get string value from Shared Preference
    public static String getSPstringValue(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, null);
    }

    //get int value from Shared Preference
    public static int getSPintValue(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, 0);
    }


    //get boolean value from Shared Preference
    public static boolean getSPbooleanValue(Context mContext, String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(key, false);
    }

    public static void setArrayList(Context context, String key, ArrayList<String> list) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static ArrayList<String> getArrayList(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


}
