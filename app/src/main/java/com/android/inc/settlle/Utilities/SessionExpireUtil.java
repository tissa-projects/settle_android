package com.android.inc.settlle.Utilities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.view.WindowManager;

import com.android.inc.settlle.Activities.HomeGuestActivity;
import com.android.inc.settlle.R;

public class SessionExpireUtil {

    public static  void logout(Context context){ showCustomDialog(context); }

    public static void showCustomDialog(final Context appContext) {
        final Dialog dialog = new Dialog(appContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_session_expire);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        dialog.findViewById(R.id.bt_close).setOnClickListener(v -> {
            Utilities.setSPboolean(appContext, Utilities.spIsMobileverify, false);
            Utilities.setSPboolean(appContext, Utilities.spIsLoggedin, false);
            Intent i = new Intent(appContext, HomeGuestActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            appContext.startActivity(i);
            ((Activity) appContext).finish();
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
