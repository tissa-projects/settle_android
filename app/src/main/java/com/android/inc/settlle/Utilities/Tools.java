package com.android.inc.settlle.Utilities;


import android.content.Context;

import androidx.annotation.DrawableRes;

import android.util.Log;
import android.widget.ImageView;

import com.android.inc.settlle.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class Tools {

    public static void displayImageOriginal(Context ctx, ImageView img, @DrawableRes int drawable) {
        try {
            Glide.with(ctx).load(drawable).into(img);
        } catch (Exception ignore) {
        }
    }

    public static void displayImageOriginalString(final ImageView img, String url) {
        try {
            Picasso.get().load(url).placeholder(R.drawable.img_load100).into(img);
        } catch (Exception e) {
            Log.e("Utilities : Tools", "displayImageOriginalString: " + e.getMessage());
        }
    }

    public static void displaySmallImageOriginalString(final ImageView img, String url) {
        try {
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.img_load100)
                    .into(img, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError(Exception e) {
                            img.setImageResource(R.drawable.ic_user1);
                        }
                    });
        } catch (Exception e) {
            Log.e("Utilities : Tools", "displayImageOriginalString: " + e.getMessage());
        }
    }


}
