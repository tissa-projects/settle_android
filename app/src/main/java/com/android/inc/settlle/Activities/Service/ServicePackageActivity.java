package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.ServicePackageAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ServiceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicePackageActivity extends AppCompatActivity {
    private RecyclerView recyclerPkg;
    private Context context;
    private Dialog loadingDialog;
    private String uni;
    private TextView txtHeading;
    private static final String TAG = ServicePackageActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_package);

        context = ServicePackageActivity.this;
        recyclerPkg = findViewById(R.id.recycler_service_pkg);
        txtHeading = findViewById(R.id.txtHeading);
        getIntentData();

        if (VU.isConnectingToInternet(context)) {
            getServicePackage();
        }

        findViewById(R.id.back_service_pkg).setOnClickListener(v -> finish());
    }

    private void getServicePackage() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServicePackage(new ServiceDetailModel(uni, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        // service_name
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        servicePackageAdapterCall(dataArray);
                    } else {
                        //handle failed logic here
                        Toast.makeText(context, "Error... " + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();

                }
            }
        });
    }

    private void servicePackageAdapterCall(JSONArray dataArray) {

        ServicePackageAdapter servicePackageAdapter;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerPkg.setLayoutManager(layoutManager);
        servicePackageAdapter = new ServicePackageAdapter(dataArray);
        recyclerPkg.setAdapter(servicePackageAdapter);


    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
        }

    }
}
