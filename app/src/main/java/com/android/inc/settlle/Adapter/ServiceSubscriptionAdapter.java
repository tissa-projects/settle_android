package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class ServiceSubscriptionAdapter extends RecyclerView.Adapter<ServiceSubscriptionAdapter.MyViewHolder> {

    private RecyclerViewItemClickListener.IRenewalClick iServiceRenewalClick;
    private final Context context;
    private final JSONArray dataArray;

    private static final String TAG = ServiceSubscriptionAdapter.class.getSimpleName();


    public ServiceSubscriptionAdapter(@NonNull Context context) {
        this.context = context;
        this.dataArray = new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_service_subscription_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        Log.e(TAG, "ServiceSubscriptionAdapter : " + dataArray);

        try {
            final JSONObject jsonObject = dataArray.getJSONObject(position);
            String serviceProvider = jsonObject.getString("company");
            String serviceType = jsonObject.getString("title");
            String subscriptionDate = jsonObject.getString("starts_on");
            String selectedPlan = jsonObject.getString("plan_type");
            String subscriptionEndDate = jsonObject.getString("ends_on");
            String imgName = jsonObject.getString("image_name");
            String imgUrl = Utilities.IMG_SERVICE_URL + imgName;
            String strStar = jsonObject.getString("stars");
            String amount = jsonObject.getString("amount");
            selectedPlan = Character.toUpperCase(selectedPlan.charAt(0)) + selectedPlan.substring(1);


            Log.e(TAG, "onBindViewHolder: " + serviceProvider + "  " + serviceType);

            if (strStar.equalsIgnoreCase("N/A")) {
                strStar = "0";
            }
            myViewHolder.ratingBar.setRating(Float.parseFloat(strStar));

            myViewHolder.serviceProvidertext.setText( serviceProvider);
            myViewHolder.serviceTypeText.setText( serviceType);
            myViewHolder.subscriptionDateText.setText( subscriptionDate);
            myViewHolder.subscriptionPlanText.setText( selectedPlan);
            myViewHolder.subscriptionDateText.setText( subscriptionDate + " to " + subscriptionEndDate);
            myViewHolder.subscriptionPlanText.setText(selectedPlan + " (\u20B9 "+CommonFunctions.formatNumber(Long.parseLong(amount))+")" );
            Tools.displayImageOriginalString(myViewHolder.imgSpace, imgUrl);
            myViewHolder.renueButton.setOnClickListener(v -> iServiceRenewalClick.onClick(v, position, jsonObject));

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onBindViewHolder: catch :" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("company", dataArray.getJSONObject(i).getString("company"));
                jsonObject.put("starts_on", dataArray.getJSONObject(i).getString("starts_on"));
                jsonObject.put("plan_type", dataArray.getJSONObject(i).getString("plan_type"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("subscription_id", dataArray.getJSONObject(i).getString("subscription_id"));
                jsonObject.put("plan_id", dataArray.getJSONObject(i).getString("plan_id"));
                jsonObject.put("amount", dataArray.getJSONObject(i).getString("amount"));
                jsonObject.put("ends_on", dataArray.getJSONObject(i).getString("ends_on"));
                jsonObject.put("uni_id", dataArray.getJSONObject(i).getString("uni_id"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                this.dataArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView serviceProvidertext, serviceTypeText, subscriptionDateText, subscriptionPlanText;
        AppCompatButton renueButton;
        ImageView imgSpace;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            // this.cardView = itemView.findViewById(R.id.cardview);

            this.serviceProvidertext = itemView.findViewById(R.id.service_provider_text);
            this.serviceTypeText = itemView.findViewById(R.id.service_type_text);
            this.subscriptionDateText = itemView.findViewById(R.id.service_subscription_date);
            this.subscriptionPlanText = itemView.findViewById(R.id.subscription_selected_plan);
            this.renueButton = itemView.findViewById(R.id.service_renue_button);
            this.imgSpace = itemView.findViewById(R.id.account_sub_service_img);
            this.ratingBar = itemView.findViewById(R.id.service_ratingBar);


        }
    }

    //Set method of OnItemClickListener object
    public void setOnRenewClickListener(RecyclerViewItemClickListener.IRenewalClick iServiceRenewalClick) {
        this.iServiceRenewalClick = iServiceRenewalClick;
    }

}
