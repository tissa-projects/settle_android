package com.android.inc.settlle.Utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.core.content.res.ResourcesCompat;

import com.android.inc.settlle.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class CommonFunctions {

    public static String ResizedBitmapEncodeToBase64(Bitmap image) {
        System.gc();
        ByteArrayOutputStream bAOS = new ByteArrayOutputStream();
        byte[] b = new byte[0];
        try {
            image.setDensity(Utilities.DENSITY);
            image.compress(Bitmap.CompressFormat.JPEG, Utilities.QUALITY, bAOS);
            b = bAOS.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public static String formatNumber(long fPrice) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(fPrice);
    }


    public static Bitmap GetResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static Bitmap GetBitmap(String image) {
        byte[] encodeByte = Base64.decode(image, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
    }

    public static String getMimeType(String url) {
        String type = "N/A";
        try {
            String extension = MimeTypeMap.getFileExtensionFromUrl(url);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
            type = url.substring(url.lastIndexOf("."));
            type = type.replace(".", "");
        } catch (Exception ex) {
            showLog("TAG", "getMimeType: " + ex);
        }
        return type;
    }

    public static InputStream getInputStreamByUri(Context context, Uri uri) {
        try {
            return context.getContentResolver().openInputStream(uri);
        } catch (Exception e) {
            e.printStackTrace();
            showLog("TAG", "getInputStreamByUri: " + e);
            return null;
        }
    }

    public static File createFileInLocalDirectory(String fileName1, Context context) {
        File folder = context.getCacheDir();
        File directory = new File(folder + "/Settle");
        if (!directory.exists()) {
            boolean check = directory.mkdir();
            showLog("TAG", "createFile: 00000 " + check);
        }
        File imgFile = new File(directory, fileName1);
        try {
            boolean check = imgFile.createNewFile();
            showLog("TAG", "createFile: 111 " + check);
            return imgFile;
        } catch (IOException e) {
            showLog("TAG", "createFile: 2222 " + e);
            return null;
        }
    }

    public static boolean writeFileInLocalDirectory(InputStream inp, File directory) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            byte[] buffer = new byte[Utilities.MEGABYTE];
            int bufferLength;
            while ((bufferLength = inp.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();
            return true;
        } catch (Exception e) {
            showLog("TAG", "downloadFile:4444 createFile " + e);
            return false;
        }
    }

    public static String GetBase64FromFile(File path) {
        String base64 = "";
        try {
            byte[] buffer = new byte[(int) path.length() + 100];
            @SuppressWarnings("resource")
            int length = new FileInputStream(path).read(buffer);
            base64 = Base64.encodeToString(buffer, 0, length,
                    Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        showLog("TAG", "GetBase64FromPath: " + base64);
        return base64;
    }

    public static void showLog(String tag, String msg) {
        Log.d(tag, msg);
    }

    public static void statusAlert(Context context, String title, String msg, String buttonText) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                buttonText,
                (dialog, id) -> dialog.dismiss());
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static String[] toStringArray(ArrayList<String> arrayList) {
        return arrayList.toArray(new String[0]);
    }

    // Function to convert ArrayList<String> to String[]
    public static String[] GetStringArray(ArrayList<String> arr) {

        // declaration and initialise String Array
        String[] str = new String[arr.size()];
        // ArrayList to Array Conversion
        for (int j = 0; j < arr.size(); j++) {
            // Assign each value to String array
            str[j] = arr.get(j);
        }
        return str;
    }


    public static Drawable getRulesIcon(Context context, String rulesName) {

        Log.d("TAG", "getRulesIcon: " + rulesName);

        switch (rulesName.trim()) {
            case "No Smoking":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_smoking, null);
            case "No Cooking":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_no_cooking, null);
            case "No Ticket Sales":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_ticket, null);
            case "No Music":
            case "No Loud Music/Dancing":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_no_music, null);
            case "No Under-Age (18-21)":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_under_age_18, null);
            case "No Teenagers (10-18)":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_under_age_10, null);
            case "No Children (0-10)":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_no_children, null);
            case "No Outside Catering":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_catering, null);
            case "No Alcohol (Serving)":
            case "No Alcohol (Selling)":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_no_alcohol, null);
            case "No Late Nights Parties":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_party, null);
            case "Air Conditioning":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_aircondition, null);
            case "Bathrooms":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_bathroom, null);
            case "Private Entrance":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_entrance, null);
            case "Projector/Screen TV":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_projector_screen, null);
            case "Kitchen":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_kitchen_room, null);
            case "Sink":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_sink, null);
            case "Stage":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_stage2, null);
            case "Whiteboard":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_whiteboard, null);
            case "Photography Lighting":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_studio_lighting, null);
            case "Sound System":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_sound_system, null);
            case "Wifi":
                return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_wifi, null);
        }
        return ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_rule, null);
    }


}



