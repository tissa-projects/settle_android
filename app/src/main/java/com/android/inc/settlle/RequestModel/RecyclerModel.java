package com.android.inc.settlle.RequestModel;

public class RecyclerModel {
    private boolean isSelected = false;
    private String strAmenitiesName;
    private String strAmenitiesId;

    public RecyclerModel(String strAmenitiesName, String strAmenitiesId) {
        this.strAmenitiesName = strAmenitiesName;
        this.strAmenitiesId = strAmenitiesId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getStrAmenitiesName() {
        return strAmenitiesName;
    }

    public void setStrAmenitiesName(String strAmenitiesName) {
        this.strAmenitiesName = strAmenitiesName;
    }

    public String getStrAmenitiesId() {
        return strAmenitiesId;
    }

    public void setStrAmenitiesId(String strAmenitiesId) {
        this.strAmenitiesId = strAmenitiesId;
    }
}
