package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class MySpaceListAdapter extends RecyclerView.Adapter<MySpaceListAdapter.MyViewHolder> {


    private RecyclerViewItemClickListener.IViewBtnClickListener iViewItemClickListener;
    private RecyclerViewItemClickListener.IDocumentsBtnClickListener iDocumentsBtnClickListener;
    private RecyclerViewItemClickListener.IEdtBtnClickListener iEdtBtnClickListener;
    private final Context context;
    private JSONArray dataArray;

    private static final String TAG = MySpaceListAdapter.class.getName();

    public MySpaceListAdapter(@NonNull Context context) {
        Log.e(TAG, "MySpaceListAdapter: constructor " + dataArray);
        this.context = context;
        dataArray = new JSONArray();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_my_space_list_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        try {
            String imgName = dataArray.getJSONObject(position).getString("image_name");
            Log.e(TAG, "onBindViewHolder:imgName " + imgName);
            String strTitle = dataArray.getJSONObject(position).getString("title");
            String strAddress = dataArray.getJSONObject(position).getString("address");
            String strPinCode = dataArray.getJSONObject(position).getString("zip_code");
            String strStatus = dataArray.getJSONObject(position).getString("status");
            String strVenueType = dataArray.getJSONObject(position).getString("venue_name");
            String uniqueId = dataArray.getJSONObject(position).getString("unique_id");
            String spaceID = dataArray.getJSONObject(position).getString("space_id");
            final String imgUrl = Utilities.IMG_SPACE_URL + imgName;
            Tools.displayImageOriginalString(myViewHolder.img, imgUrl);


            myViewHolder.txtAddress.setText(strAddress);
            myViewHolder.txtTitle.setText(strTitle);
            myViewHolder.txtPincode.setText(strPinCode);
            myViewHolder.txtVenueType.setText(strVenueType);
            if (strStatus.equalsIgnoreCase("0")) {
                myViewHolder.txtStatus.setTextColor(context.getResources().getColorStateList(R.color.dark_red));
                myViewHolder.txtStatus.setText(R.string.Pending);
            } else {
                myViewHolder.txtStatus.setTextColor(context.getResources().getColorStateList(R.color.green));
                myViewHolder.txtStatus.setText(R.string.Approved);
            }

            // TODO :  btn click check on respective activity or fragment
            myViewHolder.btnEdit.setOnClickListener(v -> iEdtBtnClickListener.onItemClick(uniqueId, spaceID));

            myViewHolder.btnView.setOnClickListener(v -> iViewItemClickListener.onItemClick(uniqueId, spaceID));
            myViewHolder.btnDocuments.setOnClickListener(v -> iDocumentsBtnClickListener.onItemClick(uniqueId, spaceID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                jsonObject.put("address", dataArray.getJSONObject(i).getString("address"));
                jsonObject.put("zip_code", dataArray.getJSONObject(i).getString("zip_code"));
                jsonObject.put("status", dataArray.getJSONObject(i).getString("status"));
                jsonObject.put("unique_id", dataArray.getJSONObject(i).getString("unique_id"));
                jsonObject.put("space_id", dataArray.getJSONObject(i).getString("space_id"));
                jsonObject.put("venue_name", dataArray.getJSONObject(i).getString("venue_name"));
                this.dataArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        AppCompatButton btnView, btnEdit, btnDocuments;
        ImageView img;
        TextView txtTitle, txtAddress, txtPincode, txtStatus, txtVenueType;
        ProgressBar progressBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
            cardView = itemView.findViewById(R.id.cardview);
            btnEdit = itemView.findViewById(R.id.btn_edit);
            btnView = itemView.findViewById(R.id.btn_view);
            btnDocuments = itemView.findViewById(R.id.btn_documents);
            img = itemView.findViewById(R.id.img_recycler_my_space_list);
            txtTitle = itemView.findViewById(R.id.title_recycler_my_space_list);
            txtAddress = itemView.findViewById(R.id.address_recycler_my_space_list);
            txtPincode = itemView.findViewById(R.id.pincode_recycler_my_space_list);
            txtStatus = itemView.findViewById(R.id.status_recycler_my_space_list);
            txtVenueType = itemView.findViewById(R.id.txt_venue_type);
        }
    }

    public void setOnEditBtnClickListener(RecyclerViewItemClickListener.IEdtBtnClickListener iEdtBtnClickListener) {
        this.iEdtBtnClickListener = iEdtBtnClickListener;
    }

    public void setOnViewBtnClickListener(RecyclerViewItemClickListener.IViewBtnClickListener iViewItemClickListener) {
        this.iViewItemClickListener = iViewItemClickListener;
    }

    public void setOnDocumentsBtnClickListener(RecyclerViewItemClickListener.IDocumentsBtnClickListener iDocumentsBtnClickListener) {
        this.iDocumentsBtnClickListener = iDocumentsBtnClickListener;
    }

    public void clearData() {
        this.dataArray = null;
        this.dataArray = new JSONArray();
    }

}
