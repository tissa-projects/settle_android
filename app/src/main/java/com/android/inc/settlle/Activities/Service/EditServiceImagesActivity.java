package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.EditServiceImageAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.DeleteImageModel;
import com.android.inc.settlle.RequestModel.ServiceMultipleImageModel;
import com.android.inc.settlle.RequestModel.getMultipleImagesModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditServiceImagesActivity extends AppCompatActivity implements View.OnClickListener {
    private Context context;
    private String serviceId, UniqueId;
    private RecyclerView editServiceImageRecycler;
    private ArrayList<String> imageList, imageIdList, imagesUpdate;
    private Dialog dialog;
    private EditServiceImageAdapter myAdapter = null;
    private static final String TAG = EditServiceImagesActivity.class.getSimpleName();
    ActivityResultLauncher<Intent> activityResultLauncher;
    int selectionType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_service_images);
        context = EditServiceImagesActivity.this;
        initOnStartActivity();
        getIntentData();
        initialise();
        if (VU.isConnectingToInternet(context)) {
            getImageArrayData();
        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            serviceId = bundle.getString("serviceId");
            UniqueId = bundle.getString("uniqueId");
            Log.e(TAG, "getIntentData: serviceId : " + serviceId + " UniqueId : " + UniqueId);
        }
    }

    private void initialise() {

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Update Service Image");

        editServiceImageRecycler = findViewById(R.id.recycler);
        imageList = new ArrayList<>();
        imageIdList = new ArrayList<>();
        imagesUpdate = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        editServiceImageRecycler.setLayoutManager(mLayoutManager);
        setImagesAdapter();
        findViewById(R.id.btn_add_images).setOnClickListener(this);
        findViewById(R.id.btn).setOnClickListener(this);
        findViewById(R.id.fab).setOnClickListener(this);


    }

    //get list of images
    private void getImageArrayData() {

        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMultipleServiceImages(new getMultipleImagesModel(UniqueId, auth_token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        JSONArray imagesArray = dataObj.getJSONArray("service_image");
                        for (int i = 0; i < imagesArray.length(); i++) {
                            imageList.add(imagesArray.getJSONObject(i).getString("service_image"));
                            imageIdList.add(imagesArray.getJSONObject(i).getString("ser_image_id"));
                        }
                        Log.e(TAG, "onResponse: imageIdList " + imageIdList);
                        // setImagesAdapter(imageList, imageIdList);
                        myAdapter.setListData(imageList, imageIdList);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
    }

    public void setImagesAdapter() {
        myAdapter = new EditServiceImageAdapter(context);
        editServiceImageRecycler.setAdapter(myAdapter);

        myAdapter.setOnCLickListener((position, imgList, imgIdList) -> {
            Log.d(TAG, "setImagesAdapter: po " + position);
            if (imgList.size() <= 3) {
                Toast.makeText(context, "Space Must have Atleast 3 Images", Toast.LENGTH_SHORT).show();
            } else {
                Log.d(TAG, "setImagesAdapter: po 111 " + imgIdList.get(position));
                if (!imgIdList.get(position).equals("0")) {
                    deleteImage(position, imgList, imgIdList);
                } else {
                    imgList.remove(position);
                    imgIdList.remove(position);
                    myAdapter.setUpdatedListData(imgList, imgIdList);
                }
            }
        });
    }

    //delete images
    private void deleteImage(int position, List<String> imgList, List<String> imgIdList) {

        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String table = "service";

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteServiceImage(new DeleteImageModel(table, imgIdList.get(position), auth_token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (jsonObject.getInt("status_code") == 200) {
                        String msg = jsonObject.getString("message");
                        imgList.remove(position);
                        imgIdList.remove(position);
                        myAdapter.setUpdatedListData(imgList, imgIdList);
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    } else {
                        String msg = jsonObject.getString("message");
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
    }

    //update  images
    private void updateImages(List<String> imgList, List<String> imgIdList) {
        //  Log.e(TAG, "updateImages: imagesUpdate: " + imagesUpdate);
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);


        for (int i = 0; i < imgIdList.size(); i++) {
            Log.d(TAG, "updateImages: ifconditions value " + imgIdList.get(i));
            if (imgIdList.get(i).equalsIgnoreCase("0")) {
                Log.d(TAG, "updateImages: ifconditions");
                imagesUpdate.add(imgList.get(i));
            }
        }

        //  CommonFunctions.showLog("updateImages ", imagesUpdate.toString());

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateServiceImages(new ServiceMultipleImageModel(imagesUpdate, auth_token, userId, serviceId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        finish();
                    }

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        statusAlert("Information", "Image successfully uploaded", "Success");
                    } else {
                        statusAlert("Alert", msg, "Error");
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_images:
            case R.id.fab:
                imageList = new ArrayList<>();
                imageIdList = new ArrayList<>();
                if (myAdapter.getImageList().size() < 5)
                    if (checkNRequestPermission())
                        imageTypeDialog();
                    else
                        Toast.makeText(context, "Please allow permission", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(context, "Service does not contain more than 5 images", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btn:
                List<String> imgList = myAdapter.getImageList();
                List<String> imgIdList = myAdapter.getImageIdList();

                if (imgIdList.size() < 3) {
                    Toast.makeText(context, "Service must have atleast 3 images", Toast.LENGTH_SHORT).show();
                } else {
                    if (VU.isConnectingToInternet(context)) {
                        updateImages(imgList, imgIdList);
                    }
                }
                break;
        }
    }

    public void LoadImage() {
        myAdapter.setListData(imageList, imageIdList);
    }

    private static final int PERMISSION_REQUEST_CODE = 200;

    String imgselectionType, serviceProofDocExtension;
    boolean accessPermission = false;

    private boolean checkNRequestPermission() {
        //check which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String perm : Utilities.appPermission) {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);
            accessPermission = false;
            return false;
        }
        accessPermission = true;
        return true;
    }

    private void imageTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(context);
            docTypeAlert.setTitle("Add Photo!");
            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];
                //handle logic here
                if (imgselectionType.equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    selectionType = Utilities.RESULT_CAMERA;
                    activityResultLauncher.launch(cameraIntent);
                    // startActivityForResult(cameraIntent, Utilities.RESULT_CAMERA);
                } else if (imgselectionType.equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    selectionType = Utilities.RESULT_GALLERY;
                    activityResultLauncher.launch(intent);
                    // startActivityForResult(Intent.createChooser(intent, "Select Picture"), Utilities.RESULT_GALLERY);
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImageURI = data.getData();
                getDataForImage(selectedImageURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Utilities.RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
            serviceProofDocExtension = "png";
            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
            imageList.add(strProfileImg);
            imageIdList.add("0");
            LoadImage();
        }
    }*/

    @SuppressLint("SetTextI18n")
    public void initOnStartActivity() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                getDataForImage(selectedImageURI);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Utilities.RESULT_CAMERA == selectionType) {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            serviceProofDocExtension = "png";
                            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                            imageList.add(strProfileImg);
                            imageIdList.add("0");
                            LoadImage();
                        }
                    }
                });
    }

    private void getDataForImage(Uri selectedImageURI) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory("SPACE_IMAGE.png", context);
            if (fileName != null) {
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    imageList.add(CommonFunctions.GetBase64FromFile(fileName));
                    imageIdList.add("0");
                    serviceProofDocExtension = "png";
                    LoadImage();
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        dialog.dismiss();
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}
