package com.android.inc.settlle.Utilities;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;

public class CustomToast {

    public static void custom_Toast(Context context, String toastText,View layout){
        TextView text = layout.findViewById(R.id.text);
        text.setText(toastText);

        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}
