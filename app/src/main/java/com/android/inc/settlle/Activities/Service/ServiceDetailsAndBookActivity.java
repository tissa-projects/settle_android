package com.android.inc.settlle.Activities.Service;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.LoginActivity;
import com.android.inc.settlle.Activities.MapsActivity;
import com.android.inc.settlle.Activities.ShowAllImagesActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ServiceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceDetailsAndBookActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private SliderLayout sliderLayout;
    private AppCompatButton btnBookService;
    private String uni,star;
    private Dialog loadingDialog;
    private TextView serviceProviderText, aboutUS, serviceNameText, priceTypeText, serviceProviderNameText, serviceProviderMobileText, serviceProviderEmailText, txtHostMemberSince;
    private RatingBar ratingBar;
    ArrayList<String> imagesAddress;
    private float rating;
    private String serviceLatitude, serviceLongitude, serviceProvider, serviceId;
    private static final String TAG = ServiceDetailsAndBookActivity.class.getName();
    LinearLayout serviceViewLayout;
    TextView noOfGuestTxt;


    @SuppressLint({"SetTextI18n", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_details_and_book);

        context = ServiceDetailsAndBookActivity.this;

        findViewById(R.id.service_show_back).setOnClickListener(v -> finish());
        initialize();
        getIntentData();
        if (Utilities.getSPbooleanValue(context, Utilities.spIsBookingType)) {
            btnBookService.setText("Send Booking Request");
        } else {
            btnBookService.setText("Add to Cart");
        }
    }

    private void initialize() {

        imagesAddress = new ArrayList<>();
        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.SWAP); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setSliderTransformAnimation(SliderAnimations.FADETRANSFORMATION);
        sliderLayout.setScrollTimeInSec(3); //set scroll delay in seconds :

        serviceProviderText = findViewById(R.id.service_show_service_privider);
        priceTypeText = findViewById(R.id.service_show_price_type);
        serviceNameText = findViewById(R.id.service_show_service_service_type);
        ratingBar = findViewById(R.id.service_show_rating);
        serviceProviderNameText = findViewById(R.id.service_show_host_name);
        serviceProviderMobileText = findViewById(R.id.service_show_host_mobile);
        serviceProviderEmailText = findViewById(R.id.service_show_host_email);
        serviceViewLayout = findViewById(R.id.service_view_layout);
        btnBookService = findViewById(R.id.book_service_button);
        txtHostMemberSince = findViewById(R.id.host_member_since);
        serviceViewLayout.setVisibility(View.GONE);
        noOfGuestTxt = findViewById(R.id.no_of_guest);
        aboutUS = findViewById(R.id.aboutUS);

        findViewById(R.id.service_show_ll_map).setOnClickListener(this);
        findViewById(R.id.service_show_ll_review).setOnClickListener(this);
        findViewById(R.id.service_show_ll_pkg).setOnClickListener(this);
        findViewById(R.id.service_show_img_show_all_images).setOnClickListener(this);
        btnBookService.setOnClickListener(this);

    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.service_show_ll_map:
                Log.d(TAG, "onClick: 1 ");
                Intent mapIntent = new Intent(context, MapsActivity.class);
                mapIntent.putExtra("latitude", serviceLatitude);
                mapIntent.putExtra("longitude", serviceLongitude);
                mapIntent.putExtra("name", serviceProvider);
                startActivity(mapIntent);
                break;

            case R.id.service_show_ll_review:
                Log.d(TAG, "onClick: 2 ");
                Intent reviewIntent = new Intent(context, ServiceReviewActivity.class);
                reviewIntent.putExtra("uni", uni);
                reviewIntent.putExtra("star", star);
                reviewIntent.putExtra("name", serviceProvider);
                startActivity(reviewIntent);

                break;

            case R.id.service_show_ll_pkg:
                Log.d(TAG, "onClick: 3 ");
                Intent intent = new Intent(context, ServicePackageActivity.class);
                intent.putExtra("uni", uni);
                startActivity(intent);

                break;

            case R.id.service_show_img_show_all_images:
                Log.d(TAG, "onClick: 4 ");
                Intent imgIntent = new Intent(context, ShowAllImagesActivity.class);
                imgIntent.putExtra("imgUrls", imagesAddress);
                startActivity(imgIntent);
                break;

            case R.id.book_service_button:
                Log.d(TAG, "onClick: 5 ");
                boolean spIsLoggedin = Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin);
                Intent serviceIintent;
                if (spIsLoggedin) {
                    serviceIintent = new Intent(context, ServiceBookingFormActivity.class);
                } else {
                    Utilities.setSPboolean(context, Utilities.spAtBookingServiceActivity, true);
                    Utilities.setSPboolean(context, Utilities.spAtBookingSpaceActivity, false);
                    Utilities.setSPboolean(context, Utilities.spAtGuestSpaceActivity, false);
                    serviceIintent = new Intent(context, LoginActivity.class);
                }
                serviceIintent.putExtra("unique_id", uni);
                serviceIintent.putExtra("id", serviceId);
                serviceIintent.putExtra("type", "space");
                startActivity(serviceIintent);

                break;
        }
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
            if (uni != null) {
                if (VU.isConnectingToInternet(context)) {
                    getServiceDetail();
                }
            }
        }
    }


    //get service detail
    private void getServiceDetail() {
        final String[] guestList = getResources().getStringArray(R.array.GuestList);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Log.e(TAG, "getServiceDetail: " + uni);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServiceDetail(new ServiceDetailModel(uni, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint({"SetTextI18n", "InflateParams"})
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    loadingDialog.dismiss();
                    assert response.body() != null;
                    String api_response = response.body().string();
                    //guest
                    JSONObject jsonObject = new JSONObject(api_response);

                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {

                        serviceViewLayout.setVisibility(View.VISIBLE);
                        //handle success logic here
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONObject serviceDetailObject = dataObject.getJSONObject("service_details");

                        Log.e(TAG, "serviceDetailObject: " + serviceDetailObject);
                        serviceProvider = serviceDetailObject.getString("company");
                        serviceId = serviceDetailObject.getString("service_details_id");
                        serviceProviderText.setText(serviceProvider);
                        serviceProviderNameText.setText(serviceDetailObject.getString("fname") + " " + serviceDetailObject.getString("lname"));
                        serviceProviderMobileText.setText(serviceDetailObject.getString("mobileno"));
                        serviceProviderEmailText.setText(serviceDetailObject.getString("email_id"));
                        serviceNameText.setText(serviceDetailObject.getString("service_name"));
                        priceTypeText.setText(serviceDetailObject.getString("price_type"));
                        String ratingStar = serviceDetailObject.getString("stars");
                        star = ratingStar;

                        noOfGuestTxt.setText(guestList[Integer.parseInt(serviceDetailObject.getString("guest")) - 1]);

                        aboutUS.setText(serviceDetailObject.getString("description"));


                        if (ratingStar.equalsIgnoreCase("N/A")) {
                            rating = Float.parseFloat("0");
                        } else {
                            rating = Float.parseFloat(ratingStar);
                        }
                        ratingBar.setRating(rating);
                        serviceLatitude = serviceDetailObject.getString("lat");
                        serviceLongitude = serviceDetailObject.getString("lon");

                        String startTime = serviceDetailObject.getString("from_time");
                        String endTime = serviceDetailObject.getString("to_time");
                        String time = startTime + " - " + endTime;

                        String strGuestId = serviceDetailObject.getString("guest");
                        int guestid = Integer.parseInt(strGuestId);


                        txtHostMemberSince.setText(serviceDetailObject.getString("created_on"));


                        JSONArray dayArray = dataObject.getJSONArray("service_days");

                        Log.d(TAG, "onResponse: ServiceDate " + dayArray);

                        for (int i = 0; i < dayArray.length(); i++) {
                            JSONObject dayObject = dayArray.getJSONObject(i);
                            // String day = dayObject.getString("day");
                            View view;
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            view = inflater.inflate(R.layout.timing_layout, null);
                            TextView dayText = view.findViewById(R.id.day);
                            TextView daytime = view.findViewById(R.id.timing);
                            dayText.setText(dayObject.getString("day"));
                            daytime.setText(time);
                            LinearLayout linearLayout = findViewById(R.id.service_show_timing_layout);
                            linearLayout.addView(view);
                        }

                        JSONArray imgAddressArray = dataObject.getJSONArray("service_image");
                        for (int i = 0; i < imgAddressArray.length(); i++) {
                            JSONObject imgObject = imgAddressArray.getJSONObject(i);
                            imagesAddress.add(Utilities.IMG_SERVICE_URL + imgObject.getString("service_image"));

                            //   Toast.makeText(SpaceDetailAndBookActivity.this, imgObject.getString("space_image"), Toast.LENGTH_SHORT).show();
                        }
                        CommonFunctions.showLog("images", "onResponse: ");
                        CommonFunctions.showLog("IMAGES", imagesAddress.toString());
                        setSliderViews();
                        // ratingBar;
                    } else {
                        //handle failed logic here
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onResponse: " + e.getMessage());
                }


            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, getResources().getString(R.string.onResponseFail), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setSliderViews() {

        try {
            Log.d("IMAGES", imagesAddress.toString());
            for (int i = 0; i <= 2; i++) {

                DefaultSliderView sliderView = new DefaultSliderView(context);

                switch (i) {
                    case 0:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 1:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                    case 2:
                        sliderView.setImageUrl(imagesAddress.get(i));
                        break;
                }

                sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
                sliderView.setOnSliderClickListener(sliderView1 -> {
                });

                //at last add this view in your layout :
                sliderLayout.addSliderView(sliderView);
            }
        } catch (Exception e) {
            Log.e(TAG, "setSliderViews: catch : " + e.getMessage());
        }

    }
}