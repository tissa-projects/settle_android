package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.AddSpaceAmenitiesAdapter;
import com.android.inc.settlle.Adapter.AddSpaceEventAdapter;
import com.android.inc.settlle.Adapter.AddSpaceRulesAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AmenitiesRulesModel;
import com.android.inc.settlle.RequestModel.AuthModel;
import com.android.inc.settlle.RequestModel.EventModel;
import com.android.inc.settlle.RequestModel.SpaceRegistartionDetailsModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceRegisAmenitiesEventsRulesActivity extends AppCompatActivity implements
        /*AddSpaceAmenitiesAdapter. MyCallBack ,*/View.OnClickListener {

    public String space_title, venue_id, subscription_id, user_id, guest, country, state, city, address, latitude, longitude, zip_code, landmark, accommodate, description, discount = "0", price_type, from_time, to_time, auth_token;
    private Context context;
    private RecyclerView recyclerViewEvent, recyclerViewAmenities, recyclerViewRules;
    private ArrayList<Integer> days = null;
    private ArrayList<String> ruleNameList = null, ruleIdlist = null, eventNameList = null, eventIdList = null,
            amenitiesNamelist = null, amenitiesIdlist = null, venueIdList, venueNameList, subscriptionIdList;
    private Dialog SpaceRigisDialog;
    private AddSpaceAmenitiesAdapter addSpaceAmenitiesAdapter = null;
    private AddSpaceRulesAdapter addSpaceRulesAdapter = null;
    //private HashMap<Integer,String> map;
    private AddSpaceEventAdapter addSpaceEventAdapter = null;
    //private ArrayList<Integer> amenitiesIds, eventListIds, eventPriceList, rulesIdList;
    private ArrayList<String> newEventIds = null, newEventPrice = null, newAmenitiesId = null, newRulesId = null;
    private View layout;
    private static final String TAG = SpaceRegisAmenitiesEventsRulesActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_regis_amenities_events_rules);
        context = SpaceRegisAmenitiesEventsRulesActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

        getIntentData();
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Add Amenities and Rules");

        initialize();
        initRecyclerAmenities();
        initRecyclerRules();
        initRecyclerEvent();

        if (VU.isConnectingToInternet(context)) {
            getEventsAmenitesRules();
        }


    }

    private void initialize() {
        recyclerViewEvent = findViewById(R.id.recycler_add_space_event);
        recyclerViewAmenities = findViewById(R.id.recycler_add_space_amenities);
        recyclerViewRules = findViewById(R.id.recycler_add_space_rules);
        findViewById(R.id.btn_next_space_regis_ame_event_rules).setOnClickListener(this);

    }


    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            space_title = extras.getString("space_title");
            venueIdList = extras.getStringArrayList("venue_id_list");
            venueNameList = extras.getStringArrayList("venue_name_list");
            subscriptionIdList = extras.getStringArrayList("subscription_id_list");
            guest = extras.getString("guest");
            country = extras.getString("country");
            state = extras.getString("state");
            city = extras.getString("city");
            address = extras.getString("address");
            latitude = extras.getString("latitude");
            longitude = extras.getString("longitude");
            zip_code = extras.getString("zip_code");
            landmark = extras.getString("landmark");
            accommodate = extras.getString("accommodate");
            description = extras.getString("description");
            discount = extras.getString("discount");
            price_type = extras.getString("price_type");
            from_time = extras.getString("from_time");
            to_time = extras.getString("to_time");
            days = extras.getIntegerArrayList("daysId");
        }

        Log.e(TAG, "getIntentData: " + space_title + venue_id + guest + country + state + city + " " +
                address + latitude + longitude + zip_code + landmark + accommodate + description + discount + price_type + from_time + to_time + days);

    }


    //get Events amenities and rules
    private void getEventsAmenitesRules() {
        ruleNameList = new ArrayList<>();
        ruleIdlist = new ArrayList<>();
        eventNameList = new ArrayList<>();
        eventIdList = new ArrayList<>();
        amenitiesNamelist = new ArrayList<>();
        amenitiesIdlist = new ArrayList<>();
        SpaceRigisDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "getEventsAmenitesRules: " + authToken);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getEventsAmenitesRules(new AuthModel(authToken));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceRigisDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: getEventsAmenitesRules : " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String strStatusMsg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            JSONArray eventArray = dataObj.getJSONArray("events");
                            JSONArray rulesArray = dataObj.getJSONArray("rules");
                            JSONArray amenitiesArray = dataObj.getJSONArray("amenities");
                            for (int i = 0; i < rulesArray.length(); i++) {
                                ruleNameList.add(rulesArray.getJSONObject(i).getString("rules_name"));
                                ruleIdlist.add(rulesArray.getJSONObject(i).getString("rules_id"));
                            }
                            // set rules Adapter
                            addRulesAdapter();

                            for (int j = 0; j < eventArray.length(); j++) {
                                eventNameList.add(eventArray.getJSONObject(j).getString("event_name"));
                                eventIdList.add(eventArray.getJSONObject(j).getString("event_id"));
                            }
                            //set event adapter
                            addEventsAdapter();

                            for (int k = 0; k < amenitiesArray.length(); k++) {
                                amenitiesNamelist.add(amenitiesArray.getJSONObject(k).getString("amenities_name"));
                                amenitiesIdlist.add(amenitiesArray.getJSONObject(k).getString("amenities_id"));
                            }

                            //set Amenities adapter
                            addAmenitiesAdapter();

                        } else {
                            Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();

                        }
                    }
                    Log.e(TAG, "onResponse: rules: " + ruleNameList + " " + ruleIdlist + " events : " + eventNameList + " " + eventIdList + " amenities: " + amenitiesNamelist + " " + amenitiesIdlist);
                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                SpaceRigisDialog.dismiss();
            }
        });
    }

    private void initRecyclerAmenities() {
        @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerViewAmenities.setLayoutManager(layoutManager);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addAmenitiesAdapter() {

        //  addSpaceAmenitiesAdapter = new AddSpaceAmenitiesAdapter(context, amenitiesNamelist, amenitiesIdlist,this );
        addSpaceAmenitiesAdapter = new AddSpaceAmenitiesAdapter(amenitiesNamelist, amenitiesIdlist, this);
        recyclerViewAmenities.setAdapter(addSpaceAmenitiesAdapter);
        addSpaceAmenitiesAdapter.notifyDataSetChanged();

        addSpaceAmenitiesAdapter.setOnItemClickListener();
    }


    private void initRecyclerRules() {
        @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerViewRules.setLayoutManager(layoutManager);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addRulesAdapter() {
        addSpaceRulesAdapter = new AddSpaceRulesAdapter(ruleNameList, ruleIdlist, this);
        recyclerViewRules.setAdapter(addSpaceRulesAdapter);
        addSpaceRulesAdapter.notifyDataSetChanged();
        addSpaceRulesAdapter.setOnItemClickListener();
    }


    private void initRecyclerEvent() {
        @SuppressLint("WrongConstant") RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerViewEvent.setLayoutManager(layoutManager);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void addEventsAdapter() {
        addSpaceEventAdapter = new AddSpaceEventAdapter(eventNameList, eventIdList);
        recyclerViewEvent.setAdapter(addSpaceEventAdapter);
        addSpaceEventAdapter.notifyDataSetChanged();

        addSpaceEventAdapter.setOnItemClickListener();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_next_space_regis_ame_event_rules) {
            addAdaptersData();
            if (validate()) {
                Log.e(TAG, "onClick: newEventIds : " + newEventIds + "size :" + newEventIds.size() + " newEventPrice : " + newEventPrice + " size :" + newEventPrice.size());
                setSpaceRegistrationData();
            }
        }
    }

    private void addAdaptersData() {
        Map<Integer, EventModel> eventArrayList = addSpaceEventAdapter.getEventList();
        Map<Integer, AmenitiesRulesModel> amenitiesArrayList = addSpaceAmenitiesAdapter.getAmenitiesList();
        Map<Integer, AmenitiesRulesModel> rulesArrayList = addSpaceRulesAdapter.getRuleList();

        newEventIds = new ArrayList<>();
        newEventPrice = new ArrayList<>();
        newAmenitiesId = new ArrayList<>();
        newRulesId = new ArrayList<>();

        for (int key : eventArrayList.keySet()) {
            EventModel eventModel = eventArrayList.get(key);
            assert eventModel != null;
            String id = eventModel.getEventNameId();
            String price = eventModel.getEventPrice();
            Log.e(TAG, "onClick: events: " + id + "  " + price);
            if (id != null || price != null) {
                newEventIds.add(id);
                newEventPrice.add(price);
            }
        }

        for (int key : amenitiesArrayList.keySet()) {
            AmenitiesRulesModel amenitiesModel = amenitiesArrayList.get(key);
            assert amenitiesModel != null;
            String id = amenitiesModel.getAmenitiesRulesNameId();
            Log.e(TAG, "onClick: amenities: " + id + "  ");
            if (id != null) {
                newAmenitiesId.add(id);
            }
        }

        for (int key : rulesArrayList.keySet()) {
            AmenitiesRulesModel rulesModel = rulesArrayList.get(key);
            assert rulesModel != null;
            String id = rulesModel.getAmenitiesRulesNameId();
            Log.e(TAG, "onClick: rules: " + id + "  ");
            if (id != null) {
                newRulesId.add(id);
            }
        }
    }

    public boolean validate() {
        if (newEventIds.size() == 0) {
            CustomToast.custom_Toast(context, "Select Atleast One Event And Add Price", layout);
            return false;
        } else if (newAmenitiesId.size() == 0) {
            CustomToast.custom_Toast(context, "Select Atleast One Amenities", layout);
            return false;
        } else if (newRulesId.size() == 0) {
            CustomToast.custom_Toast(context, "Select Atleast One Rule", layout);
            return false;
        }
        return true;
    }


    //Get releted Data
    private void setSpaceRegistrationData() {
        String phone = Utilities.getSPstringValue(context, Utilities.spMobileNo);
        //   String phone = "9673140477";
        Log.e(TAG, "setSpaceRegistrationData: " + newEventPrice);

        subscription_id = Utilities.getSPstringValue(context, Utilities.spSubscriptionId);
        user_id = Utilities.getSPstringValue(context, Utilities.spUserId);
        auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "setSpaceRegistrationData: country : " + country + " city : " + city + " state : " + state);
        Log.e(TAG, "setSpaceRegistrationData: newEventIds : " + newEventIds + "\n" + " newAmenitiesId : " + newAmenitiesId + "\n" + " newRulesId : " + newRulesId +
                "\n" + " venueIdList : " + venueIdList + "\n" + " venueNameList : " + venueNameList + "\n" + " subscriptionIdList : " + subscriptionIdList);
        SpaceRigisDialog = ProgressDialog.show(context, "Please wait", "Loading...");


        SpaceRegistartionDetailsModel spd = new SpaceRegistartionDetailsModel(space_title, phone,
                venueIdList, venueNameList, subscriptionIdList,
                user_id, guest, country, state, city, address, latitude, longitude, zip_code, landmark, accommodate, description, discount,
                price_type, from_time, to_time, auth_token, days, newEventIds, newEventPrice, newAmenitiesId, newRulesId);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().setSpaceRegistrationData(spd);


        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: space_title 11  " + spd);

        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: space_title " + space_title);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: phone " + phone);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: venueIdList " + venueIdList.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: venueNameList " + venueNameList.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: subscriptionIdList " + subscriptionIdList.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: user_id " + user_id);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: guest " + guest);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: country " + country);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: state " + state);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: city " + city);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: address " + address);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: latitude " + latitude);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: longitude " + longitude);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: zip_code " + zip_code);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: landmark " + landmark);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: accommodate " + accommodate);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: description " + description);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData:discount " + discount);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: price_type " + price_type);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: from_time " + from_time);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: to_time " + to_time);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: auth_token " + auth_token);
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: days " + days.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: newEventIds " + newEventIds.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: newEventPrice " + newEventPrice.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: newAmenitiesId " + newAmenitiesId.toString());
        CommonFunctions.showLog(TAG, "setSpaceRegistrationData: newRulesId " + newRulesId.toString());


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceRigisDialog.dismiss();
                try {
                    Log.e(TAG, "onResponse: " + response.message());
                    Log.e(TAG, "onResponse: " + response.code());

                    assert response.body() != null;
                    String api_response = response.body().string();

                    JSONObject Obj = new JSONObject(api_response);
                    Log.e(TAG, "onResponse: " + Obj);
                    int statusCode = Obj.getInt("status_code");
                    String msg = Obj.getString("message");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            String insertId = Obj.getString("insert_id");
                            //Toast.makeText(SpaceRegisAmenitiesEventsRulesActivity.this, msg, Toast.LENGTH_SHORT).show();
                            statusAlert("Information", msg, "Success", insertId);
                        } else {
                            statusAlert("Alert", msg, "Error", "");
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error", "");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                SpaceRigisDialog.dismiss();
            }
        });
    }

    private void statusAlert(String title, String msg, String type, String insertId) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        Intent intent = new Intent(context, SpaceAddImagesActivity.class);
                        intent.putExtra("insertId", insertId);
                        startActivity(intent);
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
