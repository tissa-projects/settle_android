package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class SpaceSubscriptionAdapter extends RecyclerView.Adapter<SpaceSubscriptionAdapter.MyViewHolder> {

    private RecyclerViewItemClickListener.IRenewalClick iSpaceRenewalClick;
    private final Context context;
    private final JSONArray dataArray;
    private static final String TAG = GuestSpaceAdapter.class.getSimpleName();

    public SpaceSubscriptionAdapter(@NonNull Context context) {
        this.context = context;
        this.dataArray = new JSONArray();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_space_subscription_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {


        try {
            final JSONObject jsonObject = dataArray.getJSONObject(position);
            String spaceName = jsonObject.getString("company");
            String selectedVenue = jsonObject.getString("title");
            String subscriptionDate = jsonObject.getString("starts_on");
            String subscriptionEndDate = jsonObject.getString("ends_on");
            String selectedPlan = jsonObject.getString("plan_type");

            selectedPlan = Character.toUpperCase(selectedPlan.charAt(0)) + selectedPlan.substring(1);

            String amount = jsonObject.getString("amount");
            String imgName = jsonObject.getString("image_name");
            String imgUrl = Utilities.IMG_SPACE_URL + imgName;
            String strStar = jsonObject.getString("stars");

            if (strStar.equalsIgnoreCase("N/A")) {
                strStar = "0";
            }
            myViewHolder.ratingBar.setRating(Float.parseFloat(strStar));
            myViewHolder.spaceNametext.setText("" + spaceName);
            myViewHolder.selecteVenueText.setText("" + selectedVenue);
            myViewHolder.subscriptionDateText.setText("" + subscriptionDate + " to " + subscriptionEndDate);
            myViewHolder.subscriptionPlanText.setText(selectedPlan +" (\u20B9 "+ CommonFunctions.formatNumber(Long.parseLong(amount))+")");

            Tools.displayImageOriginalString(myViewHolder.imgSpace, imgUrl);

            myViewHolder.renueButton.setOnClickListener(v -> iSpaceRenewalClick.onClick(v, position, jsonObject));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("company", dataArray.getJSONObject(i).getString("company"));
                jsonObject.put("starts_on", dataArray.getJSONObject(i).getString("starts_on"));
                jsonObject.put("plan_type", dataArray.getJSONObject(i).getString("plan_type"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("subscription_id", dataArray.getJSONObject(i).getString("subscription_id"));
                jsonObject.put("plan_id", dataArray.getJSONObject(i).getString("plan_id"));
                jsonObject.put("amount", dataArray.getJSONObject(i).getString("amount"));
                jsonObject.put("ends_on", dataArray.getJSONObject(i).getString("ends_on"));
                jsonObject.put("uni_id", dataArray.getJSONObject(i).getString("uni_id"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                this.dataArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView spaceNametext, selecteVenueText, subscriptionDateText, subscriptionPlanText;
        AppCompatButton renueButton;
        ImageView imgSpace;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            //this.cardView = itemView.findViewById(R.id.cardview);
            this.spaceNametext = itemView.findViewById(R.id.space_name_text);
            this.selecteVenueText = itemView.findViewById(R.id.selected_venue_text);
            this.subscriptionDateText = itemView.findViewById(R.id.subscription_date_text);
            this.subscriptionPlanText = itemView.findViewById(R.id.subscription_plan_text);
            this.renueButton = itemView.findViewById(R.id.renue_button);
            this.imgSpace = itemView.findViewById(R.id.account_sub_space_img);
            this.ratingBar = itemView.findViewById(R.id.space_ratingBar);

        }
    }

    //Set method of OnItemClickListener object
    public void setOnRenewClickListener(RecyclerViewItemClickListener.IRenewalClick iSpaceRenewalClick) {
        this.iSpaceRenewalClick = iSpaceRenewalClick;
    }

}
