package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.EditSpaceImageAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.DeleteImageModel;
import com.android.inc.settlle.RequestModel.UpdateSpaceImagesModel;
import com.android.inc.settlle.RequestModel.getMultipleImagesModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSpaceImagesActivity extends AppCompatActivity implements /*BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate,*/ View.OnClickListener {
    private Context context;
    private String SpaceId, UniqueId;
    private RecyclerView editSpaceImageRecycler;
    private Dialog SpaceUploadImagesDialog;
    private List<String> myList, myIdList, updatedImages, updatedIds;
    private EditSpaceImageAdapter myAdapter = null;
    private static final String TAG = EditSpaceImagesActivity.class.getName();
    ActivityResultLauncher<Intent> activityResultLauncher;
    int selectionType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_space_images);
        context = EditSpaceImagesActivity.this;
        getIntentData();
        initOnStartActivity();
        initialise();
        if (VU.isConnectingToInternet(context)) {
            getImageArrayData();
        }
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            SpaceId = bundle.getString("spaceId");
            UniqueId = bundle.getString("uniqueId");
            Log.e(TAG, "getIntentData: SpaceId : " + SpaceId + " UniqueId :  " + UniqueId);
        }
    }

    private void initialise() {
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        editSpaceImageRecycler = findViewById(R.id.recycler);
        myList = new ArrayList<>();
        myIdList = new ArrayList<>();
        updatedImages = new ArrayList<>();
        updatedIds = new ArrayList<>();
        setImagesAdapter();
        findViewById(R.id.btn_add_images).setOnClickListener(this);
        findViewById(R.id.btn).setOnClickListener(this);
        findViewById(R.id.fab).setOnClickListener(this);

        TextView h = findViewById(R.id.txtHeading);
        h.setText("Edit Space Images");
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: called " + v.getId());
        switch (v.getId()) {
            case R.id.btn_add_images:
            case R.id.fab:
                Log.d(TAG, "onClick: called ");
                myList = new ArrayList<>();
                myIdList = new ArrayList<>();
                if (myAdapter.getImageList().size() < 5)
                    if (checkNRequestPermission())
                        imageTypeDialog();
                    else
                        Toast.makeText(context, "Please allow permission", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(context, "Space does not contain more than 5 images", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn:
                List<String> imgList = myAdapter.getImageList();
                List<String> imgIdList = myAdapter.getImageIdList();
                if (imgIdList.size() < 3) {
                    Toast.makeText(context, "Space must have atleast 3 images", Toast.LENGTH_SHORT).show();
                } else {
                    if (VU.isConnectingToInternet(context)) {
                        updateImages(imgList, imgIdList);
                    }
                }
                break;
        }
    }

    //get list of images
    private void getImageArrayData() {
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        SpaceUploadImagesDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getMultipleSpaceImages(new getMultipleImagesModel(UniqueId, auth_token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceUploadImagesDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    CommonFunctions.showLog(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getInt("status_code") == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (jsonObject.getInt("status_code") == 200) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        JSONArray imagesArray = dataObj.getJSONArray("space_image");
                        if (imagesArray.length() > 0) {
                            for (int i = 0; i < imagesArray.length(); i++) {
                                myList.add(imagesArray.getJSONObject(i).getString("space_image"));
                                myIdList.add(imagesArray.getJSONObject(i).getString("image_id"));
                            }
                        } else {
                            Toast.makeText(context, "No images available for space", Toast.LENGTH_SHORT).show();
                        }
                        CommonFunctions.showLog(TAG, "onResponse: imageIdList " + myIdList);
                        myAdapter.setListData(myList, myIdList);
                    }
                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                CommonFunctions.showLog(TAG, "onFailure: " + t.getMessage());
                SpaceUploadImagesDialog.dismiss();
            }
        });
    }

    //update  images
    private void updateImages(List<String> imgList, List<String> imgIdList) {
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        for (int i = 0; i < imgIdList.size(); i++) {
            if (imgIdList.get(i).equalsIgnoreCase("0")) {
                updatedImages.add(imgList.get(i));
                updatedIds.add(imgIdList.get(i));
            }
        }
        CommonFunctions.showLog(TAG, "updateImages: updatedImages: " + updatedImages.toString() + "\n" + "updatedIds :" + updatedIds.toString());
        SpaceUploadImagesDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateSpaceImages(new UpdateSpaceImagesModel(userId, SpaceId, auth_token, updatedImages));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceUploadImagesDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");

                    //  Toast.makeText(EditSpaceImagesActivity.this, msg, Toast.LENGTH_SHORT).show();
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        statusAlert("Information", "Image successfully uploaded", "Success");
                    } else {
                        statusAlert("Alert", msg, "Error");
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                SpaceUploadImagesDialog.dismiss();
            }
        });
    }

    public void setImagesAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        editSpaceImageRecycler.setLayoutManager(mLayoutManager);
        myAdapter = new EditSpaceImageAdapter(context);
        editSpaceImageRecycler.setAdapter(myAdapter);

        myAdapter.setOnCLickListener((position, imgList, imgIdList) -> {
            if (imgList.size() <= 3) {
                Toast.makeText(context, "Space Must have Atleast 3 Images", Toast.LENGTH_SHORT).show();
            } else {
                if (!imgIdList.get(position).equalsIgnoreCase("0")) {
                    deleteImage(position, imgList, imgIdList);
                } else {
                    imgList.remove(position);
                    imgIdList.remove(position);
                    myAdapter.setListData(imgList, imgIdList);
                }
            }
        });
    }

    //delete images
    private void deleteImage(int position, List<String> imgList, List<String> imgIdList) {
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String table = "space";
        SpaceUploadImagesDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteSpaceImage(new DeleteImageModel(table, imgIdList.get(position), auth_token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceUploadImagesDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    CommonFunctions.showLog(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getInt("status_code") == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (jsonObject.getInt("status_code") == 200) {
                        String msg = jsonObject.getString("message");
                        imgList.remove(position);
                        imgIdList.remove(position);
                        myAdapter.setUpdatedListData(imgList, imgIdList);
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                CommonFunctions.showLog(TAG, "onFailure: " + t.getMessage());
                SpaceUploadImagesDialog.dismiss();
            }
        });
    }

    public void LoadImage() {
        myAdapter.setListData(myList, myIdList);
    }

    private static final int PERMISSION_REQUEST_CODE = 200;

    String imgselectionType, serviceProofDocExtension;
    boolean accessPermission = false;

    private boolean checkNRequestPermission() {
        //check which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String perm : Utilities.appPermission) {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);
            accessPermission = false;
            return false;
        }
        accessPermission = true;
        return true;
    }

    private void imageTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(context);
            docTypeAlert.setTitle("Add Photo!");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];
                //handle logic here
                if (imgselectionType.equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    selectionType = Utilities.RESULT_CAMERA;
                    activityResultLauncher.launch(cameraIntent);
                    // startActivityForResult(cameraIntent, Utilities.RESULT_CAMERA);
                } else if (imgselectionType.equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    selectionType = Utilities.RESULT_GALLERY;
                    activityResultLauncher.launch(intent);
                    //  startActivityForResult(Intent.createChooser(intent, "Select Picture"), Utilities.RESULT_GALLERY);
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImageURI = data.getData();
                getDataForImage(selectedImageURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Utilities.RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
            serviceProofDocExtension = "png";
            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
            myList.add(strProfileImg);
            myIdList.add("0");
            LoadImage();
        }
    }*/

    public void initOnStartActivity() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                getDataForImage(selectedImageURI);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Utilities.RESULT_CAMERA == selectionType) {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            serviceProofDocExtension = "png";
                            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                            myList.add(strProfileImg);
                            myIdList.add("0");
                            LoadImage();
                        }
                    }
                });
    }

    private void getDataForImage(Uri selectedImageURI) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory("SPACE_IMAGE.png", context);
            if (fileName != null) {
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    myList.add(CommonFunctions.GetBase64FromFile(fileName));
                    myIdList.add("0");
                    serviceProofDocExtension = "png";
                    LoadImage();
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}