package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.PaymentMediatorActivity;
import com.android.inc.settlle.Activities.Service.ServiceRegistrationDetailsActivity;
import com.android.inc.settlle.Adapter.ServiceListAdapter;
import com.android.inc.settlle.DataModel.ServiceModel;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.DiscountModel;
import com.android.inc.settlle.RequestModel.UserIdModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomDialog;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.razorpay.Checkout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyServiceServiceFragment extends Fragment implements View.OnClickListener {

    Button btnPlanYearly, btnplanhalfYearly, btnAlreadySubscribed;
    Context context;
    View view;
    private ArrayList<ServiceModel> serviceList;
    private TextView txtSelectService;
    private TextView txtYearlyAmt;
    private TextView txtApplyCoupon;
    private Dialog dialog;
    private int totalAmt = 0, subscriptioncount = 0;


    private LinearLayout llServicebtn1, llServicebtn2, llServicebtn3;
    private ImageView imgService1, imgService2, imgService3;
    private TextView txtService1, txtService2, txtService3;

    private ArrayList<String> serviceNameList;
    private ArrayList<String> selectdServiceIdList, selectedServiceNameList, subscribedServiceIdList;
    private final static String TAG = MyServiceServiceFragment.class.getName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_service_service, container, false);
        context = getActivity();
        initialize();
        Checkout.preload(requireActivity());
        if (VU.isConnectingToInternet(context)) {
            getServiceList();
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActivityView();
        spaceBtn1Click();
    }

    public void initialize() {
        btnPlanYearly = view.findViewById(R.id.btn_select_plan);
        btnplanhalfYearly = view.findViewById(R.id.btn_plan_half_yearly);
        txtSelectService = view.findViewById(R.id.txt_select_service);
        txtYearlyAmt = view.findViewById(R.id.txt_yearly_amt);

        btnAlreadySubscribed = view.findViewById(R.id.btn_already_subscribed);
        txtApplyCoupon = view.findViewById(R.id.txt_apply_coupon);

        btnPlanYearly.setOnClickListener(this);
        btnplanhalfYearly.setOnClickListener(this);
        view.findViewById(R.id.lay1).setOnClickListener(this);
        btnAlreadySubscribed.setOnClickListener(this);
        txtApplyCoupon.setOnClickListener(this);

        selectdServiceIdList = new ArrayList<>();
        selectedServiceNameList = new ArrayList<>();
        subscribedServiceIdList = new ArrayList<>();

        serviceNameList = new ArrayList<>();


    }

    private void initializeActivityView() {
        llServicebtn1 = requireActivity().findViewById(R.id.service_button1);
        llServicebtn2 = requireActivity().findViewById(R.id.service_button2);
        llServicebtn3 = requireActivity().findViewById(R.id.service_button3);
        imgService1 = requireActivity().findViewById(R.id.img_service1);
        imgService2 = requireActivity().findViewById(R.id.img_service2);
        imgService3 = requireActivity().findViewById(R.id.img_service3);
        txtService1 = requireActivity().findViewById(R.id.txt_service1);
        txtService2 = requireActivity().findViewById(R.id.txt_service2);
        txtService3 = requireActivity().findViewById(R.id.txt_service3);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceBtn1Click() {
        imgService1.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtService1.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgService3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtService3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgService2.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtService2.setTextColor(getResources().getColorStateList(R.color.grey_60));

        llServicebtn2.setClickable(true);
        llServicebtn1.setClickable(false);
        llServicebtn3.setClickable(true);
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_plan_half_yearly:
                String strPlanId;   // fixed halfyearly id
                String strPlanType;
                String strAmt;
                break;

            case R.id.btn_select_plan:
                if (validation()) {
                    if (VU.isConnectingToInternet(context)) {
                        strPlanId = "1";   // fixed yearly id
                        strPlanType = "yearly";
                        strAmt = txtYearlyAmt.getText().toString().replace(",", "");
                        Utilities.setSPstring(context, Utilities.spPaymentFromActivity, "MyServiceServiceFragment");
                        Intent paymentIntent = new Intent(context, PaymentMediatorActivity.class);
                        paymentIntent.putExtra("strPlanId", strPlanId);
                        paymentIntent.putExtra("strPlanType", strPlanType);
                        paymentIntent.putExtra("strAmt", strAmt);
                        paymentIntent.putStringArrayListExtra("strIds", selectdServiceIdList);
                        paymentIntent.putStringArrayListExtra("selectedName", selectedServiceNameList);
                        startActivity(paymentIntent);
                        // checkPaymentDone();
                    }
                }
                break;

            case R.id.btn_already_subscribed:
                Intent i = new Intent(context, ServiceRegistrationDetailsActivity.class);
                startActivity(i);
                break;


            case R.id.lay1:
                //setServiceList();
                checkBoxListDialog();
                break;

            case R.id.txt_apply_coupon:
                if (Integer.parseInt(txtYearlyAmt.getText().toString().replace(",", "")) > 0)
                    showCouponCodeDialog();
                else
                    Toast.makeText(context, "First select venues", Toast.LENGTH_SHORT).show();
                break;


        }
    }


    public void showCouponCodeDialog() {
        final Dialog dialog = new Dialog(context, R.style.Theme_Design_BottomSheetDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_coupon_code);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        EditText edtDiscount = dialog.findViewById(R.id.edt_coupon_code);
        dialog.findViewById(R.id.btn_apply_coupon).setOnClickListener(v -> {
            if (!edtDiscount.getText().toString().isEmpty())
                getDiscount(edtDiscount);
            else
                Toast.makeText(context, "Enter coupon code", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    @SuppressLint("SetTextI18n")
    private void calculateDiscount(String discount) {
        // getting value from editPrice and parsing to double
        double price = Integer.parseInt(txtYearlyAmt.getText().toString().replace(",", ""));
        // same like that getting value from ePercent and parsing to double
        Log.e(TAG, "calculateDiscount: discount :" + discount);
        double ePer = Integer.parseInt(discount);
        // percent
        double per = (price / 100f) * ePer;
        double finalAmt = price - per;
        // txtYearlyAmt.setText("₹ " + finalAmt + "0");
        txtYearlyAmt.setText(CommonFunctions.formatNumber((long) finalAmt));
        txtApplyCoupon.setVisibility(View.GONE);
    }

    private void getDiscount(EditText editDiscount) {
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        dialog = ProgressDialog.show(getActivity(), "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getDiscount(new DiscountModel(editDiscount.getText().toString(), authToken, "Space", Utilities.getSPstringValue(context, Utilities.spUserId)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
                // dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String msg = Obj.getString("message");
                    int statusCode = Obj.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        String discount = Obj.getString("data");
                        Log.e(TAG, "onResponse: discount :" + discount);
                        calculateDiscount(discount);
                    } else {
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    private boolean validation() {
        Log.d(TAG, "validation: " + txtSelectService.getText().toString());
        if (txtSelectService.getText().toString().trim().isEmpty() || txtSelectService.getText().toString().equalsIgnoreCase("Select service")) {
            Toast.makeText(context, "Please select service", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    //Get service releted Data
    private void getServiceList() {
        dialog = ProgressDialog.show(getActivity(), "Please wait", "Loading...");
        serviceList = new ArrayList<>();
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServiceList(new UserIdModel(Utilities.getSPstringValue(context, Utilities.spUserId)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
                // dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObj = Obj.getJSONObject("data");
                        subscriptioncount = dataObj.getInt("subscount");

                        JSONArray ServiceArray = dataObj.getJSONArray("service");
                        Log.e(TAG, "onResponse: ServiceArray " + ServiceArray);
                        for (int i = 0; i < ServiceArray.length(); i++) {
                            String serviceId = ServiceArray.getJSONObject(i).getString("service_id");
                            String serviceName = ServiceArray.getJSONObject(i).getString("service_name");
                            String yearlyAmt = ServiceArray.getJSONObject(i).getString("amount");  // yearly amt
                            String halfyearlyAmt = ServiceArray.getJSONObject(i).getString("halfly");
                            serviceList.add(new ServiceModel(serviceId, serviceName, yearlyAmt, halfyearlyAmt, false));
                        }

                        for (int i = 0; i < serviceList.size(); i++) {
                            serviceNameList.add(serviceList.get(i).getServiceName() + "     ₹. " + serviceList.get(i).getServiceYearly());
                        }
                        if (subscriptioncount > 0) {
                            btnAlreadySubscribed.setVisibility(View.VISIBLE);

                            JSONArray sArray = dataObj.getJSONArray("subscription_id");
                            for (int i = 0; i < sArray.length(); i++) {
                                JSONObject sObject = sArray.getJSONObject(i);
                                subscribedServiceIdList.add(sObject.getString("service_id"));
                            }


                            Log.e(TAG, "onResponse: sID = " + subscribedServiceIdList.toString());
                        } else {
                            btnAlreadySubscribed.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((dialog != null) && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        dialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        dialog.dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        dialog.dismiss();
    }


    @SuppressLint("WrongConstant")
    private void checkBoxListDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.checkbox_list_layout, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final RecyclerView recyclerView = promptsView
                .findViewById(R.id.checkbox_list_recycler);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        ServiceListAdapter serviceListAdapter = new ServiceListAdapter(serviceList);
        recyclerView.setAdapter(serviceListAdapter);

        serviceListAdapter.setOnCheckBoxClickListener((view, position, dataArray) -> {
            CheckBox checkBox = (CheckBox) view;
            boolean isChecked = checkBox.isChecked();

            if (isChecked) {
                if (!checkValuePresentInArray(subscribedServiceIdList, Integer.parseInt(serviceList.get(position).getServiceId()))) {
                    selectdServiceIdList.add(serviceList.get(position).getServiceId());
                    selectedServiceNameList.add(serviceList.get(position).getServiceName());
                    totalAmt = totalAmt + Integer.parseInt(serviceList.get(position).getServiceYearly());
                    serviceList.get(position).setSelected(true);

                } else {
                    checkBox.setChecked(false);
                    // Toast.makeText(context, "Already subscribed", Toast.LENGTH_SHORT).show();

                    CustomDialog.alertDialog(context, "Already subscribed!");
                }


            } else {

                selectdServiceIdList.remove(serviceList.get(position).getServiceId());
                selectedServiceNameList.remove(serviceList.get(position).getServiceName());
                totalAmt = totalAmt - Integer.parseInt(serviceList.get(position).getServiceYearly());
                serviceList.get(position).setSelected(false);


            }

            Log.e(TAG, "onCheckBoxClickListner: Selected id" + selectdServiceIdList);
        });


        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        (dialog, id) -> {
                            onChangeSelectedReceivers();
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle("Select venue");
        alertDialog.setCancelable(false);

        // show it
        alertDialog.show();

    }

    @SuppressLint("SetTextI18n")
    protected void onChangeSelectedReceivers() {
        StringBuilder stringBuilder = new StringBuilder();
        //txtYearlyAmt.setText("₹ " + totalAmt);
        txtYearlyAmt.setText(CommonFunctions.formatNumber(totalAmt));
        if (selectedServiceNameList.size() > 0) {
            txtApplyCoupon.setVisibility(View.VISIBLE);
            if (selectedServiceNameList.size() < 3) {
                for (String service : selectedServiceNameList) {

                    if (selectedServiceNameList.size() > 1)
                        stringBuilder.append(service).append(",");
                    else
                        stringBuilder.append(service);
                    txtSelectService.setText(stringBuilder.toString());
                }
            } else {
                txtSelectService.setText(selectedServiceNameList.size() + " service selected");

            }
        } else {
            txtSelectService.setText("Select Service");
            txtApplyCoupon.setVisibility(View.GONE);
        }
    }


    //check value present or not in list
    private boolean checkValuePresentInArray(ArrayList<String> array, int toCheckValue) {
        String[] arr = array.toArray(new String[0]);
        boolean isAvailable = false;
        for (String element : arr) {
            if (element.equals("" + toCheckValue)) {
                isAvailable = true;
                break;
            }
        }

        return isAvailable;
    }


}
