package com.android.inc.settlle.Activities;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.android.inc.settlle.Adapter.AllImagesAdapter;
import com.android.inc.settlle.R;

import java.util.ArrayList;

public class ShowAllImagesActivity extends AppCompatActivity {

    ViewPager viewPager;
    AllImagesAdapter allImagesAdapter;
    ArrayList<String> imgUrls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_all_images);
        findViewById(R.id.back_space_all_imeges).setOnClickListener(v -> finish());
        imgUrls=getIntent().getStringArrayListExtra("imgUrls");
        setImages();
    }

    private  void setImages()
    {
        viewPager=findViewById(R.id.all_images_viewpager);
        allImagesAdapter=new AllImagesAdapter(this,imgUrls);
        viewPager.setAdapter(allImagesAdapter);
       // startAutoSliderUpper(allImagesAdapter.getCount());
    }
}
