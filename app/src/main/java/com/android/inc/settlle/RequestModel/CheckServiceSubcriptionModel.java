package com.android.inc.settlle.RequestModel;

public class CheckServiceSubcriptionModel {

    private String  user_id  ;
    private String  service_id  ;
    private String   auth_token ;


    public CheckServiceSubcriptionModel(String user_id, String service_id, String auth_token) {
        this.user_id = user_id;
        this.service_id = service_id;
        this.auth_token = auth_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
