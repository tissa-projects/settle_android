package com.android.inc.settlle.RequestModel;

import java.io.Serializable;
import java.util.ArrayList;

public class SpaceSubscriptionModel implements Serializable {
    public ArrayList<String> venue_id;
    public String venue_name;
    public String user_id;
    public String plan_id;
    public String plan_type;
    public String amount;
    public String auth_token;
    public String payment_id;

    public SpaceSubscriptionModel(ArrayList<String> venue_id, String venue_name, String user_id, String plan_id, String plan_type, String amount, String auth_token,String payment_id) {
        this.venue_id = venue_id;
        this.venue_name = venue_name;
        this.user_id = user_id;
        this.plan_id = plan_id;
        this.plan_type = plan_type;
        this.amount = amount;
        this.auth_token = auth_token;
        this.payment_id = payment_id;
    }
}
