package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class EditServiceImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private List<String> imgList, imageIdList;
    private RecyclerViewItemClickListener.ICrossBtnClickListeners onCLickListener;

    public EditServiceImageAdapter(Context context) {
        this.imgList = new ArrayList<>();
        this.imageIdList = new ArrayList<>();
        //     this.imageIdList = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_add_images, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, final int pos) {
        if (myViewHolder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) myViewHolder;
            if (imgList.get(pos).length() < 100) {
                String imgUrl = Utilities.IMG_SERVICE_URL + imgList.get(pos);
                Tools.displayImageOriginalString(view.imageView, imgUrl);
            } else {
                view.imageView.setImageBitmap(CommonFunctions.GetBitmap(imgList.get(pos)));
            }
            view.imgCross.setOnClickListener(v -> onCLickListener.onItemClick(pos, imgList, imageIdList));
        }
    }

    @Override
    public int getItemCount() {
        return imgList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView, imgCross;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_image1);
            imgCross = itemView.findViewById(R.id.cross1);
        }
    }

    public void setOnCLickListener(RecyclerViewItemClickListener.ICrossBtnClickListeners onCLickListener) {
        this.onCLickListener = onCLickListener;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setUpdatedListData(List<String> list, List<String> IdList) {
        this.imgList = list;
        this.imageIdList = IdList;
        notifyDataSetChanged();
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setListData(List<String> list, List<String> IdList) {
        for (int i = 0; i < list.size(); i++) {
            this.imgList.add(list.get(i));
            this.imageIdList.add(IdList.get(i));
        }
        notifyDataSetChanged();
    }

    public List<String> getImageIdList() {
        return imageIdList;
    }

    public List<String> getImageList() {
        return imgList;
    }
}