package com.android.inc.settlle.DataModel;

public class SpaceEventModel {

    private String event_id;
    private String event_name;
    private String event_price;
    private String space_event_id;

    public SpaceEventModel(String event_id, String event_name, String event_price, String space_event_id) {
        this.event_id = event_id;
        this.event_name = event_name;
        this.event_price = event_price;
        this.space_event_id = space_event_id;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_price() {
        return event_price;
    }

    public void setEvent_price(String event_price) {
        this.event_price = event_price;
    }

    public String getSpace_event_id() {
        return space_event_id;
    }

    public void setSpace_event_id(String space_event_id) {
        this.space_event_id = space_event_id;
    }
}
