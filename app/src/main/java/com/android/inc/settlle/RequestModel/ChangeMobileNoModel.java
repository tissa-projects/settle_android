package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class ChangeMobileNoModel implements Serializable {


    private String mobile_no;
    private String user_id;
    private String auth_token;

    public ChangeMobileNoModel(String mobile_no, String user_id, String auth_token) {
        this.mobile_no = mobile_no;
        this.user_id = user_id;
        this.auth_token = auth_token;
    }
}
