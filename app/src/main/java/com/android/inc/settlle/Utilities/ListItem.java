package com.android.inc.settlle.Utilities;


public class ListItem {

    private String searchItem;

    public  ListItem(){}
    public ListItem(String searchItem) {
        this.searchItem = searchItem;
    }

    public String getSearchItem() {
        return searchItem;
    }

    public void setSearchItem(String searchItem) {
        this.searchItem = searchItem;
    }

}
