package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.AddImageAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.SpaceMultipleImagesModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceAddImagesActivity extends AppCompatActivity implements View.OnClickListener {
    private Context context;

    private List<String> myList;
    private String insertId;
    private Dialog SpaceUploadImagesDialog;
    private AddImageAdapter myAdapter;
    // Options options;
    private static final String TAG = SpaceAddImagesActivity.class.getName();
    ActivityResultLauncher<Intent> activityResultLauncher;
    int selectionType = 0;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_space_images);
        context = SpaceAddImagesActivity.this;
        initialise();
        initOnStartActivity();
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Add Space Images");
        getIntentData();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            insertId = bundle.getString("insertId");
            CommonFunctions.showLog(TAG, "getIntentData: Space ID " + insertId);
        }
    }

    private void initialise() {
        RecyclerView addSpaceImageRecycler = findViewById(R.id.recycler);
        myList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        addSpaceImageRecycler.setLayoutManager(mLayoutManager);
        myAdapter = new AddImageAdapter();
        addSpaceImageRecycler.setAdapter(myAdapter);
        myAdapter.setOnCLickListener((v, position) -> {
            myList.remove(position);
            myAdapter.setListData(myList);
        });

        findViewById(R.id.btn_add_images).setOnClickListener(this);
        findViewById(R.id.btn).setOnClickListener(this);
        findViewById(R.id.fab).setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_images:
            case R.id.fab:
                try {
                    if (myList.size() < 5)
                        if (checkNRequestPermission())
                            imageTypeDialog();
                        else
                            Toast.makeText(context, "Please allow permission", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, "Space does not contain more than 5 images", Toast.LENGTH_SHORT).show();
                    // new GligarPicker().requestCode(PICKER_REQUEST_CODE).withActivity(SpaceAddImagesActivity.this).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn:
                if (myList.size() < 3) {
                    Toast.makeText(context, "Space must have at least 3 images", Toast.LENGTH_SHORT).show();
                } else if (myList
                        .size() > 5) {
                    Toast.makeText(context, "Space does not contain more than 5 images", Toast.LENGTH_SHORT).show();
                } else {
                    if (VU.isConnectingToInternet(context)) {
                        UploadImageArrayData();
                    }
                }
                break;
        }
    }

    //upload images
    private void UploadImageArrayData() {
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        SpaceUploadImagesDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().setSpaceMultipleImages(new SpaceMultipleImagesModel(myList, auth_token, userId, insertId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                SpaceUploadImagesDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String msg = Obj.getString("message");
                    int statusCode = Obj.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            statusAlert("Congratulations", "Space images added successfully", "Success", Obj);
                        } else {
                            statusAlert("Alert", msg, "error", Obj);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    statusAlert("Alert", getResources().getString(R.string.network_error), "error", new JSONObject());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                SpaceUploadImagesDialog.dismiss();
            }
        });
    }

    @SuppressLint("NotifyDataSetChanged")
    public void LoadImage() {
        myAdapter.setListData(myList);
        myAdapter.notifyDataSetChanged();
    }

    private static final int PERMISSION_REQUEST_CODE = 200;

    String imgselectionType, serviceProofDocExtension;
    boolean accessPermission = false;

    private boolean checkNRequestPermission() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : Utilities.appPermission) {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);
            accessPermission = false;
            return false;
        }
        accessPermission = true;
        return true;
    }

    private void imageTypeDialog() {
        try {
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(context);
            docTypeAlert.setTitle("Add Photo!");
            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];
                if (imgselectionType.equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    selectionType = Utilities.RESULT_CAMERA;
                    activityResultLauncher.launch(cameraIntent);
                } else if (imgselectionType.equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    selectionType = Utilities.RESULT_GALLERY;
                    activityResultLauncher.launch(intent);
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImageURI = data.getData();
                getDataForImage(selectedImageURI);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Utilities.RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
            serviceProofDocExtension = "png";
            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
            myList.add(strProfileImg);
            LoadImage();
        }
    }

    public void initOnStartActivity() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                getDataForImage(selectedImageURI);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Utilities.RESULT_CAMERA == selectionType) {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            serviceProofDocExtension = "png";
                            String strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                            myList.add(strProfileImg);
                            LoadImage();
                        }
                    }
                });
    }

    private void getDataForImage(Uri selectedImageURI) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory("SPACE_IMAGE.png", context);
            if (fileName != null) {
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    myList.add(CommonFunctions.GetBase64FromFile(fileName));
                    serviceProofDocExtension = "png";
                    LoadImage();
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void statusAlert(String title, String msg, String type, JSONObject Obj) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equals("Success")) {
                        try {
                            String insertId = Obj.getString("insert_id");
                            Intent intent = new Intent(context, SpaceKYCDocumentsActivity.class);
                            intent.putExtra("insertId", insertId);
                            startActivity(intent);
                        } catch (Exception ignore) {
                        }
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}