package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class BookingRequestDetailModel implements Serializable {

    private String auth_token;
    private String booking_id;

    public BookingRequestDetailModel(String auth_token, String booking_id) {
        this.auth_token = auth_token;
        this.booking_id = booking_id;
    }
}
