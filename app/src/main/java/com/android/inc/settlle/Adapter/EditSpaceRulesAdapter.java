package com.android.inc.settlle.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AmenitiesRulesModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditSpaceRulesAdapter extends RecyclerView.Adapter<EditSpaceRulesAdapter.MyViewHolder> {


    private final ArrayList<String> rulesIdlist;
    private final ArrayList<String> rulesNamelist;
    private final ArrayList<AmenitiesRulesModel> models = new ArrayList<>();
    Map<Integer, AmenitiesRulesModel> rulesModels = new HashMap<>();
    private static final String TAG = AddSpaceRulesAdapter.class.getSimpleName();


    public EditSpaceRulesAdapter(ArrayList<String> rulesNamelist, ArrayList<String> rulesIdlist, ArrayList<AmenitiesRulesModel> rulesList) {

        this.rulesIdlist = rulesIdlist;
        this.rulesNamelist = rulesNamelist;
        for (int i = 0; i < this.rulesIdlist.size(); i++) {
            models.add(new AmenitiesRulesModel(rulesList.get(i).getAmenitiesRulesName(),
                    rulesList.get(i).getAmenitiesRulesNameId(), rulesList.get(i).getSelected()));
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_add_space_amenities_rules, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myHolder, final int pos) {
        final MyViewHolder myViewHolder = (MyViewHolder) myHolder;
        myViewHolder.checkBoxEvent.setText(rulesNamelist.get(pos));


        if (rulesNamelist.size() != 0) {
            myViewHolder.checkBoxEvent.setChecked(models.get(pos).isSelected());
            myViewHolder.checkBoxEvent.setTag(models.get(pos));

            if (models.get(pos).isSelected()) {
                rulesModels.put(pos, new AmenitiesRulesModel(rulesIdlist.get(pos)));
            }
        }

        myViewHolder.checkBoxEvent.setOnClickListener(v -> {
            CheckBox cb = (CheckBox) v;
            AmenitiesRulesModel contact = (AmenitiesRulesModel) cb.getTag();
            models.get(pos).setPosition(pos);
            if (!contact.getSelected()) {
                Log.e(TAG, "onClick: " + "true");
                myViewHolder.checkBoxEvent.setChecked(true);
                contact.setSelected(cb.isChecked());
                models.get(pos).setSelected(cb.isChecked());
                rulesModels.put(pos, new AmenitiesRulesModel(rulesIdlist.get(pos)));
            } else {
                Log.e(TAG, "onClick: " + "false");
                myViewHolder.checkBoxEvent.setChecked(false);
                contact.setSelected(cb.isChecked());
                models.get(pos).setSelected(cb.isChecked());
                rulesModels.remove(pos);
            }
        });

    }

    @Override
    public int getItemCount() {
        return rulesNamelist.size();
    }

    public Map<Integer, AmenitiesRulesModel> getRuleList() {
        return rulesModels;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBoxEvent;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBoxEvent = itemView.findViewById(R.id.checkbox_rules_amenities);
        }
    }

    //Set method of OnItemClickListener object
    public void setOnItemClickListener() {
    }
}
