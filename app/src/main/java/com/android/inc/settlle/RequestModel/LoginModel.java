package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class LoginModel implements Serializable {
    public String email_id;
    public String password;

    public LoginModel(String email_id, String password) {
        this.email_id = email_id;
        this.password = password;
    }
}
