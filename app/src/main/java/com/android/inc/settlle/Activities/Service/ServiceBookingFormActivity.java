package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import com.android.inc.settlle.Utilities.CommonFunctions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AddtocartServiceModel;
import com.android.inc.settlle.RequestModel.CartServiceDetailUpdate;
import com.android.inc.settlle.RequestModel.CartViewDetailsModel;
import com.android.inc.settlle.RequestModel.GetServiceBookFormDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.DTUtil;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceBookingFormActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatButton btnSendBookingRqst, btnAddToCart;
    private EditText edtPkg, edtGuest, edtStartDate, edtEndDate, edtStartTime, edtEndTime;
    private TextView txtServicePrice;
    private TextView txtDiscountOffered;
    private TextView txtServiceTotalPrice;
    private TextView txtEstimatePrice;
    private TextView txtFinalPrice;
    private Context context;
    private Dialog loadingDialog;
    private String strUniqueId, Id, strGuestId, servicePkgId, strType,
            servicePrice, fromTime, toTime, discount, pkgName, strCartId;
    // private ArrayList<SpaceEventModel> servicePkgList;
    private ArrayList<String> pkgList;
    private ArrayList<String> pkgAlertList;
    private ArrayList<String> pkgIdList;
    private ArrayList<String> pkgPriceList;
    private int selectedPkg = -1;


    private String startDate, endDate, serviceId;
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private Date startdate, enddate;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private int timeType = 0;
    private String serviceName = "";

    private static final String TAG = ServiceBookingFormActivity.class.getSimpleName();

    @SuppressLint({"SetTextI18n", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_booking_form);
        context = ServiceBookingFormActivity.this;
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Book Your Service");

        getIntentData();
        initialize();

        if (VU.isConnectingToInternet(context)) {
            if (strType.equals("cart")) {
                getBookingDetails(strUniqueId, Id);  // if coming from viewCartActivity  : update booking details api
                btnSendBookingRqst.setText("Update Booking Request");
            } else {
                btnSendBookingRqst.setText("Send Booking Request");  //  if coming from ServiceDetailAndBookActivity
            }
        }

        if (Utilities.getSPbooleanValue(context, Utilities.spIsBookingType)) { // coming from space/ service  true and  scenario false
            btnAddToCart.setVisibility(View.GONE);
            btnSendBookingRqst.setVisibility(View.VISIBLE);
        } else {
            btnAddToCart.setVisibility(View.VISIBLE);
            btnSendBookingRqst.setVisibility(View.GONE);
        }

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            strUniqueId = bundle.getString("unique_id");
            Id = bundle.getString("id");
            strType = bundle.getString("type");
            Log.e(TAG, "getIntentData: strUniqueId: " + strUniqueId + " strType: " + strType + " Id: " + Id);
        }
        if (VU.isConnectingToInternet(context)) {
            getDetailsForBooking();
        }
    }


    private void initialize() {
        //  servicePkgList = new ArrayList<>();
        pkgList = new ArrayList<>();
        pkgAlertList = new ArrayList<>();
        pkgIdList = new ArrayList<>();
        pkgPriceList = new ArrayList<>();
        //pkgDescriptionList = new ArrayList<>();
        btnSendBookingRqst = findViewById(R.id.book_service_button);
        btnAddToCart = findViewById(R.id.add_to_cart_button);
        edtPkg = findViewById(R.id.edt_booking_pkg);
        edtGuest = findViewById(R.id.edt_booking_guest);
        edtStartDate = findViewById(R.id.edt_booking_start_date);
        edtEndDate = findViewById(R.id.edt_booking_end_date);
        edtStartTime = findViewById(R.id.edt_booking_start_time);
        edtEndTime = findViewById(R.id.edt_booking_end_time);

        txtServiceTotalPrice = findViewById(R.id.txt_total_service_price);
        txtServicePrice = findViewById(R.id.txt_service_price);
        txtDiscountOffered = findViewById(R.id.txt_discount);
        txtEstimatePrice = findViewById(R.id.txt_estimate_price);
        txtFinalPrice = findViewById(R.id.txt_fnal_price);

        View bottom_sheet = findViewById(R.id.bottom_sheet2);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        btnSendBookingRqst.setOnClickListener(this);
        edtStartDate.setOnClickListener(this);
        edtEndDate.setOnClickListener(this);
        edtPkg.setOnClickListener(this);
        edtStartTime.setOnClickListener(this);
        edtEndTime.setOnClickListener(this);
        btnAddToCart.setOnClickListener(this);
    }

    private void selectDate(final String dateType) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        try {
            if (dateType.equals("endDate")) {
                mYear = Integer.parseInt(startDate.split("/")[2].trim());
                mMonth = Integer.parseInt(startDate.split("/")[1].trim());
                mDay = Integer.parseInt(startDate.split("/")[0].trim());
            }
        } catch (Exception ignore) {
        }

        @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year, monthOfYear, dayOfMonth) -> {
                    try {
                        if (dateType.equalsIgnoreCase("startDate")) {
                            edtStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            startDate = dayOfMonth + "/" + monthOfYear + "/" + year;
                            startdate = sdf.parse(startDate);
                            edtEndDate.setText("");
                            edtEndTime.setText("");
                            edtStartTime.setText("");
                        } else {
                            edtEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            endDate = dayOfMonth + "/" + monthOfYear + "/" + year;
                            enddate = sdf.parse(endDate);
                            showPricingDetails();
                            edtEndTime.setText("");
                        }
                    } catch (Exception e) {
                        CommonFunctions.showLog(TAG, "onDateSet: catch : " + e.getMessage());
                    }

                }, mYear, mMonth, mDay);
        if (dateType.equalsIgnoreCase("startDate")) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.set(mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            calendar.add(Calendar.DATE, Utilities.MAX_ALLOWED_BOOKING_DAY);
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        }
        datePickerDialog.show();

    }

    @SuppressLint("SetTextI18n")
    private void showPricingDetails() {
        long diff = enddate.getTime() - startdate.getTime();
        long daysBetweenDates = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        daysBetweenDates++;
        CommonFunctions.showLog(TAG, "onDateSet: days: " + daysBetweenDates);
        txtServicePrice.setText(" (" + daysBetweenDates + " * " + servicePrice + ") ");
        txtServiceTotalPrice.setText(String.valueOf((daysBetweenDates) * Long.parseLong(servicePrice)));

        long fPricee = (daysBetweenDates) * Long.parseLong(servicePrice);
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(fPricee);

        Log.d(TAG, "showPricingDetails: " + yourFormattedString);


        // txtEstimatePrice.setText(txtServiceTotalPrice.getText().toString());

        txtEstimatePrice.setText(CommonFunctions.formatNumber((daysBetweenDates) * Long.parseLong(servicePrice)));

        strikeThroughText(txtEstimatePrice);
        calculateDiscount(txtServiceTotalPrice);

    }

    private void strikeThroughText(TextView price) {
        price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    private void gotoConformBookingActivity() {
        Intent intent = new Intent(context, ConfirmServiceBookingActivity.class);
        intent.putExtra("serviceId", Id);
        intent.putExtra("pkgId", servicePkgId);
        intent.putExtra("guestId", strGuestId);
        intent.putExtra("uniqueId", strUniqueId);
        intent.putExtra("startDate", edtStartDate.getText().toString());
        intent.putExtra("endDate", edtEndDate.getText().toString());
        intent.putExtra("startTime", edtStartTime.getText().toString());
        intent.putExtra("endTime", edtEndTime.getText().toString());
        intent.putExtra("finalPrice", txtFinalPrice.getText().toString().replace(",", ""));
        intent.putExtra("discount", discount);
        intent.putExtra("servicePrice", txtServiceTotalPrice.getText().toString());
        intent.putExtra("estimatePrice", txtEstimatePrice.getText().toString().replace(",", ""));
        intent.putExtra("pkgName", pkgName);
        intent.putExtra("serviceName", serviceName);
        startActivity(intent);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.book_service_button:
                if (VU.isConnectingToInternet(context)) {
                    if (validate()) {
                        if (validate()) {
                            if (strType.equals("cart")) {
                                cartAlert("Are you sure want to update cart space detail");
                            } else {
                                gotoConformBookingActivity();
                            }
                        }
                    }
                }
                break;

            case R.id.add_to_cart_button:
                if (VU.isConnectingToInternet(context)) {
                    if (validate())
                        cartAlert("Are you sure want to add into the cart");
                }
                break;

            case R.id.edt_booking_start_date:
                if (!edtPkg.getText().toString().isEmpty()) {
                    selectDate("startDate");
                } else {
                    Toast.makeText(context, "Select package first", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.edt_booking_end_date:
                if (!edtStartDate.getText().toString().isEmpty()) {
                    selectDate("endDate");
                } else {
                    Toast.makeText(context, "Select start date first", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.edt_booking_pkg:
                pkgAlert();
                break;

            case R.id.edt_booking_start_time:
                if (!edtStartDate.getText().toString().isEmpty()) {
                    timeType = 1;
                    DTUtil.newTimeArray.clear();
                    ArrayList<String> startTimeArrsy = DTUtil.getTiming(edtStartDate.getText().toString(), fromTime, toTime, context, timeType);
                    if (startTimeArrsy.size() > 1)
                        showBottomSheetDialog(CommonFunctions.GetStringArray(startTimeArrsy));
                    else
                        Toast.makeText(context, "Booking is closed for the day (" + edtStartDate.getText().toString() + ")", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Select start date first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.edt_booking_end_time:
                if (!edtEndDate.getText().toString().isEmpty()) {
                    timeType = 2;
                    DTUtil.newTimeArray.clear();
                    String startTimeTemp = fromTime;
                    if (DTUtil.compareTwoDate(edtStartDate.getText().toString().trim(), edtEndDate.getText().toString().trim(), "=="))
                        startTimeTemp = edtStartTime.getText().toString().trim();
                    ArrayList<String> endTimeArrsy = DTUtil.getTiming(edtEndDate.getText().toString(), startTimeTemp, toTime, context, timeType);
                    showBottomSheetDialog(CommonFunctions.GetStringArray(endTimeArrsy));
                } else {
                    Toast.makeText(context, "Select end date first", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @SuppressLint("SetTextI18n")
    private void calculateDiscount(TextView amt) {
        // getting value from editPrice and parsing to double
        double price = Integer.parseInt(amt.getText().toString());
        // same like that getting value from ePercent and parsing to double
        double ePer = Integer.parseInt(discount);
        // percent
        double per = (price / 100f) * ePer;
        double finalAmt = price - per;
        //  txtFinalPrice.setText(finalAmt + "0");
        txtFinalPrice.setText(CommonFunctions.formatNumber((long) finalAmt));
    }

    @SuppressLint("SetTextI18n")
    private void pkgAlert() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose package");

        // add a radio button list
        String[] pkgs = pkgAlertList.toArray(new String[0]);
        builder.setSingleChoiceItems(pkgs, selectedPkg, (dialog, index) -> {
            selectedPkg = index;
            pkgName = pkgList.get(index);

            servicePkgId = pkgIdList.get(index);
            servicePrice = pkgPriceList.get(index);
            edtPkg.setText("" + pkgList.get(index));
            endDate = "";
            startDate = "";
            txtFinalPrice.setText("00");
            txtServiceTotalPrice.setText("00");
            txtEstimatePrice.setText("00");
            txtServicePrice.setText("");
            edtEndDate.setText(endDate);
            edtStartDate.setText(startDate);
            edtEndTime.setText("");
            edtStartTime.setText("");
            dialog.dismiss();

        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void getDetailsForBooking() {
        final String[] guestList = getResources().getStringArray(R.array.GuestList);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getServiceBookingFormDetail(new GetServiceBookFormDetailModel(strUniqueId));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    loadingDialog.dismiss();
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);

                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        Log.d(TAG, "onResponse: getDetailsForBooking " + dataObject);
                        JSONArray pkgArray = dataObject.getJSONArray("service_package");
                        if (pkgArray.length() > 0) {
                            for (int i = 0; i < pkgArray.length(); i++) {
                                JSONObject jsArray = pkgArray.getJSONObject(i);
                                pkgAlertList.add(jsArray.getString("package_name") + " [INR " + jsArray.getString("package_price") + "/day]");
                                pkgList.add(jsArray.getString("package_name"));
                                pkgIdList.add(jsArray.getString("package_id"));
                                pkgPriceList.add(jsArray.getString("package_price"));
                                // pkgDescriptionList.add(jsArray.getString("package_description"));
                            }
                        } else {
                            Toast.makeText(ServiceBookingFormActivity.this, "Package is not available", Toast.LENGTH_SHORT).show();
                        }
                        Log.e(TAG, "onResponse: pkglist" + pkgList.toString());
                        JSONObject detailObject = dataObject.getJSONObject("service_details");
                        strGuestId = detailObject.getString("guest");
                        fromTime = detailObject.getString("from_time");
                        toTime = detailObject.getString("to_time");
                        discount = detailObject.getString("discount");
                        txtDiscountOffered.setText(discount + " %");
                        serviceName = dataObject.getJSONArray("service").getJSONObject(0).getString("service_name");
                        int guestid = Integer.parseInt(strGuestId);
                        edtGuest.setText(guestList[guestid - 1]);

                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch" + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
            }
        });
    }

    //Bottom shhet code
    @SuppressLint("SetTextI18n")
    private void showBottomSheetDialog(final String[] timeArray) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_list, null);

        ListView bottom_listview = view.findViewById(R.id.bottom_listview);
        TextView txtheading = view.findViewById(R.id.bottom_heading);
        if (timeType == 1) {
            txtheading.setText("Select Start Time");

        } else if (timeType == 2) {
            txtheading.setText("Select End Time");

        }

        List<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        list.addAll(Arrays.asList(timeArray));

        bottom_listview.setAdapter(adapter);
        bottom_listview.setOnItemClickListener((parent, view1, position, id) -> {

            if (timeType == 1) {
                edtStartTime.setText("" + timeArray[position]);
                edtEndTime.setText("");
            } else if (timeType == 2) {
                edtEndTime.setText("" + timeArray[position]);
            }
            mBottomSheetDialog.cancel();
            DTUtil.newTimeArray.clear();
        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    private void cartAlert(String title) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        // builder1.setMessage(title);
        builder1.setTitle(title);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> {
                    if (VU.isConnectingToInternet(context)) {
                        if (strType.equals("cart")) {
                            updateCart();
                        } else {
                            addToCart();

                        }
                    }


                });

        builder1.setNegativeButton(
                "No",
                (dialog, id) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private void addToCart() {
        String struserId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String strAuthToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().AddToCartService(new AddtocartServiceModel(strUniqueId, Id,
                struserId, servicePkgId, strGuestId, startDate, endDate, edtStartTime.getText().toString(), edtEndTime.getText().toString(),
                txtFinalPrice.getText().toString().replace(",", ""), strAuthToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        //handle success logic here
                        //Toast.makeText(context, "Added to cart successfully", Toast.LENGTH_SHORT).show();
                        statusAlert("Information", "Item successfully added in your cart", "Success");
                        /*Intent i = new Intent(context, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();*/
                    } else {
                        //handle failed logic here
                        statusAlert("Alert", msg, "Error");
                        //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    Log.e(TAG, "onResponse: catch " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void updateCart() {
        String struserId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String strAuthToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");


        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateToCartServiceDetail(new CartServiceDetailUpdate(Id, strUniqueId, serviceId,
                struserId, servicePkgId, strGuestId, startDate, endDate, edtStartTime.getText().toString(), edtEndTime.getText().toString(),
                txtFinalPrice.getText().toString().replace(",", ""), strAuthToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = Objects.requireNonNull(response.body()).string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        //handle success logic here
                        statusAlert("Information", "Cart service booking detail updated successfully", "Success");
                        // Toast.makeText(context, "Cart service booking detail updated successfully", Toast.LENGTH_SHORT).show();
                        // finish();
                    } else {
                        //handle failed logic here
                        statusAlert("Alert", msg, "Error");
                        // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    //  Log.e(TAG, "onResponse: catch " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();
                Toast.makeText(context, getResources().getString(R.string.onFailErrorMsg), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private boolean validate() {
        if (VU.isEmpty(edtPkg)) {
            Toast.makeText(context, "Choose your package", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtStartDate)) {
            Toast.makeText(context, "Select start date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtEndDate)) {
            Toast.makeText(context, "Select end date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtStartTime)) {
            Toast.makeText(context, "Select start time", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtEndTime)) {
            Toast.makeText(context, "Select end time", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void getBookingDetails(String unique_id, String service_cart_id) {
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().cartViewDetails(new CartViewDetailsModel(unique_id, service_cart_id, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("servicelist");
                        JSONObject serviceDataObject = dataArray.getJSONObject(0);
                        strCartId = serviceDataObject.getString("service_cart_id");
                        servicePkgId = serviceDataObject.getString("package_id");
                        strGuestId = serviceDataObject.getString("guest_id");
                        startDate = serviceDataObject.getString("startdate");
                        endDate = serviceDataObject.getString("enddate");
                        fromTime = serviceDataObject.getString("start_time");
                        toTime = serviceDataObject.getString("end_time");
                        discount = serviceDataObject.getString("discount");
                        pkgName = serviceDataObject.getString("package_name");
                        servicePrice = serviceDataObject.getString("package_price");
                        serviceId = serviceDataObject.getString("service_id");


                        Log.e(TAG, "onResponse: strCartId: " + strCartId + " servicePkgId: " + servicePkgId + " strGuestId: " + strGuestId +
                                " startDate: " + startDate + " endDate: " + endDate + " fromTime: " + fromTime +
                                " toTime: " + toTime + " discount: " + discount + " pkgName: " + pkgName + " servicePkgPrice: " + servicePrice);
                        edtPkg.setText(pkgName);
                        startdate = sdf.parse(startDate);
                        enddate = sdf.parse(endDate);
                        edtStartDate.setText(startDate);
                        edtEndDate.setText(endDate);
                        edtStartTime.setText(fromTime);
                        edtEndTime.setText(toTime);
                        showPricingDetails();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
