package com.android.inc.settlle.Retrofit;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.inc.settlle.Utilities.Utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AddCookiesInterceptor implements Interceptor {

    public static final String PREF_COOKIES = "PREF_COOKIES";
    private Context context;
    private static final String TAG = AddCookiesInterceptor.class.getSimpleName();

    public AddCookiesInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        HashSet<String> preferences = (HashSet<String>) PreferenceManager.getDefaultSharedPreferences(context).getStringSet(PREF_COOKIES, new HashSet<String>());
        List<String> nameList = new ArrayList<String>(preferences);

        String cookie = "";
        cookie = Utilities.getSPstringValue(context, Utilities.cookie);
        Request original = chain.request();

       /* if (list != null) {
            for (String cookie : preferences) {
                jsessionid = (cookie.split(";"))[0];
                Log.e(TAG, "intercept: jsessionid "+jsessionid );
                String jsessionid1 = jsessionid.split("=")[0];
                Log.e(TAG, "intercept:jsessionid1 "+jsessionid1   );
                if (jsessionid1.equals("sessionid")|| jsessionid1.equals("django_language"))
                    Log.e(TAG, "intercept: jsessionid if : "+jsessionid );
                    builder.addHeader("Cookie", jsessionid);

            }
        }*/
        if(original.url().toString().contains("shop/auth/login")) {
            Log.e(TAG, "intercept: " + cookie);
            builder.addHeader("Cookie", cookie);
        }
        return chain.proceed(builder.build());
    }
}
