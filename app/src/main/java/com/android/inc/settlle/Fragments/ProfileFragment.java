package com.android.inc.settlle.Fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.ChangeMobileNoActivity;
import com.android.inc.settlle.Activities.ChangePasswordActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.ProfileDataModel;
import com.android.inc.settlle.RequestModel.ProfileImageModel;
import com.android.inc.settlle.RequestModel.UpdateProfileModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.IU;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private View view;
    private ImageView imageProfile;
    private EditText edtFirstName, edtLastName, edtEmail, edtMobileNo, edtAddress, edtOccupation, edtAboutMe;
    private LinearLayout llProgress;
    AppCompatButton saveButton;
    private String strProfileImg = "", strImgName = "";
    private Dialog lodingDialog;
    Bitmap profileBitmap;
    boolean isUpdated = false;
    public static Bitmap bitmap;
    private ImageView imgAccount1, imgAccount2, imgAccount3, imgAccount4;
    private TextView txtAccount1, txtAccount2, txtAccount3, txtAccount4;
    private LinearLayout llAccountbtn1, llAccountbtn2, llAccountbtn3, llAccountbtn4;
    private static final String TAG = ProfileFragment.class.getSimpleName();
    ActivityResultLauncher<Intent> someActivityResultLauncher;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profiles, container, false);

        context = getActivity();
        checkNRequestPermission();
        initialize();
        initOnStartActivity();

        if (VU.isConnectingToInternet(context)) {
            getProfileData();
        }


        return view;
    }

    private void initialize() {

        ImageView menuPopup = requireActivity().findViewById(R.id.account_button_edit);
        menuPopup.setVisibility(View.VISIBLE);
        saveButton = view.findViewById(R.id.profile_editButton);
        saveButton.setVisibility(View.GONE);
        imageProfile = view.findViewById(R.id.account_profile_pic);
        ImageView picImage = view.findViewById(R.id.picProfile);

        edtFirstName = view.findViewById(R.id.edt_profile_first_name);
        edtLastName = view.findViewById(R.id.edt_profile_last_name);
        edtMobileNo = view.findViewById(R.id.edt_profile_phone_no);
        edtAddress = view.findViewById(R.id.edt_profile_address);
        edtOccupation = view.findViewById(R.id.edt_profile_occupation);
        edtAboutMe = view.findViewById(R.id.edt_profile_about_me);
        llProgress = view.findViewById(R.id.lyt_progress);
        edtEmail = view.findViewById(R.id.edt_profile_email);

        imageProfile.setOnClickListener(this);
        menuPopup.setOnClickListener(this);
        picImage.setOnClickListener(this);
        saveButton.setOnClickListener(this);

        setTextFieldDisabled();
    }

    @Override
    public void onResume() {
        super.onResume();
        edtMobileNo.setText(Utilities.getSPstringValue(context, Utilities.spMobileNo));
        initializeActivityView();
    }

    private void initializeActivityView() {

        imgAccount1 = requireActivity().findViewById(R.id.img_account1);
        imgAccount2 = requireActivity().findViewById(R.id.img_account2);
        imgAccount3 = requireActivity().findViewById(R.id.img_account3);
        imgAccount4 = requireActivity().findViewById(R.id.img_account4);

        txtAccount1 = requireActivity().findViewById(R.id.txt_account1);
        txtAccount2 = requireActivity().findViewById(R.id.txt_account2);
        txtAccount3 = requireActivity().findViewById(R.id.txt_account3);
        txtAccount4 = requireActivity().findViewById(R.id.txt_account4);

        llAccountbtn1 = requireActivity().findViewById(R.id.account_button1);
        llAccountbtn2 = requireActivity().findViewById(R.id.account_button2);
        llAccountbtn3 = requireActivity().findViewById(R.id.account_button3);
        llAccountbtn4 = requireActivity().findViewById(R.id.account_button4);

        accountBtn1Click();
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void accountBtn1Click() {
        imgAccount1.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtAccount1.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgAccount3.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount3.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount2.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount2.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgAccount4.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtAccount4.setTextColor(getResources().getColorStateList(R.color.grey_60));
        llAccountbtn2.setClickable(true);
        llAccountbtn1.setClickable(false);
        llAccountbtn3.setClickable(true);
        llAccountbtn4.setClickable(true);

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.account_profile_pic:
                showDiag();
                break;

            case R.id.profile_editButton:
                if (validate()) {
                    if (VU.isConnectingToInternet(context)) {
                        updateProfile();
                    }
                }
                break;

            case R.id.account_button_edit:
                popUpMenu();
                break;

            case R.id.picProfile:
                try {
                    if (accessPermission)
                        imageTypeDialog();
                    else
                        checkNRequestPermission();
                    //new GligarPicker().requestCode(PICKER_REQUEST_CODE).withActivity((Activity) context).show();
                } catch (Exception e) {
                    Log.d(TAG, "onClick: " + e);
                    e.printStackTrace();
                }
                break;
        }
    }

    private void popUpMenu() {
        PopupMenu popup = new PopupMenu(context, requireActivity().findViewById(R.id.account_button_edit));
        popup.getMenu().add("Edit profile");
        Log.e(TAG, "popUpMenu: " + Utilities.getSPstringValue(context, Utilities.spLoginType));
        if (Utilities.getSPstringValue(context, Utilities.spLoginType).equals("settleLogin")) {
            popup.getMenu().add("Change password");
        }
        popup.getMenu().add("Change mobile number");

        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            menuItemSelection((String) item.getTitle());
            return true;
        });
        popup.show();
    }

    private void menuItemSelection(String title) {
        int id = 0;
        switch (title) {
            case "Edit profile":
                id = 1;
                break;
            case "Change password":
                id = 2;
                break;
            case "Change mobile number":
                id = 3;
                break;
        }

        switch (id) {
            case 1:  // edit profile
                setTextFieldEnabled();
                break;

            case 2:  // change password
                // Toast.makeText(context, "password Clicked", Toast.LENGTH_SHORT).show();
                Intent intentPassword = new Intent(context, ChangePasswordActivity.class);
                startActivity(intentPassword);

                break;

            case 3:  //change mobile no
                Intent intentMobile = new Intent(context, ChangeMobileNoActivity.class);
                startActivity(intentMobile);

                break;
        }
    }

    private void setTextFieldEnabled() {
        // imageProfile.setEnabled(true);
        saveButton.setVisibility(View.VISIBLE);
        edtEmail.setSaveEnabled(false);
        edtFirstName.setEnabled(true);
        edtFirstName.requestFocus();

        edtFirstName.setFocusableInTouchMode(true);

        edtLastName.setEnabled(true);
        edtMobileNo.setEnabled(false);
        edtAddress.setEnabled(true);
        edtOccupation.setEnabled(true);
        edtAboutMe.setEnabled(true);
        Toast.makeText(context, "Edit profile", Toast.LENGTH_SHORT).show();

    }

    private void setTextFieldDisabled() {
        saveButton.setVisibility(View.GONE);
        edtEmail.setSaveEnabled(false);
        edtFirstName.setEnabled(false);
        edtLastName.setEnabled(false);
        edtMobileNo.setEnabled(false);
        edtAddress.setEnabled(false);
        edtOccupation.setEnabled(false);
        edtAboutMe.setEnabled(false);
    }

    //set profile image APi
    private void setProfileImage() {
        isUpdated = false;

        llProgress.setVisibility(View.VISIBLE);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().setProfileImage(new ProfileImageModel(strProfileImg, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                llProgress.setVisibility(View.GONE);
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onProfileResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            imageProfile.setImageBitmap(CommonFunctions.GetBitmap(strProfileImg));
                            profileBitmap = CommonFunctions.GetBitmap(strProfileImg);
                            isUpdated = true;
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                llProgress.setVisibility(View.GONE);
            }
        });
    }

    //set profile Data APi
    private void getProfileData() {
        lodingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getProfileData(new ProfileDataModel(Utilities.getSPstringValue(context, Utilities.spUserId), Utilities.getSPstringValue(context, Utilities.spAuthToken)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                try {
                    if ((lodingDialog != null) && lodingDialog.isShowing()) {
                        lodingDialog.dismiss();
                    }
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            JSONObject dataObj = jsonObject.getJSONObject("data");
                            strImgName = dataObj.getString("profile_image");
                            String strMainImgUrl = Utilities.IMG_PROFILE_URL + strImgName;
                            if (strImgName.equalsIgnoreCase("profile_pic.png")) {
                                Log.e(TAG, "onResponse: " + strImgName);
                                Tools.displayImageOriginal(getActivity(), imageProfile, R.drawable.ic_user1);

                            } else {
                                Tools.displaySmallImageOriginalString(imageProfile, strMainImgUrl);

                            }
                            edtFirstName.setText(dataObj.getString("fname"));
                            edtLastName.setText(dataObj.getString("lname"));
                            edtMobileNo.setText(dataObj.getString("mobileno"));
                            edtAddress.setText(dataObj.getString("address"));
                            edtEmail.setText(dataObj.getString("email_id"));
                            edtOccupation.setText(dataObj.getString("job_title"));  //occupation
                            edtAboutMe.setText(dataObj.getString("about_them"));
                        } else {
                            Toast.makeText(getActivity(), "Server Error!!!...Please try again", Toast.LENGTH_SHORT).show();

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if ((lodingDialog != null) && lodingDialog.isShowing()) {
                        lodingDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((lodingDialog != null) && lodingDialog.isShowing()) {
                    lodingDialog.dismiss();
                }
            }
        });
    }

    public boolean validate() {
        if (VU.isEmpty(edtFirstName)) {
            Toast.makeText(context, "Enter first name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtLastName)) {
            Toast.makeText(context, "Enter last name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtMobileNo)) {
            Toast.makeText(context, "Enter mobile number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtMobileNo.getText().toString().trim().length() < 9) {
            Toast.makeText(context, "Enter valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtAddress)) {
            Toast.makeText(context, "Enter address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtOccupation)) {
            Toast.makeText(context, "Enter occupation", Toast.LENGTH_SHORT).show();
            return false;
        } else if (VU.isEmpty(edtAboutMe)) {
            Toast.makeText(context, "Enter about me", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void updateProfile() {

        lodingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        String fname = edtFirstName.getText().toString().trim();
        String lname = edtLastName.getText().toString().trim();
        String mobileno = edtMobileNo.getText().toString().trim();
        String about_me = edtAboutMe.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String occupation = edtOccupation.getText().toString().trim();
        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String user_id = Utilities.getSPstringValue(context, Utilities.spUserId);


        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateProfileData(new UpdateProfileModel(fname, lname, mobileno, about_me, address,
                occupation, auth_token, user_id));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((lodingDialog != null) && lodingDialog.isShowing()) {
                    lodingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            setTextFieldDisabled();
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((lodingDialog != null) && lodingDialog.isShowing()) {
                    lodingDialog.dismiss();
                }
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lodingDialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        lodingDialog.dismiss();
    }

    @Override
    public void onStop() {
        super.onStop();
        if ((lodingDialog != null) && lodingDialog.isShowing()) {
            lodingDialog.dismiss();
        }
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    private void showDiag() {

        final View dialogView = View.inflate(context, R.layout.dialog, null);

        final Dialog dialog = new Dialog(context, R.style.MaterialDialogSheet);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        ImageView imageView = dialog.findViewById(R.id.closeDialogImg);
        ImageView profileImg = dialog.findViewById(R.id.profileImg);

        Log.e(TAG, "showDiag: " + strImgName);
        if (strImgName.equalsIgnoreCase("")) {
            if (strProfileImg == null) {
                profileImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_user1));
            } else {
                profileImg.setImageBitmap(CommonFunctions.GetBitmap(strProfileImg));
            }
        } else {
            String strMainImgUrl = Utilities.IMG_PROFILE_URL + strImgName;
            if (strImgName.equalsIgnoreCase("profile_pic.png")) {
                Log.e(TAG, "onResponse: " + strImgName);
                Tools.displayImageOriginal(getActivity(), profileImg, R.drawable.ic_user1);

            } else {
                if (isUpdated) {
                    profileImg.setImageBitmap(CommonFunctions.GetBitmap(strProfileImg));

                } else {
                    Tools.displaySmallImageOriginalString(profileImg, strMainImgUrl);
                }
            }
        }
        imageView.setOnClickListener(v -> revealShow(dialogView, false, dialog));

        dialog.setOnShowListener(dialogInterface -> revealShow(dialogView, true, null));

        dialog.setOnKeyListener((dialogInterface, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_BACK) {

                revealShow(dialogView, false, dialog);
                return true;
            }

            return false;
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    private void revealShow(View dialogView, boolean b, final Dialog dialog) {

        final View view = dialogView.findViewById(R.id.dialog);

        int w = view.getWidth();
        int h = view.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        // int cx = (int) (fab.getX() + (fab.getWidth()/2));
        // int cy = (int) (fab.getY())+ fab.getHeight() + 56;
        int cx = (int) (imageProfile.getX() + (imageProfile.getWidth() / 2));
        int cy = imageProfile.getBottom();


        if (b) {
            Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, endRadius);

            view.setVisibility(View.VISIBLE);
            revealAnimator.setDuration(700);
            revealAnimator.start();

        } else {

            Animator anim =
                    ViewAnimationUtils.createCircularReveal(view, cx, cy, endRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    dialog.dismiss();
                    view.setVisibility(View.INVISIBLE);

                }
            });
            anim.setDuration(700);
            anim.start();

        }

    }

    private static final int PERMISSION_REQUEST_CODE = 200;

    String imgselectionType, serviceProofDocExtension;
    boolean accessPermission = false;
    int selectionType = 0;

    private void checkNRequestPermission() {
        //check which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String perm : Utilities.appPermission) {
            if (ContextCompat.checkSelfPermission(context, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);
            accessPermission = false;
            return;
        }
        accessPermission = true;
    }

    private void imageTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(context);
            docTypeAlert.setTitle("Add Photo!");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];
                //handle logic here
                switch (imgselectionType) {
                    case "Take Photo":
                        selectionType = Utilities.RESULT_CAMERA;
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        if (someActivityResultLauncher != null)
                            someActivityResultLauncher.launch(cameraIntent);
                        // startActivityForResult(cameraIntent, Utilities.RESULT_CAMERA);
                        break;
                    case "Choose from Gallery":
                        selectionType = Utilities.RESULT_GALLERY;
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        String[] mimeTypes = {"image/jpeg", "image/png"};
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        //  startActivityForResult(Intent.createChooser(intent, "Select Picture"), Utilities.RESULT_GALLERY);
                        if (someActivityResultLauncher != null)
                            someActivityResultLauncher.launch(intent);
                        break;
                    case "Cancel":
                        break;
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

    public void initOnStartActivity() {
        someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                strProfileImg = IU.getPath(context, selectedImageURI);
                                assert strProfileImg != null;
                                File imgFile = new File(strProfileImg);
                                setImage(imgFile);
                                if (!strProfileImg.equalsIgnoreCase("")) {
                                    if (VU.isConnectingToInternet(context)) {
                                        setProfileImage();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            serviceProofDocExtension = "png";
                            strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                            imageProfile.setImageBitmap(CommonFunctions.GetBitmap(strProfileImg));
                            if (VU.isConnectingToInternet(context)) {
                                setProfileImage();
                            }
                        }
                    }
                });
    }

/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        CommonFunctions.showLog("onActivityResult ", "onActivityResult Called");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utilities.RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
            try {
                Uri selectedImageURI = data.getData();
                strProfileImg = IU.getPath(context, selectedImageURI);
                assert strProfileImg != null;
                File imgFile = new File(strProfileImg);
                setImage(imgFile);
                if (!strProfileImg.equalsIgnoreCase("")) {
                    if (VU.isConnectingToInternet(context)) {
                        setProfileImage();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Utilities.RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
            serviceProofDocExtension = "png";
            strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
            imageProfile.setImageBitmap(CommonFunctions.GetBitmap(strProfileImg));
            if (VU.isConnectingToInternet(context)) {
                setProfileImage();
            }
        }
    }*/

    private void setImage(File imgFile) {
        long imgLengthInKB = (imgFile.length() / 1024);
        Log.e(TAG, "setImage: fileLength " + imgLengthInKB + "kb");
        if (imgFile.exists() && imgLengthInKB > 200) {
            Bitmap bitmap1 = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            serviceProofDocExtension = "png";
            strProfileImg = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap1);
            imageProfile.setImageBitmap(CommonFunctions.GetBitmap(strProfileImg));
        } else {
            Toast.makeText(context, "Selected image should not be less than 200KB", Toast.LENGTH_SHORT).show();
        }
    }


}
