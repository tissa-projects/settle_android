package com.android.inc.settlle.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.EventModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddSpaceEventAdapter extends RecyclerView.Adapter<AddSpaceEventAdapter.MyViewHolder> {

    private final ArrayList<String> eventsNamelist;
    private final ArrayList<String> eventsIdlist;
    private final ArrayList<EventModel> models = new ArrayList<>();
    Map<Integer, EventModel> eventModels = new HashMap<>();
    private static final String TAG = AddSpaceEventAdapter.class.getSimpleName();
    private EventModel contact = null;

    public AddSpaceEventAdapter(ArrayList<String> eventsNamelist, ArrayList<String> eventsIdlist) {
        this.eventsNamelist = eventsNamelist;
        this.eventsIdlist = eventsIdlist;
        for (int i = 0; i < eventsNamelist.size(); i++) {
            models.add(new EventModel(eventsIdlist.get(i), ""));
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_add_space_event_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myHolder, final int pos) {

        // myHolder.setIsRecyclable(false);
        final MyViewHolder myViewHolder = myHolder;
        final EventModel eventModel = new EventModel();
        myViewHolder.eventCheckBox.setText(eventsNamelist.get(pos));

        if (eventsNamelist.size() != 0) {
            myViewHolder.eventCheckBox.setChecked(models.get(pos).isSelected());
            myViewHolder.eventCheckBox.setTag(models.get(pos));
        }

        myViewHolder.eventCheckBox.setOnClickListener(v -> {
            CheckBox cb = (CheckBox) v;
            contact = (EventModel) cb.getTag();
            models.get(pos).setPosition(pos);
            //   Log.e("Adapter", "onClick: "+contact.toString() );
            if (!contact.getSelected()) {
                Log.e(TAG, "onClick: " + "true");
                myViewHolder.eventCheckBox.setChecked(true);
                contact.setSelected(cb.isChecked());
                models.get(pos).setSelected(cb.isChecked());
                Log.e(TAG, "onClick: eventPrice" + myViewHolder.edtPrice.getText().toString());
                myViewHolder.edtPrice.setEnabled(true);
                eventModel.setEventNameId(eventsIdlist.get(pos));
                eventModel.setSelected(true);
            } else {
                Log.e(TAG, "onClick: " + "false");
                myViewHolder.eventCheckBox.setChecked(false);
                myViewHolder.edtPrice.setText("");
                contact.setSelected(cb.isChecked());
                models.get(pos).setSelected(cb.isChecked());
                myViewHolder.edtPrice.setEnabled(false);
                eventModels.remove(pos);
            }

        });

        myViewHolder.edtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (myViewHolder.edtPrice.length() > 2) {
                    eventModel.setEventPrice(myViewHolder.edtPrice.getText().toString());
                }
            }
        });
        eventModels.put(pos, eventModel);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return eventsNamelist.size();
    }


    public Map<Integer, EventModel> getEventList() {
        return eventModels;

    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox eventCheckBox;
        EditText edtPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            eventCheckBox = itemView.findViewById(R.id.checkbox_events);
            edtPrice = itemView.findViewById(R.id.edt_event_price);
            edtPrice.setEnabled(false);
        }
    }


    //Set method of OnItemClickListener object
    public void setOnItemClickListener() {
    }
}
