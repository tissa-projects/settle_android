package com.android.inc.settlle.RequestModel;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public class AddServiceDescriptionModel implements Serializable {

    private ArrayList<String>  service_id_list;
    private ArrayList<String>   subscription_id_list;
    private ArrayList<String>   service_name_list;
    private String  company_name;
    private String  mobile;
    private String  country;
    private String  state;
    private String  city;
    private String  guest;
    private String  user_id;
    private String  address;
    private String  latitude;
    private String  longitude;
    private String  landmark;
    private String  zip_code;
    private String  description ;
    private String  price_type;
    private String  from_time;
    private String  to_time;
    private String  discount;
    private String  auth_token;
    private ArrayList<Integer> days;
    private ArrayList<String> package_name;
    private ArrayList<String> package_description;
    private ArrayList<Integer> package_price;
    private ArrayList<String> service_type;





    public AddServiceDescriptionModel(ArrayList<String> service_id, ArrayList<String>  subscription_id,ArrayList<String>  service_name_list, String company_name, String mobile, String country, String state,
                                      String city, String guest, String user_id, String address, String latitude, String longitude, String landmark,
                                      String zip_code, String description, String price_type, String from_time, String to_time, String discount,
                                      String auth_token, ArrayList<Integer> days, ArrayList<String> package_name, ArrayList<String> package_description,
                                      ArrayList<Integer> package_price, ArrayList<String> service_type) {
        this.service_id_list = service_id;
        this.subscription_id_list = subscription_id;
        this.service_name_list = service_name_list;
        this.company_name = company_name;
        this.mobile = mobile;
        this.country = country;
        this.state = state;
        this.city = city;
        this.guest = guest;
        this.user_id = user_id;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.landmark = landmark;
        this.zip_code = zip_code;
        this.description = description;
        this.price_type = price_type;
        this.from_time = from_time;
        this.to_time = to_time;
        this.discount = discount;
        this.auth_token = auth_token;
        this.days = days;
        this.package_name = package_name;
        this.package_description = package_description;
        this.package_price = package_price;
        this.service_type=service_type;

    }

    @Override
    public String toString() {
        return "AddServiceDescriptionModel{" +
                "service_id_list=" + service_id_list +
                ", subscription_id_list=" + subscription_id_list +
                ", service_name_list=" + service_name_list +
                ", company_name='" + company_name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", guest='" + guest + '\'' +
                ", user_id='" + user_id + '\'' +
                ", address='" + address + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", landmark='" + landmark + '\'' +
                ", zip_code='" + zip_code + '\'' +
                ", description='" + description + '\'' +
                ", price_type='" + price_type + '\'' +
                ", from_time='" + from_time + '\'' +
                ", to_time='" + to_time + '\'' +
                ", discount='" + discount + '\'' +
                ", auth_token='" + auth_token + '\'' +
                ", days=" + days +
                ", package_name=" + package_name +
                ", package_description=" + package_description +
                ", package_price=" + package_price +
                ", service_type=" + service_type +
                '}';
    }
}
