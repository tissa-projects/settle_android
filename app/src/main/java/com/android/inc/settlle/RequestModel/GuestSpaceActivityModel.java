package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class GuestSpaceActivityModel implements Serializable {
    private String location;
    private String event;
    private String user_id;
    private int page;
    private int limit;

    public GuestSpaceActivityModel(String location, String event, String user_id, int page, int limit) {
        this.location = location;
        this.event = event;
        this.user_id = user_id;
        this.page = page;
        this.limit = limit;
    }
}
