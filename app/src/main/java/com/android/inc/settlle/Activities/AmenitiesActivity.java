package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.AmenitiesAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetSpaceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AmenitiesActivity extends AppCompatActivity {


    private RecyclerView recyclerAmenities;
    private Context context;
    private Dialog loadingDialog;
    private String uni;
    private static final String TAG = AmenitiesActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amenities);
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h=findViewById(R.id.txtHeading);
        h.setText("Amenities");

        context = AmenitiesActivity.this;
        getIntentData();
        initRecycler();
        getSpaceAmenities();

    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
        }
    }

    private void initRecycler() {
        recyclerAmenities = findViewById(R.id.recycler_space_amenities);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerAmenities.setLayoutManager(layoutManager);
    }

    private void getSpaceAmenities() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceAmenities(new GetSpaceDetailModel(uni));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    if (jsonObject.getBoolean("status")) {
//
                        JSONArray dataArray = new JSONArray();
                        String selectedKeys = jsonObject.getString("selected");

                        String[] selectedKeysArray = selectedKeys.split(",");
                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        JSONArray amenitiesArray = dataObject.getJSONArray("amenities");
                        for (int i = 0; i < amenitiesArray.length(); i++) {
                            JSONObject amenityObject = amenitiesArray.getJSONObject(i);
                            String amenityId = amenityObject.getString("amenities_id");
                            String amenity = amenityObject.getString("amenities_name");

                            for (String key : selectedKeysArray) {
                                if (key.equals(amenityId)) {
                                    JSONObject json = new JSONObject();
                                    json.put("amenities_id", amenityId);
                                    json.put("amenities_name", amenity);
                                    dataArray.put(json);
                                }
                            }
                        }

                        spaceAmenitiesAdapterCall(dataArray);

                    } else {
                        //handle failed logic here
                        Toast.makeText(context, "Error... " + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void spaceAmenitiesAdapterCall(JSONArray dataArray) {
        AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(dataArray,this);
        recyclerAmenities.setAdapter(amenitiesAdapter);

    }
}
