package com.android.inc.settlle.Utilities;

import android.content.Context;
import android.util.Log;

import com.android.inc.settlle.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class DTUtil {

    private static final String TAG = DTUtil.class.getSimpleName();
    public static ArrayList<String> newTimeArray = new ArrayList<>();


    public static ArrayList<String> getTiming(String startEndDate, String startTime, String endTime, Context context, int timeType) {
        String[] timeArray = context.getResources().getStringArray(R.array.time_array);
        newTimeArray = new ArrayList<>();
        String strCurrentDateTime = Utilities.SDF_WITH_HOUR.format(new Date());

        try {
            Date startDateTime = Utilities.SDF_WITH_HOUR.parse(convertTo24Hrs(startEndDate + " " + startTime));
            Date endDateTime = Utilities.SDF_WITH_HOUR.parse(convertTo24Hrs(startEndDate + " " + endTime));
            for (String s : timeArray) {
                if (timeType == 1) {
                    //Compare same date
                    if (Objects.requireNonNull(Utilities.SDF_WITHOUT_HOUR.parse(startEndDate)).compareTo(Utilities.SDF_WITHOUT_HOUR.parse(strCurrentDateTime)) == 0) {
                        Date selectedDateTime = Utilities.SDF_WITH_HOUR.parse(convertTo24Hrs(startEndDate + " " + s));
                        Date currentDateTime = Utilities.SDF_WITH_HOUR.parse(strCurrentDateTime);
                        //Check current hour is less than selected hour
                        assert selectedDateTime != null;
                        if (selectedDateTime.compareTo(currentDateTime) > 0) {
                            //Check selected hour is less than end hour
                            if (selectedDateTime.compareTo(startDateTime) >= 0 && selectedDateTime.compareTo(endDateTime) <= 0) {
                                String temp = convertTo24Hrs(startEndDate + " " + s);
                                newTimeArray.add(convertTo12Hrs(temp));
                            }
                        }
                    } else { // Other Date booking
                        Date selectedDateTime = Utilities.SDF_WITH_HOUR.parse(convertTo24Hrs(startEndDate + " " + s));
                        assert selectedDateTime != null;
                      if (selectedDateTime.compareTo(startDateTime) >= 0 && selectedDateTime.compareTo(endDateTime) <= 0) {
                            String temp = convertTo24Hrs(startEndDate + " " + s);
                            newTimeArray.add(convertTo12Hrs(temp));
                        } /*else {
                            Log.d(TAG, "getTiming: else else = "+startEndDate + ":" + s + (selectedDateTime.compareTo(startDateTime) >= 0) + " = " + (selectedDateTime.compareTo(endDateTime) <= 0));
                        }*/
                    }
                } else {
                    Date selectedDateTime = Utilities.SDF_WITH_HOUR.parse(convertTo24Hrs(startEndDate + " " + s));
                    assert selectedDateTime != null;
                    if (selectedDateTime.compareTo(startDateTime) >= 0 && selectedDateTime.compareTo(endDateTime) <= 0) {
                        String temp = convertTo24Hrs(startEndDate + " " + s);
                        Log.d(TAG, "getTiming: " + temp);
                        newTimeArray.add(convertTo12Hrs(temp));
                    }
                }
            }
        } catch (Exception ignore) {
        }
        return newTimeArray;
    }


    /*//To get space/service start and end time
    public static ArrayList<String> checkTimings(String startEndDate, String starttime, String endTime, Context context, int timeType) {
        String pattern = "dd/MM/yyyy HH";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        try {
            String startNewDate = convertTo24Hrs(startEndDate + " " + starttime);

            SimpleDateFormat format = new SimpleDateFormat("HH", Locale.ENGLISH);
            String hour = format.format(new Date());
            String DatePattern = "dd/MM/yyyy";
            SimpleDateFormat onlyDate = new SimpleDateFormat(DatePattern, Locale.ENGLISH);
            String temp = onlyDate.format(new Date());
            Date comDate = onlyDate.parse(temp);

            Date date1 = sdf.parse(startNewDate);
            String[] timeArray = context.getResources().getStringArray(R.array.time_array);
            for (String s : timeArray) {
                String strNewDate = convertTo24Hrs(startEndDate + " " + s);
                Date newDate = sdf.parse(strNewDate);
                if (timeType == 1) {
                    assert newDate != null;
                    String hour2 = format.format(newDate);
                    String endHour = format.format(Objects.requireNonNull(sdf.parse(convertTo24Hrs(startEndDate + " " + endTime))));
                    CommonFunctions.showLog("DateCheck ", hour2 + " " + endHour);

                    //Check same dat booking
                    if (date1.compareTo(comDate) == 0) {
                        if (Integer.parseInt(endHour) >= Integer.parseInt(hour2)) {
                            String strNewTime = convertTo12Hrs(strNewDate);
                            newTimeArray.add(strNewTime);
                        }
                    } else {
                        String strNewTime = convertTo12Hrs(strNewDate);
                        newTimeArray.add(strNewTime);
                    }


*//*                    if (Integer.parseInt(endHour) >= Integer.parseInt(hour2))
                        if (newDate.compareTo(date1) >= 0) {
                            if ((Objects.requireNonNull(onlyDate.parse(startNewDate)).compareTo(comDate) == 0)) {
                                if (Integer.parseInt(hour) <= Integer.parseInt(hour2)) {
                                    String strNewTime = convertTo12Hrs(strNewDate);
                                    newTimeArray.add(strNewTime);
                                }
                            } else {
                                String strNewTime = convertTo12Hrs(strNewDate);
                                newTimeArray.add(strNewTime);
                            }
                        }*//*


                } else if (timeType == 2) {

                    String startHour = format.format(Objects.requireNonNull(sdf.parse(convertTo24Hrs(startEndDate + " " + starttime))));

                    String endHour = format.format(Objects.requireNonNull(sdf.parse(convertTo24Hrs(startEndDate + " " + endTime))));

                    String compareTime = format.format(Objects.requireNonNull(sdf.parse(convertTo24Hrs(startEndDate + " " + s))));

                    final boolean b = Integer.parseInt(startHour) <= Integer.parseInt(compareTime) && Integer.parseInt(endHour) >= Integer.parseInt(compareTime);
                    if ((Objects.requireNonNull(onlyDate.parse(startNewDate)).compareTo(comDate) == 0)) {
                        Log.d(TAG, "DateCheck: IFFF " + startHour + " " + endHour);
                        if (b) {
                            String strNewTime = convertTo12Hrs(strNewDate);
                            newTimeArray.add(strNewTime);
                        }
                    } else {
                        Log.d(TAG, "DateCheck: IFFF b else " + startHour + " " + endHour);
                        if (b) {
                            String strNewTime = convertTo12Hrs(strNewDate);
                            newTimeArray.add(strNewTime);
                        }
                    }
                }
            }
            Log.e(TAG, "checktimings: " + newTimeArray);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newTimeArray;
    }
*/
    //Convert time into 24 hours format
    private static String convertTo24Hrs(String input) {
        //Format of the date defined in the input String
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh aa", Locale.ENGLISH);
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat outputformat = new SimpleDateFormat("dd/MM/yyyy HH", Locale.ENGLISH);
        Date date;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(input);
            //Changing the format of date and storing it in String
            assert date != null;
            output = outputformat.format(date);
            //Displaying the date
            Log.e(TAG, "convertTo24Hrs: " + output);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return output;
    }

    //Convert time into 12 hours format
    private static String convertTo12Hrs(String input) {
        //Format of the date defined in the input String
        DateFormat outputformat = new SimpleDateFormat("hh aa", Locale.ENGLISH);
        //Desired format: 24 hour format: Change the pattern as per the need
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH", Locale.ENGLISH);
        Date date;
        String output = null;
        try {
            //Converting the input String to Date
            date = df.parse(input);
            //Changing the format of date and storing it in String
            assert date != null;
            output = outputformat.format(date);
            //Displaying the date
            Log.e(TAG, "convertTo24Hrs: " + output);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return output;
    }

    //Compare two date
    public static boolean compareTwoDate(String day1, String day2, String condition) {
        try {
            String pattern = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
            Date date1 = sdf.parse(day1);
            Date date2 = sdf.parse(day2);
            if (date1 != null)
                switch (condition) {
                    case "==":
                        return date1.compareTo(date2) == 0;
                    case ">":
                        return date1.compareTo(date2) > 0;
                    case "<":
                        return date1.compareTo(date2) < 0;
                }
        } catch (Exception ex) {
            Log.d(TAG, "compareTwoDate: " + ex);
        }
        return false;
    }
}
