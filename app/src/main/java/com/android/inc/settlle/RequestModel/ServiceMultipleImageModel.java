package com.android.inc.settlle.RequestModel;

import java.util.List;

public class ServiceMultipleImageModel {

    private List<String> service_image;
    private  String auth_token;
    private  String user_id;
    private  String service_id;

    public ServiceMultipleImageModel(List<String> service_image, String auth_token, String user_id, String service_id) {
        this.service_image = service_image;
        this.auth_token = auth_token;
        this.user_id = user_id;
        this.service_id = service_id;
    }

    public List<String> getService_image() {
        return service_image;
    }

    public void setService_image(List<String> service_image) {
        this.service_image = service_image;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }
}
