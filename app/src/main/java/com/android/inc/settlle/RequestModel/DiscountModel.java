package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class DiscountModel implements Serializable {

    private String coupon_name;
    private String auth_token;
    private String coupon_for;
    private String user_id;

    public DiscountModel(String coupon_name, String auth_token, String coupon_for, String user_id) {
        this.coupon_name = coupon_name;
        this.auth_token = auth_token;
        this.coupon_for = coupon_for;
        this.user_id = user_id;
    }
}
