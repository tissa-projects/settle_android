package com.android.inc.settlle.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.UploadSpaceKYCDocModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceKYCDocumentsActivity extends AppCompatActivity implements View.OnClickListener {


    TextView idProof, spaceProof, idProoffFileNameTV, spaceProofFileNameTV;
    String docType, imgselectionType;
    ImageView idProofImageIV, spaceProofImageIV, imgBackBtn;
    private static final String TAG = SpaceKYCDocumentsActivity.class.getSimpleName();

    String idProofDocExtension, spaceProofDocExtension;
    String proofType;
    Button sendKYCDoc;
    private String insertId;
    String idProofBase64 = "", spaceProofBase64 = "";
    Context context;

    private Dialog lodingDialog;

    public static final int RESULT_GALLERY = 1;
    public static final int RESULT_CAMERA = 2;
    public static final int RESULT_FM = 3;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String[] appPermission = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };
    ActivityResultLauncher<Intent> activityResultLauncher;
    int selectionType = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_space_kyc);
        context = SpaceKYCDocumentsActivity.this;
        initOnStartActivity();
        initialize();
        getIntentData();
        checkNRequestPermission();


    }

    private void initialize() {
        idProof = findViewById(R.id.btn_choose_identity_proof);
        spaceProof = findViewById(R.id.btn_choose_space_proof);
        idProofImageIV = findViewById(R.id.KYC_img_1);
        spaceProofImageIV = findViewById(R.id.KYC_img_2);
        sendKYCDoc = findViewById(R.id.btn_send_kyc_documents);


        idProoffFileNameTV = findViewById(R.id.id_proof_file_name_text);
        spaceProofFileNameTV = findViewById(R.id.space_proof_file_name_text);
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());

        TextView h = findViewById(R.id.txtHeading);
        h.setText("Add Space KYC DOC");


        idProof.setOnClickListener(this);
        spaceProof.setOnClickListener(this);
        sendKYCDoc.setOnClickListener(this);


        idProofImageIV.setOnClickListener(this);
        spaceProofImageIV.setOnClickListener(this);
        idProoffFileNameTV.setOnClickListener(this);
        spaceProofFileNameTV.setOnClickListener(this);


    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
            case R.id.btn_choose_identity_proof:
                proofType = "ID_PROOF";
                documentTypeDialog();
                break;
            case R.id.btn_choose_space_proof:
                proofType = "SPACE_PROOF";
                documentTypeDialog();
                break;
            case R.id.btn_send_kyc_documents:
                if (idProofBase64.isEmpty()) {
                    Toast.makeText(context, "Choose ID proof ", Toast.LENGTH_SHORT).show();
                } else if (spaceProofBase64.isEmpty()) {
                    Toast.makeText(context, "Choose space proof ", Toast.LENGTH_SHORT).show();
                } else {
                    if (VU.isConnectingToInternet(context)) {
                        uploadKYCDOC();
                    }
                }
                break;
            case R.id.id_proof_file_name_text:
            case R.id.KYC_img_1:
                Log.d(TAG, "onClick:  " + idProofBitMap + " " + Utilities.SERVICE_DOCUMENT);
                if (idProofBitMap) {
                    callPreviewActivity(".Png", "idProofBase64", true, idProofBase64);
                } else {
                    Log.d(TAG, "onClick: " + CommonFunctions.getMimeType(idProofPath));
                    if (idProofPath != null && idProofPath.length() > 5 && CommonFunctions.getMimeType(idProofPath) != null)
                        callPreviewActivity(CommonFunctions.getMimeType(idProofPath), idProofPath, false, "");
                    else
                        Toast.makeText(getApplicationContext(), "Please select file first or Please select PDF/Image file", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.space_proof_file_name_text:
            case R.id.KYC_img_2:
                if (idSpaceBitmap) {
                    Log.d(TAG, "openFileForRegistration: From Local 111  " + spaceProofBase64);
                    callPreviewActivity(".Png", "serviceProofBase64", true, spaceProofBase64);
                } else {
                    if (idSpaceProofPath != null && idSpaceProofPath.length() > 5 && CommonFunctions.getMimeType(idSpaceProofPath) != null) {
                        callPreviewActivity(CommonFunctions.getMimeType(idSpaceProofPath), idSpaceProofPath, false, "");
                    } else
                        Toast.makeText(getApplicationContext(), "Please select file first or Please select PDF/Image file", Toast.LENGTH_LONG).show();
                }
        }
    }

    public void callPreviewActivity(String fileType, String filePath, boolean isBitmap, String bitMap) {
        Intent intent = new Intent(getApplicationContext(), PreviewPDFImage.class);
        intent.putExtra("fileType", fileType);
        intent.putExtra("filePath", filePath);
        intent.putExtra("isBitmap", isBitmap);
        intent.putExtra("bitmap", " ");
        Utilities.TRANSFER_BITMAP_VALUE = bitMap;
        Log.d(TAG, "openFileForRegistration: From Local 222  " + bitMap);
        startActivity(intent);
    }

    private void documentTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"IMAGE", "PDF"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(this);
            docTypeAlert.setTitle("Select Document Type");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                docType = doc_type_list[which];

                //handle logic here
                if (docType.equals("IMAGE")) {
                    imageTypeDialog();
                } else if (docType.equals("PDF")) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("application/pdf");
                    selectionType = Utilities.RESULT_FM;
                    activityResultLauncher.launch(intent);
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

    private void imageTypeDialog() {
        try {
            // setup the alert builder
            final String[] doc_type_list = {"Take Photo", "Choose from Gallery", "Cancel"};
            AlertDialog.Builder docTypeAlert = new AlertDialog.Builder(this);
            docTypeAlert.setTitle("Add Photo!");

            docTypeAlert.setItems(doc_type_list, (dialog, which) -> {
                imgselectionType = doc_type_list[which];

                //handle logic here

                if (imgselectionType.equals("Take Photo")) {

                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    selectionType = Utilities.RESULT_CAMERA;
                    activityResultLauncher.launch(cameraIntent);

                } else if (imgselectionType.equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_PICK);
                    // Sets the type as image/*. This ensures only components of type image are selected
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    selectionType = Utilities.RESULT_GALLERY;
                    activityResultLauncher.launch(intent);
                }
            });
            docTypeAlert.create().show();
        } catch (NullPointerException ignore) {
        }
    }

    String idProofPath = "", idSpaceProofPath = "";
    boolean idProofBitMap = false, idSpaceBitmap = false;


    @SuppressLint("SetTextI18n")
    public void initOnStartActivity() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    CommonFunctions.showLog("onActivityResult ", "initOnStartActivity Called");
                    if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                        if (Utilities.RESULT_GALLERY == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                if (proofType.equalsIgnoreCase("SPACE_PROOF")) {
                                    getDataForSpaceProof(selectedImageURI, "image", "SPACE_PROOF.png");
                                } else {
                                    getDataForID(selectedImageURI, "image", "ID-PROOF.png");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (Utilities.RESULT_CAMERA == selectionType) {
                            Bitmap bitmap = (Bitmap) result.getData().getExtras().get("data");
                            bitmap = Bitmap.createScaledBitmap(bitmap, 2000, 3000, false);
                            if (proofType.equals("ID_PROOF")) {
                                idProofDocExtension = "png";
                                idProofBitMap = true;
                                idProoffFileNameTV.setText("ID-PROOF.png");
                                idProofBase64 = CommonFunctions.ResizedBitmapEncodeToBase64(CommonFunctions.GetResizedBitmap(bitmap, 5000));
                                idProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(idProofBase64));
                            } else if (proofType.equals("SPACE_PROOF")) {
                                idSpaceBitmap = true;
                                spaceProofDocExtension = "png";
                                spaceProofBase64 = CommonFunctions.ResizedBitmapEncodeToBase64(bitmap);
                                spaceProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(spaceProofBase64));
                            }
                        } else if (Utilities.RESULT_FM == selectionType) {
                            try {
                                Uri selectedImageURI = result.getData().getData();
                                if (proofType.equals("ID_PROOF")) {
                                    getDataForID(selectedImageURI, "PDF", "ID_PROOF.pdf");
                                }
                                if (proofType.equals("SPACE_PROOF")) {
                                    getDataForSpaceProof(selectedImageURI, "PDF", "SPACE_PROOF.pdf");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d(TAG, "onActivityResult: " + e);
                            }
                        }
                    }
                });
    }


    private void getDataForID(Uri selectedImageURI, String type, String tempFileName) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory(tempFileName, context);//CommonFunctions.createFile("ID_PROOF.pdf");
            if (fileName != null) {//&& (fileName.length() / 1024) > 200
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    idProofBase64 = CommonFunctions.GetBase64FromFile(fileName);
                    idProofPath = fileName.getPath();
                    Log.d(TAG, "onActivityResult: " + idProofPath);
                    if (type.equalsIgnoreCase("image")) {
                        idProofDocExtension = "png";
                        idProoffFileNameTV.setText(idProofPath.substring(idProofPath.lastIndexOf("/") + 1));
                        idProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(idProofBase64));
                        idProofBitMap = true;
                    } else {
                        idProofDocExtension = "pdf";
                        idProoffFileNameTV.setText(idProofPath.substring(idProofPath.lastIndexOf("/") + 1));
                        idProofImageIV.setImageResource(R.drawable.ic_pdf);
                        idProofBitMap = false;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void getDataForSpaceProof(Uri selectedImageURI, String type, String tempFileName) {
        InputStream fileInputData = CommonFunctions.getInputStreamByUri(context, selectedImageURI);
        if (fileInputData != null) {
            File fileName = CommonFunctions.createFileInLocalDirectory(tempFileName, context);
            if (fileName != null) {
                boolean status = CommonFunctions.writeFileInLocalDirectory(fileInputData, fileName);
                if (status) {
                    idSpaceProofPath = fileName.getPath();
                    spaceProofBase64 = CommonFunctions.GetBase64FromFile(fileName);
                    if (type.equalsIgnoreCase("image")) {
                        spaceProofImageIV.setImageBitmap(CommonFunctions.GetBitmap(spaceProofBase64));
                        spaceProofDocExtension = "png";
                        spaceProofFileNameTV.setText(idSpaceProofPath.substring(idSpaceProofPath.lastIndexOf("/") + 1));
                        idSpaceBitmap = true;
                    } else {
                        idSpaceBitmap = false;
                        spaceProofDocExtension = "pdf";
                        spaceProofFileNameTV.setText(idSpaceProofPath.substring(idSpaceProofPath.lastIndexOf("/") + 1));
                        spaceProofImageIV.setImageResource(R.drawable.ic_pdf);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Not able to download file form drive right now, Please try after some time", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Not able to create a file right now, Please try after some time", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Selected file is not a valid format or File is corrupted", Toast.LENGTH_LONG).show();
        }
    }

    private void getIntentData() {
       /* Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            insertId = bundle.getString("insertId");
            Log.e(TAG, "getIntentData: " + insertId);
        }*/
    }

    private void uploadKYCDOC() {

        lodingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Log.e(TAG, "uploadKYCDOC: idProofBase64 :" + idProofBase64);
        Log.e(TAG, "uploadKYCDOC: spaceProofBase64 :" + spaceProofBase64);
        Log.e(TAG, "uploadKYCDOC: idProofDocExtension :" + idProofDocExtension);
        Log.e(TAG, "uploadKYCDOC: spaceProofDocExtension :" + spaceProofDocExtension);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().uploadSpaceKYCDoc(new UploadSpaceKYCDocModel(idProofBase64, spaceProofBase64, idProofDocExtension, spaceProofDocExtension,
                insertId, Utilities.getSPstringValue(context, Utilities.spUserId),
                Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                lodingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "uploadKYCDOC: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String msg = jsonObject.getString("message");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            statusAlert("Information", "Document uploaded successfully", "Success");
                        } else {
                            statusAlert("Alert", msg, "Error");
                        }
                    }
                } catch (Exception e) {
                    statusAlert("Alert", getResources().getString(R.string.network_error), "Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                lodingDialog.dismiss();

            }
        });
    }

    private void checkNRequestPermission() {
        //check which permissions are granted
        List<String> listPermissionsNeeded = new ArrayList<>();

        for (String perm : appPermission) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }

        //Ask for non-granted permissions
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[0]), PERMISSION_REQUEST_CODE);

        }

        //app has all permissions go ahead
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            HashMap<String, Integer> permissionResult = new HashMap<>();
            int deniedCount = 0;

            //gather permission grant result
            for (int i = 0; i < grantResults.length; i++) {

                //add only permission which denied
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResult.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            //check if all permissions are granted
            if (deniedCount == 0) {
                CommonFunctions.showLog(" ", " ");
            }
            // atlest one or all permissions are denied
            else {
                for (Map.Entry<String, Integer> entry : permissionResult.entrySet()) {
                    String permName = entry.getKey();
                    //permission is denied (this is the first time, when "never asked  again" is not clicked)
                    //so aske again explaining the usage of permission
                    // shouldShowRequestPermission will return true
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {

                        showDialog("This app needs Camera and Storage permissions to work without any problem",
                                "Yes, Grant Permissions",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    checkNRequestPermission();
                                },
                                "No, Exit app",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    finish();
                                }, false);
                    }
                    //permission is denied (and never asked again is checked)
                    else {
                        // asked user to go to setting and manually allow  permission
                        showDialog("You have denied some permissions.Allow all permissions at [Setting] > [Permissions]",
                                "Go to Settings",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                            Uri.fromParts("package", getPackageName(), null));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                },
                                "No, Exit app",
                                (dialog, which) -> {
                                    dialog.dismiss();
                                    finish();
                                }, false);
                    }
                }
            }
        }

    }

    public void showDialog(String msg, String positiveLable, DialogInterface.OnClickListener positiveOnCLick,
                           String negativeLable, DialogInterface.OnClickListener negativeOnCLick, boolean isCancelable) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("title");
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLable, positiveOnCLick);
        builder.setNegativeButton(negativeLable, negativeOnCLick);
        android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void statusAlert(String title, String msg, String type) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(msg);
        builder1.setTitle(title);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Okay",
                (dialog, id) -> {
                    if (type.equalsIgnoreCase("Success")) {
                        Intent i = new Intent(context, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                    dialog.dismiss();
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
