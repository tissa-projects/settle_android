
package com.android.inc.settlle.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.GuestSpaceActivity;
import com.android.inc.settlle.Activities.MyRequestUpdateActivity;
import com.android.inc.settlle.Activities.SearchItemListActivity;
import com.android.inc.settlle.Activities.Service.EditServiceImagesActivity;
import com.android.inc.settlle.Activities.Service.GuestServiceActivity;
import com.android.inc.settlle.Activities.Service.ServiceAddImagesActivity;
import com.android.inc.settlle.Activities.Service.ServiceKYCDocumentActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.UserAuthModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.android.inc.settlle.newDesign.NewSpaceListActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener {


    private TextView txtSpace;
    private TextView txtService;
    private TextView headerTxt;
    private TextView txtCartCount;
    private AutoCompleteTextView autoCmpltTxtPlaces, autoCmpltTxtService, autoCmpltTxtGuest, autoCmpltTxtLocation, autoCmpltTxtEvent;
    private LinearLayout llPlaces, llService, llGuest, llLocation, llEvent;
    private String strSelectSearchType = "space";  //by default because on search btn click we are going to space fragment on first time
    private Context context;
    private View view;
    public static HashMap<String, String> venueIdNameList;
    private ArrayList<String> spaceLocationList, spaceLocationIdList, spaceEventList, spaceEventIdLIst;
    public static ArrayList<String> PlaceList, PlaceIdList, serviceList, serviceIdList, guestList, guestIdList, cityList, cityIdList;

    private String mEventId;
    private String mServiceId;
    private String mGuestLimitId;
    private String mSpaceLocation;
    private String mServiceLocationId;

    private final int LOCATION_STATUS_CODE = 100;
    private final int EVENT_STATUS_CODE = 200;
    private final int PLACES_STATUS_CODE = 300;
    private final int SERVICE_STATUS_CODE = 400;
    private final int GUEST_STATUS_CODE = 500;

    private Dialog loadingDialog;
    private static final String TAG = HomeFragment.class.getName();


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();
        initialize();
        printDeviceInfo();

        Utilities.guestLimitList = new ArrayList<>(Arrays.asList(Utilities.guestLimitArray));
        Utilities.guestLimitIdList = new ArrayList<>(Arrays.asList(Utilities.guestLimitIdArray));
        String auth = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "auth : " + auth);


        if (VU.isConnectingToInternet(context)) {
            getSpaceEventLocationList();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            txtCartCount.setText(Utilities.getSPstringValue(context, Utilities.CHECK_OUT_COUNT));
        } catch (Exception ignore) {

        }
    }

    private void initialize() {
        venueIdNameList = new HashMap<>();
        spaceLocationList = new ArrayList<>();
        spaceLocationIdList = new ArrayList<>();
        spaceEventList = new ArrayList<>();
        spaceEventIdLIst = new ArrayList<>();
        PlaceList = new ArrayList<>();
        PlaceIdList = new ArrayList<>();
        serviceList = new ArrayList<>();
        serviceIdList = new ArrayList<>();
        guestList = new ArrayList<>();
        guestIdList = new ArrayList<>();
        cityList = new ArrayList<>();
        cityIdList = new ArrayList<>();

        String[] guestIds = getResources().getStringArray(R.array.GuestIdList);
        String[] guestNames = getResources().getStringArray(R.array.GuestList);

        for (int i = 0; i < guestIds.length; i++) {
            guestList.add(i, guestNames[i]);
            guestIdList.add(i, guestIds[i]);
        }

        txtCartCount = requireActivity().findViewById(R.id.txt_cart_count);
        autoCmpltTxtEvent = view.findViewById(R.id.auto_txt_event);
        headerTxt = view.findViewById(R.id.txtHeader);
        autoCmpltTxtGuest = view.findViewById(R.id.auto_txt_guest);
        autoCmpltTxtLocation = view.findViewById(R.id.auto_txt_location);
        autoCmpltTxtPlaces = view.findViewById(R.id.auto_txt_places);
        autoCmpltTxtService = view.findViewById(R.id.auto_txt_service);
        txtSpace = view.findViewById(R.id.home_txt_space);
        txtService = view.findViewById(R.id.home_txt_service);
        TextView txtNagpur = view.findViewById(R.id.txt_nagpur);
        TextView txtPune = view.findViewById(R.id.txt_pune);
        TextView txtNasik = view.findViewById(R.id.txt_nasik);
        llEvent = view.findViewById(R.id.ll_event);
        llGuest = view.findViewById(R.id.ll_guest);
        llPlaces = view.findViewById(R.id.ll_places);
        llService = view.findViewById(R.id.ll_services);
        llLocation = view.findViewById(R.id.ll_location);

        Button btnSearch = view.findViewById(R.id.home_search_button);

        autoCmpltTxtLocation.setOnClickListener(this);
        autoCmpltTxtEvent.setOnClickListener(this);
        autoCmpltTxtGuest.setOnClickListener(this);
        autoCmpltTxtPlaces.setOnClickListener(this);
        autoCmpltTxtService.setOnClickListener(this);
        txtSpace.setOnClickListener(this);
        txtService.setOnClickListener(this);
        txtNagpur.setOnClickListener(this);
        txtPune.setOnClickListener(this);
        txtNasik.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
    }

    @SuppressLint({"SetTextI18n", "UseCompatLoadingForColorStateLists", "NonConstantResourceId"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_txt_space:
                strSelectSearchType = "space";
                headerTxt.setText(getResources().getString(R.string.find_ideal_space_for_event));
                txtSpace.setBackgroundResource(R.drawable.btn_curve_transparent_orange);
                txtService.setBackgroundResource(R.drawable.btn_curve_transparent_grey);
                txtSpace.setTextColor(getResources().getColorStateList(R.color.white));
                txtService.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
                llPlaces.setVisibility(View.GONE);
                llService.setVisibility(View.GONE);
                llGuest.setVisibility(View.GONE);
                llLocation.setVisibility(View.VISIBLE);
                llEvent.setVisibility(View.VISIBLE);
                break;
            case R.id.home_txt_service:
                strSelectSearchType = "service";
                headerTxt.setText(getResources().getString(R.string.find_ideal_services_for_event));
                txtService.setBackgroundResource(R.drawable.btn_curve_transparent_orange);
                txtSpace.setBackgroundResource(R.drawable.btn_curve_transparent_grey);
                txtService.setTextColor(getResources().getColorStateList(R.color.white));
                txtSpace.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
                llPlaces.setVisibility(View.VISIBLE);
                llService.setVisibility(View.VISIBLE);
                llGuest.setVisibility(View.VISIBLE);
                llLocation.setVisibility(View.GONE);
                llEvent.setVisibility(View.GONE);
                break;

            case R.id.home_search_button:
                if (strSelectSearchType.equalsIgnoreCase("space")) {
                    Log.e(TAG, "SpaceData: eventId : " + mEventId + "  location: " + mSpaceLocation);
                   Intent intent = new Intent(context, GuestSpaceActivity.class);
                    intent.putExtra("loaction", mSpaceLocation);
                    intent.putExtra("eventId", mEventId);
                    intent.putExtra("eventNames", spaceEventList);
                    intent.putExtra("eventIds", spaceEventIdLIst);
                    intent.putExtra("venueIdNamesMap", venueIdNameList);
                    intent.putExtra("eventName", autoCmpltTxtEvent.getText().toString());
                    Utilities.setSPboolean(context, Utilities.spIsBookingType, true);


                   // Intent intent = new Intent(context, MyRequestUpdateActivity.class);
                    startActivity(intent);
                    requireActivity().finish();
                } else if (strSelectSearchType.equalsIgnoreCase("service")) {
                    Intent intent = new Intent(context, GuestServiceActivity.class);
                    intent.putExtra("citiId", mServiceLocationId);
                    intent.putExtra("serviceID", mServiceId);
                    intent.putExtra("guestID", mGuestLimitId);
                    intent.putExtra("citiName", autoCmpltTxtPlaces.getText().toString());
                    intent.putExtra("serviceName", autoCmpltTxtService.getText().toString());
                    intent.putExtra("guestCount", autoCmpltTxtGuest.getText().toString());
                    intent.putExtra("cities", cityList);
                    intent.putExtra("cityIdList", cityIdList);
                    intent.putExtra("services", serviceList);
                    intent.putExtra("serviceIdList", serviceIdList);
                    Utilities.setSPboolean(context, Utilities.spIsBookingType, true);
                    startActivity(intent);
                    requireActivity().finish();
                }
                break;

            case R.id.auto_txt_location:
                Intent locationIntent = new Intent(getActivity(), SearchItemListActivity.class);
              /*  locationIntent.putExtra("searchList", spaceLocationList);
                locationIntent.putExtra("IdList", spaceLocationIdList);
                locationIntent.putExtra("searchType", "place");//location*/

                locationIntent.putExtra("searchList", PlaceList);
                locationIntent.putExtra("IdList", PlaceIdList);
                locationIntent.putExtra("searchList", cityList);
                locationIntent.putExtra("IdList", cityIdList);
                locationIntent.putExtra("searchType", "place");
                getActivity().startActivityForResult(locationIntent, LOCATION_STATUS_CODE);
                break;

            case R.id.auto_txt_event:
                Intent eventIntent = new Intent(getActivity(), SearchItemListActivity.class);
                eventIntent.putExtra("searchList", spaceEventList);
                eventIntent.putExtra("IdList", spaceEventIdLIst);
                eventIntent.putExtra("searchType", "event");
                getActivity().startActivityForResult(eventIntent, EVENT_STATUS_CODE);
                break;
            case R.id.auto_txt_guest:
                Intent guestIntent = new Intent(getActivity(), SearchItemListActivity.class);
                guestIntent.putExtra("searchList", guestList);
                guestIntent.putExtra("IdList", guestIdList);
                guestIntent.putExtra("searchType", "guest");
                getActivity().startActivityForResult(guestIntent, GUEST_STATUS_CODE);
                break;

            case R.id.auto_txt_places:
                Intent placeIntent = new Intent(getActivity(), SearchItemListActivity.class);
                placeIntent.putExtra("searchList", PlaceList);
                placeIntent.putExtra("IdList", PlaceIdList);
                placeIntent.putExtra("searchList", cityList);
                placeIntent.putExtra("IdList", cityIdList);
                placeIntent.putExtra("searchType", "place");
                getActivity().startActivityForResult(placeIntent, PLACES_STATUS_CODE);
                break;

            case R.id.auto_txt_service:
                Intent serviceIntent = new Intent(getActivity(), SearchItemListActivity.class);
                serviceIntent.putExtra("searchList", serviceList);
                serviceIntent.putExtra("IdList", serviceIdList);
                serviceIntent.putExtra("searchType", "service");
                getActivity().startActivityForResult(serviceIntent, SERVICE_STATUS_CODE);
                break;
        }
    }


    //Get Space releted Data
    private void getSpaceEventLocationList() {
        String userid = Utilities.getSPstringValue(context, Utilities.spUserId);
        String token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Log.e(TAG, "getSpaceEventLocationList: " + userid);
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceEventLocationList(new UserAuthModel(userid, token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loadingDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: getSpaceEventLocationList 2 " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);

                    if (jsonObject.getBoolean("status")) {
                        //handle success logic here
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e(TAG, "getSpaceEventLocationList 4 " + dataObject);

                        try {
                            String count = dataObject.getString("count");
                            Utilities.setSPstring(context, Utilities.CHECK_OUT_COUNT, String.valueOf(count));
                            txtCartCount.setVisibility(View.VISIBLE);
                            txtCartCount.setText(count);
                        } catch (Exception ignore) {
                        }
                        JSONArray spaceLocationArray = dataObject.getJSONArray("space_address");
                        for (int i = 0; i < spaceLocationArray.length(); i++) {
                            JSONObject c = spaceLocationArray.getJSONObject(i);
                            spaceLocationList.add(c.getString("address"));
                            spaceLocationIdList.add(c.getString("space_id"));
                        }

                        JSONArray eventsArray = dataObject.getJSONArray("events");
                        for (int i = 0; i < eventsArray.length(); i++) {
                            JSONObject c = eventsArray.getJSONObject(i);
                            spaceEventList.add(c.getString("event_name"));
                            spaceEventIdLIst.add(c.getString("event_id"));
                        }
                        // places
                        JSONArray serviceLocationArray = dataObject.getJSONArray("service_address");
                        for (int i = 0; i < serviceLocationArray.length(); i++) {
                            JSONObject c = serviceLocationArray.getJSONObject(i);
                            PlaceIdList.add(c.getString("service_details_id"));
                            PlaceList.add(c.getString("address"));

                        }

                        JSONArray serviceArray = dataObject.getJSONArray("service");
                        for (int i = 0; i < serviceArray.length(); i++) {
                            JSONObject c = serviceArray.getJSONObject(i);
                            serviceList.add(c.getString("service_name"));
                            serviceIdList.add(c.getString("service_id"));
                        }

                        JSONArray venueArray = dataObject.getJSONArray("venue");
                        for (int i = 0; i < venueArray.length(); i++) {
                            JSONObject c = venueArray.getJSONObject(i);
                            venueIdNameList.put(c.getString("venue_id"), c.getString("venue_name"));
                        }

                        JSONArray cityArray = dataObject.getJSONArray("cities");
                        for (int i = 0; i < cityArray.length(); i++) {
                            JSONObject c = cityArray.getJSONObject(i);
                            cityList.add(c.getString("location_name"));
                            cityIdList.add(c.getString("location_id"));
                        }
                    } else {
                        //handle failed logic here
                        Toast.makeText(getActivity(), "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Exception   " + e);
                    Log.d(TAG, "getSpaceEventLocationList Exception " + e);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                loadingDialog.dismiss();


            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String strSearchName = data.getStringExtra("searchName");
            String strSearchId = data.getStringExtra("searchId");
            Log.e(TAG, "onActivityResult: strSearchName : " + strSearchName + " strSearchId : " + strSearchId);
            switch (requestCode) {
                case LOCATION_STATUS_CODE:
                    if (resultCode == Activity.RESULT_OK) {
                        setSearchtext(autoCmpltTxtLocation, strSearchName);
                        mSpaceLocation = strSearchName;
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        Toast.makeText(context, "You Cancelled The Search", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case EVENT_STATUS_CODE:
                    if (resultCode == Activity.RESULT_OK) {
                        setSearchtext(autoCmpltTxtEvent, strSearchName);
                        mEventId = strSearchId;
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        Toast.makeText(context, "You Cancelled The Search", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case PLACES_STATUS_CODE:
                    if (resultCode == Activity.RESULT_OK) {
                        setSearchtext(autoCmpltTxtPlaces, strSearchName);
                        mServiceLocationId = strSearchId;
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        Toast.makeText(context, "You Cancelled The Search", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case SERVICE_STATUS_CODE:
                    if (resultCode == Activity.RESULT_OK) {
                        setSearchtext(autoCmpltTxtService, strSearchName);
                        mServiceId = strSearchId;
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        Toast.makeText(context, "You Cancelled The Search", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case GUEST_STATUS_CODE:
                    if (resultCode == Activity.RESULT_OK) {
                        setSearchtext(autoCmpltTxtGuest, strSearchName);
                        mGuestLimitId = strSearchId;
                    } else if (resultCode == Activity.RESULT_CANCELED) {
                        Toast.makeText(context, "You Cancelled The Search", Toast.LENGTH_SHORT).show();
                    }
                    break;

            }
        } else {
            Toast.makeText(context, "You Cancelled The Search", Toast.LENGTH_SHORT).show();
        }
    }

    private void setSearchtext(EditText editText, String strSearch) {
        if (strSearch != null) {
            editText.setText(strSearch);
        }
    }

    public void printDeviceInfo() {
        String brand = Build.BRAND;
        String model = Build.MODEL;
        String buildId = Build.ID;
        int sdk = Build.VERSION.SDK_INT;
        String manufacture = Build.MANUFACTURER;
        String version_Code = Build.VERSION.RELEASE;

        Log.d(TAG, "printDeviceInfo: Brand = " + brand);
        Log.d(TAG, "printDeviceInfo: Model = " + model);
        Log.d(TAG, "printDeviceInfo: BuildId = " + buildId);
        Log.d(TAG, "printDeviceInfo: SDK = " + sdk);
        Log.d(TAG, "printDeviceInfo: Manufacture = " + manufacture);
        Log.d(TAG, "printDeviceInfo: Version Code = " + version_Code);
    }
}
