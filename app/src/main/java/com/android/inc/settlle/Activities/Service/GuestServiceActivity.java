package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.android.inc.settlle.Activities.SpaceDetailAndBookActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Activities.HomeActivity;
import com.android.inc.settlle.Activities.HomeGuestActivity;
import com.android.inc.settlle.Activities.LoginActivity;
import com.android.inc.settlle.Adapter.GuestServiceAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.FavServiceModel;
import com.android.inc.settlle.RequestModel.SearchServiceModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuestServiceActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtGuest, txtDate, txtCity, txtService;
    private Button btnSearch;
    private ShimmerFrameLayout shimmerFrameLayout;
    private RecyclerView recyclerView;
    private Context context;
    private ImageView imgNoDataFound;
    private GuestServiceAdapter guestServiceAdapter;
    private String strCityId = "", strDate = "", strguestId = "", strServiceId = "";

    private JSONArray dataArray;
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

    private final String[] guestLimitArray = {"0-100", "100-200", "200-300", "300-400", "400-500", "500 & UP"};
    private final String[] guestIdsArray = {"1", "2", "3", "4", "5", "6"};
    private String[] cityIds, cityNames, serviceIds, serviceNames;

    ProgressBar progressBar;
    int page = 1;
    private boolean isPageAvailable;
    //variables for pagination
    private boolean isLoading = true;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private final int view_threshold = 3;

    private static final String TAG = GuestServiceActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_service);
        context = GuestServiceActivity.this;
        initialize();
        initRecycler();
        getIntentData();

    }

    private void getIntentData() {
        ArrayList<String> cityIdList, cityNameList, serviceNameList, serviceIdList;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            cityNameList = bundle.getStringArrayList("cities");
            cityIdList = bundle.getStringArrayList("cityIdList");
            serviceNameList = bundle.getStringArrayList("services");
            serviceIdList = bundle.getStringArrayList("serviceIdList");

            cityIds = cityIdList.toArray(new String[0]);
            cityNames = cityNameList.toArray(new String[0]);
            serviceIds = serviceIdList.toArray(new String[0]);
            serviceNames = serviceNameList.toArray(new String[0]);

            strCityId = bundle.getString("citiId");
            strServiceId = bundle.getString("serviceID");
            strguestId = bundle.getString("guestID");

            String strL = bundle.getString("citiName");
            String strG = bundle.getString("guestCount");
            String strS = bundle.getString("serviceName");

            if (strL != null && strL.length() > 1)
                txtCity.setText(strL);

            if (strG != null && strG.length() > 1)
                txtGuest.setText(strG);

            if (strS != null && strS.length() > 1)
                txtService.setText(strS);


//        Toast.makeText(context, ""+strCityId+ " "+strServiceId , Toast.LENGTH_SHORT).show();

            if (VU.isConnectingToInternet(context)) {
                page = 1;
                getServiceData();
            }

        }

    }

    private void initialize() {
        txtGuest = findViewById(R.id.txt_guest);
        txtService = findViewById(R.id.txt_service);
        txtCity = findViewById(R.id.txt_city);
        txtDate = findViewById(R.id.txt_date);
        btnSearch = findViewById(R.id.guest_service_search);
        imgNoDataFound = findViewById(R.id.img_nodatafound);
        progressBar = findViewById(R.id.progress_bar);

        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);
        shimmerFrameLayout.startShimmerAnimation();
        recyclerView = findViewById(R.id.recycler_guest_service);

        findViewById(R.id.lay1).setOnClickListener(this);
        findViewById(R.id.lay2).setOnClickListener(this);
        findViewById(R.id.lay3).setOnClickListener(this);
        findViewById(R.id.lay4).setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        findViewById(R.id.txt_clear_all_filter).setOnClickListener(this);

        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);


        findViewById(R.id.back_service_search).setOnClickListener(v -> onBackPressed());
    }

    private void initRecycler() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        guestServiceAdapter = new GuestServiceAdapter(context);
        recyclerView.setAdapter(guestServiceAdapter);


        guestServiceAdapter.setOnItemClickListener((View view, int position, JSONArray jsonArray) -> {
            String uni;
            try {
                uni = jsonArray.getJSONObject(position).getString("uni");
                Intent intent = new Intent(context, ServiceDetailsAndBookActivity.class);
                intent.putExtra("uni", uni);
                intent.putExtra("guestCount", txtGuest.getText().toString());
                intent.putExtra("ServiceName", txtService.getText().toString());
                intent.putExtra("guestId", strguestId);
                intent.putExtra("serviceId", strServiceId);
                startActivity(intent);
            } catch (JSONException e) {
                Log.d(TAG, "onItemClick: setOnItemClickListener " + e);
                e.printStackTrace();
            }
        });

        guestServiceAdapter.setOnFavBtnClickListener((view, position, dataArray) -> {
            if (dataArray != null) {
                try {
                    if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin)) {
                        if (VU.isConnectingToInternet(context)) {
                            boolean isFav = dataArray.getJSONObject(position).getBoolean("is_fav");
                            if (!isFav) {
                                addServiceAsFav(dataArray.getJSONObject(position).getString("service_details_id"), view, dataArray, position);
                            } else {
                                deleteFavService(dataArray.getJSONObject(position).getString("service_details_id"), view, dataArray, position);
                            }
                        }
                    } else {
                        sharedPreference();
                        Intent intent = new Intent(context, LoginActivity.class);
                        startActivity(intent);
                    }
                } catch (Exception ignore) {
                }
            }

        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVariableItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (!compleateLoadData)
                    if (dy > 0) {
                        if (isLoading) {
                            if (totalItemCount > privious_total) {
                                isLoading = false;
                                privious_total = totalItemCount;
                            }
                        }
                        if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                            isLoading = true;
                            Log.d(TAG, "onScrolled: " + totalItemCount + " " + visibleItemCount + " " + pastVariableItems + " " + view_threshold);
                            if (isPageAvailable) {
                                page++;
                                progressBar.setVisibility(View.VISIBLE);
                                if (VU.isConnectingToInternet(context)) {
                                    compleateLoadData = true;
                                    getServiceData();
                                }
                            } /*else {
                                Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                            }*/
                        }
                    }
            }
        });
    }

    boolean compleateLoadData = false;

    private void getServiceData() {
        dataArray = new JSONArray();
        if (page == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();
        }
        Log.d(TAG, "getServiceData: ServiceInputData Page=" + page + " Limit =" + Utilities.LIMIT + " strCityId=" + strCityId + " strServiceId=" + strServiceId + " strDate=" + strDate + " strguestId=" + strguestId);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSearchedService(new SearchServiceModel(strCityId, strServiceId, strDate, strguestId, Utilities.getSPstringValue(context, Utilities.spUserId), page, Utilities.LIMIT));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        isPageAvailable = dataObject.getBoolean("count");
                        dataArray = dataObject.getJSONArray("servicelist");
                        Log.d("TAG", " ServiceInputData onResponse: Length " + dataArray.length());
                        if (dataArray.length() > 0) {
                            if (page == 1) {
                                guestServiceAdapter.clearData();
                                guestServiceAdapter.setData(new JSONArray());
                            }
                            recyclerView.setVisibility(View.VISIBLE);
                            imgNoDataFound.setVisibility(View.GONE);
                            guestServiceAdapter.setData(dataArray);
                            compleateLoadData = false;
                        } else {
                            if (page == 1) {
                                recyclerView.setVisibility(View.GONE);
                                imgNoDataFound.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        imgNoDataFound.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    page--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                    btnSearch.setEnabled(true);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                page--;
            }
        });
    }

    private void sharedPreference() {
        Utilities.setSPboolean(context, Utilities.spAtGuestServiceActivity, true);
        Utilities.setSPboolean(context, Utilities.spAtGuestSpaceActivity, false);
        Utilities.setSPboolean(context, Utilities.spAtBookingSpaceActivity, false);
        Utilities.setSPboolean(context, Utilities.spAtBookingServiceActivity, false);
    }

    private void addServiceAsFav(String spaceId, final View view, final JSONArray dataArrayL, final int position) {
        // loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().addFavService(new FavServiceModel(Utilities.getSPstringValue(context, Utilities.spUserId), spaceId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e("api_response", "api_response: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        ImageView imageView = (ImageView) view;
                        imageView.setImageResource(R.drawable.ic_red_heart);
                        dataArrayL.getJSONObject(position).put("is_fav", true);
                        guestServiceAdapter.clearData();
                        guestServiceAdapter.setData(dataArrayL);
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        });
    }

    private void deleteFavService(String spaceId, final View view, final JSONArray dataArrayL, final int position) {
        // loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().deleteFavService(new FavServiceModel(Utilities.getSPstringValue(context, Utilities.spUserId), spaceId, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e("api_response", "api_response: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        ImageView imageView = (ImageView) view;
                        imageView.setImageResource(R.drawable.ic_white_heart);
                        dataArrayL.getJSONObject(position).put("is_fav", false);
                        guestServiceAdapter.clearData();
                        guestServiceAdapter.setData(dataArrayL);
                    } else {
                        Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
            }
        });
    }

    //Bottom shhet code
    @SuppressLint("SetTextI18n")
    private void showBottomSheetDialog(final String[] filterArray, final String type) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_list, null);

        ListView bottom_listview = view.findViewById(R.id.bottom_listview);
        TextView txtheading = view.findViewById(R.id.bottom_heading);
        txtheading.setText("Select " + type);

        List<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        list.addAll(Arrays.asList(filterArray));

        bottom_listview.setAdapter(adapter);
        bottom_listview.setOnItemClickListener((parent, view1, position, id) -> {
            if (type.equalsIgnoreCase("guest count")) {
                txtGuest.setText(filterArray[position]);
                strguestId = guestIdsArray[position];
            } else if (type.equalsIgnoreCase("services")) {
                txtService.setText(filterArray[position]);
                strServiceId = serviceIds[position];
            } else if (type.equalsIgnoreCase("location")) {
                txtCity.setText(filterArray[position]);
                strCityId = cityIds[position];
            }
            mBottomSheetDialog.cancel();
        });


        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }

    public void dialogDatePickerLight() {
        // Get Current Date
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year1, monthOfYear, dayOfMonth) -> {
                    txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year1);
                    strDate = year1 + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
                }, year, month, day);
        //Calendar c = Calendar.getInstance();

        datePickerDialog.setTitle("Select date Of event");
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        calendar.add(Calendar.MONTH, Utilities.MAX_ALLOWED_MONTH);
        datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    @SuppressLint({"NonConstantResourceId", "SetTextI18n"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay1:  //select places
                showBottomSheetDialog(cityNames, "location");
                break;

            case R.id.lay2:  //select service
                showBottomSheetDialog(serviceNames, "services");
                break;

            case R.id.lay3:  //select guest
                showBottomSheetDialog(guestLimitArray, "guest count");
                break;

            case R.id.lay4:  //select date
                dialogDatePickerLight();
                break;

            case R.id.guest_service_search:  //search button click
                if (VU.isConnectingToInternet(context)) {
                    page = 1;
                    guestServiceAdapter.clearData();
                    guestServiceAdapter.setData(new JSONArray());
                    getServiceData();
                }
                break;
            case R.id.txt_clear_all_filter:
                strCityId = "";
                strServiceId = "";
                strDate = "";
                strguestId = "";
                txtGuest.setText("Select guest count");
                txtService.setText("Select services");
                txtCity.setText("Select location");
                txtDate.setText("Select date");
                page = 1;
                guestServiceAdapter.clearData();
                guestServiceAdapter.setData(new JSONArray());
                getServiceData();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Utilities.getSPbooleanValue(context, Utilities.spIsBookingType)) {
            Intent intent;
            if (Utilities.getSPbooleanValue(context, Utilities.spIsLoggedin)) {
                intent = new Intent(context, HomeActivity.class);
            } else {
                intent = new Intent(context, HomeGuestActivity.class);
            }
            startActivity(intent);
            finish();
        } else {
            finish();   //going back to scenarioHomeActivity
        }
    }
}
