package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.Activities.Service.ServiceDetailsAndBookActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class FavourateServiceAdapter extends RecyclerView.Adapter<FavourateServiceAdapter.MyViewHolder> {

    private final Context context;
    private final JSONArray dataArray;
    public RecyclerViewItemClickListener.DelFavListner deleteClickListener;
    private static final String TAG = FavourateServiceAdapter.class.getSimpleName();

    public FavourateServiceAdapter(Context context) {
        this.context = context;
        dataArray = new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_favourate_service_fragment, parent, false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        try {
            myViewHolder.serviceProvider.setText(dataArray.getJSONObject(i).getString("company_name"));
            myViewHolder.serviceType.setText(dataArray.getJSONObject(i).getString("title"));
            myViewHolder.serviceAddress.setText(dataArray.getJSONObject(i).getString("address"));

            String imageName = dataArray.getJSONObject(i).getString("image_name");
            String imageURl = Utilities.IMG_SERVICE_URL + imageName;
            String rating = dataArray.getJSONObject(i).getString("stars");

            if (rating.equalsIgnoreCase("N/A")) {
                rating = "0";
            }
            myViewHolder.ratingBar.setRating(Float.parseFloat(rating));

            if (VU.isConnectingToInternet(context)) {
                Tools.displayImageOriginalString(myViewHolder.servImg, imageURl);
            }

            myViewHolder.viewButton.setOnClickListener(v -> {
                try {
                    Intent intent = new Intent(context, ServiceDetailsAndBookActivity.class);
                    intent.putExtra("uni", dataArray.getJSONObject(i).getString("unique_id"));
                    context.startActivity(intent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            });

            myViewHolder.deleteBtn.setOnClickListener(v -> deleteClickListener.onDeleteClickListner(v, i, dataArray));

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "onBindViewHolder: " + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("company_name", dataArray.getJSONObject(i).getString("company_name"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                jsonObject.put("address", dataArray.getJSONObject(i).getString("address"));
                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("unique_id", dataArray.getJSONObject(i).getString("unique_id"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("fav_id", dataArray.getJSONObject(i).getString("fav_id"));
                jsonObject.put("service_details_id", dataArray.getJSONObject(i).getString("service_details_id"));
                this.dataArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
            e.printStackTrace();
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearData(int position) {
        dataArray.remove(position);
        notifyDataSetChanged();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView servImg;
        TextView serviceProvider, serviceType, serviceAddress;
        AppCompatButton viewButton, deleteBtn;
        RatingBar ratingBar;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.cardview);
            this.servImg = itemView.findViewById(R.id.fav_service_serImg);
            this.serviceProvider = itemView.findViewById(R.id.fav_service_provider);
            this.serviceType = itemView.findViewById(R.id.fav_service_serType);

            this.serviceAddress = itemView.findViewById(R.id.fav_service_address);
            this.viewButton = itemView.findViewById(R.id.fav_service_viewbtn);
            this.deleteBtn = itemView.findViewById(R.id.fav_service_deletebtn);
            this.ratingBar = itemView.findViewById(R.id.service_ratingBar);


        }
    }

    public void setOnDeleteClickListener(RecyclerViewItemClickListener.DelFavListner deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }
}
