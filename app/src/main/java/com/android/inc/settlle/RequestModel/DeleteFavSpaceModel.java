package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class DeleteFavSpaceModel implements Serializable {

    private String  fav_id;
    private String user_id;
    private String space_id;
    private String auth_token;

    public DeleteFavSpaceModel(String fav_id,String user_id, String space_id, String auth_token) {
        this.fav_id = fav_id;
        this.user_id = user_id;
        this.space_id = space_id;
        this.auth_token = auth_token;
    }
}
