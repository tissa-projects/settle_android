package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyServiceBookingListAdapter extends RecyclerView.Adapter<MyServiceBookingListAdapter.MyViewHolder> {
    private RecyclerViewItemClickListener.IViewBookingBtn viewBookingBtn;
    private RecyclerViewItemClickListener.IReviewBtn reviewBtn;
    private final Context context;
    private JSONArray jsonArray;
    private static final String TAG = MySpaceBookingListAdapter.class.getSimpleName();

    public MyServiceBookingListAdapter(@NonNull Context context) {
        this.context = context;
        this.jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_my_booking_service_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint({"Range", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        try {
            final JSONObject jsonObject = jsonArray.getJSONObject(i);
            String statusType;
            float rating;
            String status = jsonObject.getString("chk_status");
            String imgName = jsonObject.getString("image_name");
            String date = jsonObject.getString("startdate");
            String ratingStar = jsonObject.getString("stars");
            String isReviewed = jsonObject.getString("is_reviewed");
            String imgUrl = Utilities.IMG_SERVICE_URL + imgName;

            if (status.equalsIgnoreCase("0")) {
                statusType = "In Review";  //
                myViewHolder.btnReview.setVisibility(View.GONE);
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_inreviewed));
            } else if (status.equalsIgnoreCase("1")) {
                statusType = "Confirmed";
                myViewHolder.btnReview.setVisibility(View.GONE);
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_confirmed));
            } else if (status.equalsIgnoreCase("2")) {
                statusType = "Completed";  // review visible
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_completed));
                if (isReviewed.equalsIgnoreCase("1")) {
                    myViewHolder.btnReview.setText("Reviewed");
                    myViewHolder.btnReview.setVisibility(View.GONE);
                } else {
                    myViewHolder.btnReview.setVisibility(View.VISIBLE);
                }
            } else {
                statusType = "Canceled";
                myViewHolder.btnReview.setVisibility(View.GONE);
                myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.status_cancled));
            }

            if (ratingStar.equalsIgnoreCase("N/A")) {
                rating = Float.parseFloat("0");
            } else {
                rating = Float.parseFloat(ratingStar);
            }

            myViewHolder.txtServiceType.setText(jsonObject.getString("title"));
            myViewHolder.txtBookingDate.setText(date);
            myViewHolder.txtStatus.setText(statusType);
            myViewHolder.txtServiceProvider.setText(jsonObject.getString("company"));
            myViewHolder.serviceRating.setRating(rating);

            Tools.displayImageOriginalString(myViewHolder.imgSpace, imgUrl);
            myViewHolder.btnReview.setOnClickListener(v -> reviewBtn.onReviewClick(v, i, jsonObject));
            myViewHolder.btnViewBooking.setOnClickListener(v -> viewBookingBtn.onViewBookingClick(v, i, jsonObject));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            Log.e(TAG, "onViewBookingClick: " + dataArray);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("image_name", dataArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("chk_status", dataArray.getJSONObject(i).getString("chk_status"));
                jsonObject.put("startdate", dataArray.getJSONObject(i).getString("startdate"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("is_reviewed", dataArray.getJSONObject(i).getString("is_reviewed"));
                jsonObject.put("company", dataArray.getJSONObject(i).getString("company"));
                jsonObject.put("title", dataArray.getJSONObject(i).getString("title"));
                jsonObject.put("uni_id", dataArray.getJSONObject(i).getString("uni_id"));
                jsonObject.put("service_booking_id", dataArray.getJSONObject(i).getString("service_booking_id"));
                jsonObject.put("service_details_id", dataArray.getJSONObject(i).getString("service_details_id"));
                jsonArray.put(jsonObject);
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, "setData: " + e.getMessage());
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView txtServiceType, txtBookingDate, txtStatus, txtServiceProvider;
        ImageView imgSpace;
        AppCompatButton btnViewBooking, btnReview;
        RatingBar serviceRating;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = itemView.findViewById(R.id.cardview);
            this.txtServiceType = itemView.findViewById(R.id.txt_service_type);
            this.txtBookingDate = itemView.findViewById(R.id.txt_booking_date);
            this.txtStatus = itemView.findViewById(R.id.txt_status);
            this.txtServiceProvider = itemView.findViewById(R.id.txt_service_provider_name);
            this.imgSpace = itemView.findViewById(R.id.img_my_booking);
            this.btnViewBooking = itemView.findViewById(R.id.btn_view_booking);
            this.btnReview = itemView.findViewById(R.id.btn_review);
            this.serviceRating = itemView.findViewById(R.id.service_rating);

        }
    }

    //Set method of OnItemClickListener object
    public void setOnViewBookingClickListener(RecyclerViewItemClickListener.IViewBookingBtn viewBookingBtn) {
        this.viewBookingBtn = viewBookingBtn;
    }


    //Set method of OnItemClickListener object
    public void setOnReviewClickListener(RecyclerViewItemClickListener.IReviewBtn reviewBtn) {
        this.reviewBtn = reviewBtn;
    }


    public void clearData() {
        jsonArray = null;
        jsonArray = new JSONArray();
    }


}

