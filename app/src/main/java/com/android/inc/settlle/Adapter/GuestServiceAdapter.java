package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class GuestServiceAdapter extends RecyclerView.Adapter<GuestServiceAdapter.MyViewHolder> {

    private RecyclerViewItemClickListener.IRecyclerViewClick recyclerViewItemClickListener;
    private RecyclerViewItemClickListener.AddFavBtn addFavBtn;
    private final Context context;
    private JSONArray dataArray;

    public GuestServiceAdapter(@NonNull Context context) {
        this.context = context;
        this.dataArray = new JSONArray();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtserviceProviderName, txtServiceType, txtAddress, txtLandMark;
        CardView cardView;
        RatingBar ratingBar;
        ImageView imgService;
        ImageView favImg;
        LinearLayout service_search_item_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtserviceProviderName = itemView.findViewById(R.id.txt_service_provider_name);
            this.txtServiceType = itemView.findViewById(R.id.txt_service_type);
            this.txtAddress = itemView.findViewById(R.id.txt_address);
            this.txtLandMark = itemView.findViewById(R.id.txt_landmark);
            this.cardView = itemView.findViewById(R.id.cardview);
            this.ratingBar = itemView.findViewById(R.id.service_ratingBar);
            this.imgService = itemView.findViewById(R.id.img_service);
            this.favImg = itemView.findViewById(R.id.fav_img);
            this.service_search_item_layout = itemView.findViewById(R.id.service_search_item);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_guest_service_layout, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        try {
            //Hide favourite
            boolean isScenario = Utilities.getSPbooleanValue(context, Utilities.spIsBookingType);
            if (!isScenario) {
                myViewHolder.favImg.setVisibility(View.GONE);
            } else {
                myViewHolder.favImg.setVisibility(View.VISIBLE);
            }

            // Toast.makeText(context, ""+dataArray.toString(), Toast.LENGTH_SHORT).show();
            String providerName = dataArray.getJSONObject(position).getString("company");
            String strAddress = dataArray.getJSONObject(position).getString("my_add");
            String strStar = dataArray.getJSONObject(position).getString("stars");
            String strLandMark = dataArray.getJSONObject(position).getString("landmark");
            String imgName = dataArray.getJSONObject(position).getString("name");
            String serviceType = dataArray.getJSONObject(position).getString("service_name");
            String imgUrl = Utilities.IMG_SERVICE_URL + imgName;

            boolean isFav = dataArray.getJSONObject(position).getBoolean("is_fav");

            if (strStar.equalsIgnoreCase("N/A")) {
                strStar = "0";
            }
            myViewHolder.ratingBar.setRating(Float.parseFloat(strStar));
            myViewHolder.txtserviceProviderName.setText(providerName);
            myViewHolder.txtServiceType.setText(serviceType);
            myViewHolder.txtLandMark.setText(strLandMark);
            myViewHolder.txtAddress.setText(strAddress);
            Tools.displayImageOriginalString(myViewHolder.imgService, imgUrl);
            if (isFav) {
                myViewHolder.favImg.setImageResource(R.drawable.ic_red_heart);
            } else {
                myViewHolder.favImg.setImageResource(R.drawable.ic_white_heart);
            }
            myViewHolder.service_search_item_layout.setOnClickListener(v -> recyclerViewItemClickListener.onCardClickListner(v, position,dataArray));



            myViewHolder.favImg.setOnClickListener(v -> addFavBtn.onAddFavClick(v, position, dataArray));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    //Set method of OnItemClickListener object
    public void setOnItemClickListener(RecyclerViewItemClickListener.IRecyclerViewClick cardClickBtn) {
        this.recyclerViewItemClickListener = cardClickBtn;
    }

    //Set method of OnFavClickListener object
    public void setOnFavBtnClickListener(RecyclerViewItemClickListener.AddFavBtn addFavBtn) {
        this.addFavBtn = addFavBtn;
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray dataArray) {
        try {
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("company", dataArray.getJSONObject(i).getString("company"));
                jsonObject.put("my_add", dataArray.getJSONObject(i).getString("my_add"));
                jsonObject.put("stars", dataArray.getJSONObject(i).getString("stars"));
                jsonObject.put("landmark", dataArray.getJSONObject(i).getString("landmark"));
                jsonObject.put("name", dataArray.getJSONObject(i).getString("name"));
                jsonObject.put("service_name", dataArray.getJSONObject(i).getString("service_name"));
                jsonObject.put("uni", dataArray.getJSONObject(i).getString("uni"));
                jsonObject.put("service_details_id", dataArray.getJSONObject(i).getString("service_details_id"));
                jsonObject.put("is_fav", dataArray.getJSONObject(i).getString("is_fav"));
                this.dataArray.put(jsonObject);
            }
        } catch (Exception e) {
            Log.d("TAG", "ServiceInputData: " + e);
            e.printStackTrace();
        }
        Log.e("TAG", " ServiceInputData onResponse: this " + this.dataArray.length());
        notifyDataSetChanged();
    }


    public void clearData() {
        this.dataArray = null;
        dataArray = new JSONArray();
    }


}
