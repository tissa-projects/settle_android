package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class GuestSpaceAdapter extends RecyclerView.Adapter<GuestSpaceAdapter.MyViewHolder> {

    private RecyclerViewItemClickListener.IRecyclerViewClick cardClickBtn;
    private RecyclerViewItemClickListener.AddFavBtn addFavBtn;
    private final Context context;
    private JSONArray dataArray;

    public GuestSpaceAdapter(@NonNull Context context) {
        this.context = context;
        dataArray = new JSONArray();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_guest_space, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {

        try {
            //Hide favourite
            boolean isScenario = Utilities.getSPbooleanValue(context, Utilities.spIsBookingType);
            if (!isScenario) {
                myViewHolder.favImg.setVisibility(View.GONE);
            } else {
                myViewHolder.favImg.setVisibility(View.VISIBLE);
            }

            // Log.d(TAG, "onBindViewHolder: "+dataArray);


            String strTitle = dataArray.getJSONObject(position).getString("title");
            String strAddress = dataArray.getJSONObject(position).getString("my_add");
            String strStar = dataArray.getJSONObject(position).getString("stars");
            String imgName = dataArray.getJSONObject(position).getString("name");
            String strLandmark = dataArray.getJSONObject(position).getString("landmark");
            String spaceTpye = dataArray.getJSONObject(position).getString("venue_name");
            String imgUrl = Utilities.IMG_SPACE_URL + imgName;

            boolean isFav = dataArray.getJSONObject(position).getBoolean("is_fav");

            if (strStar.equalsIgnoreCase("N/A")) {
                strStar = "0";
            }
            myViewHolder.ratingBar.setRating(Float.parseFloat(strStar));
            myViewHolder.txtDestinationName.setText(strTitle);
            myViewHolder.txtAddress.setText(strAddress);
            myViewHolder.txtLandMark.setText(strLandmark);
            myViewHolder.spaceType.setText(spaceTpye);
            if (imgName.equalsIgnoreCase("N/A")) {
                myViewHolder.imgSpace.setImageResource(R.drawable.background_image);
            } else {
                Tools.displayImageOriginalString(myViewHolder.imgSpace, imgUrl);
            }
            if (isFav) {
                myViewHolder.favImg.setImageResource(R.drawable.ic_red_heart);
            } else {
                myViewHolder.favImg.setImageResource(R.drawable.ic_white_heart);
            }

            myViewHolder.space_search_item_layout.setOnClickListener(v -> cardClickBtn.onCardClickListner(v, position, dataArray));
            myViewHolder.favImg.setOnClickListener(v -> addFavBtn.onAddFavClick(v, position, dataArray));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    public void setDataArray(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("title", jsonArray.getJSONObject(i).getString("title"));
                jsonObject.put("my_add", jsonArray.getJSONObject(i).getString("my_add"));
                jsonObject.put("stars", jsonArray.getJSONObject(i).getString("stars"));
                jsonObject.put("name", jsonArray.getJSONObject(i).getString("name"));
                jsonObject.put("landmark", jsonArray.getJSONObject(i).getString("landmark"));
                jsonObject.put("venue_name", jsonArray.getJSONObject(i).getString("venue_name"));
                jsonObject.put("is_fav", jsonArray.getJSONObject(i).getBoolean("is_fav"));
                jsonObject.put("space_id", jsonArray.getJSONObject(i).getString("space_id"));
                jsonObject.put("uni", jsonArray.getJSONObject(i).getString("uni"));
                this.dataArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("TAG", " ServiceInputData onResponse: this " + this.dataArray.length());
        notifyDataSetChanged();
    }

    public void cleardata() {
        this.dataArray = null;
        dataArray = new JSONArray();
    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtDestinationName, txtAddress, txtLandMark;
        CardView cardView;
        RatingBar ratingBar;
        ImageView imgSpace;
        ImageView favImg;
        TextView spaceType;
        LinearLayout space_search_item_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtDestinationName = itemView.findViewById(R.id.txt_destination_name);
            this.txtAddress = itemView.findViewById(R.id.txt_address);
            this.txtLandMark = itemView.findViewById(R.id.txt_landmark);
            this.cardView = itemView.findViewById(R.id.cardview);
            this.ratingBar = itemView.findViewById(R.id.ratingBar);
            this.imgSpace = itemView.findViewById(R.id.img_space);
            this.favImg = itemView.findViewById(R.id.fav_img);
            this.spaceType = itemView.findViewById(R.id.txt_space_type);
            this.space_search_item_layout = itemView.findViewById(R.id.space_search_item);
        }
    }

    //Set method of OnItemClickListener object
    public void setOnItemClickListener(RecyclerViewItemClickListener.IRecyclerViewClick cardClickBtn) {
        this.cardClickBtn = cardClickBtn;
    }

    //Set method of OnFavClickListener object
    public void setOnFavBtnClickListener(RecyclerViewItemClickListener.AddFavBtn addFavBtn) {
        this.addFavBtn = addFavBtn;
    }
}
