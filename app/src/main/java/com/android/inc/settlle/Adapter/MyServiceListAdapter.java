package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

public class MyServiceListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private RecyclerViewItemClickListener.IViewBtnClickListener iViewItemClickListener;
    private RecyclerViewItemClickListener.IDocumentsBtnClickListener iDocumentsBtnClickListener;
    private RecyclerViewItemClickListener.IEdtBtnClickListener iEdtBtnClickListener;
    //  private OnLoadMoreListener onLoadMoreListener;
    private final Context context;
    private JSONArray dataArray;

    private static final String TAG = MyServiceListAdapter.class.getName();

    public MyServiceListAdapter(Context context) {
        this.context = context;
        dataArray = new JSONArray();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_my_service_list_fragment, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, final int position) {

        if (myViewHolder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) myViewHolder;
            try {
                String imgName = dataArray.getJSONObject(position).getString("image_name");
                Log.e(TAG, "onBindViewHolder:imgName " + imgName);
                String strTitle = dataArray.getJSONObject(position).getString("company_name");  // service provider name
                String strAddress = dataArray.getJSONObject(position).getString("address");
                String strServiceType = dataArray.getJSONObject(position).getString("service_name");  //service name
                String strStatus = dataArray.getJSONObject(position).getString("status");
                String imgUrl = Utilities.IMG_SERVICE_URL + imgName;

                String uniqueID = dataArray.getJSONObject(position).getString("unique_id");
                String serviceID = dataArray.getJSONObject(position).getString("unique_id");

                view.txtAddress.setText(strAddress);
                view.txtTitle.setText(strTitle);
                view.txtServiceType.setText(strServiceType);
                // view.txtPincode.setText(strPinCode);
                Tools.displayImageOriginalString(view.img, imgUrl);
                if (strStatus.equalsIgnoreCase("0")) {
                    view.txtStatus.setTextColor(context.getResources().getColorStateList(R.color.dark_red));
                    view.txtStatus.setText(R.string.Pending);
                } else {
                    view.txtStatus.setTextColor(context.getResources().getColorStateList(R.color.green));
                    view.txtStatus.setText(R.string.Approved);
                }

                // TODO :  btn click check on respective activity or fragment
                view.btnEdit.setOnClickListener(v -> iEdtBtnClickListener.onItemClick(uniqueID, serviceID));
                view.btnView.setOnClickListener(v -> iViewItemClickListener.onItemClick(uniqueID, serviceID));
                view.btnDocuments.setOnClickListener(v -> iDocumentsBtnClickListener.onItemClick(uniqueID, serviceID));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public int getItemCount() {
        return dataArray.length();
    }


    @SuppressLint("NotifyDataSetChanged")
    public void setData(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("image_name", jsonArray.getJSONObject(i).getString("image_name"));
                jsonObject.put("company_name", jsonArray.getJSONObject(i).getString("company_name"));
                jsonObject.put("address", jsonArray.getJSONObject(i).getString("address"));
                jsonObject.put("service_name", jsonArray.getJSONObject(i).getString("service_name"));
                jsonObject.put("status", jsonArray.getJSONObject(i).getString("status"));
                jsonObject.put("unique_id", jsonArray.getJSONObject(i).getString("unique_id"));
                this.dataArray.put(jsonObject);
            }
        } catch (Exception e) {
            CommonFunctions.showLog("CheckDataSize Exception ", e.toString());
            e.printStackTrace();
        }
        CommonFunctions.showLog("CheckDataSize Exception ", " " + dataArray.length());
        notifyDataSetChanged();
    }

    public void clearData() {
        this.dataArray = null;
        this.dataArray = new JSONArray();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        AppCompatButton btnView, btnEdit, btnDocuments;
        ImageView img;
        TextView txtTitle, txtAddress, txtServiceType, txtStatus;


        public MyViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardview);
            btnEdit = itemView.findViewById(R.id.btn_edit_service);
            btnView = itemView.findViewById(R.id.btn_view_service);
            btnDocuments = itemView.findViewById(R.id.btn_documents_service);
            img = itemView.findViewById(R.id.img_recycler_my_service_list);
            txtTitle = itemView.findViewById(R.id.provider_recycler_my_service_list);
            txtAddress = itemView.findViewById(R.id.address_recycler_my_service_list);
            txtServiceType = itemView.findViewById(R.id.service_type);
            txtStatus = itemView.findViewById(R.id.status_recycler_my_service_list);
        }
    }

    public void setOnEditBtnClickListener(RecyclerViewItemClickListener.IEdtBtnClickListener iEdtBtnClickListener) {
        this.iEdtBtnClickListener = iEdtBtnClickListener;
    }

    public void setOnViewBtnClickListener(RecyclerViewItemClickListener.IViewBtnClickListener iViewItemClickListener) {
        this.iViewItemClickListener = iViewItemClickListener;
    }

    public void setOnDocumentsBtnClickListener(RecyclerViewItemClickListener.IDocumentsBtnClickListener iDocumentsBtnClickListener) {
        this.iDocumentsBtnClickListener = iDocumentsBtnClickListener;
    }

}
