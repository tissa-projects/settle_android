package com.android.inc.settlle.AppleLogin;

public class AppleConstants {
// Test
    public static final String CLIENT_ID = "settle.ind.in";
    public static final String REDIRECT_URI = "https://settle.ind.in/";
    public static final String SCOPE = "name%20email";
    public static final String AUTHURL = "https://appleid.apple.com/auth/authorize";
    public static final String TOKENURL = "https://appleid.apple.com/auth/token";

}
