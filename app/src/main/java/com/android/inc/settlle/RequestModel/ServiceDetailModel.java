package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class ServiceDetailModel implements Serializable {

    private String unique_id;
    private String auth_token;

    public ServiceDetailModel(String unique_id, String auth_token) {
        this.unique_id = unique_id;
        this.auth_token = auth_token;
    }
}