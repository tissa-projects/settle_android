package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class getUserDataRequestModel implements Serializable {
    public String email;
    public String mobile;

    public getUserDataRequestModel(String email, String mobile) {
        this.email = email;
        this.mobile = mobile;
    }
}