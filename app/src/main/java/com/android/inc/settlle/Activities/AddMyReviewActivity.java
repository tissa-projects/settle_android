package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AddServiceReviewModel;
import com.android.inc.settlle.RequestModel.SpaceReviewModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMyReviewActivity extends AppCompatActivity {

    private Context context;
    private Dialog dialog;
    private String bookingId, id, review, type;
    private RatingBar ratingBar;
    private EditText edtReview;
    private View layout;


    private static final String TAG = AddMyReviewActivity.class.getSimpleName();

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_my_review);
        context = AddMyReviewActivity.this;
        getIntentData();
        initialize();
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("My Review");

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            bookingId = bundle.getString("bookingId");
            id = bundle.getString("id");
            type = bundle.getString("type");
        }
    }


    private void initialize() {
        ratingBar = findViewById(R.id.RatingBar);
        edtReview = findViewById(R.id.edt_review);

        findViewById(R.id.btn_submit_review).setOnClickListener(v -> {
            if (VU.isConnectingToInternet(context)) {
                if (validation())
                    if (type.equals("space")) {
                        addSpaceReview();
                    } else if (type.equals("service")) {
                        addServiceReview();
                    }
            }
        });
    }

    private boolean validation() {
        if (VU.isEmpty(edtReview)) {
            CustomToast.custom_Toast(context, "Add Your Review", layout);
            return false;
        }
        return true;
    }

    //give a review
    private void addSpaceReview() {

        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String stars = String.valueOf(ratingBar.getRating());
        review = edtReview.getText().toString();
        Log.e(TAG, "setMyReview: stars : " + stars + " review : " + review);

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().addSpaceReview(new SpaceReviewModel(userId, bookingId, id, stars,
                review, auth_token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    int statusCode = Obj.getInt("status_code");
                    String msg = Obj.getString("message");
                    Log.e(TAG, "onResponse: " + statusCode + " " + msg);
                    Toast.makeText(AddMyReviewActivity.this, msg, Toast.LENGTH_SHORT).show();

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        finish();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
    }

    //give a review
    private void addServiceReview() {

        String auth_token = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String stars = String.valueOf(ratingBar.getRating());
        review = edtReview.getText().toString();
        Log.e(TAG, "setMyReview: stars : " + stars + " review : " + review);

        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().addServiceReview(new AddServiceReviewModel(userId, bookingId, id, stars,
                review, auth_token));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject Obj = new JSONObject(api_response);
                    String msg = Obj.getString("message");
                    Log.e(TAG, "onResponse: " + " " + msg);
                    Toast.makeText(AddMyReviewActivity.this, msg, Toast.LENGTH_SHORT).show();
                    int statusCode = Obj.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        finish();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                dialog.dismiss();
            }
        });
    }
}
