package com.android.inc.settlle.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.inc.settlle.Activities.Scenario.ScenarioHomeActivity;
import com.android.inc.settlle.Activities.Scenario.ViewCartActivity;
import com.android.inc.settlle.Activities.Service.MyServiceActivity;
import com.android.inc.settlle.Fragments.HomeFragment;
import com.android.inc.settlle.Menu.DrawerAdapter;
import com.android.inc.settlle.Menu.DrawerItem;
import com.android.inc.settlle.Menu.SimpleItem;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.AuthModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import org.json.JSONObject;
import java.util.Arrays;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener {

    Context context;
    ImageView imageView;
    TextView txtHeading;
    View toolBarView;
    private Dialog loadingDialog;

    boolean doubleBackToExitPressedOnce = false;
    private SlidingRootNav slidingRootNav;
    private static final int POS_HOME = 0;
    private static final int POS_ACCOUNT = 1;
    private static final int POS_SPACE = 2;
    private static final int POS_SERVICE = 3;
    private static final int POS_SCENARIO = 4;
    private static final int POS_LOGOUT = 6;
    private static final int POS_ABOUT_APP = 5;

    private String[] screenTitles;
    private Drawable[] screenIcons;
    private static final String TAG = HomeActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        FacebookSdk.sdkInitialize(getApplicationContext());

        context = HomeActivity.this;
        initialize();
        Log.e(TAG, "onCreate: " + Utilities.getSPstringValue(context, Utilities.spAuthToken));
        //  initToolbar();
        initSideNavigation();
    }

    private void initialize() {
        imageView = findViewById(R.id.imageView);
        txtHeading = findViewById(R.id.txt_heading);
        toolBarView = findViewById(R.id.toolbar_view);
        findViewById(R.id.ll_imageView).setOnClickListener(view -> slidingRootNav.openMenu());

        findViewById(R.id.ll_cart).setOnClickListener(v -> startActivity(new Intent(context, ViewCartActivity.class)));
    }

    private void initSideNavigation() {
        //NAVIGATION
        slidingRootNav = new SlidingRootNavBuilder(this)
                //  .withToolbarMenuToggle((toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(true)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        //   new SpaceItem(48),
        // private DrawerAdapter drawadapter;
        DrawerAdapter drawadapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_HOME).setChecked(true),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_SPACE),
                createItemFor(POS_SERVICE),
                createItemFor(POS_SCENARIO),
                createItemFor(POS_ABOUT_APP),
                //   new SpaceItem(48),
                createItemFor(POS_LOGOUT)));
        drawadapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(drawadapter);

        drawadapter.setSelected(POS_HOME);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public void onItemSelected(int position) {

        switch (position) {
            case POS_HOME:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                Utilities.fragmentTransaction = getSupportFragmentManager().beginTransaction();
                Utilities.fragmentTransaction.replace(R.id.container, new HomeFragment());
                Utilities.fragmentTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                Utilities.fragmentTransaction.commit();
                break;
            case POS_ACCOUNT:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, AccountActivity.class));
                break;
            case POS_SPACE:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, MySpaceActivity.class));
                //startActivity(new Intent(context, EditSpaceImagesActivity.class));
                break;
            case POS_SERVICE:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, MyServiceActivity.class));
                // startActivity(new Intent(context, EditServiceImagesActivity.class));
                break;
           /* case POS_SCENARIO:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, ViewCartActivity.class));
                finish();
                break;*/

            case POS_SCENARIO:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                startActivity(new Intent(context, ScenarioHomeActivity.class));

                break;

            case POS_LOGOUT:
                txtHeading.setVisibility(View.VISIBLE);
                toolBarView.setVisibility(View.VISIBLE);
                dailogLogOut();

                break;

            case POS_ABOUT_APP:
//                 txtHeading.setVisibility(View.VISIBLE);
//                toolBarView.setVisibility(View.VISIBLE);
//                changeFragment(new AboutUsFragment(), "aboutusFragment");
                startActivity(new Intent(context, ShowAboutUS.class));

                //  Toast.makeText(context, "Under development...", Toast.LENGTH_SHORT).show();

                break;
        }
        slidingRootNav.closeMenu();


    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.white))
                .withTextTint(color(R.color.white))
                .withSelectedIconTint(color(R.color.white))
                .withSelectedTextTint(color(R.color.white));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitlesAfterLogin);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIconsAfterLogin);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    private void dailogLogOut() {
        final String logoutType = Utilities.getSPstringValue(context, Utilities.spLoginType);
        Log.e(TAG, "dailogLogOut: " + logoutType);
        final Dialog mBottomSheetDialog = new Dialog(context);
        mBottomSheetDialog.setContentView(R.layout.dialog_logout); // your custom view.
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.show();

        final Button btnYes = mBottomSheetDialog.findViewById(R.id.btn_yes);
        final Button btnNo = mBottomSheetDialog.findViewById(R.id.btn_no);
        btnYes.setOnClickListener(view -> {

            if (VU.isConnectingToInternet(context)) {
                if (logoutType.equalsIgnoreCase("settleLogin")) {
                    logOutRequest();
                } else if (logoutType.equalsIgnoreCase("FaceBookLogin")) {
                    // Toast.makeText(context, "Facebook logout", Toast.LENGTH_SHORT).show();
                    LoginManager.getInstance().logOut();
                    logOutRequest();

                } else if (logoutType.equalsIgnoreCase("googleLogin")) {
                    GooglesignOut();
                }
                mBottomSheetDialog.dismiss();
            }
        });

        btnNo.setOnClickListener(v -> mBottomSheetDialog.dismiss());
    }

    //call  request
    private void logOutRequest() {
        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().logout(new AuthModel(Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                Utilities.setSPstring(context, Utilities.spUserId, "");
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    String statusCode = jsonObject.getString("status_code");
                    Log.e(TAG, "onResponse: " + msg);
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    if (statusCode.equals("401")) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode.equals("200")) {
                        Utilities.setSPboolean(context, Utilities.spIsMobileverify, false);
                        Utilities.setSPboolean(context, Utilities.spIsLoggedin, false);
                        Intent i = new Intent(context, HomeGuestActivity.class);
                        new GraphRequest(
                                AccessToken.getCurrentAccessToken(),
                                "/me/permissions/",
                                null,
                                HttpMethod.DELETE,
                                graphResponse -> {LoginManager.getInstance().logOut();
                                })
                                .executeAsync();

                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void GooglesignOut() {
        if (VU.isConnectingToInternet(context)) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, task -> logOutRequest());
        }


    }

    // for gettting on activity recsult on home fragment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, intent);
        }
    }

}
