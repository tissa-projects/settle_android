package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;

import org.json.JSONArray;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {

    private final Context context;
    private final JSONArray dataArray;
    public ReviewsAdapter(Context context, JSONArray dataArray)
    {
        this.context = context;
        this.dataArray=dataArray;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_reviews, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {

        try {


            String ratingDate = dataArray.getJSONObject(position).getString("created_at");
            String reviewMsg = dataArray.getJSONObject(position).getString("review");
            String raterName =  dataArray.getJSONObject(position).getString("fname")+" "+dataArray.getJSONObject(position).getString("lname");
            String rating = dataArray.getJSONObject(position).getString("stars");
            String profilrImgName = dataArray.getJSONObject(position).getString("profile_image");
            String imgUrl = Utilities.IMG_PROFILE_URL + profilrImgName;


            String delimiter = " ";
            String[] words = ratingDate.split(delimiter);

            myViewHolder.txtDate.setText(""+words[0]);
            myViewHolder.txtReview.setText(""+reviewMsg);
            myViewHolder.txtName.setText(""+raterName);
            myViewHolder.ratingBar.setRating(Float.parseFloat(rating));
            Tools.displayImageOriginalString(myViewHolder.imgProfile,imgUrl);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return dataArray.length();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtDate, txtReview,txtName;
        RatingBar ratingBar;
        CircleImageView imgProfile;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtDate = itemView.findViewById(R.id.txt_date);
            this.txtReview = itemView.findViewById(R.id.txt_review);
            this.txtName = itemView.findViewById(R.id.txt_name);
            this.imgProfile = itemView.findViewById(R.id.profile_image);
            this.ratingBar = itemView.findViewById(R.id.rating_bar);
        }
    }
}
