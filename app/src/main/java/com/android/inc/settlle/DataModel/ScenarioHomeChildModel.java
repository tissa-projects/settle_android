package com.android.inc.settlle.DataModel;

public class ScenarioHomeChildModel {
    private int image;
    private String title;
    private String type;
    private String address;

    public ScenarioHomeChildModel(int image, String title, String type, String address) {
        this.image = image;
        this.title = title;
        this.type = type;
        this.address = address;
    }
}
