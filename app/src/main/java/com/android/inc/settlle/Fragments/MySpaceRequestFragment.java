package com.android.inc.settlle.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.MySpaceRequestAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.SpaceRequestListModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySpaceRequestFragment extends Fragment implements View.OnClickListener
{

    private RecyclerView recyclerView;
    private View view;
    private ShimmerFrameLayout shimmerFrameLayout;
    private Context context;
    MySpaceRequestAdapter mySpaceRequestAdapter;

    ProgressBar progressBar;
    int page = 1;
    private boolean isPageAvailable = false;
    //variables for pagination
    private boolean isLoading = true;
    private int pastVariableItems, visibleItemCount, totalItemCount, privious_total = 0;
    private final int view_threshold = 3;

    private LinearLayout llSpacebtn1, llSpacebtn2, llSpacebtn3;
    private ImageView imgSpace1, imgSpace2, imgSpace3;
    private TextView txtSpace1, txtSpace2, txtSpace3;

    private static final String TAG = MySpaceRequestFragment.class.getSimpleName();
    private TextView filter_all,filter_new_request,filter_confirmed,filter_comopleted,filter_canceled;
    private int filterCode = 0 ;
    ImageView noDataFoundImg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_space_request, container, false);
        context = getActivity();
        initialize();

        return view;
    }

    private void initialize() {
        recyclerView = view.findViewById(R.id.recycler_my_space_request_fragmen);
        shimmerFrameLayout = view.findViewById(R.id.shimmer_view_container_space_request);
        progressBar = view.findViewById(R.id.progress_bar);
        noDataFoundImg =view.findViewById(R.id.img_nodatafound);
        noDataFoundImg.setVisibility(View.GONE);

        filter_all = view.findViewById(R.id.filter_all_request);
        filter_all.setOnClickListener(this);
        filter_new_request = view.findViewById(R.id.filter_new_request);
        filter_new_request.setOnClickListener(this);
        filter_confirmed= view.findViewById(R.id.filter_confirmed);
        filter_confirmed.setOnClickListener(this);
        filter_comopleted = view.findViewById(R.id.filter_completed);
        filter_comopleted.setOnClickListener(this);
        filter_canceled = view.findViewById(R.id.filter_canceled);
        filter_canceled.setOnClickListener(this);

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        mySpaceRequestAdapter = new MySpaceRequestAdapter(context);
        recyclerView.setAdapter(mySpaceRequestAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVariableItems = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (isLoading) {
                        if (totalItemCount > privious_total) {
                            isLoading = false;
                            privious_total = totalItemCount;
                        }
                    }

                    if (!isLoading && (totalItemCount - visibleItemCount) <= (pastVariableItems + view_threshold)) {
                        if (isPageAvailable) {
                            page++;
                            progressBar.setVisibility(View.VISIBLE);
                            if (VU.isConnectingToInternet(context))
                                getBookingRequest();
                        } else {
                            Toast.makeText(context, "No more data found", Toast.LENGTH_SHORT).show();
                        }
                        isLoading = true;
                    }
                }


            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeActivityView();
        spaceBtn3Click();
        if (VU.isConnectingToInternet(context)) {
            page = 1;
            mySpaceRequestAdapter.clearData();
            getBookingRequest();
        }
    }


    private void initializeActivityView(){
        llSpacebtn1 = requireActivity().findViewById(R.id.space_button1);
        llSpacebtn2 = requireActivity().findViewById(R.id.space_button2);
        llSpacebtn3 = requireActivity().findViewById(R.id.space_button3);

        imgSpace1 = requireActivity().findViewById(R.id.img_space1);
        imgSpace2 = requireActivity().findViewById(R.id.img_space2);
        imgSpace3 = requireActivity().findViewById(R.id.img_space3);
        txtSpace1 = requireActivity().findViewById(R.id.txt_space1);
        txtSpace2 = requireActivity().findViewById(R.id.txt_space2);
        txtSpace3 = requireActivity().findViewById(R.id.txt_space3);
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private void spaceBtn3Click(){
        imgSpace3.setImageTintList(getResources().getColorStateList(R.color.colorPrimary));
        txtSpace3.setTextColor(getResources().getColorStateList(R.color.colorPrimary));
        imgSpace2.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtSpace2.setTextColor(getResources().getColorStateList(R.color.grey_60));
        imgSpace1.setImageTintList(getResources().getColorStateList(R.color.grey_60));
        txtSpace1.setTextColor(getResources().getColorStateList(R.color.grey_60));

        llSpacebtn1.setClickable(true);
        llSpacebtn2.setClickable(true);
        llSpacebtn3.setClickable(false);

    }

    // by default getting 5 records
    private void getBookingRequest() {
        if (page == 1) {
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmerAnimation();

        }
        else
        {
            noDataFoundImg.setVisibility(View.GONE);

        }


        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceRequestList(
                new SpaceRequestListModel(Utilities.getSPstringValue(context, Utilities.spUserId),
                        filterCode, Utilities.getSPstringValue(context, Utilities.spAuthToken), page));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                // Stopping Shimmer Effect's animation after data is loaded to ListView
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);

                try {
                    assert response.body() != null;
                    String apiResponse = response.body().string();
                    Log.e("SpaceRequest", "onResponse: " + apiResponse);
                    JSONObject jsonObject = new JSONObject(apiResponse);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else {
                        if (statusCode == 200) {
                            JSONArray arrayObject = jsonObject.getJSONArray("data");
                            isPageAvailable = jsonObject.getBoolean("count");
                            Log.e("SpaceRequest", "onResponse: " + arrayObject);
                            mySpaceRequestAdapter.setData(arrayObject);
                            noDataFoundImg.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        } else  {
                            // no record found
                            if(page==1)
                            {
                                noDataFoundImg.setVisibility(View.VISIBLE);

                            }
                            else
                            {
                                noDataFoundImg.setVisibility(View.GONE);

                            }

                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch: " + e.getMessage());
                    e.printStackTrace();
                    page--;
                } finally {
                    shimmerFrameLayout.setVisibility(View.GONE);
                    shimmerFrameLayout.stopShimmerAnimation();
                    progressBar.setVisibility(View.GONE);
                    isLoading = false;
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                progressBar.setVisibility(View.GONE);
                isLoading = false;
                page--;

            }
        });


    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {

        page=1;
        mySpaceRequestAdapter.clearData();

        switch(v.getId())
        {

            case  R.id.filter_all_request:
                filter_all.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_new_request.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_comopleted.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.white));
                filter_new_request.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_comopleted.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));


                filterCode = 0;
                if (VU.isConnectingToInternet(context)) {
                    getBookingRequest();
                }
                break;

            case  R.id.filter_new_request:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_new_request.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_comopleted.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_new_request.setTextColor(getResources().getColor(R.color.white));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_comopleted.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));


                filterCode = 4;
                if (VU.isConnectingToInternet(context)) {
                    getBookingRequest();
                }
                break;


            case R.id.filter_confirmed:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_new_request.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_comopleted.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_new_request.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.white));
                filter_comopleted.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));
                filterCode = 1;
                if (VU.isConnectingToInternet(context)) {
                    getBookingRequest();
                }
                break;

            case R.id.filter_completed:

                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_new_request.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_comopleted.setBackgroundResource(R.drawable.curve_button_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_border_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_new_request.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_comopleted.setTextColor(getResources().getColor(R.color.white));
                filter_canceled.setTextColor(getResources().getColor(R.color.colorPrimary));

                filterCode = 2;
                if (VU.isConnectingToInternet(context)) {
                    getBookingRequest();
                }
                break;

            case R.id.filter_canceled:
                filter_all.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_new_request.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_confirmed.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_comopleted.setBackgroundResource(R.drawable.curve_border_primary_color);
                filter_canceled.setBackgroundResource(R.drawable.curve_button_primary_color);

                filter_all.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_new_request.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_confirmed.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_comopleted.setTextColor(getResources().getColor(R.color.colorPrimary));
                filter_canceled.setTextColor(getResources().getColor(R.color.white));
                filterCode = 3;
                if (VU.isConnectingToInternet(context)) {
                    getBookingRequest();
                }
                break;
        }
    }
}
