package com.android.inc.settlle.RequestModel;

public class UploadServiceKYCDocModel {

    private String id_proof;
    private String service_proof;
    private String id_proof_extension;
    private String service_proof_extension;
    private String service_id;
    private String user_id;
    private String auth_token;


    public UploadServiceKYCDocModel(String id_proof, String service_proof, String id_proof_extension, String service_proof_extension, String service_id,
                                    String user_id, String auth_token) {
        this.id_proof = id_proof;
        this.service_proof = service_proof;
        this.id_proof_extension = id_proof_extension;
        this.service_proof_extension = service_proof_extension;
        this.service_id = service_id;
        this.user_id = user_id;
        this.auth_token = auth_token;
    }

    public String getId_proof() {
        return id_proof;
    }

    public void setId_proof(String id_proof) {
        this.id_proof = id_proof;
    }

    public String getService_proof() {
        return service_proof;
    }

    public void setService_proof(String service_proof) {
        this.service_proof = service_proof;
    }

    public String getId_proof_extension() {
        return id_proof_extension;
    }

    public void setId_proof_extension(String id_proof_extension) {
        this.id_proof_extension = id_proof_extension;
    }

    public String getService_proof_extension() {
        return service_proof_extension;
    }

    public void setService_proof_extension(String service_proof_extension) {
        this.service_proof_extension = service_proof_extension;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
