package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class DeleteFavServiceModel implements Serializable {

    private String  fav_id;
    private String user_id;
    private String service_id;
    private String auth_token;

    public DeleteFavServiceModel(String  fav_id,String user_id, String service_id, String auth_token) {
        this.fav_id = fav_id;
        this.user_id = user_id;
        this.service_id = service_id;
        this.auth_token = auth_token;
    }
}
