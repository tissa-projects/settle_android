package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.UpdateVendorRequestModel;
import com.android.inc.settlle.RequestModel.UpdateVendorServiceRequestModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyRequestUpdateActivity extends AppCompatActivity implements View.OnClickListener {


    TextView startTimeTxt, endTimeTxt, startDateTxt, endDateTxt, txtheading;
    EditText amountTxt;
    String startTime, endTime, startDate, endDate, amount, bookingId;
    Context context;
    String TAG = MyRequestUpdateActivity.class.getName();
    private Dialog loader;
    int dateType = 0;
    int timeType = 0;
    String type = "";
    private final String[] timeList = {"12 AM", "1 AM", "2 AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM", "12 PM",
            "1 PM", "2 PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM"};

    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;

//    ArrayList guestList,guestIdList;

    String[] guestIds;
    String[] guestNames;
    String guestId;

    TextView guestText;


    Button update, cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_request_update);
        context = MyRequestUpdateActivity.this;

        initialize();
    }

    @SuppressLint("SetTextI18n")
    private void initialize() {

        startDateTxt = findViewById(R.id.request_updte_start_date);
        endDateTxt = findViewById(R.id.request_updte_end_date);
        startTimeTxt = findViewById(R.id.request_updte_start_time);
        endTimeTxt = findViewById(R.id.request_updte_end_time);
        amountTxt = findViewById(R.id.request_updte_amount);
        cancel = findViewById(R.id.request_updte_cancel_button);
        update = findViewById(R.id.request_updte_update_btn);
        guestText = findViewById(R.id.request_updte_guest);

        guestText.setOnClickListener(this);
        startDateTxt.setOnClickListener(this);
        endDateTxt.setOnClickListener(this);
        endTimeTxt.setOnClickListener(this);
        startTimeTxt.setOnClickListener(this);
        cancel.setOnClickListener(this);
        update.setOnClickListener(this);

        View bottom_sheet = findViewById(R.id.bottom_sheet);
        mBehavior = BottomSheetBehavior.from(bottom_sheet);

        guestIds = getResources().getStringArray(R.array.GuestIdList);
        guestNames = getResources().getStringArray(R.array.GuestList);

//
//        guestList = new ArrayList();
//        guestIdList = new ArrayList();
//
//        for (int i = 0 ;i<guestIds.length ;i++){
//            guestList.add(i,guestNames[i]);
//            guestIdList.add(i,guestIds[i]);
//        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            startTime = bundle.getString("startTime");
            endTime = bundle.getString("endTime");
            startDate = bundle.getString("startDate");
            endDate = bundle.getString("endDate");
            amount = bundle.getString("amount");
            bookingId = bundle.getString("bookingId");
            guestId = bundle.getString("guestId");
            type = bundle.getString("type");
            Log.e(TAG, "initialize: GuestId " + guestId);
            guestText.setText("" + guestNames[Integer.parseInt(guestId)]);
        }
        startTimeTxt.setText(startTime);
        endTimeTxt.setText(endTime);
        startDateTxt.setText(startDate);
        endDateTxt.setText(endDate);
        amountTxt.setText(amount);


        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Update Booking Details");
    }


    // update request detail
    private void updateSpaceRequest() {
        startDate = startDateTxt.getText().toString().trim();
        endDate = endDateTxt.getText().toString().trim();
        startTime = startTimeTxt.getText().toString().trim();
        endTime = endTimeTxt.getText().toString().trim();
        amount = amountTxt.getText().toString().trim();
        loader = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateBookingSpaceRequest(new UpdateVendorRequestModel(bookingId, startDate, endDate, startTime, endTime, amount, Utilities.getSPstringValue(context, Utilities.spAuthToken), guestId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onException: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void dialogDatePickerLight() {

        // Get Current Date
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        @SuppressLint("SetTextI18n") DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                (view, year1, monthOfYear, dayOfMonth) -> {
                    if (dateType == 1) {
                        startDateTxt.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year1);
                        endDateTxt.setText("");
                        endDateTxt.setHint("Select date");

                    } else if (dateType == 2) {
                        endDateTxt.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year1);


                    }
                }, year, month, day);

        // Toast.makeText(context, ""+(System.currentTimeMillis()-1000), Toast.LENGTH_SHORT).show();
        if (dateType == 1) {
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        } else {
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
            try {
                Date date = format.parse(startDateTxt.getText().toString());
                datePickerDialog.getDatePicker().setMinDate(date.getTime());

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        if (type.equals("space")) {
            datePickerDialog.setTitle("Select date Of event");

        } else if (type.equals("service")) {
            datePickerDialog.setTitle("Select date of service");

        }
        datePickerDialog.show();
    }

    //Bottom shhet code
    @SuppressLint("SetTextI18n")
    private void showBottomSheetDialog(final String[] listArray) {

        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        @SuppressLint("InflateParams") final View view = getLayoutInflater().inflate(R.layout.bottom_list, null);

        ListView bottom_listview = view.findViewById(R.id.bottom_listview);
        txtheading = view.findViewById(R.id.bottom_heading);
        if (timeType == 1) {
            txtheading.setText("Select start time");

        } else if (timeType == 2) {
            txtheading.setText("Select end time");

        } else if (timeType == 3) {
            txtheading.setText("Select guest limit");

        }

        List<String> list = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        list.addAll(Arrays.asList(listArray));

        bottom_listview.setAdapter(adapter);
        bottom_listview.setOnItemClickListener((parent, view1, position, id) -> {

            if (timeType == 1) {
                startTimeTxt.setText("" + listArray[position]);

            } else if (timeType == 2) {
                endTimeTxt.setText("" + listArray[position]);

            } else if (timeType == 3) {
                guestText.setText("" + listArray[position]);
                guestId = guestIds[position];
                Log.e(TAG, "onItemClick: Guest" + guestId);
            }
            mBottomSheetDialog.cancel();


        });

        mBottomSheetDialog = new BottomSheetDialog(context);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(dialog -> mBottomSheetDialog = null);
    }


    @SuppressLint("NonConstantResourceId")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.request_updte_cancel_button:
                finish();
                break;
            case R.id.request_updte_end_date:
                dateType = 2;
                dialogDatePickerLight();

                break;

            case R.id.request_updte_start_date:
                dateType = 1;
                dialogDatePickerLight();
                break;

            case R.id.request_updte_start_time:
                timeType = 1;
                showBottomSheetDialog(timeList);
                break;

            case R.id.request_updte_end_time:
                timeType = 2;
                showBottomSheetDialog(timeList);
                break;

            case R.id.request_updte_update_btn:
                if (VU.isConnectingToInternet(context)) {
                    if (type.equals("space") && validation()) {
                        updateSpaceRequest();

                    } else if (type.equals("service") && validation()) {
                        updateServiceRequest();

                    }
                }

                break;

            case R.id.request_updte_guest:
                timeType = 3;  //for guest limit list
                showBottomSheetDialog(guestNames);
                break;


        }
    }

    private boolean validation() {
        if (startDateTxt.getText().toString().isEmpty()) {
            Toast.makeText(context, "Select start date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (endDateTxt.getText().toString().isEmpty()) {
            Toast.makeText(context, "Select end date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (amountTxt.getText().toString().isEmpty()) {
            Toast.makeText(context, "Enter booking amount", Toast.LENGTH_SHORT).show();

            return false;
        }
        return true;
    }

    private void updateServiceRequest() {
        startDate = startDateTxt.getText().toString().trim();
        endDate = endDateTxt.getText().toString().trim();
        startTime = startTimeTxt.getText().toString().trim();
        endTime = endTimeTxt.getText().toString().trim();
        amount = amountTxt.getText().toString().trim();


        loader = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateBookingServiceRequest(new UpdateVendorServiceRequestModel(bookingId, startDate, endDate, startTime, endTime, amount, Utilities.getSPstringValue(context, Utilities.spAuthToken), guestId));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "onException: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
            }
        });

    }

}