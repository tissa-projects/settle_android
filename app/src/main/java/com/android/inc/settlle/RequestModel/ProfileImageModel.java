package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class ProfileImageModel implements Serializable {

    public String profile_pic;
    public String auth_token;

    public ProfileImageModel(String profile_pic, String auth_token) {
        this.profile_pic = profile_pic;
        this.auth_token = auth_token;
    }
}
