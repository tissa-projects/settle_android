package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.ReviewsAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.GetSpaceDetailModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SpaceReviewsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Context context;
    private TextView rating, ratingCount;
    private RatingBar starRating;
    private Dialog loadingDialog;
    private String uni, star;
    private ProgressBar progress5star, progress4star, progress3star, progress2star, progress1star;
    private TextView percentage5star, percentage4star, percentage3star, percentage2star, percentage1star;
    private static final String TAG = SpaceReviewsActivity.class.getSimpleName();
    LinearLayout review_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_reviews);
        context = SpaceReviewsActivity.this;

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Reviews");
        getIntentData();
        initialize();
        getSpaceReview();
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            uni = extras.getString("uni");
            star = extras.getString("star");
            Log.e(TAG, "getIntentData: " + uni + " " + star);
        }

    }

    @SuppressLint("SetTextI18n")
    private void initialize() {


        rating = findViewById(R.id.space_ratingText);
        starRating = findViewById(R.id.star_rating);
        ratingCount = findViewById(R.id.space_rating_count);
        progress5star = findViewById(R.id.space_rating_pb5);
        progress4star = findViewById(R.id.space_rating_pb4);
        progress3star = findViewById(R.id.space_rating_pb3);
        progress2star = findViewById(R.id.space_rating_pb2);
        progress1star = findViewById(R.id.space_rating_pb1);

        percentage5star = findViewById(R.id.space_5star_percentage_text);
        percentage4star = findViewById(R.id.space_4star_percentage_text);
        percentage3star = findViewById(R.id.space_3star_percentage_text);
        percentage2star = findViewById(R.id.space_2star_percentage_text);
        percentage1star = findViewById(R.id.space_1star_percentage_text);
        review_layout = findViewById(R.id.review_layout);
        review_layout.setVisibility(View.GONE);


        DecimalFormat df = new DecimalFormat("#.##");
        rating.setText("" + df.format(getIntent().getFloatExtra("rating", 0)));

        recyclerView = findViewById(R.id.recycler_reviews);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void getSpaceReview() {

        loadingDialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceReview(new GetSpaceDetailModel(uni));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    if (jsonObject.getBoolean("status")) {
                        review_layout.setVisibility(View.VISIBLE);
                        //Handle success logic here
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        int count = dataObject.getInt("count");
                        ratingCount.setText(count + " Rating");


                        progress1star.setProgress(getRatingPercentage(dataObject.getInt("1_count"), count));
                        progress2star.setProgress(getRatingPercentage(dataObject.getInt("2_count"), count));
                        progress3star.setProgress(getRatingPercentage(dataObject.getInt("3_count"), count));
                        progress4star.setProgress(getRatingPercentage(dataObject.getInt("4_count"), count));
                        progress5star.setProgress(getRatingPercentage(dataObject.getInt("5_count"), count));

                        percentage1star.setText(getRatingPercentage(dataObject.getInt("1_count"), count) + " %");
                        percentage2star.setText(getRatingPercentage(dataObject.getInt("2_count"), count) + " %");
                        percentage3star.setText(getRatingPercentage(dataObject.getInt("3_count"), count) + " %");
                        percentage4star.setText(getRatingPercentage(dataObject.getInt("4_count"), count) + " %");
                        percentage5star.setText(getRatingPercentage(dataObject.getInt("5_count"), count) + " %");

                        JSONArray arrayObject = dataObject.getJSONArray("review");
                        spaceReviewAdapterCall(arrayObject);
                        Float FstarRating;
                        if (star.equalsIgnoreCase("N/A")) {
                            FstarRating = Float.parseFloat("0");
                        } else {
                            FstarRating = Float.parseFloat(star);
                        }
                        starRating.setRating(FstarRating);
                        rating.setText(String.valueOf(FstarRating));

                    } else {
                        Toast.makeText(SpaceReviewsActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loadingDialog != null) && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void spaceReviewAdapterCall(JSONArray dataArray) {
        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(context, dataArray);
        recyclerView.setAdapter(reviewsAdapter);

    }

    private int getRatingPercentage(int ratingCount, int totalCount) {

        return (ratingCount * 100) / totalCount;

    }


}
