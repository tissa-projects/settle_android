package com.android.inc.settlle.RequestModel;

public class SpaceReviewModel {

    private String  user_id;
    private String  booking_id;
    private String  space_id;
    private String  stars;
    private String  review;
    private String  auth_token;

    public SpaceReviewModel(String user_id, String booking_id, String space_id, String stars, String review, String auth_token) {
        this.user_id = user_id;
        this.booking_id = booking_id;
        this.space_id = space_id;
        this.stars = stars;
        this.review = review;
        this.auth_token = auth_token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getSpace_id() {
        return space_id;
    }

    public void setSpace_id(String space_id) {
        this.space_id = space_id;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
