
package com.android.inc.settlle.RequestModel;

public class EventModel {

    private String eventNameId;
    private String eventPrice;
    private boolean isSelected = false;
    private int position;

    public EventModel(){}

    public EventModel(String eventNameId, String eventPrice,boolean isSelected) {
        this.eventNameId = eventNameId;
        this.eventPrice = eventPrice;
        this.isSelected = isSelected;
    }

    public EventModel(String eventNameId, String eventPrice) {
        this.eventNameId = eventNameId;
        this.eventPrice = eventPrice;
    }

    public String getEventNameId() {
        return eventNameId;
    }

    public void setEventNameId(String eventNameId) {
        this.eventNameId = eventNameId;
    }

    public String getEventPrice() {
        return eventPrice;
    }

    public void setEventPrice(String eventPrice) {
        this.eventPrice = eventPrice;
    }
    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "EventModel{" +
                "eventNameId='" + eventNameId + '\'' +
                ", eventPrice='" + eventPrice + '\'' +
                ", isSelected=" + isSelected +
                ", position=" + position +
                '}';
    }
}