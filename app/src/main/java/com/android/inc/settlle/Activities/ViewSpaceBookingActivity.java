package com.android.inc.settlle.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.BookingRequestDetailModel;
import com.android.inc.settlle.RequestModel.ChangeBookingRequestStatusModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONObject;
import org.w3c.dom.Text;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewSpaceBookingActivity extends AppCompatActivity implements View.OnClickListener {

    private Dialog loader;
    Context context;
    final static String TAG = ViewSpaceBookingActivity.class.getSimpleName();
    String startDate, endDate, startTime, endTime, amount;
    TextView spacenameTextView, bookedByTextView, mobileNoTextView, emailTextView, locationTextView, eventNameTextView,
            noOfGuestTextView, startDateTextView, endDateTextView, startTimeTextView, endTimeTextView, amountTextView, statusTextView;

    ImageView profileView;
    AppCompatButton confirmButton, updateButton, cacelButton;

    String bookingId = null;
    String unique_id = null, type = null;
    String status = null;
    int guestIndex;


    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_space_booking);
        context = ViewSpaceBookingActivity.this;

        initialize();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bookingId = bundle.getString("booking_id");
            unique_id = bundle.getString("unique_id");
            type = bundle.getString("type");
        }

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Booking Details");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (VU.isConnectingToInternet(context)) {
            getBookingRequestDetail(bookingId);
        }
    }

    private void initialize() {
        eventNameTextView = findViewById(R.id.booking_txt_event_name);
        spacenameTextView = findViewById(R.id.booking_space_name);
        bookedByTextView = findViewById(R.id.booking_txt_user_name);
        mobileNoTextView = findViewById(R.id.booking_txt_mobile_no);
        emailTextView = findViewById(R.id.booking_txt_email);
        locationTextView = findViewById(R.id.booking_location);
        noOfGuestTextView = findViewById(R.id.booking_txt_number_of_guest);
        startDateTextView = findViewById(R.id.booking_txt_start_date);
        endDateTextView = findViewById(R.id.booking_txt_end_date);
        startTimeTextView = findViewById(R.id.booking_txt_start_time);
        endTimeTextView = findViewById(R.id.booking_txt_end_time);
        amountTextView = findViewById(R.id.booking_txt_amt);
        profileView = findViewById(R.id.view_space_user_pic);
        statusTextView = findViewById(R.id.booking_txt_status);


        confirmButton = findViewById(R.id.booking_btn_confirm);
        confirmButton.setOnClickListener(this);
        updateButton = findViewById(R.id.booking_btn_update);
        updateButton.setOnClickListener(this);
        cacelButton = findViewById(R.id.booking_btn_cancel);
        cacelButton.setOnClickListener(this);


    }

    private void getBookingRequestDetail(String bookingId) {

        loader = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getSpaceRequestDetail(new BookingRequestDetailModel(Utilities.getSPstringValue(context, Utilities.spAuthToken), bookingId));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();

                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {

                        JSONObject dataObject = jsonObject.getJSONObject("data");

                        startDate = dataObject.getString("startdate");
                        endDate = dataObject.getString("enddate");
                        startTime = dataObject.getString("start_time");
                        endTime = dataObject.getString("end_time");
                        amount = dataObject.getString("amount");

                        eventNameTextView.setText(" : " + dataObject.getString("event_name"));
                        spacenameTextView.setText(dataObject.getString("title"));
                        bookedByTextView.setText(dataObject.getString("fname") + " " + dataObject.getString("lname"));
                        mobileNoTextView.setText( dataObject.getString("mobileno"));
                        emailTextView.setText(dataObject.getString("email_id"));
                        locationTextView.setText( dataObject.getString("address"));
                        noOfGuestTextView.setText( dataObject.getString("guest"));
                        String guestIndexStr = dataObject.getString("guest");
                        guestIndex = Integer.parseInt(guestIndexStr) - 1;
                        final String[] guestList = getResources().getStringArray(R.array.GuestList);
                        noOfGuestTextView.setText( guestList[guestIndex]);
                        startDateTextView.setText( startDate);
                        endDateTextView.setText( endDate);
                        startTimeTextView.setText(startTime);
                        endTimeTextView.setText( endTime);

                        amountTextView.setText(CommonFunctions.formatNumber(Long.parseLong(amount.split("\\.")[0])));

                       // amountTextView.setText(" : " + amount);
                        Tools.displaySmallImageOriginalString(profileView, Utilities.IMG_PROFILE_URL + dataObject.getString("profile_image"));
                        status = dataObject.getString("status");
                        // Toast.makeText(ViewSpaceBookingActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                        switch (status) {
                            case "0":
                                statusTextView.setText("New Request");
                                statusTextView.setTextColor(Color.RED);
                                break;
                            case "1":
                                statusTextView.setText("Confirmed");
                                confirmButton.setText("Complete");
                                statusTextView.setTextColor(Color.parseColor("#ff9500"));
                                break;
                            case "2":
                                statusTextView.setText("Completed");
                                confirmButton.setText("Completed");
                                disableButtons();
                                statusTextView.setTextColor(Color.parseColor("#0b5711"));
                                break;
                            default:
                                statusTextView.setText("Canceled");
                                confirmButton.setText("Canceled");
                                disableButtons();
                                statusTextView.setTextColor(Color.BLACK);
                                break;
                        }
                    } else {
                        Toast.makeText(ViewSpaceBookingActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onException: " + e.getMessage());
                    Toast.makeText(ViewSpaceBookingActivity.this, "Please try again" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
                Toast.makeText(ViewSpaceBookingActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        if (VU.isConnectingToInternet(context)) {

            switch (v.getId()) {
                case R.id.booking_btn_confirm:
                    if (status.equals("0")) {
                        status = "1";
                        statusChangeAlert("Are you sure want to confirm booking request?");
                    } else if (status.equals("1")) {
                        status = "2";
                        statusChangeAlert("Are you sure want to confirm booking request?");
                    }
                    break;

                case R.id.booking_btn_update:
                    Intent intent = new Intent(context, MyRequestUpdateActivity.class);
                    intent.putExtra("startDate", startDate);
                    intent.putExtra("endDate", endDate);
                    intent.putExtra("startTime", startTime);
                    intent.putExtra("endTime", endTime);
                    intent.putExtra("amount", amount);
                    intent.putExtra("bookingId", bookingId);
                    intent.putExtra("type", type);
                    final String[] guestList = getResources().getStringArray(R.array.GuestIdList);
                    intent.putExtra("guestId", guestList[guestIndex]);
                    startActivity(intent);
                    break;

                case R.id.booking_btn_cancel:
                    if (VU.isConnectingToInternet(context)) {
                        status = "3";
                        statusChangeAlert("Are you sure want to cancel booking request?");
                    }
                    break;
            }
        }
    }


    private void chnageRequeststatus() {
        loader = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().changeBookingRequestStatus(new ChangeBookingRequestStatusModel(bookingId, status, unique_id, type, Utilities.getSPstringValue(context, Utilities.spAuthToken)));
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loader.dismiss();

                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");

                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Toast.makeText(ViewSpaceBookingActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        switch (status) {
                            case "0":
                                statusTextView.setText("New Request");
                                statusTextView.setTextColor(Color.RED);
                                break;
                            case "1":
                                statusTextView.setText("Confirmed");
                                confirmButton.setText("Complete");
                                statusTextView.setTextColor(Color.parseColor("#ff9500"));
                                break;
                            case "2":
                                statusTextView.setText("Completed");
                                confirmButton.setText("Completed");
                                disableButtons();
                                statusTextView.setTextColor(Color.parseColor("#0b5711"));
                                break;
                            default:
                                statusTextView.setText("Canceled");
                                confirmButton.setText("Canceled");
                                disableButtons();
                                statusTextView.setTextColor(Color.BLACK);
                                break;
                        }
                    } else {
                        Toast.makeText(ViewSpaceBookingActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onException: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                if ((loader != null) && loader.isShowing()) {
                    loader.dismiss();
                }
            }
        });
    }

    private void disableButtons() {
        updateButton.setEnabled(false);
        cacelButton.setEnabled(false);
        confirmButton.setEnabled(false);
        confirmButton.setAlpha((float) 0.4);
        updateButton.setAlpha((float) 0.4);
        cacelButton.setAlpha((float) 0.4);

    }

    private void statusChangeAlert(String title) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        // builder1.setMessage(title);
        builder1.setTitle(title);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> chnageRequeststatus());

        builder1.setNegativeButton(
                "No",
                (dialog, id) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }
}
