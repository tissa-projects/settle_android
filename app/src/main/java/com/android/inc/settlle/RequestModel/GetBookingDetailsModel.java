package com.android.inc.settlle.RequestModel;

public class GetBookingDetailsModel {

    private String unique_id;

    public GetBookingDetailsModel(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }
}
