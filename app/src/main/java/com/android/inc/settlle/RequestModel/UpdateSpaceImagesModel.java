package com.android.inc.settlle.RequestModel;

import java.util.ArrayList;
import java.util.List;

public class UpdateSpaceImagesModel {

    private String user_id;
    private String space_id;
    private String auth_token;
    private List<String>  space_image;

    public UpdateSpaceImagesModel(String user_id, String space_id, String auth_token, List<String> space_image) {
        this.user_id = user_id;
        this.space_id = space_id;
        this.auth_token = auth_token;
        this.space_image = space_image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSpace_id() {
        return space_id;
    }

    public void setSpace_id(String space_id) {
        this.space_id = space_id;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public List<String> getSpace_image() {
        return space_image;
    }

    public void setSpace_image(ArrayList<String> space_image) {
        this.space_image = space_image;
    }
}
