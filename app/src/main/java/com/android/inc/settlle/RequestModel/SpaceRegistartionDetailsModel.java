package com.android.inc.settlle.RequestModel;

import java.io.Serializable;
import java.util.ArrayList;

public class SpaceRegistartionDetailsModel implements Serializable {
    public String space_title;
    public String phone;
    public ArrayList<String> venue_id_list;
    public ArrayList<String> venue_name_list;
    public ArrayList<String> subscription_id_list;
    public String user_id;
    public String guest;
    public String country;
    public String state;
    public String city;
    public String address;
    public String latitude;
    public String longitude;
    public String zip_code;
    public String landmark;
    public String accommodate;
    public String description;
    public String discount;
    public String price_type;
    public String from_time;
    public String to_time;
    public String auth_token;
    public ArrayList<Integer> days;
    public ArrayList<String> event;
    public ArrayList<String> event_price;
    public ArrayList<String> amenities;
    public ArrayList<String> rules;
    public ArrayList<String> space_event_id;
    public String unique_id;
    public String space_id;
    public String venue_id;
    public String venue_name;
    public String subscription_id;

    //add space
    public SpaceRegistartionDetailsModel(String space_title, String phone, ArrayList<String> venue_id,ArrayList<String> venue_name,
                                         ArrayList<String> subscription_id, String user_id, String guest, String country, String state, String city, String address, String latitude, String longitude,
                                         String zip_code, String landmark, String accommodate, String description, String discount, String price_type,
                                         String from_time, String to_time, String auth_token, ArrayList<Integer> days, ArrayList<String> event,
                                         ArrayList<String> event_price, ArrayList<String> amenities, ArrayList<String> rules) {
        this.space_title = space_title;
        this.phone = phone;
        this.venue_id_list = venue_id;
        this.venue_name_list = venue_name;
        this.subscription_id_list = subscription_id;
        this.user_id = user_id;
        this.guest = guest;
        this.country = country;
        this.state = state;
        this.city = city;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.zip_code = zip_code;
        this.landmark = landmark;
        this.accommodate = accommodate;
        this.description = description;
        this.discount = discount;
        this.price_type = price_type;
        this.from_time = from_time;
        this.to_time = to_time;
        this.auth_token = auth_token;
        this.days = days;
        this.event = event;
        this.event_price = event_price;
        this.amenities = amenities;
        this.rules = rules;
    }

    // edit space
    public SpaceRegistartionDetailsModel(String space_title, String phone, String  venue_id,String venue_name, String  subscription_id,
                                         String user_id, String guest, String country, String state, String city,
                                         String address, String latitude, String longitude, String zip_code, String landmark, String accommodate,
                                         String description, String discount, String price_type, String from_time, String to_time, String auth_token,
                                         ArrayList<Integer> days, ArrayList<String> event, ArrayList<String> event_price, ArrayList<String> amenities,
                                         ArrayList<String> rules, ArrayList<String> space_event_id, String uniqueId, String spaceId) {
        this.space_title = space_title;
        this.phone = phone;
        this.venue_id = venue_id;
        this.venue_name = venue_name;
        this.subscription_id = subscription_id;
        this.user_id = user_id;
        this.guest = guest;
        this.country = country;
        this.state = state;
        this.city = city;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.zip_code = zip_code;
        this.landmark = landmark;
        this.accommodate = accommodate;
        this.description = description;
        this.discount = discount;
        this.price_type = price_type;
        this.from_time = from_time;
        this.to_time = to_time;
        this.auth_token = auth_token;
        this.days = days;
        this.event = event;
        this.event_price = event_price;
        this.amenities = amenities;
        this.rules = rules;
        this.space_event_id = space_event_id;
        unique_id = uniqueId;
        space_id = spaceId;
    }
}
       /* "space_event_id":[272,273,274],
        "event":[2,3,4],
        "event_price":[2000,3000,4000],*/
