package com.android.inc.settlle.Activities.Service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.StateCityModel;
import com.android.inc.settlle.RequestModel.UserAuthModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.CustomToast;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceRegistrationDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtAddServiceProvider, edtZipCOde, edtLandMark, edtDiscount;
    private TextView txtCountry, txtState, txtCity, txtAddress, txtFromTime, txtToTime, txtAddServiceType, txtNoOfGuest;
    private Context context;
    private CheckBox checkboxSelectAll, checkboxMon, checkboxTue, checkboxWed, checkboxThur, checkboxFri, checkboxSat, checkboxSun;
    private Dialog ServiceRegistrationDialog;
    private String strPriceType = null;
    private String strGuestId;
    private String strCountryId;
    private String strStateId;
    private String strCityId;
    private ArrayList<String> cityNameList = null, cityIdList = null, stateNameList = null, stateIdList = null,
            strServiceNameList, strServiceIdList, strSubscriptionIdList, selectedServiceNameList, selectServiceIdList, selectedSubscriptionIdList;
    private final HashMap<Integer, Integer> daysIds = new HashMap<>();
    private static final String TAG = ServiceRegistrationDetailsActivity.class.getSimpleName();
    private static final int AUTOCOMPLETE_REQUEST_CODE = 200;
    private View layout;
    private double lat, lng;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_registration_details);

        context = ServiceRegistrationDetailsActivity.this;
        layout = getLayoutInflater().inflate(R.layout.simple_custom_toast, findViewById(R.id.custom_toast_layout_id));
        strCountryId = "1";   // india country id fixed
        initialize();
        getIntentData();
        String apiKey = getResources().getString(R.string.Api_Key);
        Utilities.guestLimitList = new ArrayList<>(Arrays.asList(Utilities.guestLimitArray));
        Utilities.guestLimitIdList = new ArrayList<>(Arrays.asList(Utilities.guestLimitIdArray));

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }
        // Create a new Places client instance.
        //  PlacesClient placesClient = Places.createClient(this);

        if (VU.isConnectingToInternet(context)) {
            getAlreadySubscribedServiceList();
        }
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String strServiceName = extras.getString("serviceType");
            // String strServiceId = extras.getString("serviceId");
            txtAddServiceType.setText(strServiceName);
        }
    }

    @SuppressLint("SetTextI18n")
    private void initialize() {
        edtAddServiceProvider = findViewById(R.id.edt_service_provider);
        txtNoOfGuest = findViewById(R.id.txt_service_no_of_guest);
        edtZipCOde = findViewById(R.id.edt_service_zip_code);
        edtLandMark = findViewById(R.id.edt_service_land_mark);
        edtDiscount = findViewById(R.id.edt_service_discount);
        RadioButton radiobtnFixed = findViewById(R.id.radiobtn_service_fixed);
        RadioButton radiobtnNegotiable = findViewById(R.id.radiobtn_service_negotiable);

        txtAddServiceType = findViewById(R.id.txt_service_type);
        txtCountry = findViewById(R.id.txt_service_country);
        txtState = findViewById(R.id.txt_service_state);
        txtCity = findViewById(R.id.txt_service_city);
        txtAddress = findViewById(R.id.txt_service_address);
        txtFromTime = findViewById(R.id.txt_service_from_time);
        txtToTime = findViewById(R.id.txt_service_to_time);

       /* radiobtnFixed = findViewById(R.id.radiobtn_service_fixed);
        radiobtnNegotiable = findViewById(R.id.radiobtn_service_negotiable);*/

        checkboxSelectAll = findViewById(R.id.check_box_select_all);
        checkboxMon = findViewById(R.id.check_box_monday);
        checkboxTue = findViewById(R.id.check_box_tuesday);
        checkboxWed = findViewById(R.id.check_box_wednesday);
        checkboxThur = findViewById(R.id.check_box_thrusday);
        checkboxFri = findViewById(R.id.check_box_friday);
        checkboxSat = findViewById(R.id.check_box_saturday);
        checkboxSun = findViewById(R.id.check_box_sunday);

        findViewById(R.id.btn_next_service_registration).setOnClickListener(this);
        txtNoOfGuest.setOnClickListener(this);
        findViewById(R.id.ll_state).setOnClickListener(this);
        findViewById(R.id.ll_city).setOnClickListener(this);
        findViewById(R.id.ll_from_time).setOnClickListener(this);
        findViewById(R.id.ll_to_time).setOnClickListener(this);
        radiobtnNegotiable.setOnClickListener(this);
        radiobtnFixed.setOnClickListener(this);
        checkboxSelectAll.setOnClickListener(this);
        checkboxMon.setOnClickListener(this);
        checkboxTue.setOnClickListener(this);
        checkboxThur.setOnClickListener(this);
        checkboxWed.setOnClickListener(this);
        checkboxFri.setOnClickListener(this);
        checkboxSat.setOnClickListener(this);
        checkboxSun.setOnClickListener(this);
        txtAddress.setOnClickListener(this);
        txtAddServiceType.setOnClickListener(this);

        txtCountry.setText("India");

        selectedServiceNameList = new ArrayList<>();
        selectServiceIdList = new ArrayList<>();
        selectedSubscriptionIdList = new ArrayList<>();

        strServiceNameList = new ArrayList<>();
        strServiceIdList = new ArrayList<>();
        strSubscriptionIdList = new ArrayList<>();

        findViewById(R.id.imgBack).setOnClickListener(v -> finish());
        TextView h=findViewById(R.id.txtHeading);
        h.setText("Add Service Details");

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        ArrayList<Integer> days = new ArrayList<>();
        switch (v.getId()) {
            case R.id.txt_service_type:
                selectServiceDialog();
                break;

            case R.id.btn_next_service_registration:
                if (validation()) {
                    Set<Integer> keys = daysIds.keySet();
                    days.addAll(keys);
                    String strlat = String.valueOf(lat);
                    String strLng = String.valueOf(lng);

                    Log.e(TAG, "onClick: strlat: " + strlat + " strLng: " + strLng);

                    Intent i = new Intent(context, AddServicePackageActivity.class);
                    i.putExtra("serviceProvider", edtAddServiceProvider.getText().toString());
                    i.putStringArrayListExtra("service_id", selectServiceIdList);
                    i.putStringArrayListExtra("subscription_ids", selectedSubscriptionIdList);
                    i.putStringArrayListExtra("service_name_list", selectedServiceNameList);


                    i.putExtra("guest", strGuestId);
                    i.putExtra("country", strCountryId);
                    i.putExtra("state", strStateId);
                    i.putExtra("city", strCityId);
                    i.putExtra("address", txtAddress.getText().toString());
                    i.putExtra("latitude", strlat);
                    i.putExtra("longitude", strLng);
                    i.putExtra("zip_code", edtZipCOde.getText().toString());
                    i.putExtra("landmark", edtLandMark.getText().toString());
                    i.putExtra("discount", edtDiscount.getText().toString());
                    i.putExtra("price_type", strPriceType);
                    i.putExtra("from_time", txtFromTime.getText().toString());
                    i.putExtra("to_time", txtToTime.getText().toString());
                    i.putIntegerArrayListExtra("daysId", days);
                    startActivity(i);
                }
                break;

            case R.id.txt_service_no_of_guest:
                setNoOfGuest();
                break;

            case R.id.txt_service_address:
                if (txtCity != null && txtCity.getText().toString().trim().length() > 1)
                    onSearchCalled();
                else
                    CustomToast.custom_Toast(context, "Please First Select City", layout);
                break;

            case R.id.ll_city:
                if (txtState.getText().toString().equalsIgnoreCase("") || txtState.getText().toString().equalsIgnoreCase("null")) {
                    CustomToast.custom_Toast(context, "Please First Select State", layout);
                } else {
                    getCityData();
                }
                break;

            case R.id.ll_state:
                getStateData();
                break;

            case R.id.ll_from_time:
                setTime("fromTime");
                break;

            case R.id.ll_to_time:
                setTime("toTime");
                break;

            case R.id.radiobtn_service_fixed:
                strPriceType = "fixed";
                break;

            case R.id.radiobtn_service_negotiable:
                strPriceType = "negotiable";
                break;

            case R.id.check_box_select_all:
                if (checkboxSelectAll.isChecked()) {
                    checkboxSelectAll.setChecked(true);
                    checkboxMon.setChecked(true);
                    checkboxTue.setChecked(true);
                    checkboxWed.setChecked(true);
                    checkboxThur.setChecked(true);
                    checkboxFri.setChecked(true);
                    checkboxSat.setChecked(true);
                    checkboxSun.setChecked(true);
                    daysIds.put(1, 1);
                    daysIds.put(2, 2);
                    daysIds.put(3, 3);
                    daysIds.put(4, 4);
                    daysIds.put(5, 5);
                    daysIds.put(6, 6);
                    daysIds.put(7, 7);

                } else {
                    checkboxSelectAll.setChecked(false);
                    checkboxMon.setChecked(false);
                    checkboxTue.setChecked(false);
                    checkboxWed.setChecked(false);
                    checkboxThur.setChecked(false);
                    checkboxFri.setChecked(false);
                    checkboxSat.setChecked(false);
                    checkboxSun.setChecked(false);
                    daysIds.remove(1);
                    daysIds.remove(2);
                    daysIds.remove(3);
                    daysIds.remove(4);
                    daysIds.remove(5);
                    daysIds.remove(6);
                    daysIds.remove(7);
                }

                break;

            case R.id.check_box_monday:
                if (checkboxMon.isChecked()) {
                    checkboxMon.setChecked(true);
                    if (!daysIds.containsKey(1)) {
                        daysIds.put(1, 1);
                    }
                } else {
                    checkboxMon.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(1);
                }
                break;
            case R.id.check_box_tuesday:
                if (checkboxTue.isChecked()) {
                    checkboxTue.setChecked(true);
                    if (!daysIds.containsKey(2)) {
                        daysIds.put(2, 2);
                    }
                } else {
                    checkboxTue.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(2);
                }
                break;
            case R.id.check_box_wednesday:
                if (checkboxWed.isChecked()) {
                    checkboxWed.setChecked(true);
                    if (!daysIds.containsKey(3)) {
                        daysIds.put(3, 3);
                    }
                } else {
                    checkboxWed.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(3);
                }
                break;
            case R.id.check_box_thrusday:
                if (checkboxThur.isChecked()) {
                    checkboxThur.setChecked(true);
                    if (!daysIds.containsKey(4)) {
                        daysIds.put(4, 4);
                    }
                } else {
                    checkboxThur.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(4);
                }
                break;
            case R.id.check_box_friday:
                if (checkboxFri.isChecked()) {
                    checkboxFri.setChecked(true);
                    if (!daysIds.containsKey(5)) {
                        daysIds.put(5, 5);
                    }
                } else {
                    checkboxFri.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(5);
                }
                break;
            case R.id.check_box_saturday:
                if (checkboxSat.isChecked()) {
                    checkboxSat.setChecked(true);
                    if (!daysIds.containsKey(6)) {
                        daysIds.put(6, 6);
                    }
                } else {
                    checkboxSat.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(6);
                }
                break;
            case R.id.check_box_sunday:
                if (checkboxSun.isChecked()) {
                    checkboxSun.setChecked(true);
                    if (!daysIds.containsKey(7)) {
                        daysIds.put(7, 7);
                    }
                } else {
                    checkboxSun.setChecked(false);
                    checkboxSelectAll.setChecked(false);
                    daysIds.remove(7);
                }
                break;


        }
    }


    //get state response
    private void getStateData() {
        stateNameList = new ArrayList<>();
        stateIdList = new ArrayList<>();
        ServiceRegistrationDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getStateData(new StateCityModel(strCountryId));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                ServiceRegistrationDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String strStatusMsg = jsonObject.getString("message");
                    if (statusCode == 200) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            stateNameList.add(dataArray.getJSONObject(i).getString("location_name"));
                            stateIdList.add(dataArray.getJSONObject(i).getString("location_id"));
                        }
                        // state dialog call
                        setState();
                    } else {
                        Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    ServiceRegistrationDialog.dismiss();
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                ServiceRegistrationDialog.dismiss();
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //get City Data
    private void getCityData() {
        cityNameList = new ArrayList<>();
        cityIdList = new ArrayList<>();
        ServiceRegistrationDialog = ProgressDialog.show(context, "Please wait", "Loading...");
        Log.e(TAG, "getCityData: " + strStateId);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getCityData(new StateCityModel(strStateId));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                ServiceRegistrationDialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String strStatusMsg = jsonObject.getString("message");
                    if (statusCode == 200) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            cityNameList.add(dataArray.getJSONObject(i).getString("location_name"));
                            cityIdList.add(dataArray.getJSONObject(i).getString("location_id"));
                        }
                        //city dialog called
                        setCity();
                    } else {
                        Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                ServiceRegistrationDialog.dismiss();
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // No Of Guest dialog
    private void setNoOfGuest() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Guest");
        // add a list
        builder.setItems(Utilities.GetStringArray(Utilities.guestLimitList), (dialog, position) -> {
            txtNoOfGuest.setText(Utilities.guestLimitList.get(position));
            strGuestId = Utilities.guestLimitIdList.get(position);
            dialog.dismiss();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    //state dialog
    private void setState() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select State");
        // add a list
        builder.setItems(Utilities.GetStringArray(stateNameList), (dialog, position) -> {
            txtState.setText(stateNameList.get(position));
            strStateId = stateIdList.get(position);
            dialog.dismiss();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    //city dialog
    private void setCity() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select City");
        // add a list
        builder.setItems(Utilities.GetStringArray(cityNameList), (dialog, position) -> {
            txtCity.setText(cityNameList.get(position));
            strCityId = cityIdList.get(position);
            dialog.dismiss();
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private boolean validation() {
        if (VU.isEmpty(edtAddServiceProvider)) {
            CustomToast.custom_Toast(context, "Add Service Provider", layout);
            return false;
        } else if (VU.isTxtEmpty(txtAddServiceType)) {
            CustomToast.custom_Toast(context, "Add Service Type", layout);
            return false;
        } else if (VU.isTxtEmpty(txtNoOfGuest)) {
            CustomToast.custom_Toast(context, "Select Number Of Guest", layout);
            return false;
        } else if (VU.isTxtEmpty(txtCountry)) {
            CustomToast.custom_Toast(context, "Select Country", layout);
            return false;
        } else if (VU.isTxtEmpty(txtState)) {
            CustomToast.custom_Toast(context, "Select State", layout);
            return false;
        } else if (VU.isTxtEmpty(txtCity)) {
            CustomToast.custom_Toast(context, "Select City", layout);
            return false;
        } /*else if (VU.isTxtEmpty(txtAddress)) {
            CustomToast.custom_Toast(context, "Add Address", layout);
            return false;
        }*/ else if (VU.isEmpty(edtZipCOde)) {
            CustomToast.custom_Toast(context, "Add ZipCode", layout);
            return false;
        } else if (VU.isEmpty(edtLandMark)) {
            CustomToast.custom_Toast(context, "Add LandMark", layout);
            return false;
        } else if (strPriceType == null) {
            CustomToast.custom_Toast(context, "Select Price Type", layout);
            return false;
        } else if ((!checkboxMon.isChecked()) && (!checkboxTue.isChecked()) &&
                (!checkboxWed.isChecked()) && (!checkboxThur.isChecked()) &&
                (!checkboxFri.isChecked()) && (!checkboxSat.isChecked()) && (!checkboxSun.isChecked())) {
            CustomToast.custom_Toast(context, "Select Days", layout);
            return false;

        } else if (VU.isTxtEmpty(txtFromTime)) {
            CustomToast.custom_Toast(context, "Add Start Time", layout);
            return false;
        } else if (VU.isTxtEmpty(txtToTime)) {
            CustomToast.custom_Toast(context, "Add End Time", layout);
            return false;
        }
        return true;
    }


    //search google address
    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN") //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = Autocomplete.getPlaceFromIntent(data);
                 String address = place.getAddress();
                LatLng latlng = place.getLatLng();
                if (latlng != null) {
                    lat = latlng.latitude;
                    lng = latlng.longitude;
                }
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                List<Address> addresses;
                String city;
                try {
                    addresses = geocoder.getFromLocation(Objects.requireNonNull(place.getLatLng()).latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    city = addresses.get(0).getLocality();
                } catch (IOException e) {
                    city = txtCity.getText().toString().trim();
                    e.printStackTrace();
                }
                CommonFunctions.showLog("TAG", city);
                txtAddress.setText(address);
                /*if (txtCity.getText().toString().trim().equalsIgnoreCase(city)) {
                    txtAddress.setText(address);
                } else {
                    CustomToast.custom_Toast(context, "Please select correct city name", layout);
                }*/
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                assert data != null;
                Status status = Autocomplete.getStatusFromIntent(data);
                Toast.makeText(context, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG).show();
                Log.i(TAG, status.getStatusMessage());
            }
        }
    }


    private void setTime(final String type) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, (timePicker, selectedHour, selectedMinute) -> {
            //    String time = selectedHour + ":" + selectedMinute;
            String time = String.valueOf(selectedHour);
            convertTO12Hrs(type, time);
        }, hour, minute, false);//no 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void convertTO12Hrs(final String type, String time) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H", Locale.ENGLISH);
            final Date dateObj = sdf.parse(time);
            assert dateObj != null;
            String newTime = new SimpleDateFormat("K aa ", Locale.ENGLISH).format(dateObj);
            if (type.equalsIgnoreCase("fromTime")) {
                txtFromTime.setText(newTime);
            } else if (type.equalsIgnoreCase("toTime")) {
                txtToTime.setText(newTime);
            }
        } catch (final ParseException e) {
            e.printStackTrace();
        }
    }


    //   getAlreadySubscribedVenueList
    private void getAlreadySubscribedServiceList() {
        // Toast.makeText(context, "Api calling", Toast.LENGTH_SHORT).show();
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getAlreadySubscribedServices(new UserAuthModel(userId, authToken));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string();
                    Log.e(TAG, "onResponse subsclist: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    int statusCode = jsonObject.getInt("status_code");
                    String strStatusMsg = jsonObject.getString("message");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        JSONObject recordObj = jsonObject.getJSONObject("records");
                        JSONArray serviceList = recordObj.getJSONArray("service");

                        for (int i = 0; i < serviceList.length(); i++) {
                            try {

                                strServiceNameList.add(serviceList.getJSONObject(i).getString("service_name"));
                                strServiceIdList.add(serviceList.getJSONObject(i).getString("service_id"));
                                strSubscriptionIdList.add(serviceList.getJSONObject(i).getString("subscription_id"));

                                Log.e(TAG, "onResponse - strServiceNameList : " + strServiceNameList.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
//                        Intent i = new Intent(context, SpaceRegistrationDetailsActivity.class);
//                        i.putStringArrayListExtra("venueType", selectedVenueNameList);
//                        i.putStringArrayListExtra("venueId", selectVeneueIdList);
//                        i.putStringArrayListExtra("subscriptionId", subscriptionIdList);
//                        startActivity(i);
                    } else {
                        Toast.makeText(context, strStatusMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                dialog.dismiss();
                Toast.makeText(context, "Server Error!!!...Please Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void selectServiceDialog() {
        //  serviceIdArray = new String[venueList.size()];
        boolean[] checkedServices = new boolean[strServiceNameList.size()];
        int count = strServiceNameList.size();


        for (int i = 0; i < count; i++)
            checkedServices[i] = selectServiceIdList.contains(strServiceIdList.get(i));

        DialogInterface.OnMultiChoiceClickListener servicesDialogListener = (dialog, which, isChecked) -> {
            if (isChecked) {

                selectServiceIdList.add(strServiceIdList.get(which));
                selectedServiceNameList.add(strServiceNameList.get(which));
                selectedSubscriptionIdList.add(strSubscriptionIdList.get(which));

            } else {
                selectServiceIdList.remove(strServiceIdList.get(which));
                selectedServiceNameList.remove(strServiceNameList.get(which));
                selectedSubscriptionIdList.remove(strSubscriptionIdList.get(which));
            }
            onChangeSelectedReceivers();
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setTitle("Select service")
                .setMultiChoiceItems(Utilities.GetStringArray(strServiceNameList), checkedServices, servicesDialogListener)
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, id) -> {
                    Log.e(TAG, "onClick: selectVeneueIdList: " + selectServiceIdList + " selectedVenueNameList: " + selectedServiceNameList + " " +
                            "selectedSubscriptionIdList: " + selectedSubscriptionIdList);
                    dialog.dismiss();
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    protected void onChangeSelectedReceivers() {
        StringBuilder stringBuilder = new StringBuilder();
        if (selectedServiceNameList.size() < 3) {
            for (String service : selectedServiceNameList) {

                if (selectedServiceNameList.size() > 1)
                    stringBuilder.append(service).append(",");
                else
                    stringBuilder.append(service);
                txtAddServiceType.setText(stringBuilder.toString());
            }
        } else {
            txtAddServiceType.setText(selectedServiceNameList.size() + " selected");
        }
    }


}
