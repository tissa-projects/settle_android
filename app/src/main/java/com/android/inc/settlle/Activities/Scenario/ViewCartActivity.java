package com.android.inc.settlle.Activities.Scenario;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.inc.settlle.Adapter.ViewCartAdapter;
import com.android.inc.settlle.R;
import com.android.inc.settlle.RequestModel.RemoveItemFromCartModel;
import com.android.inc.settlle.RequestModel.UserAuthModel;
import com.android.inc.settlle.Retrofit.RetrofitClient;
import com.android.inc.settlle.Utilities.CommonFunctions;
import com.android.inc.settlle.Utilities.SessionExpireUtil;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.Utilities.VU;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCartActivity extends AppCompatActivity {

    private TextView txtCartItemCount, txtCartTotalAmt, txtFinalPayAmt;
    private RecyclerView recyclerView;
    private Context context;
    private ViewCartAdapter viewCartAdapter;
    private Dialog dialog;
    private JSONArray jsonArray;
    private ImageView imgEmptyCart;
    private RelativeLayout rlView;
    private static final String TAG = ViewCartActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cart);
        context = ViewCartActivity.this;
        initialize();
    }

    private void initialize() {
        rlView = findViewById(R.id.rl_view);
        imgEmptyCart = findViewById(R.id.img_empty_cart);
        txtCartItemCount = findViewById(R.id.txt_cart_item_count);
        txtCartTotalAmt = findViewById(R.id.txt_cart_total_amt);
        txtFinalPayAmt = findViewById(R.id.txt_final_pay_amt);
        recyclerView = findViewById(R.id.recycler_view_cart);

        setAdapter();

        findViewById(R.id.btn_checkout).setOnClickListener((v) -> {
            try {
                CartAlert("Are you sure want to checkout from the cart?", "place_for_checkout", 0, jsonArray);  // note: jsonArray and position not in use
            } catch (Exception e) {
                Log.e(TAG, "onClick: catch : " + e.getMessage());
            }
        });

        findViewById(R.id.imgBack).setOnClickListener((v) -> finish());
        TextView h = findViewById(R.id.txtHeading);
        h.setText("Your Cart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        jsonArray = new JSONArray();
        if (VU.isConnectingToInternet(context)) {
            getCartData();
        }
    }

    private void setAdapter() {
        @SuppressLint("WrongConstant")
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayout.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        viewCartAdapter = new ViewCartAdapter(context);
        recyclerView.setAdapter(viewCartAdapter);

        //remove from cart(space or service) : adapter
        viewCartAdapter.setRemoveFromCartListener((view, position, dataArray, showType) -> {
            try {
                if (showType.equalsIgnoreCase("space")) {
                    CartAlert("Are you sure want to remove this space from the cart?", "remove_space", position, dataArray);
                } else {
                    CartAlert("Are you sure want to remove this service from the cart?", "remove_service", position, dataArray);
                }
            } catch (Exception e) {
                CommonFunctions.showLog(TAG, "onRemoveCLick: catch : " + e.getMessage());
            }
        });
    }

    private void getCartData() {
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().getCartList(new UserAuthModel(userId, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    JSONObject jsonObject = new JSONObject(api_response);
                    CommonFunctions.showLog(TAG, "onResponse: cartResponse: " + jsonObject);
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        imgEmptyCart.setVisibility(View.GONE);
                        rlView.setVisibility(View.VISIBLE);
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        JSONArray spaceArray = dataObj.getJSONArray("spacelist");
                        JSONArray serviceArray = dataObj.getJSONArray("servicelist");
                        int cartItemSize = serviceArray.length() + spaceArray.length();
                        viewCartAdapter.setNewData(addArrayData(spaceArray, serviceArray));
                        setAdapterData(cartItemSize);
                    } else {
                        imgEmptyCart.setVisibility(View.VISIBLE);
                        rlView.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    CommonFunctions.showLog(TAG, "onResponse: catch: " + e.getMessage());
                }


            }


            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();


            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setAdapterData(int cartItemSize) throws JSONException {
        float cartTotal = 0;
        txtCartItemCount.setText("(" + cartItemSize + " Items)");
        for (int i = 0; i < jsonArray.length(); i++) {
            cartTotal = cartTotal + Float.parseFloat(jsonArray.getJSONObject(i).getString("amount"));
        }
        txtCartTotalAmt.setText(cartTotal + "".trim());
        // calculateDiscount();
        txtFinalPayAmt.setText(cartTotal + "".trim());

        txtCartTotalAmt.setText(CommonFunctions.formatNumber((long) cartTotal));
        txtFinalPayAmt.setText(CommonFunctions.formatNumber((long) cartTotal));

    }

    //remove space from cart
    private void removeSpaceFromCart(final int position, final JSONArray dataArray) throws JSONException {
        String spaceCartId = dataArray.getJSONObject(position).getString("cart_id");
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().RemoveSpaceFromCart(new RemoveItemFromCartModel(spaceCartId, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {

                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        dataArray.remove(position);
                        int cartItemSize = dataArray.length();
                        if (cartItemSize > 0) {
                            imgEmptyCart.setVisibility(View.GONE);
                            rlView.setVisibility(View.VISIBLE);
                        } else {
                            imgEmptyCart.setVisibility(View.VISIBLE);
                            rlView.setVisibility(View.GONE);
                        }
                        viewCartAdapter.setNewData(dataArray);
                        Log.e(TAG, "onResponse: cartItemSize: " + cartItemSize);

                        Utilities.setSPstring(context, Utilities.CHECK_OUT_COUNT, String.valueOf(cartItemSize));
                        setAdapterData(cartItemSize);
                    } else {
                        imgEmptyCart.setVisibility(View.VISIBLE);
                        rlView.setVisibility(View.GONE);
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onResponse: catch: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    //remove Service from cart
    private void removeServiceFromCart(final int position, final JSONArray dataArray) throws JSONException {
        String serviceCartId = dataArray.getJSONObject(position).getString("cart_id");
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().RemoveServiceFromCart(new RemoveItemFromCartModel(serviceCartId, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {

                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        dataArray.remove(position);
                        int cartItemSize = dataArray.length();
                        if (cartItemSize > 0) {
                            imgEmptyCart.setVisibility(View.GONE);
                            rlView.setVisibility(View.VISIBLE);
                        } else {
                            imgEmptyCart.setVisibility(View.VISIBLE);
                            rlView.setVisibility(View.GONE);
                        }
                        viewCartAdapter.setNewData(dataArray);
                        CommonFunctions.showLog(TAG, "onResponse: cartItemSize: " + cartItemSize);
                        Utilities.setSPstring(context, Utilities.CHECK_OUT_COUNT, String.valueOf(cartItemSize));
                        setAdapterData(cartItemSize);
                    } else {
                        imgEmptyCart.setVisibility(View.VISIBLE);
                        rlView.setVisibility(View.GONE);
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onResponse: catch: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();


            }
        });
    }

    private JSONArray addArrayData(JSONArray spaceArray, JSONArray serviceArray) {
        try {
            for (int i = 0; i < spaceArray.length(); i++) {
                JSONObject spaceObj = new JSONObject();

                spaceObj.put("showType", "space");
                spaceObj.put("title", spaceArray.getJSONObject(i).getString("title"));
                spaceObj.put("type", spaceArray.getJSONObject(i).getString("event_name"));
                spaceObj.put("guest", spaceArray.getJSONObject(i).getString("guest"));
                spaceObj.put("address", spaceArray.getJSONObject(i).getString("address"));
                spaceObj.put("rating", spaceArray.getJSONObject(i).getString("stars"));
                spaceObj.put("image_name", spaceArray.getJSONObject(i).getString("image_name"));
                spaceObj.put("cart_id", spaceArray.getJSONObject(i).getString("space_cart_id"));
                spaceObj.put("unique_id", spaceArray.getJSONObject(i).getString("uni_id"));
                spaceObj.put("id", spaceArray.getJSONObject(i).getString("space_id"));
                spaceObj.put("amount", spaceArray.getJSONObject(i).getString("amount"));
                jsonArray.put(spaceObj);
            }

            for (int i = 0; i < serviceArray.length(); i++) {
                JSONObject serviceObj = new JSONObject();
                //     cartTotal = cartTotal + Integer.valueOf(serviceArray.getJSONObject(i).getString("amount"));
                serviceObj.put("showType", "service");
                serviceObj.put("title", serviceArray.getJSONObject(i).getString("company"));
                serviceObj.put("type", serviceArray.getJSONObject(i).getString("title"));  // service type
                serviceObj.put("guest", serviceArray.getJSONObject(i).getString("guest"));
                serviceObj.put("address", serviceArray.getJSONObject(i).getString("address"));
                serviceObj.put("rating", serviceArray.getJSONObject(i).getString("stars"));
                serviceObj.put("image_name", serviceArray.getJSONObject(i).getString("image_name"));
                serviceObj.put("cart_id", serviceArray.getJSONObject(i).getString("service_cart_id"));
                serviceObj.put("unique_id", serviceArray.getJSONObject(i).getString("uni_id"));
                serviceObj.put("id", serviceArray.getJSONObject(i).getString("service_id"));
                serviceObj.put("amount", serviceArray.getJSONObject(i).getString("amount"));
                jsonArray.put(serviceObj);
            }
            CommonFunctions.showLog(TAG, "setData: jsonArray: " + jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
            CommonFunctions.showLog(TAG, "setData: catch" + e.getMessage());
        }

        return jsonArray;
    }

    private void CheckoutFromCart() {
        String userId = Utilities.getSPstringValue(context, Utilities.spUserId);
        String authToken = Utilities.getSPstringValue(context, Utilities.spAuthToken);
        dialog = ProgressDialog.show(context, "Please wait", "Loading...");

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().CheckOutFromCart(new UserAuthModel(userId, authToken));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    assert response.body() != null;
                    String api_response = response.body().string().trim();
                    Log.e(TAG, "onResponse: " + api_response);
                    JSONObject jsonObject = new JSONObject(api_response);
                    String msg = jsonObject.getString("message");
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    int statusCode = jsonObject.getInt("status_code");
                    if (statusCode == 401) {
                        SessionExpireUtil.logout(context);
                    } else if (statusCode == 200) {
                        Utilities.setSPstring(context, Utilities.CHECK_OUT_COUNT, String.valueOf(0));
                        finish();
                    }
                } catch (Exception e) {
                    CommonFunctions.showLog(TAG, "onResponse: catch: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Network error", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void CartAlert(String title, String type, int position, JSONArray dataArray) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        // builder1.setMessage(title);
        builder1.setTitle(title);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> {
                    try {
                        switch (type) {
                            case "remove_space":
                                if (VU.isConnectingToInternet(context))
                                    removeSpaceFromCart(position, dataArray);
                                break;

                            case "remove_service":
                                removeServiceFromCart(position, dataArray);
                                break;

                            case "place_for_checkout":
                                if (VU.isConnectingToInternet(context))
                                    CheckoutFromCart();
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        builder1.setNegativeButton(
                "No",
                (dialog, id) -> dialog.cancel());

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

}
