package com.android.inc.settlle.RequestModel;

import java.io.Serializable;

public class VerifyOtpModel implements Serializable {

    private String mobileNo;
    private String otp;
    private String auth_token;
    private String user_id;

    public VerifyOtpModel(String mobileNo, String otp, String user_id,String auth_token) {
        this.mobileNo = mobileNo;
        this.otp = otp;
        this.auth_token = auth_token;
        this.user_id = user_id;
    }
}
