package com.android.inc.settlle.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.inc.settlle.Activities.Service.ServiceBookingFormActivity;
import com.android.inc.settlle.Activities.Service.ServiceShowActivity;
import com.android.inc.settlle.Activities.SpaceBookingFormActivity;
import com.android.inc.settlle.Activities.SpaceViewActivity;
import com.android.inc.settlle.R;
import com.android.inc.settlle.Utilities.Tools;
import com.android.inc.settlle.Utilities.Utilities;
import com.android.inc.settlle.interfaces.RecyclerViewItemClickListener;

import org.json.JSONArray;

public class ViewCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private JSONArray jsonArray;
    String[] guestList;
    private RecyclerViewItemClickListener.IRemoveFromCartBtn removeFromCartListener;
    private static final String TAG = ViewCartAdapter.class.getSimpleName();

    public ViewCartAdapter(Context context) {
        this.context = context;
        jsonArray = new JSONArray();
        guestList = context.getResources().getStringArray(R.array.GuestList);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_cart, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, final int i) {
        if (myViewHolder instanceof MyViewHolder) {
            final MyViewHolder view = (MyViewHolder) myViewHolder;
            final String type, imgName, strShowType, strCartid;
            try {
                type = jsonArray.getJSONObject(i).getString("type");
                imgName = jsonArray.getJSONObject(i).getString("image_name");
                String strStar = jsonArray.getJSONObject(i).getString("rating");
                strShowType = jsonArray.getJSONObject(i).getString("showType");
                strCartid = jsonArray.getJSONObject(i).getString("cart_id");

                Log.d(TAG, "onBindViewHolder: " + jsonArray);


                final String strUniqueId = jsonArray.getJSONObject(i).getString("unique_id");

                Log.e(TAG, "onBindViewHolder: strUniqueId: " + strUniqueId + "  strCartid: " + strCartid);
                if (strStar.equalsIgnoreCase("N/A")) {
                    strStar = "0";
                }

                view.txtTitle.setText(jsonArray.getJSONObject(i).getString("title"));
                view.txtAddress.setText(jsonArray.getJSONObject(i).getString("address"));
                view.txtGuest.setText(guestList[Integer.parseInt(jsonArray.getJSONObject(i).getString("guest")) - 1]);
                view.ratingBar.setRating(Float.parseFloat(strStar));
                if (strShowType.equalsIgnoreCase("space")) {
                    String imgUrl = Utilities.IMG_SPACE_URL + imgName;
                    Tools.displayImageOriginalString(view.imageView, imgUrl);
                    view.txtType.setText("Selected Event: " + type);
                } else {
                    String imgUrl = Utilities.IMG_SERVICE_URL + imgName;
                    Tools.displayImageOriginalString(view.imageView, imgUrl);
                    view.txtType.setText("Selected Service: " + type);
                }
                view.btnRemoveItemFromCart.setOnClickListener((v) ->
                        removeFromCartListener.onRemoveCLick(v, i, jsonArray, strShowType)
                );

                view.cardView.setOnClickListener((v) -> {
                    Intent intent;
                    if (strShowType.equalsIgnoreCase("space")) {
                        intent = new Intent(context, SpaceViewActivity.class);
                    } else {
                        intent = new Intent(context, ServiceShowActivity.class);
                    }
                    intent.putExtra("uni", strUniqueId);
                    context.startActivity(intent);
                });

                view.btnBookingDetails.setOnClickListener((v) -> {
                    Utilities.setSPboolean(context, Utilities.spIsBookingType, true);
                    Intent intent;
                    if (strShowType.equalsIgnoreCase("space")) {
                        intent = new Intent(context, SpaceBookingFormActivity.class);
                    } else {
                        intent = new Intent(context, ServiceBookingFormActivity.class);
                    }
                    intent.putExtra("unique_id", strUniqueId);
                    intent.putExtra("type", "cart");
                    intent.putExtra("id", strCartid);

                    intent.putExtra("eventID", "");
                    intent.putExtra("eventName", "");
                    context.startActivity(intent);
                });


            } catch (Exception e) {
                Log.e(TAG, "onBindViewHolder: " + e.getMessage());
            }
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setNewData(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        notifyDataSetChanged();
    }

    public JSONArray getData() {
        return jsonArray;
    }

    public void setRemoveFromCartListener(RecyclerViewItemClickListener.IRemoveFromCartBtn removeFromCartListener) {
        this.removeFromCartListener = removeFromCartListener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txtType, txtGuest, txtAddress;
        RatingBar ratingBar;
        ImageView imageView;
        AppCompatButton btnRemoveItemFromCart, btnBookingDetails;
        CardView cardView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtTitle = itemView.findViewById(R.id.title);
            txtType = itemView.findViewById(R.id.type);
            txtGuest = itemView.findViewById(R.id.no_of_guest);
            txtAddress = itemView.findViewById(R.id.address);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            imageView = itemView.findViewById(R.id.img);
            cardView = itemView.findViewById(R.id.cardview);
            btnRemoveItemFromCart = itemView.findViewById(R.id.btn_remove_item);
            btnBookingDetails = itemView.findViewById(R.id.btn_view_booking_details);
        }
    }

}
