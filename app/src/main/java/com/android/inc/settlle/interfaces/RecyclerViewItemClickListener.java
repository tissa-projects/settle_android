package com.android.inc.settlle.interfaces;

import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public interface RecyclerViewItemClickListener {

    void onItemClick(View view, int position);

    void onItemLongClick(View view, int position);

    interface IViewBtnClickListener {
        void onItemClick(String uniqueID, String SpaceServiceID);
    }

    interface IDocumentsBtnClickListener {
        void onItemClick(String uniqueID, String SpaceServiceID);
    }

    interface IEdtBtnClickListener {
        void onItemClick(String uniqueId, String SpaceServiceID);
    }

    interface ICrossBtnClickListener {
        void onItemClick(View view, int position, String img);
    }

    interface DelFavListner {
        void onDeleteClickListner(View view, int postion, JSONArray jsonArray);
    }

    interface IViewBookingBtn {
        void onViewBookingClick(View view, int position, JSONObject jsonObject);
    }

    interface IReviewBtn {
        void onReviewClick(View view, int position, JSONObject jsonObject);
    }

    interface AddFavBtn {
        void onAddFavClick(View view, int position, JSONArray dataArray);
    }

    interface IRemoveFromCartBtn {
        void onRemoveCLick(View view, int position, JSONArray dataArray, String showType);
    }

    interface OnItemClick {
        void onItemClickListner(View view, int position, JSONArray dataArray);
    }

    interface IRenewalClick {
        void onClick(View view, int position, JSONObject jsonObject);
    }

    interface IRecyclerViewClick {
        void onCardClickListner(View view, int position, JSONArray dataArray);
    }

    interface ICheckBoxClick {
        void onCheckBoxClickListner(View view, int position, JSONArray dataArray);
    }

    interface ICrossBtnClickListeners {
        void onItemClick(int position, List<String> imgList, List<String> imgIdList);
    }
}